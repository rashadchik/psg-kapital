<?php
use App\Logic\Menu;


Breadcrumbs::register('web', function($breadcrumbs, $parentPage, $page, $homePageTitle, $article)
{
    $menu = Menu::getPages();

    $breadcrumbs->push($homePageTitle, route('home'));

    foreach($menu as $key => $nav)
    {

        if($key == 0)
        {
            $breadcrumbs->push(normalTitle($parentPage->name), route("showPage", $parentPage->slug));
        }

        
        foreach($nav->menuChildren as $subnav)
        {
            if($subnav->id == $page->parent_id)
            {
                $breadcrumbs->push(normalTitle($subnav->name), route("showPage", $subnav->slug));
            }
        }

    }


    if($page->id != $parentPage->id)
    {
        if($article == true)
        {
            $breadcrumbs->push(normalTitle($page->name), route("showPage", $page->slug));
        }
        else{
            $breadcrumbs->push(normalTitle($page->name));

        }
    }

    if($article != false)
    {
        $breadcrumbs->push(normalTitle($article->title));
    }


});

