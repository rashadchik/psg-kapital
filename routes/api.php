<?php

Route::pattern('id', '[0-9]+');
Route::pattern('type', '[0-9]+');


    Route::get('/', function (){

        return redirect()->route('dashboard');
    });

    Route::get('dashboard', ['as' => 'dashboard', 'uses' => 'DashboardController@index']);

    #Route::resource('brand', 'BrandController', ['only' => ['index', 'create', 'store', 'destroy']]);

    Route::resource('slider', 'SliderController',
        ['names' => ['create' => 'create_slider', 'store' => 'store_slider', 'edit' => 'edit_slider', 'update' => 'update_slider', 'destroy' => 'destroy_slider' ], 'except' => ['show']
    ]);

    Route::group(['prefix'=>'slider'], function()
    {
        Route::get('trashed',           ['as' => 'trashed_slider', 'uses' => 'SliderController@trashed']);
        Route::patch('{id}/trash',      ['as' => 'trash_slider', 'uses' => 'SliderController@trash']);
        Route::patch('{id}/restore',    ['as' => 'restore_slider', 'uses' => 'SliderController@restore']);
        Route::get('order/{lang}',      ['as' => 'order_slider', 'uses' => 'SliderController@order']);
        Route::post('postOrder',        ['as' => 'post_order_slider', 'uses' => 'SliderController@postOrder']);
    });


    Route::resource('article', 'ArticleController',
        ['names' => ['create' => 'create_article', 'edit' => 'edit_article', 'store' => 'store_article', 'destroy' => 'destroy_article', 'update' => 'update_article'], 'except' => ['show']
    ]);
    Route::group(['prefix'=>'article'], function()
    {
        Route::get('category',          ['as' => 'cat_article', 'uses' => 'ArticleController@categories']);
        Route::get('featured',          ['as' => 'featured_article', 'uses' => 'ArticleController@featured']);
        Route::get('trashed',           ['as' => 'trashed_article', 'uses' => 'ArticleController@trashed']);
        Route::patch('{id}/trash',      ['as' => 'trash_article', 'uses' => 'ArticleController@trash']);
        Route::patch('{id}/restore',    ['as' => 'restore_article', 'uses' => 'ArticleController@restore']);
        Route::post('{id}/{lang}/copy', ['as' => 'copy_article', 'uses' => 'ArticleController@copy']);
    });


    Route::resource('serviceStat', 'ServiceStatController',
        ['names' => ['create' => 'create_serviceStat', 'store' => 'store_serviceStat', 'edit' => 'edit_serviceStat', 'update' => 'update_serviceStat', 'destroy' => 'destroy_serviceStat' ], 'except' => ['show']
    ]);

    Route::group(['prefix'=>'serviceStat'], function()
    {
        Route::get('trashed',           ['as' => 'trashed_serviceStat', 'uses' => 'ServiceStatController@trashed']);
        Route::patch('{id}/trash',      ['as' => 'trash_serviceStat', 'uses' => 'ServiceStatController@trash']);
        Route::patch('{id}/restore',    ['as' => 'restore_serviceStat', 'uses' => 'ServiceStatController@restore']);
    });


    Route::resource('legalPhysical', 'LegalPhysicalController',
        ['names' => ['create' => 'create_legalPhysical', 'store' => 'store_legalPhysical', 'edit' => 'edit_legalPhysical', 'update' => 'update_legalPhysical', 'destroy' => 'destroy_legalPhysical' ], 'except' => ['show']
    ]);

    Route::group(['prefix'=>'legalandphysical'], function()
    {
        Route::get('trashed',           ['as' => 'trashed_legalPhysical', 'uses' => 'LegalPhysicalController@trashed']);
        Route::patch('{id}/trash',      ['as' => 'trash_legalPhysical', 'uses' => 'LegalPhysicalController@trash']);
        Route::patch('{id}/restore',    ['as' => 'restore_legalPhysical', 'uses' => 'LegalPhysicalController@restore']);
    });


    Route::resource('brokerTable', 'BrokerTableController',
        ['names' => ['create' => 'create_brokerTable', 'store' => 'store_brokerTable', 'edit' => 'edit_brokerTable', 'update' => 'update_brokerTable', 'destroy' => 'destroy_brokerTable' ], 'except' => ['show']
    ]);

    Route::group(['prefix'=>'brokerTable'], function()
    {
        Route::get('trashed',           ['as' => 'trashed_brokerTable', 'uses' => 'BrokerTableController@trashed']);
        Route::patch('{id}/trash',      ['as' => 'trash_brokerTable', 'uses' => 'BrokerTableController@trash']);
        Route::patch('{id}/restore',    ['as' => 'restore_brokerTable', 'uses' => 'BrokerTableController@restore']);
    });


    Route::resource('banner', 'BannerController',
        ['names' => ['create' => 'create_banner', 'store' => 'store_banner', 'edit' => 'edit_banner', 'update' => 'update_banner', 'destroy' => 'destroy_banner' ], 'except' => ['show']
    ]);

    Route::group(['prefix'=>'banner'], function()
    {
        Route::get('trashed',           ['as' => 'trashed_banner', 'uses' => 'BannerController@trashed']);
        Route::patch('{id}/trash',      ['as' => 'trash_banner', 'uses' => 'BannerController@trash']);
        Route::patch('{id}/restore',    ['as' => 'restore_banner', 'uses' => 'BannerController@restore']);
    });

    Route::resource('loan', 'LoanController',
        ['names' => ['create' => 'create_loan', 'store' => 'store_loan', 'edit' => 'edit_loan', 'update' => 'update_loan', 'destroy' => 'destroy_loan' ], 'except' => ['show']
    ]);

    Route::group(['prefix'=>'loan'], function()
    {
        Route::get('trashed',           ['as' => 'trashed_loan', 'uses' => 'LoanController@trashed']);
        Route::patch('{id}/trash',      ['as' => 'trash_loan', 'uses' => 'LoanController@trash']);
        Route::patch('{id}/restore',    ['as' => 'restore_loan', 'uses' => 'LoanController@restore']);
    });

    Route::resource('stock', 'StockController',
        ['names' => ['create' => 'create_stock', 'store' => 'store_stock', 'edit' => 'edit_stock', 'update' => 'update_stock', 'destroy' => 'destroy_stock' ], 'except' => ['show']
    ]);

    Route::group(['prefix'=>'stock'], function()
    {
        Route::get('trashed',           ['as' => 'trashed_stock', 'uses' => 'StockController@trashed']);
        Route::patch('{id}/trash',      ['as' => 'trash_stock', 'uses' => 'StockController@trash']);
        Route::patch('{id}/restore',    ['as' => 'restore_stock', 'uses' => 'StockController@restore']);
    });


    Route::resource('reproduction', 'ReproductionController',
        ['names' => ['create' => 'create_reproduction', 'store' => 'store_reproduction', 'edit' => 'edit_reproduction', 'update' => 'update_reproduction', 'destroy' => 'destroy_reproduction' ], 'except' => ['show']
    ]);
    
    Route::group(['prefix'=>'reproduction'], function()
    {
        Route::get('trashed',           ['as' => 'trashed_reproduction', 'uses' => 'ReproductionController@trashed']);
        Route::patch('{id}/trash',      ['as' => 'trash_reproduction', 'uses' => 'ReproductionController@trash']);
        Route::patch('{id}/restore',    ['as' => 'restore_reproduction', 'uses' => 'ReproductionController@restore']);
    });


    Route::resource('reproduction/client', 'ReproductionClientController',
        ['names' => ['create' => 'create_client', 'store' => 'store_client', 'edit' => 'edit_client', 'update' => 'update_client', 'destroy' => 'destroy_client' ], 'except' => ['show']
    ]);
    
    Route::group(['prefix'=>'reproduction/client'], function()
    {
        Route::get('trashed',           ['as' => 'trashed_client', 'uses' => 'ReproductionClientController@trashed']);
        Route::patch('{id}/trash',      ['as' => 'trash_client', 'uses' => 'ReproductionClientController@trash']);
        Route::patch('{id}/restore',    ['as' => 'restore_client', 'uses' => 'ReproductionClientController@restore']);
    });




    Route::resource('strategy', 'StrategyController',
        ['names' => ['create' => 'create_strategy', 'store' => 'store_strategy', 'edit' => 'edit_strategy', 'update' => 'update_strategy', 'destroy' => 'destroy_strategy' ], 'except' => ['show']
    ]);

    Route::group(['prefix'=>'strategy'], function()
    {
        Route::get('trashed',           ['as' => 'trashed_strategy', 'uses' => 'StrategyController@trashed']);
        Route::patch('{id}/trash',      ['as' => 'trash_strategy', 'uses' => 'StrategyController@trash']);
        Route::patch('{id}/restore',    ['as' => 'restore_strategy', 'uses' => 'StrategyController@restore']);
    });


    Route::resource('strategy/chart', 'StrategyChartController',
        ['names' => ['create' => 'create_str_chart', 'store' => 'store_str_chart', 'edit' => 'edit_str_chart', 'update' => 'update_str_chart', 'destroy' => 'destroy_str_chart' ], 'except' => ['show']
    ]);

    Route::resource('charts', 'MarketChartController',
        ['names' => ['create' => 'create_chart', 'store' => 'store_chart', 'edit' => 'edit_chart', 'update' => 'update_chart', 'destroy' => 'destroy_chart' ], 'except' => ['show']
    ]);

    Route::resource('marketLoan', 'MarketLoanController',
        ['names' => ['create' => 'create_marketLoan', 'store' => 'store_marketLoan', 'edit' => 'edit_marketLoan', 'update' => 'update_marketLoan', 'destroy' => 'destroy_marketLoan' ], 'except' => ['show']
    ]);

    Route::group(['prefix'=>'marketLoan'], function()
    {
        Route::get('trashed',           ['as' => 'trashed_marketLoan', 'uses' => 'MarketLoanController@trashed']);
        Route::patch('{id}/trash',      ['as' => 'trash_marketLoan', 'uses' => 'MarketLoanController@trash']);
        Route::patch('{id}/restore',    ['as' => 'restore_marketLoan', 'uses' => 'MarketLoanController@restore']);
    });


    Route::resource('question', 'QuestionController',
        ['names' => ['create' => 'create_question', 'store' => 'store_question', 'edit' => 'edit_question', 'update' => 'update_question', 'destroy' => 'destroy_question' ], 'except' => ['show']
    ]);

    Route::resource('strategy/chart2', 'StrategyChart2Controller',
          ['names' => ['create' => 'create_str_chart2', 'store' => 'store_str_chart2', 'edit' => 'edit_str_chart2', 'update' => 'update_str_chart2', 'destroy' => 'destroy_str_chart2' ], 'except' => ['show']
    ]);

    Route::resource('answer', 'AnswerController',
        ['names' => ['create' => 'create_answer', 'store' => 'store_answer', 'edit' => 'edit_answer', 'update' => 'update_answer', 'destroy' => 'destroy_answer' ], 'except' => ['show']
    ]);


    Route::resource('rekvisit', 'RekvisitController',
        ['names' => ['edit' => 'edit_rekvisit', 'update' => 'update_rekvisit', ], 'only' => ['show','edit','update']
    ]);

    Route::resource('dictionary', 'DictionaryController',
        ['names' => ['edit' => 'edit_dictionary', 'update' => 'update_dictionary', 'create' => 'create_dictionary', 'store' => 'store_dictionary'], 'only' => ['index', 'edit', 'update', 'create', 'store']
    ]);

    Route::resource('service', 'ServiceController',
        ['names' => ['create' => 'create_service', 'store' => 'store_service', 'edit' => 'edit_service', 'update' => 'update_service', 'destroy' => 'destroy_service' ], 'except' => ['show']
    ]);

    Route::group(['prefix'=>'service'], function()
    {
        Route::get('trashed',           ['as' => 'trashed_service', 'uses' => 'ServiceController@trashed']);
        Route::patch('{id}/trash',      ['as' => 'trash_service', 'uses' => 'ServiceController@trash']);
        Route::patch('{id}/restore',    ['as' => 'restore_service', 'uses' => 'ServiceController@restore']);
    });


    Route::resource('vacancy', 'VacancyController',
        ['names' => ['edit' => 'edit_vacancy', 'update' => 'update_vacancy'], 'only' => ['edit', 'update']
    ]);

    Route::resource('document', 'DocumentController',
        ['names' => ['create' => 'create_document', 'edit' => 'edit_document', 'store' => 'store_document', 'destroy' => 'destroy_document', 'update' => 'update_document'], 'except' => ['show']
    ]);
    Route::group(['prefix'=>'document'], function()
    {
        Route::get('trashed',           ['as' => 'trashed_document', 'uses' => 'DocumentController@trashed']);
        Route::patch('{id}/trash',      ['as' => 'trash_document', 'uses' => 'DocumentController@trash']);
        Route::patch('{id}/restore',    ['as' => 'restore_document', 'uses' => 'DocumentController@restore']);
    });


    Route::resource('currency', 'CurrencyController',
        ['names' => ['create' => 'create_currency', 'edit' => 'edit_currency', 'store' => 'store_currency', 'destroy' => 'destroy_currency', 'update' => 'update_currency'], 'except' => ['show']
    ]);
    Route::group(['prefix'=>'currency'], function()
    {
        Route::get('trashed',           ['as' => 'trashed_currency', 'uses' => 'CurrencyController@trashed']);
        Route::patch('{id}/trash',      ['as' => 'trash_currency', 'uses' => 'CurrencyController@trash']);
        Route::patch('{id}/restore',    ['as' => 'restore_currency', 'uses' => 'CurrencyController@restore']);
    });


    Route::resource('page', 'PageController',
        ['names' => ['create' => 'create_page', 'edit' => 'edit_page', 'store' => 'store_page', 'destroy' => 'destroy_page', 'update' => 'update_page'], 'except' => ['show']
    ]);
    Route::group(['prefix'=>'page'], function()
    {
        Route::get('trashed',           ['as' => 'trashed_page', 'uses' => 'PageController@trashed']);
        Route::patch('{id}/trash',      ['as' => 'trash_page', 'uses' => 'PageController@trash']);
        Route::patch('{id}/restore',    ['as' => 'restore_page', 'uses' => 'PageController@restore']);

        Route::get('order/{lang}',      ['as' => 'order_page', 'uses' => 'PageController@orderPage']);
        Route::post('postOrder',        ['as' => 'post_order', 'uses' => 'PageController@postOrder']);
    });


    Route::resource('category', 'CategoryController',
        ['names' => ['create' => 'create_category', 'edit' => 'edit_category', 'store' => 'store_category', 'destroy' => 'destroy_category', 'update' => 'update_category'], 'except' => ['show']
    ]);
    Route::group(['prefix'=>'category'], function()
    {
        Route::get('trashed',           ['as' => 'trashed_category', 'uses' => 'CategoryController@trashed']);
        Route::get('order/{lang}',      ['as' => 'order_category', 'uses' => 'PageController@orderCategory']);

    });


    Route::group(['prefix'=>'{id}/{type}/gallery'], function()
    {
        Route::get('/',            ['as' => 'gallery_index',    'uses' => 'GalleryController@index']);
        Route::post('store',       ['as' => 'gallery_store',    'uses' => 'GalleryController@store']);
        Route::get('getImages',    ['as' => 'gallery_get',      'uses' => 'GalleryController@getImages']);
        Route::delete('destroy',   ['as' => 'gallery_destroy',  'uses' => 'GalleryController@destroy']);
    });


    /*Route::resource('subscriber', 'SubscriberController',
        ['names' => ['create' => 'create_subscriber', 'store' => 'store_subscriber', 'destroy' => 'destroy_subscriber'], 'except' => ['show', 'edit', 'update']
    ]);
    Route::group(['prefix'=>'subscriber'], function()
    {
        Route::get('pending',           ['as' => 'pending_subscriber', 'uses' => 'SubscriberController@pending']);
        Route::patch('{id}/approve',    ['as' => 'approve_subscriber', 'uses' => 'SubscriberController@approve']);
    });*/


    Route::resource('partners', 'PartnerController',
        ['names' => ['create' => 'create_partner', 'store' => 'store_partner', 'edit' => 'edit_partner', 'update' => 'update_partner', 'destroy' => 'destroy_partner' ], 'except' => ['show']
    ]);
    Route::group(['prefix'=>'partners'], function()
    {
        Route::get('trashed',           ['as' => 'trashed_partner', 'uses' => 'PartnerController@trashed']);
        Route::patch('{id}/trash',      ['as' => 'trash_partner', 'uses' => 'PartnerController@trash']);
        Route::patch('{id}/restore',    ['as' => 'restore_partner', 'uses' => 'PartnerController@restore']);
    });

    Route::resource('rekvisit', 'RekvisitController',
        ['names' => ['show' => 'show_rekvisit', 'edit' => 'edit_rekvisit', 'update' => 'update_rekvisit'], 'only' => ['edit', 'update', 'show']
    ]);


    Route::resource('user', 'UserController',
        ['names' => ['create' => 'create_user', 'edit' => 'edit_user', 'store' => 'store_user', 'destroy' => 'destroy_user', 'update' => 'update_user'], 'except' => ['show']
    ]);
    Route::group(['prefix'=>'user'], function()
    {
        Route::get('show',              ['as' => 'show_user', 'uses' => 'UserController@show']); //+
        Route::get('edit-password',     ['as' => 'edit_pass', 'uses' => 'UserController@editPassword']); //+
        Route::get('blocked',           ['as' => 'blocked_user', 'uses' => 'UserController@blocked']);  //+
        Route::post('updatePass',       ['as' => 'update_user_pass', 'uses' => 'UserController@updatePassword']);  //+
        Route::patch('{id}/trash',      ['as' => 'trash_user', 'uses' => 'UserController@trash']);
        Route::patch('{id}/restore',    ['as' => 'restore_user', 'uses' => 'UserController@restore']);
    });


    Route::group(['prefix'=>'settings'], function()
    {
        Route::resource('config', 'ConfigController',
            ['names' => ['show' => 'show_config', 'edit' => 'edit_config', 'update' => 'update_config'], 'only' => ['edit', 'update', 'show']
        ]);

        Route::resource('social', 'SocialController',
            ['names' => ['create' => 'create_social', 'edit' => 'edit_social', 'store' => 'store_social', 'update' => 'update_social', 'destroy' => 'destroy_social'], 'except' => ['show']
        ]);

        Route::resource('sitemap', 'SiteMapController', ['only' => ['index', 'store']]);

    });






