<?php

Route::pattern('id', '[0-9]+');
Route::pattern('type', '[0-9]+');

Auth::routes();


if(!request()->is('manager/*') && !request()->is('manager') && !request()->is('_debugbar/*'))
{

    Route::group(
        [
            'prefix' => LaravelLocalization::setLocale(),
            'middleware' => [ 'web', 'setLocale', 'localeSessionRedirect', 'localeViewPath' ]
        ],
        function()
        {
            Route::get('/',                     ['as' => 'home', 'uses' => 'Web\MainController@index']);

            Route::get('/condition/{slug}',     ['as' => 'condition', 'uses' => 'PdfController@show']);

            Route::get('{page}/{article}',      ['as' => 'showArticle', 'uses' => 'Web\MainController@showArticle']);

            Route::get('{page}',                ['as' => 'showPage', 'uses' => 'Web\MainController@showPage']);

            Route::get('/get/{id}/{name}',      ['as' => 'download', 'uses' => 'GalleryController@getDownload']);



            #Route::post('/subscribe',           ['as' => 'subscribe', 'uses' => 'SubscriberController@store']);

            Route::post('/contact',             ['as' => 'contact', 'uses' => 'ContactController@contact']);

            Route::post('/exam',                ['as' => 'exam', 'uses' => 'QuestionController@result']);

            Route::post('/test',                ['as' => 'test', 'uses' => 'ContactController@test']);

            Route::post('/apply/{id}',          ['as' => 'apply', 'uses' => 'VacancyController@apply']);

            /*Route::group(['prefix'=>'upload'], function()
            {
                Route::post('store',       ['as' => 'upload_store',    'uses' => 'UploadFileController@store']);
                Route::delete('destroy',   ['as' => 'upload_destroy',  'uses' => 'UploadFileController@destroy']);
            }); */

        });
}
