<?php
/**
 * Created by PhpStorm.
 * User: Rashad Aghayev
 * Date: 9/26/2017
 * Time: 3:20 PM
 */ 
 
return [
    "inputs" => [
        "create_chart" =>       ['charts','date', 'percent'],
        "edit_chart" =>         ['charts','date', 'percent'],

        "create_str_chart2" =>       ['lang','title','image', 'content'],
        "edit_str_chart2" =>         ['lang','title','image', 'content'],

        "create_str_chart" =>    ['ChartType','year', 'percent'],
        "edit_str_chart" =>      ['ChartType','year', 'percent'],

        "create_currency" =>    ['currency', 'buying', 'sales'],
        "edit_currency" =>      ['currency', 'buying', 'sales'],

        "create_reproduction" =>    ['lang', 'name', 'birja','loan_type','ticker'],
        "edit_reproduction" =>      ['lang', 'name', 'birja','loan_type','ticker'],

        "create_client" =>      ['lang', 'name', 'buyer','seller'],
        "edit_client" =>        ['lang', 'name', 'buyer','seller'],

        "create_stock" =>       ['stockType', 'name', 'indicator1', 'indicator3','dividend'],
        "edit_stock" =>         ['stockType', 'name', 'indicator1', 'indicator3','dividend'],

        "create_loan" =>        ['loan', 'name', 'loan_type', 'price', 'introduction', 'coupon', 'birja', 'languages'],
        "edit_loan" =>          ['loan', 'name', 'loan_type', 'price', 'introduction', 'coupon', 'birja', 'languages'],

        "create_video" =>       ['video', 'lang', 'title', 'summary', 'url'],
        "edit_video" =>         ['video', 'lang', 'title', 'summary', 'url'],

        "create_question" =>    ['lang', 'title', 'image'],
        "edit_question" =>      ['lang', 'title', 'image'],

        "create_answer" =>      ['questions','title','point'],
        "edit_answer" =>        ['questions','title','point'],

        "create_marketLoan" =>  ['marketLoanType','languages','prise_difference1','prise_difference2','languages2'],
        "edit_marketLoan" =>    ['marketLoanType','languages','prise_difference1','prise_difference2','languages2'],

        "create_slider" =>      ['image', 'lang', 'title', 'summary', 'url'],
        "edit_slider" =>        ['image', 'lang', 'title', 'summary', 'url'],

        "create_article" =>     ['title', 'slug', 'date', 'content', 'category', 'status'],
        "edit_article" =>       ['title', 'slug', 'date', 'content', 'category', 'status'],

        "create_media" =>       ['doctor', 'youtube_id', 'image'],
        "edit_media" =>         ['doctor', 'youtube_id', 'image'],

        "edit_vacancy" =>       ['salary', 'end_date'],

        "create_serviceStat" => ['service', 'customers', 'transactions', 'loans', 'proportions'],
        "edit_serviceStat" =>   ['service', 'customers', 'transactions', 'loans', 'proportions'],

        "create_service" =>     ['service', 'languages'],
        "edit_service" =>       ['service', 'languages'],

        "create_brokerTable" => ['service', 'corp_loans', 'gov_loans', 'proportions2', 'notes'],
        "edit_brokerTable" =>   ['service', 'corp_loans', 'gov_loans', 'proportions2', 'notes'],

        "create_legalPhysical" => ['service', 'title', 'legal_physical'],
        "edit_legalPhysical" =>   ['service', 'title', 'legal_physical'],

        "create_banner" =>      ['service', 'image','document','banner_position', 'popup'],
        "edit_banner" =>        ['service', 'image','document','banner_position', 'popup'],

        "create_strategy" =>    ['asset','capital','total', 'variance'],
        "edit_strategy" =>      ['asset','capital','total', 'variance'],

        "create_page" =>        ['template', 'lang', 'name', 'slug', 'description', 'content_mini', 'content','visible', 'target', 'forward_url', 'meta_description', 'meta_keywords'],
        "edit_page" =>          ['template', 'name', 'slug', 'description', 'content_mini', 'content','visible', 'target', 'forward_url', 'meta_description', 'meta_keywords'],

        "create_category" =>    ['template2', 'name', 'slug', 'category', 'image', 'experience', 'content_mini', 'content','content_extra','service_type', 'visible-boolean', 'forward_url', 'salary', 'end_date', 'meta_description', 'meta_keywords'],
        "edit_category" =>      ['template2', 'name', 'slug', 'category', 'image', 'experience', 'content_mini', 'content','content_extra','service_type', 'visible-boolean', 'forward_url', 'meta_description', 'meta_keywords'],

        "create_user" =>        ['first_name', 'last_name', 'email', 'password', 'role'],
        "edit_user" =>          ['first_name', 'last_name', 'email', 'role'],

        "create_partner" =>     ['title', 'image'],
        "edit_partner" =>       ['title', 'image'],

        "create_document" =>    ['category','document','year','summary'],
        "edit_document" =>      ['category','document','year','summary'],

        "show_config" =>        ['company_name', 'contact_phone', 'email', 'contact_email', 'hr_email', 'location', 'site_url', 'google_api_key', 'lang', 'blog_page'],
        "edit_config" =>        ['company_name', 'contact_phone', 'email', 'contact_email', 'hr_email', 'location', 'site_url', 'google_api_key', 'lang', 'blog_page'],

        "show_rekvisit" =>      ['registration_number', 'nominal_value', 'coupon', 'coupon_payment_az', 'coupon_payment_en', 'time'],
        "edit_rekvisit" =>      ['registration_number', 'nominal_value', 'coupon', 'coupon_payment_az', 'coupon_payment_en', 'time'],

        "logo_index" =>         ['logo'],

        "create_subscriber" =>  ['full_name', 'email'],

        "create_social" =>      ['social_type', 'forward_url'],
        "edit_social" =>        ['social_type', 'forward_url'],

        "create_dictionary" =>  ['keyword','editor'],

    ],

    'currency' => [
        1 => [
            'EURUSD' => 'EUR/USD',
        ],
        3 => [
            'GBPUSD' => 'GBP/USD',
        ],
        2 => [
            'USDAUD' => 'USD/AUD',
            'USDCHF' => 'USD/CHF',
            'USDJPY' => 'USD/JPY',
            'USDCNY' => 'USD/CNY',
            'USDRUB' => 'USD/RUB',
            'USDTRY' => 'USD/TRY',
        ],
        4 => [
            'XAUUSD' => 'XAU/USD',
        ],
        5 => [
            'XAGUSD' => 'XAG/USD',
        ],
        6 => [
            'BTCUSD' => 'BTC/USD',
        ],

    ],

    "loan-type" => [1 => "Korporativ İstiqrazlar", "Dərc Dövlət İstiqrazları", "Avrobondlar"],
    "ChartType" => [1 => "İstiqraz", "Beynəlxalq səhmlər", 'Portfolio', 'S&P 500'],
    "charts-type" => [1 => "Xəzinə notları", "Maliyyə bazarının istiqrazları"],
    "slider-type" => [1 => "Slider", "Kompaniya"],
    "article-status" => [0 => "Qaralama", "Dərc olunub"],
    "label" => ["0" => "danger", "1" => "success"],
    "menu-target" => ["1" => "Self", "2" => "Blank"],
    "menu-visibility" => [0 => "Hidden", "Header and Footer", "Header", "Footer Right", "Footer Left"],
    //"menu-visibility-boolean" => [0 => "Gizli", 3=> "Menyudan gizlət", 1 => "Aktiv"],
    "menu-visibility-boolean" => [0 => "Gizli", "Aktiv"],
    "position" => [1 => "Sağ",2 => "Aşağı"],

    "stockType" => [1 => "Səhmlər", "Birja fondları"],

    "menu-visibility-label" => [0 => "danger", "success", "warning", "warning", "info"],
    "template" => ["Static", 2 => "Xəbərlər", 3 => "Xidmətlər",  8 => "Vakansiyalar", 12 => 'Hesabatlar', 14 => 'Əlaqə', 16 => 'Portfel idarəçiliyi'],
    "loanMarketType" => [ 1 => "Daxili Bazar İstiqrazları", 2 => "Xarici Bazar"],
    "variants" => [2 => "A", "B", "C", "D", "E", "F"],
    "test" => [1 => "a", "b", "c", "d", "e", "f", "g", "h"],

    "template2" => [
        0 => "Static",
        1 => "PSG Kapital haqqında",
        2 => "Xəbərlər",
        4 => "Təhlil və Məsləhət Xidməti",
        5 => "Anderraytinq",
        6 => "Broker xidmətləri",
        7 => "Market Meyker Xidməti",
        19 => 'Yanaşma',
        9 => "Vakansiya",
        10 => "Lisenziyalar",
        11 => 'Blok',
        12 => 'Hesabatlar',
        13 => 'Partnyor',
        15 => 'Qanunvericilik',
        17 => 'Sabit gəlirli portfel',
        18 => 'Kapital mühafizəli fondlar',
        27 => 'Xüsusi İdarə Edilən PSG Portfellər',
        20 => 'Xidmət üzrə tariflər',
        21 => 'Rəhbərin müraciəti',
        22 => 'İnvestisiya strategiyaları',
        23 => 'İstiqrazlar',
        24 => 'Səhmlər',
        25 => 'Törəmə alətləri',
        26 => 'Risk profilləri',

    ],

    "template-icon" => [
        4 => "anandcons",
        //5 => "underwriting",
        6 => "brserv",
        7 => "marmek",
        16 => 'prtman',
        17 => 'fixinc',
        18 => 'cpf',
        25 => 'deri',

    ],

    "role" => [1 => "Administrator", "Editor"],
    "blog_page" => [0 => "No", "Yes"],
    "social-network" => ['facebook' => 'Facebook', 'instagram' => 'Instagram', 'youtube' => 'Youtube', 'twitter' => 'Twitter', 'linkedin' => 'Linkedin'],
    "subscriber-label" => ["0" => "warning", "1" => "success"],
    "subscriber-status" => ["0" => "pending", "1" => "active"],
    "yes-no" => [0 => "minus", "plus"],

    'months' => [
        'az' => [1 => 'Yanvar', 'Fevral', 'Mart', 'Aprel', 'May', 'İyun', 'İyul', 'Avqust', 'Sentyabr', 'Oktyabr', 'Noyabr', 'Dekabr'],
        'az_min' => [1 => 'Yan', 'Fev', 'Mar', 'Apr', 'May', 'İyn', 'İyl', 'Avq', 'Sen', 'Okt', 'Noy', 'Dek'],
        'en' => [1 => 'January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
        'en_min' => [1 => 'Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
        'ru' => [1 => 'Yan', 'Fev', 'Mar', 'Apr', 'May', 'İyn', 'İyl', 'Avq', 'Sen', 'Okt', 'Noy', 'Dek']
    ],

    'prefix' => [12 => 12, 50 => 50, 51 => 51, 55 => 55, 70 => 70, 77 => 77],
    'slug_replacement' => ['az' => 'azerbaijani', 'en' => 'azerbaijani', 'ru' => 'russian', 'tr' => 'turkish'],
    "sex" => [1 => "Kişi", 2 => "Qadın"],
    "legal-physical" => [1 => "Hüquqi", 2 => "Fiziki"],
    "broker-table" =>  [ 'corp_loans' => 'Korporativ istiqraz', 'gov_loans' => 'Dövlət istiqrazları', 'proportions' => 'Təkrar bazar (səhmlər)', 'notes' => 'Notlar' ],

    'modulePlugin' => [ //by route and template_id

        'page' => [
            0 => [7],
            1 => [7],
            8 => [4],
            10 => [8, 7],
            12 => [7]
        ],
        'category' => [
            0 => [7],
            1 => [7],
            4 => [4],
            5 => [4],
            6 => [4],
            7 => [4],
            9 => [4],
            10 => [8, 7],
            12 => [7],
            13 => [4],
            15 => [7],
            17 => [4],
            18 => [4],
            19 => [4],
            20 => [7],
            21 => [7],
            22 => [4],
            23 => [4],
            24 => [4],
            25 => [4],
            26 => [4]
        ],
        'article' => [
            2 => [6, 1],
        ]

    ],


    'galleryPlugins' => [
        1 =>  ['route' => 'article', 'type' => 1, 'image' => ['dz-width' => 200, 'resize' => ['fit' => false, 'size' => [800, 675]],  'thumb' => ['fit' => true,  'size' => [225, 190]]], 'title' => 'Qalereya', 'icon' => 'image', 'maxFile' => 20  ],          //article gallery
        6 =>  ['route' => 'article', 'type' => 6, 'image' => ['dz-width' => 200, 'resize' => ['fit' => false, 'size' => [900, 760]],  'thumb' => ['fit' => true,  'size' => [431, 364 ]]], 'title' => 'Şəkil', 'icon' => 'image', 'maxFile' => 1],               //Article photo
        2 =>  ['route' => 'page',    'type' => 2, 'image' => ['dz-width' => 200, 'resize' => ['fit' => false, 'size' => [800, null]], 'thumb' => ['fit' => true,  'size' => [300, 200]]], 'title' => 'Qalereya', 'icon' => 'image', 'maxFile' => 20  ],          //page gallery
        8 =>  ['route' => 'page',    'type' => 2, 'image' => ['dz-width' => 200, 'resize' => ['fit' => false, 'size' => [800, null]], 'thumb' => ['fit' => false, 'size' => [350, null]]], 'title' => 'Lisenziyalar', 'icon' => 'certificate', 'maxFile' => 20  ],         //page certificates
        3 =>  ['route' => 'page',    'type' => 3, 'image' => ['dz-width' => 150, 'resize' => ['fit' => false, 'size' => [800, null]], 'thumb' => ['fit' => true,  'size' => [165, 230 ]]], 'title' => 'Sertifikat', 'icon' => 'certificate', 'maxFile' => 20 ],  //certificate
        4 =>  ['route' => 'page',    'type' => 4, 'image' => ['dz-width' => 600, 'resize' => ['fit' => true,  'size' => [1140, 192]], 'thumb' => ['fit' => false, 'size' => [300, null]]], 'title' => 'Cover şəkil', 'icon' => 'image', 'maxFile' => 1],         //cover
        7 =>  ['route' => 'page',    'type' => 4, 'image' => ['dz-width' => 600, 'resize' => ['fit' => true,  'size' => [847, 147]],  'thumb' => ['fit' => false, 'size' => [300, null]]], 'title' => 'Cover şəkil', 'icon' => 'image', 'maxFile' => 1],         //cover
        5 =>  ['route' => 'page',    'type' => 5, 'image' => ['dz-width' => 200, 'resize' => ['fit' => true,  'size' => [555,  555]], 'thumb' => ['fit' => true,  'size' => [340, 340 ]]], 'title' => 'Foto', 'icon' => 'image', 'maxFile' => 1],                //photo


    ],



];