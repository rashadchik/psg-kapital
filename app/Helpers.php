<?php
/**
 * Created by PhpStorm.
 * User: Riko
 * Date: 8/16/2016
 * Time: 5:12 PM
 */

use Carbon\Carbon;
use App\Logic\DeepUrl;
use Jenssegers\Date\Date;
use Mcamara\LaravelLocalization\Facades\LaravelLocalization;
use App\Logic\WebCache;

function webConfig()
{
    $config = WebCache::getConfig();

    return $config;
}


function headers(){
    return array('Content-type'=> 'application/json; charset=utf-8');
}


function activeUrl($url, $output = 'class=active')
{
    if(request()->url() == $url){
        return $output;
    }
}


function activeQuery($url, $output = 'class=active')
{
    if(request()->url().'?'.request()->getQueryString() == $url){
        return $output;
    }
}


function articleRoute($category, $article)
{
    $deepUrl = new DeepUrl;
    $branch = $deepUrl->generateBranch($category);

    $branch[] = $article;

    return $branch;
}


function  createInputs($input){

    $in = $input['input'];

    if(isset($input['relation'])){
        if($input['relation_is'] == "array"){
            return Form::select($input['db'], $input['relation'], $input['selected'], $input['attr']);
        }
        else{
            return Form::select($input['db'], call_user_func($input['relation']), $input['selected'], $input['attr']);
        }
    }
    else{
        if($in == "password"){
            return Form::password($input['db'], $input['attr']);
        }
        if($in == "checkbox"){
            return Form::checkbox($input['db'], $input['value'], $input['checked'], $input['attr']);
        }
        else if($in == "hidden"){
            return Form::hidden($input['db'], $input['value']);
        }
        else{
            return Form::$in($input['db'], isset($input['value']) ? $input['value'] : null, $input['attr']);
        }

    }
}


function  editInputs($input, $info){

    $in = $input['input'];
    $db = $input['db'];
    $column = isset($input['column']) ? $input['column'] : $input['db'];

    if(isset($input['relation'])){
        if($input['relation_is'] == "array"){
            return Form::select($input['db'], $input['relation'], $info->$db, $input['attr']);
        }
        else{
            return Form::select($input['db'], call_user_func($input['relation'], $info), $info->$db, $input['attr']);
        }
    }
    else{

        if(isset($input['meta'])){
            return Form::$in($db, json_decode($info->extras, true)[$input['meta_db']], $input['attr']);
        }

        if($in == "checkbox"){
            return Form::checkbox($db, $info->$db, $info->$db, $input['attr']);
        }
        else if($in == "hidden"){
            return Form::hidden($input['db'], $input['value']);
        }
        else if($in == "file"){
            return Form::file($input['db'], $input['attr']);
        }
        else{
            return Form::$in($db, $info->$column, $input['attr']);
        }

    }
}


function  viewInputs($input, $show){

    $db = $input['db'];

    return $show->$db;
}

function icon($class, $title = null){
    return "<i class='fa fa-$class'></i> $title";
}

function g_icon($class, $title = null, $spinning = null){
    return "<i class='fa fa-circle-o-notch fa-spin'></i>";
}

function filterDate($date, $time = false)
{
    $time == true ? $timeDate = ', '.with(new Carbon($date))->format('H:i') : $timeDate = '';

    if($date != null){
        $date = with(new Carbon($date))->format('Y-m-d');

        if($date == Carbon::today()->format('Y-m-d')) {
            return 'Bu gün'.$timeDate;
        }
        else if($date == Carbon::yesterday()->format('Y-m-d')) {
            return 'Dünən'.$timeDate;
        }
        else{
            return with(new Carbon($date))->format('d/m/Y').$timeDate;
        }
    }
    else{
        return '';
    }

}

function filterWebDate($date)
{
    return Carbon::parse($date)->format('d/m/Y');
}

function blogDate($date)
{
    Date::setLocale(LaravelLocalization::getCurrentLocale());

    return Date::parse($date)->format('d F Y');
}


function mediaFullDate($date)
{
    //Date::setLocale(LaravelLocalization::getCurrentLocale());

    //return Date::parse($date)->format('d F Y');
    //return Carbon::createFromTimeStamp(strtotime($date))->formatLocalized('%d %B %Y');

    return Carbon::parse($date)->format('d.m.Y');

}


function label($class, $title)
{
    return '<span class="label label-'.$class.'">'.$title.'</span>';
}


function switchLanguage($locale)
{
    $uri = request()->segments();

    $uri[0] = $locale;

    return implode($uri, '/');
}



function menuRoute($cat, $count = 0)
{
    if($cat->slug == 'index'){
        return route('home');
    }
    else{
        if($count == 0)
        {
            return route('showPage', $cat->slug);
        }
        else{
            return "javascript:void(0)";
        }
    }
}



function ucfirst_utf8($str)
{
    return mb_substr(mb_strtoupper($str, 'utf-8'), 0, 1, 'utf-8') . mb_substr($str, 1, mb_strlen($str)-1, 'utf-8');
}


function upperTitle($title, $lang)
{
    if($lang == 'az'){
        $title = str_replace("i", "İ", $title);
    }

    return mb_strtoupper($title, 'utf-8');
}


function normalTitle($title)
{
    return ucfirst_utf8(mb_strtolower($title, 'utf-8'));
}


function selectedLang($lang)
{
    return config("config.selected_lang.$lang");
}


function categoryName($name, $inline = false)
{

    //$name = upperTitle($name, $lang);

    //$remove = strstr($name, ' ');

    $remove = explode(' ', $name)[0];

    if($inline == false){
        $newName = str_replace($remove, "<strong>$remove</strong>", $name);
    }
    else{
        $newName = str_replace($remove, " <span style='display:inline-block'>$remove</span>", $name);
    }

    return $newName;
}


function homePageTitle($name, $lang)
{
    $title = explode('_-_', $name)[0];

    return categoryName($title, true);
}


function pageSlug($name)
{
    $slug = explode('_-_', $name)[1];

    return $slug;
}


function target($target)
{
    $target_ = config("config.menu-target.$target");

    return "_".strtolower($target_);
}


function getControllerName()
{
    return explode('@', class_basename(Route::currentRouteAction()))[0];
}


function clearCache($cache, $lang = true)
{
    if($lang == true){
        foreach(config('app.locales') as $key => $locale){
            Cache::forget($cache.'_'.$key);
        }
    }
    else{
        Cache::forget($cache);
    }


    if($cache == 'pages'){
        Cache::forget('relation');
    }
}

