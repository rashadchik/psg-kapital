<?php
/**
 * Created by PhpStorm.
 * User: Rashad
 * Date: 2/28/17
 * Time: 22:20
 */

namespace App\Logic;

use App\Models\Page;
use Cache;
use Mcamara\LaravelLocalization\Facades\LaravelLocalization;

class Menu
{
    public static function getPages()
    {
        $category = Cache::remember('pages_'.LaravelLocalization::getCurrentLocale(), 720, function () {
            return Page::whereNull('parent_id')
                ->where('visible', '<>', 0)
                ->where('lang_id', LaravelLocalization::getCurrentLocale())
                ->with('menuChildren')
                ->orderBy('order','asc')
                ->get();
        });

        return $category;
    }


    /*public static function nestedPages()
    {
        $category = Cache::remember('nested_'.LaravelLocalization::getCurrentLocale(), 120, function () {
            return Page::where('lang_id', LaravelLocalization::getCurrentLocale())
                ->orderBy('id','asc')
                ->get();
        });

        return $category;
    }*/


    public static function relationSlugs()
    {
        $relations = [];

        $category = Cache::remember('relation', 720, function () {
            return Page::select('id', 'relation_page', 'slug', 'lang_id')->get();
        });

        foreach ($category as $cat){
            $relations[$cat->id] = [$cat->lang_id => $cat->slug];

            foreach($category as $lang){
                if($lang->relation_page == $cat->relation_page){
                    $relations[$cat->id][$lang->lang_id] = $lang->slug;
                }
            }
        }


        return $relations;
    }


    public static function getRelationSlug($id, $lang)
    {

        $pages = self::relationSlugs();


        if(array_key_exists($lang, $pages[$id])){
            return $pages[$id][$lang];
        }
        else{
            return null;
        }

    }

    public static function getName($slug)
    {
        $category = self::getPages();

        $name = trans('locale.home_page');

        foreach($category as $item)
        {
            if($item->slug == $slug){
                $name = $item->name;
            }
        }

        return $name;
    }
}