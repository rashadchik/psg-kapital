<?php
/**
 * Created by PhpStorm.
 * User: Rashad
 * Date: 2/28/17
 * Time: 22:20
 */

namespace App\Logic;


class MultiLanguageSelect
{
    public static function multiLang($query)
    {
        $optgroup = [];

        $languages = config('app.locales');


        foreach($languages as $key => $lang)
        {
            $optgroup[$key]['text'] = $lang;
            $optgroup[$key]['children'] = [];
        }

        foreach($query as $cat)
        {
            $optgroup[$cat->lang_id]['children'][] = ['id' => $cat->id, 'text' => $cat->name];
        }


        $category = json_encode(array_values($optgroup));

        return $category;

    }

}