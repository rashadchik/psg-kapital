<?php
/**
 * Created by PhpStorm.
 * User: Rashad
 * Date: 2/28/17
 * Time: 22:20
 */

namespace App\Logic;

use App\Models\Config;
use App\Models\Currency;
use App\Models\Dictionary;
use App\Models\Social;
use App\Models\Slider;
use App\Models\Partner;
use Cache;

class WebCache
{
    public static function getConfig()
    {
        $config = Cache::rememberForever('webConfig', function () {
            return Config::findOrFail(1);
        });

        return $config;
    }


    public function getDictionary($lang)
    {
        $collect = [];

        $dictionary = Cache::rememberForever("dictionary_$lang", function () use($collect, $lang)
        {
           $dic =  Dictionary::where('type', 0)->where('lang_id', $lang)->select('keyword', 'content')->orderBy('keyword', 'asc')->pluck('content', 'keyword')->toArray();

            return $dic;

        });

        return $dictionary;
    }


    public function getSocial()
    {
        $social = Cache::rememberForever('social', function () {
            return Social::orderBy('id','asc')->get();
        });

        return $social;
    }


    public function getSlider($lang)
    {
        $slider = Cache::rememberForever("slider_$lang", function () use($lang) {
            return Slider::where('lang_id', $lang)->where('type', 1)->orderBy('order', 'asc')->limit(10)->get();
        });

        return $slider;
    }


    public function getPartner($lang)
    {
        $slider = Cache::rememberForever("partner", function () use($lang) {
            return Slider::where('lang_id', 'az')->where('type', 2)->orderBy('order', 'asc')->limit(30)->get();
        });

        return $slider;
    }


    public function getCurrency()
    {
        $slider = Cache::rememberForever("currency", function () {
            return Currency::orderBy('id', 'asc')->limit(10)->get();
        });

        return $slider;
    }
}