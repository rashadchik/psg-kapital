<?php

namespace App\Logic;

use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\File;
use Intervention\Image\ImageManager;
use App\Models\Gallery;


class ImageRepository
{
    public function upload( $form_data, $article_id, $type, $resize)
    {

        $validator = Validator::make($form_data, Gallery::$rules, Gallery::$messages);

        if ($validator->fails()) {

            return Response::json([
                'error' => true,
                'message' => $validator->messages()->first(),
                'code' => 400
            ], 400);

        }

        $photo = $form_data['file'];

        $originalName = $photo->getClientOriginalName();
        $extension = $photo->getClientOriginalExtension();
        $originalNameWithoutExt = substr($originalName, 0, strlen($originalName) - strlen($extension) - 1);


        $filename = $originalNameWithoutExt;

        $allowed_filename = $this->createUniqueFilename( $filename, $article_id, $extension );

        $uploadSuccess1 = $this->original( $photo, $allowed_filename, $article_id, $resize['resize'] );

        $uploadSuccess2 = $this->icon( $photo, $allowed_filename, $article_id, $resize['thumb']);


        if( !$uploadSuccess1 || !$uploadSuccess2) {

            return Response::json([
                'error' => true,
                'message' => 'Server error while uploading',
                'code' => 300
            ], 500);

        }

        $sessionImage = new Gallery;
        $sessionImage->category_id    = $article_id;
        $sessionImage->original_filename      = $originalName;
        $sessionImage->filename      = $allowed_filename;
        $sessionImage->type      = $type;
        $sessionImage->save();

        return Response::json([
            'error' => false,
            'code'  => 200
        ], 200);

    }

    public function createUniqueFilename( $filename, $article_id, $extension)
    {
        $full_size_dir = $this->originalPath($article_id);

        $new_filename = md5($filename.'-'.time());

        $full_image_path = $full_size_dir . $new_filename . '.' . $extension;

        if ( File::exists( $full_image_path ) )
        {
            // Generate token for image
            $imageToken = substr(sha1(mt_rand()), 0, 5);
            $new_filename = md5($imageToken);
        }

        return $new_filename . '.' . $extension;
    }

    /**
     * Optimize Original Image
     */
    public function original( $photo, $filename, $article_id, $resize)
    {
        $manager = new ImageManager();
        $image = $manager->make( $photo );

        $originalWidth = $image->width();

        $width = $resize['size'][0];
        $height = $resize['size'][1];

        if($resize['fit'] == true)
        {
            $image->fit($width, $height);
        }
        else{

            if($originalWidth > $width )
            {
                $image->resize($width, $height, function ($constraint) {
                    $constraint->aspectRatio();
                });
            }

        }

        $image->save( $this->originalPath($article_id)  . $filename );

        return $image;
    }

    /**
     * Create Icon From Original
     */
    public function icon( $photo, $filename, $article_id, $thumb)
    {
        $manager = new ImageManager();
        $image = $manager->make( $photo );

        $originalWidth = $image->width();
        $ratio = 16/9;

        $width = $thumb['size'][0];
        $height = $thumb['size'][1];

        if($thumb['fit'] == true)
        {
            $image->fit($width, $height);
        }
        else{
            $image->resize($width, $height, function ($constraint) {
                $constraint->aspectRatio();
            });
        }

        $image->save( $this->iconPath($article_id)  . $filename );

        return $image;
    }

    /**
     * Delete Image From Session folder, based on original filename
     */
    public function delete( $filename, $article_id, $type)
    {

        $sessionImage = Gallery::where('category_id', $article_id)
            ->where('type', $type)
            ->where(function ($query) use($filename){
                $query->orWhere('filename', $filename)
                    ->orWhere('original_filename', $filename);
            })
            ->first();

        $full_size_dir = $this->originalPath($sessionImage->category_id);
        $icon_size_dir = $this->iconPath($sessionImage->category_id);


        if(empty($sessionImage))
        {
            return Response::json([
                'error' => true,
                'code'  => 400
            ], 400);

        }

        $full_path1 = $full_size_dir . $sessionImage->filename;
        $full_path2 = $icon_size_dir . $sessionImage->filename;

        if ( File::exists( $full_path1 ) )
        {
            File::delete( $full_path1 );
        }

        if ( File::exists( $full_path2 ) )
        {
            File::delete( $full_path2 );
        }

        if( !empty($sessionImage))
        {
            $sessionImage->delete();
        }

        return Response::json([
            'error' => false,
            'code'  => 200
        ], 200);
    }


    function originalPath($article_id)
    {
        $path = "files/cache/images/$article_id/";

        File::makeDirectory($path, 0775, true, true);

        return $path;
    }


    function iconPath($article_id)
    {
        $path = "files/cache/images/$article_id/thumb/";

        File::makeDirectory($path, 0775, true, true);

        return $path;
    }


    public static function originalPathPhoto($article_id, $fileName)
    {
        $path = "files/cache/images/$article_id/$fileName";

        return $path;
    }



    function sanitize($string, $force_lowercase = true, $anal = false)
    {
        $strip = array("~", "`", "!", "@", "#", "$", "%", "^", "&", "*", "(", ")", "_", "=", "+", "[", "{", "]",
            "}", "\\", "|", ";", ":", "\"", "'", "&#8216;", "&#8217;", "&#8220;", "&#8221;", "&#8211;", "&#8212;",
            "â€”", "â€“", ",", "<", ".", ">", "/", "?");
        $clean = trim(str_replace($strip, "", strip_tags($string)));
        $clean = preg_replace('/\s+/', "-", $clean);
        $clean = ($anal) ? preg_replace("/[^a-zA-Z0-9]/", "", $clean) : $clean ;

        return ($force_lowercase) ?
            (function_exists('mb_strtolower')) ?
                mb_strtolower($clean, 'UTF-8') :
                strtolower($clean) :
            $clean;
    }


    public static function getSize($id, $filename)  //viewlarda faylin olchsusunu gostermek uchun
    {
        $image = ImageRepository::originalPathPhoto($id, $filename);

        $size = File::size($image);

        return round($size/1024/1024, 2);
    }
}