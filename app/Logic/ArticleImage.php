<?php

namespace App\Logic;

use Illuminate\Support\Facades\File;
use Intervention\Image\ImageManager;


class ArticleImage
{
    public function uploadImage($inputName, $page, $resize, $update = false)
    {
        $article_id = $page->id;
        $path = request()->input($inputName);

        $originalPath = $page->getOriginal($inputName);



        if($path != $originalPath && !is_null($originalPath))
        {
            $this->delete($article_id, $originalPath);
        }

        if(is_null($path))
        {
            return null;
        }

        if($path == $originalPath && $update == true)
        {
            return $path;
        }


        $full_path = asset($path);

        $originalName = basename($full_path);

        $originalName = str_replace(' ', '-', $originalName);

        $extension = File::extension($originalName);

        $originalNameWithoutExt = substr($originalName, 0, strlen($originalName) - strlen($extension) - 1);

        $filename = md5($originalNameWithoutExt);

        $allowed_filename = $this->createUniqueFilename( $filename, $article_id, $extension );

        $uploadSuccess1 = $this->resize( $full_path, $allowed_filename, $article_id, $resize['resize'] );

        if( !$uploadSuccess1) {

            return redirect()->back()->withInput(request()->input())->withErrors(["errors" => trans('locale.upload_image_error')]);
        }

        if(!is_null($resize['thumb']))
        {
            $uploadSuccess2 = $this->icon( $full_path, $allowed_filename, $article_id, $resize['thumb'] );

            if( !$uploadSuccess2) {

                return redirect()->back()->withInput(request()->input())->withErrors(["errors" => trans('locale.upload_image_error')]);
            }
        }


        return $allowed_filename;

    }

    public function createUniqueFilename( $filename, $article_id, $extension)
    {
        $full_size_dir = $this->originalPath($article_id);

        $full_image_path = $full_size_dir . $filename . '.' . $extension;

        if ( File::exists( $full_image_path ) )
        {
            // Generate token for image
            $imageToken = substr(sha1(mt_rand()), 0, 5);
            $filename = md5($imageToken);
        }

        return $filename . '.' . $extension;
    }

    /**
     * Optimize Original Image
     */
    public function resize( $full_path, $filename, $article_id, $resize )
    {
        $manager = new ImageManager();
        $image = $manager->make( $full_path );

        $originalWidth = $image->width();

        $width = $resize['size'][0];
        $height = $resize['size'][1];

        if($resize['fit'] == true)
        {
            $image->fit($width, $height);
        }
        else{

            if($originalWidth > $width )
            {
                $image->resize($width, $height, function ($constraint) {
                    $constraint->aspectRatio();
                });
            }

        }


        $image->save($this->originalPath($article_id). $filename);

        return $image;
    }

    /**
     * Create Icon From Original
     */
    public function icon( $full_path, $filename, $article_id, $thumb )
    {
        $manager = new ImageManager();
        $image = $manager->make( $full_path );

        //$width = $image->width();
        $ratio = 16/9;
        $width = $thumb['size'][0];
        $height = $thumb['size'][1];

        /*if($width > $resize[0] ){
            $image->fit($resize[0], $resize[1]);

        }
        else{
            $image->fit($image->width(), intval($image->width() / $ratio));
        }*/

        if($thumb['fit'] == true)
        {
            $image->fit($width, $height);
        }
        else{
            $image->resize($width, $height, function ($constraint) {
                $constraint->aspectRatio();
            });
        }

        $image->save( $this->iconPath($article_id)  . $filename );

        return $image;
    }


    public function delete($article_id, $filename)
    {
        $full_size_dir = $this->originalPath($article_id);
        $icon_size_dir = $this->iconPath($article_id);


        $full_path1 = $full_size_dir . $filename;
        $full_path2 = $icon_size_dir . $filename;

        if ( File::exists( $full_path1 ) )
        {
            File::delete( $full_path1 );
        }

        if ( File::exists( $full_path2 ) )
        {
            File::delete( $full_path2 );
        }

    }


    function originalPath($article_id)
    {
        $path = "files/cache/images/$article_id/";

        File::makeDirectory($path, 0775, true, true);

        return $path;
    }


    function iconPath($article_id)
    {
        $path = "files/cache/images/$article_id/thumb/";

        File::makeDirectory($path, 0775, true, true);

        return $path;
    }
}