<?php

namespace App\Logic;


class DeepUrl
{
    public function child($id)                       //istenilen kateqoriyanin child-larini tapir ve ozu qarishiq massive salir
    {
        $category = Menu::nestedPages();

        $children = $this->generateChild($category, $id);

        return $children;
    }


    public function nestedId($id)                       //istenilen kateqoriyanin child-larini tapir ve ozu qarishiq massive salir
    {
        $category = Menu::nestedPages();

        $children = $this->generateNestedId($category, $id);

        return $children;
    }


    public function checkUrl($catArray)                 //urllarin duzgun yazilib yazilmamasi zamani istifade olunur
    {
        $category = Menu::nestedPages();

        $branchs = $this->generateAllBranch($category);

        if(in_array($catArray, $branchs)){
            return true;
        }
        else{
            return false;
        }
    }


    public function generateBranch($slug)
    {
        $category = Menu::nestedPages();

        $branchs = $this->generateAllBranch($category);

        if(array_key_exists($slug, $branchs)){
            return $branchs[$slug];
        }
    }


    public function generateAllBranch($categories)
    {
        $branch = [];
        $items = [];

        foreach ($categories as $item) {

            $items[] = $item->slug;
            $branch[$item->slug] = [$item->slug];

            foreach ($categories as $subItem){
                if($subItem->id == $item->parent_id){
                    $branch[$item->slug] = $items;
                }
            }
        }

        return $branch;
    }


    public function generateChild($menu, $id)
    {
        $names = [];

        foreach ($menu as $item) {

            if($item->id == $id)
            {
                //current route
                $names[$item->slug] = [$item->name, $item->id];

                foreach ($menu as $subItem) {
                    if($subItem->id == $item->parent_id){
                        $names[$subItem->slug] = [$subItem->name, $subItem->id];

                        foreach ($menu as $subsubItem) {
                            if($subsubItem->id == $subItem->parent_id){
                                $names[$subsubItem->slug] = [$subsubItem->name, $subsubItem->id];

                                foreach ($menu as $subsubsubItem) {
                                    if($subsubsubItem->id == $subsubItem->parent_id){
                                        $names[$subsubsubItem->slug] = [$subsubsubItem->name, $subsubsubItem->id];
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        return $names;
    }



    public function generateNestedId($categories, $id)
    {
        $items = [$id];

        foreach ($categories as $item) {
            if($item->parent_id == $id){
                $items[] = $item->id;

                foreach ($categories as $subItem) {
                    if($subItem->parent_id == $item->id){
                        $items[] = $subItem->id;

                        foreach ($categories as $subsubItem) {
                            if($subsubItem->parent_id == $subItem->id){
                                $items[] = $subsubItem->id;
                            }
                        }
                    }
                }
            }
        }

        return $items;
    }
}
