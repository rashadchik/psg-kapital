<?php

namespace App\Logic;


class Input
{
    public function allInputs()
    {
        return array (
            "registration_number" => [
                "ad" => "Qeydiyyat nömrəsi",
                "db" => "registration_number",
                "input" => "text",
                "attr" => ['class'=>'form-control']
            ],
            "nominal_value" => [
                "ad" => "Nominal dəyər",
                "db" => "nominal_value",
                "input" => "number",
                "attr" => ['class'=>'form-control']
            ],
            "coupon" => [
                "ad" => "Kupon",
                "db" => "coupon",
                "input" => "text",
                "attr" => ['class'=>'form-control']
            ],
            "coupon_payment_az" => [
                "ad" => "Coupon Ödənişləri Az",
                "db" => "coupon_payment_az",
                "input" => "text",
                "attr" => ['class'=>'form-control']
            ],
            "coupon_payment_en" => [
                "ad" => "Coupon Ödənişləri En",
                "db" => "coupon_payment_en",
                "input" => "text",
                "attr" => ['class'=>'form-control']
            ],
            "time" => [
                "ad" => "Müddət",
                "db" => "expiration_date",
                "input" => "text",
                "attr" => ['class'=>'form-control datepicker']
            ],
            "buyer" => [
                "ad" => "Alıcı",
                "db" => "buyer",
                "input" => "text",
                "attr" => ['class'=>'form-control']
            ],
            "seller" => [
                "ad" => "Satıcı",
                "db" => "seller",
                "input" => "text",
                "attr" => ['class'=>'form-control']
            ],
            "editor" => [
                "ad" => "Editor",
                "db" => "editor",
                "input" => "checkbox",
                "value" => 1,
                "checked" => false,
            ],
            "percent" => [
                "ad" => "Faiz",
                "db" => "percent",
                "input" => "text",
                "attr" => ['class'=>'form-control']
            ],
            "questions" => [
                "ad" => "Sual",
                "db" => "parent_id",
                "input" => "select",
                "relation" => [],
                "relation_is" => "array",
                "selected" => null,
                "attr" => ['class'=>'form-control','id' => 'category_list']
            ],
             "point" => [
                "ad" => "Xal",
                "db" => "point",
                "input" => "number",
                "attr" => ['class'=>'form-control','min' => 1, 'max' => 10]
            ],
            "stockType" => [
                "ad" => "Səhm növü",
                "db" => "type",
                "input" => "select",
                "relation" => config('config.stockType'),
                "relation_is" => "array",
                "selected" => 0,
                "attr" => ['class'=>'form-control']
            ],
            "marketLoanType" => [
                "ad" => "Istiqraz növü",
                "db" => "type",
                "input" => "select",
                "relation" => config('config.loanMarketType'),
                "relation_is" => "array",
                "selected" => 0,
                "attr" => ['class'=>'form-control']
            ],
            "ChartType" => [
                "ad" => "Diagram növü",
                "db" => "type",
                "input" => "select",
                "relation" => config('config.ChartType'),
                "relation_is" => "array",
                "selected" => 0,
                "attr" => ['class'=>'form-control']
            ],
            "indicator1" => [
                "ad" => "1-illik göstərici",
                "db" => "indicator1",
                "input" => "text",
                "attr" => ['class'=>'form-control']
            ],
            "indicator3" => [
                "ad" => "3-illik göstərici",
                "db" => "indicator3",
                "input" => "text",
                "attr" => ['class'=>'form-control']
            ],
            "dividend" => [
                "ad" => "Dividend",
                "db" => "dividend",
                "input" => "text",
                "attr" => ['class'=>'form-control']
            ],
            "charts" => [
                "ad" => "Növü",
                "db" => "type",
                "input" => "select",
                "relation" => config('config.charts-type'),
                "relation_is" => "array",
                "selected" => 0,
                "attr" => ['class'=>'form-control']
            ],
            "prise_difference1" => [
                "ad" => "Qiymət / Bağlanış qiyməti",
                "db" => "prise_difference1",
                "input" => "text",
                "attr" => ['class'=>'form-control']
            ],
             "prise_difference2" => [
                "ad" => "Gəlirlilik / Açılış qiyməti",
                "db" => "prise_difference2",
                "input" => "text",
                "attr" => ['class'=>'form-control']
            ],
            "languages2" => [
                "ad" => "İstiqamət / Fyuçers",
                "db" => "name",
                "input" => "languageTab",
                "attr" => ['class'=>'form-control'],
            ],
            "name" => [
                "ad" => "Ad",
                "db" => "name",
                "input" => "text",
                "attr" => ['class'=>'form-control']
            ],
            "banner_position" => [
                "ad" => "Istiqamət",
                "db" => "pos_type",
                "input" => "select",
                "relation" => config('config.position'),
                "relation_is" => "array",
                "selected" => 0,
                "attr" => ['class'=>'form-control']
            ],
            "full_name" => [
                "ad" => "Ad, soyad",
                "db" => "full_name",
                "input" => "text",
                "attr" => ['class'=>'form-control']
            ],
            "full_name_en" => [
                "ad" => "Ad, soyad ingiliscə",
                "db" => "full_name_en",
                "input" => "text",
                "attr" => ['class'=>'form-control']
            ],
            "company_name" => [
                "ad" => "Şirkət adı",
                "db" => "company_name",
                "input" => "text",
                "attr" => ['class'=>'form-control']
            ],
            "site_url" => [
                "ad" => "Saytın ünvanı",
                "db" => "site_url",
                "input" => "text",
                "attr" => ['class'=>'form-control']
            ],
            "main_video" => [
                "ad" => "Youtube ID",
                "db" => "main_video",
                "input" => "text",
                "attr" => ['class'=>'form-control']
            ],
            "primary_phone" => [
                "ad" => "Əlaqə telefonu",
                "db" => "primary_phone",
                "input" => "text",
                "attr" => ['class'=>'form-control']
            ],
            "contact_phone" => [
                "ad" => "Əlaqə telefonu",
                "db" => "contact_phone",
                "input" => "text",
                "attr" => ['class'=>'form-control']
            ],
            "contact_phone2" => [
                "ad" => "Əlaqə telefonu",
                "db" => "contact_phone2",
                "input" => "text",
                "attr" => ['class'=>'form-control']
            ],
            "fax" => [
                "ad" => "Faks",
                "db" => "fax",
                "input" => "text",
                "attr" => ['class'=>'form-control']
            ],
            "location" => [
                "ad" => "Xəritə",
                "db" => "location",
                "input" => "text",
                "attr" => ['class'=>'form-control', 'title' => 'Fəaliyyət sahələri üçün nəzərdə tutulub.']
            ],
            "position" => [
                "ad" => "Vəzifə",
                "db" => "position",
                "input" => "text",
                "attr" => ['class'=>'form-control']
            ],
            "title" => [
                "ad" => "Başlıq",
                "db" => "title",
                "input" => "text",
                "attr" => ['class'=>'form-control']
            ],
            "slug" => [
                "ad" => "Slug (URL)",
                "db" => "slug",
                "input" => "text",
                "attr" => ['class'=>'form-control', 'title'=>"Boş saxladığınız təqdirdə sluq avtomatik yaradılacaq."],
                'hide' => 'hide11'
            ],
            "description" => [
                "ad" => "Təsvir",
                "db" => "description",
                "input" => "text",
                "hide" => 'hide0 hide3 hide8 hide12 hide14 hide16',
                "attr" => ['class'=>'form-control']
            ],
            "experience" => [
                "ad" => "İş təcrübəsi",
                "db" => "description",
                "input" => "text",
                "attr" => ['class'=>'form-control'],
                "show" =>'show9'
            ],
            "service_type" => [
                "ad" => "Xidmət növü",
                "db" => "content_extra[]",
                "column" => "content_extra",   //for retrieve data from column
                "input" => "text",
                "attr" => ['class'=>'form-control', 'title' => 'Xidmət növlərini bir birindən ayırmaq üçün vergül qoyun'],
                 "show" =>'show13'
            ],
            "salary" => [
                "ad" => "Əmək haqqı",
                "db" => "salary",
                "input" => "text",
                "attr" => ['class'=>'form-control', 'title' => 'Razılaşma yolu ilə olarsa, 0 yazın'],
                "show" =>'show9'
            ],
            "date" => [
                "ad" => "Tarix",
                "db" => "published_at",
                "input" => "text",
                "attr" => ['class'=>'form-control datepicker']
            ],
            "end_date" => [
                "ad" => "Son tarix",
                "db" => "end_date",
                "input" => "text",
                "attr" => ['class'=>'form-control datepicker'],
                 "show" =>'show9'
            ],
            "lang" => [
                "ad" => "Dil",
                "db" => "lang_id",
                "input" => "select",
                "relation" => config('app.locales'),
                "relation_is" => "array",
                "selected" => 1,
                "attr" => ['class'=>'form-control']
            ],
            "template" => [
                "ad" => "Modul",
                "db" => "template_id",
                "input" => "select",
                "relation" => config('config.template'),
                "relation_is" => "array",
                "selected" => 0,
                "attr" => ['class'=>'form-control', 'id' => 'template']
            ],
            "template2" => [
                "ad" => "Modul",
                "db" => "template_id",
                "input" => "select",
                "relation" => config('config.template2'),
                "relation_is" => "array",
                "selected" => 0,
                "attr" => ['class'=>'form-control', 'id' => 'template']
            ],
            "template_apply" => [
                "ad" => "Modul",
                "db" => "template_id",
                "input" => "select",
                "relation" => config('config.template'),
                "relation_is" => "array",
                "selected" => 0,
                "attr" => ['class'=>'form-control', 'title' => '<input type="checkbox" name="apply_module"> Alt kateqoriyaları üçün tətbiq et']
            ],
            "summary" => [
                "ad" => "Qısa məzmun",
                "db" => "summary",
                "input" => "textarea",
                "attr" => ['class'=>'form-control', 'rows' => 5]
            ],
            "content_mini" => [
                "ad" => "Qısa məzmun",
                "db" => "summary",
                "input" => "textarea",
                'value' => '<p>Dəqiq quruluşa malik olmayan bu mətn latincada hələ XVI<br />əsrdə &ouml;z qəlib formasını almışdır.</p>',
                "attr" => ['class'=>'form-control ck-editor', 'id' => 'ck-editor-mini', 'rows' => 5],
                'hide' => 'hide0 hide1 hide3 nariman.yagubov@psgkapital.azhide8 hide10 hide12 hide14 hide15 hide16 hide20 hide21'
            ],
            "content" => [
                "ad" => "Məzmun",
                "db" => "content",
                "input" => "textarea",
                "attr" => ['class'=>'form-control ck-editor', 'id' => 'ck-editor'],
                'hide' => 'hide2 hide3 hide8 hide10 hide11 hide12 hide14 hide15 hide16 hide20'
            ],
             "content_extra" => [
                "ad" => "Əlavə Məzmun",
                "db" => "content_extra[]",
                "column" => "content_extra",   //for retrieve data from column
                "input" => "textarea",
                "attr" => ['class'=>'form-control ck-editor', 'id' => 'ck-editor-extra'],
                "show" =>'show4 show5 show24'
            ],

            "content_az" => [
                "ad" => "Məzmun (AZ)",
                "db" => "content_az",
                "input" => "textarea",
                "attr" => ['class'=>'form-control', "rows" => 3]
            ],
            "content_en" => [
                "ad" => "Məzmun (EN)",
                "db" => "content_en",
                "input" => "textarea",
                "attr" => ['class'=>'form-control', "rows" => 3]
            ],
            "content_ru" => [
                "ad" => "Məzmun (RU)",
                "db" => "content_ru",
                "input" => "textarea",
                "attr" => ['class'=>'form-control', "rows" => 3]
            ],
            "details_1" => [
                "ad" => "İstifadə qaydaları",
                "db" => "details_1",
                "input" => "textarea",
                "attr" => ['class'=>'form-control ckeditor']
            ],
            "details_2" => [
                "ad" => "Əks göstəricilər",
                "db" => "details_2",
                "input" => "textarea",
                "attr" => ['class'=>'form-control ckeditor']
            ],
            "customers" => [
                "ad" => "Müştəri sayı",
                "db" => "customers",
                "input" => "number",
                "attr" => ['class'=>'form-control', 'min' => 0]
            ],
            "transactions" => [
                "ad" => "Əqd sayı",
                "db" => "transactions",
                "input" => "number",
                "attr" => ['class'=>'form-control', 'min' => 0]
            ],
            "loans" => [
                "ad" => "İstiqrazlar",
                "db" => "loans",
                "input" => "text",
                "attr" => ['class'=>'form-control']
            ],
            "loan" => [
                "ad" => "İstiqraz",
                "db" => "loans",
                "input" => "select",
                "relation" => config('config.loan-type'),
                "relation_is" => "array",
                "selected" => 0,
                "attr" => ['class'=>'form-control', 'min' => 0]
            ],
            "proportions" => [
                "ad" => "Səhmlər",
                "db" => "proportions",
                "input" => "text",
                "attr" => ['class'=>'form-control']
            ],
            "year" => [
                "ad" => "il",
                "db" => "year",
                "input" => "number",
                "attr" => ['class'=>'form-control', 'min' => 0]
            ],
            "legal_physical" => [
                "ad" => "Şəxs",
                "db" => "type",
                "input" => "select",
                "relation" => config('config.legal-physical'),
                "relation_is" => "array",
                "selected" => 0,
                "attr" => ['class'=>'form-control']
            ],
            "category" => [
                "ad" => "Kateqoriya",
                "db" => "category_id",
                "input" => "select",
                "relation" => [],
                "relation_is" => "array",
                "selected" => null,
                "attr" => ['class'=>'form-control', 'id' => 'category_list']
            ],
            "service" => [
                "ad" => "Xidmət növü",
                "db" => "category_id",
                "input" => "select",
                "relation" => [],
                "relation_is" => "array",
                "selected" => null,
                "attr" => ['class'=>'form-control', 'id' => 'category_list']
            ],
            "status" => [
                "ad" => "Status",
                "db" => "status",
                "input" => "select",
                "relation" => config('config.article-status'),
                "relation_is" => "array",
                "selected" => 1,
                "attr" => ['class'=>'form-control']
            ],
            "featured" => [
                "ad" => "Slider et",
                "db" => "featured",
                "input" => "checkbox",
                "value" => 1,
                "checked" => false,
            ],
            "image" => [
                "ad" => "Şəkil",
                "db" => "image_original",
                "input" => "text",
                "attr" => ['id' => 'feature_image', 'class'=>'form-control', 'readonly' => ''],
                'hide' => 'hide0 hide1 hide2 hide9 hide10 hide11 hide12 hide13 hide14 hide15 hide20 hide21 hide22 hide26',
                "design" => function($input){
                    $group_btn =
                        '<span class="input-group-btn">
                            <button class="btn btn-default popup_selector" type="button" data-inputid="feature_image" >
                                <i class="fa fa-cloud-upload"></i> Seçim et
                            </button>
                            <button class="btn btn-default clear_elfinder_picker" type="button" data-inputid="feature_image" >
                                <i class="fa fa-eraser"></i>
                            </button>
                        </span>';

                    return '<div class="input-group">'.$input.$group_btn.'</div>';
                }
            ],
            "image_cover" => [
                "ad" => "Cover şəkil",
                "db" => "image_cover",
                "input" => "text",
                "attr" => ['id' => 'feature_image2', 'class'=>'form-control', 'readonly' => ''],
                "design" => function($input){
                    $group_btn =
                        '<span class="input-group-btn">
                            <button class="btn btn-default popup_selector" type="button" data-inputid="feature_image2" >
                                <i class="fa fa-cloud-upload"></i> Seçim et
                            </button>
                            <button class="btn btn-default clear_elfinder_picker2" type="button" data-inputid="feature_image2" >
                                <i class="fa fa-eraser"></i>
                            </button>
                        </span>';

                    return '<div class="input-group">'.$input.$group_btn.'</div>';
                }
            ],
            "image_second" => [
                "ad" => "Şəkil 2",
                "db" => "image_second",
                "input" => "text",
                "attr" => ['id' => 'feature_image2', 'class'=>'form-control', 'readonly' => ''],
                "design" => function($input){
                    $group_btn =
                        '<span class="input-group-btn">
                            <button class="btn btn-default popup_selector" type="button" data-inputid="feature_image2" >
                                <i class="fa fa-cloud-upload"></i> Seçim et
                            </button>
                            <button class="btn btn-default clear_elfinder_picker2" type="button" data-inputid="feature_image2" >
                                <i class="fa fa-eraser"></i>
                            </button>
                        </span>';

                    return '<div class="input-group">'.$input.$group_btn.'</div>';
                }
            ],
            "image_article_thumb" => [
                "ad" => "Şəkil 2",
                "db" => "image_second",
                "input" => "text",
                "attr" => ['id' => 'feature_image2', 'class'=>'form-control', 'readonly' => '', 'title' => 'Siyahı formada xəbərlərə baxanda məhz bura qoyulan şəkil təsvir olunacaq. Format 263x378 px nisbətində olmalıdır. Əks halda, şəkil özü avtomatik qaydada crop olunacaq.'],
                "design" => function($input){
                    $group_btn =
                        '<span class="input-group-btn">
                            <button class="btn btn-default popup_selector" type="button" data-inputid="feature_image2" >
                                <i class="fa fa-cloud-upload"></i> Seçim et
                            </button>
                            <button class="btn btn-default clear_elfinder_picker2" type="button" data-inputid="feature_image2" >
                                <i class="fa fa-eraser"></i>
                            </button>
                        </span>';

                    return '<div class="input-group">'.$input.$group_btn.'</div>';
                }
            ],
            "main_video_cover" => [
                "ad" => "Youtube Cover",
                "db" => "main_video_cover",
                "input" => "text",
                "attr" => ['id' => 'feature_image', 'class'=>'form-control', 'readonly' => ''],
                "design" => function($input){
                    $group_btn =
                        '<span class="input-group-btn">
                            <button class="btn btn-default popup_selector" type="button" data-inputid="feature_image" >
                                <i class="fa fa-cloud-upload"></i>
                            </button>
                            <button class="btn btn-default clear_elfinder_picker" type="button" data-inputid="feature_image" >
                                <i class="fa fa-eraser"></i>
                            </button>
                        </span>';

                    return '<div class="input-group">'.$input.$group_btn.'</div>';
                }
            ],
            "document" => [
                "ad" => "PDF faylı",
                "db" => "document",
                "input" => "text",
                "attr" => ['id' => 'feature_image2', 'class'=>'form-control', 'readonly' => ''],
                "design" => function($input){
                    $group_btn =
                        '<span class="input-group-btn">
                            <button class="btn btn-default popup_selector" type="button" data-inputid="feature_image2" >
                                <i class="fa fa-cloud-upload"></i> Seçim et
                            </button>
                            <button class="btn btn-default clear_elfinder_picker2" type="button" data-inputid="feature_image2" >
                                <i class="fa fa-eraser"></i>
                            </button>
                        </span>';

                    return '<div class="input-group">'.$input.$group_btn.'</div>';
                }
            ],
            "document_word" => [
                "ad" => "Word faylı",
                "db" => "document_word",
                "input" => "text",
                "attr" => ['id' => 'feature_image2', 'class'=>'form-control', 'readonly' => ''],
                "design" => function($input){
                    $group_btn =
                        '<span class="input-group-btn">
                            <button class="btn btn-default popup_selector" type="button" data-inputid="feature_image2" >
                                <i class="fa fa-cloud-upload"></i> Seçim et
                            </button>
                            <button class="btn btn-default clear_elfinder_picker2" type="button" data-inputid="feature_image2" >
                                <i class="fa fa-eraser"></i>
                            </button>
                        </span>';

                    return '<div class="input-group">'.$input.$group_btn.'</div>';
                }
            ],
            "visible" => [
                "ad" => "Görünmə forması",
                "db" => "visible",
                "input" => "select",
                "relation" => config('config.menu-visibility'),
                "relation_is" => "array",
                "selected" => 1,
                "attr" => ['class'=>'form-control'],
                'hide' => 'hide11'

            ],
            "visible-boolean" => [
                "ad" => "Status",
                "db" => "visible",
                "input" => "select",
                "relation" => config('config.menu-visibility-boolean'),
                "relation_is" => "array",
                "selected" => 1,
                'hide' => 'hide11',
                "attr" => ['class'=>'form-control']
            ],
            "target" => [
                "ad" => "Linkin quruluşu",
                "db" => "target",
                "input" => "select",
                "relation" => config('config.menu-target'),
                "relation_is" => "array",
                "selected" => 1,
                "attr" => ['class'=>'form-control'],
                'hide' => 'hide11'
            ],
            "url" => [
                "ad" => "Link",
                "db" => "link",
                "input" => "text",
                "attr" => ['class'=>'form-control'],
                "design" => function($input){
                    $group_btn =
                        '<span class="input-group-addon">'.config('app.url').'/</span>';

                    return '<div class="input-group">'.$group_btn.$input.'</div>';
                }
            ],
            "forward_url" => [
                "ad" => "Link",
                "db" => "forward_url",
                "input" => "text",
                'hide' => 'hide11',
                "attr" => ['class'=>'form-control', 'title'=>"Səhifəni başqa ünvana yönləndirmək üçün nəzərdə tutulub.", 'placeholder' => 'Məs: http://google.az'],
            ],
            "type" => [
                "ad" => "Tip",
                "db" => "type",
                "input" => "select",
                "relation" => config('config.slider-type'),
                "relation_is" => "array",
                "selected" => 1,
                "attr" => ['class'=>'form-control']
            ],
            "linkedin" => [
                "ad" => "Linkedin",
                "db" => "linkedin",
                "input" => "text",
                "attr" => ['class'=>'form-control']
            ],
            "meta_description" => [
                "ad" => "Meta description",
                "db" => "meta_description",
                "input" => "textarea",
                'hide' => 'hide11',
                "attr" => ['class'=>'form-control', 'maxlength' => 160, 'rows' => 3, 'title' => 'Maksimum simvol sayı 160 olmalıdır.Sosial şəbəkələrdə paylaşılan zaman məhz bu yazı linkin altında göstəriləcək']
            ],
            "meta_keywords" => [
                "ad" => "Meta keywords",
                "db" => "meta_keywords[]",
                "input" => "select",
                "relation" => [],
                "relation_is" => "array",
                "selected" => null,
                'hide' => 'hide11',
                "attr" => ['class'=>'form-control', 'id' => 'meta_keywords', 'multiple' => 'multiple', 'autocomplete' => 'off', 'title' => 'Keyword-ləri bir birindən ayırmaq üçün Enter düyməsini sıxın.']
            ],
            "first_name" => [
                "ad" => "Ad",
                "db" => "first_name",
                "input" => "text",
                "attr" => ['class'=>'form-control']
            ],
            "last_name" => [
                "ad" => "Soyad",
                "db" => "last_name",
                "input" => "text",
                "attr" => ['class'=>'form-control']
            ],
            "email" => [
                "ad" => "Email",
                "db" => "email",
                "input" => "text",
                "attr" => ['class'=>'form-control']
            ],
            "contact_email" => [
                "ad" => "Əlaqə forması üçün e-poçt",
                "db" => "contact_form_email",
                "input" => "text",
                "attr" => ['class'=>'form-control']
            ],
            "hr_email" => [
                "ad" => "İnsan resursları üçün e-poçt",
                "db" => "hr_form_email",
                "input" => "text",
                "attr" => ['class'=>'form-control']
            ],
            "customer_email" => [
                "ad" => "Müştəri xidmətləri üçün e-poçt",
                "db" => "customer_form_email",
                "input" => "text",
                "attr" => ['class'=>'form-control']
            ],
            "google_api_key" => [
                "ad" => "Google api key",
                "db" => "google_api_key",
                "input" => "text",
                "attr" => ['class'=>'form-control']
            ],
            "password" => [
                "ad" => "Şifrə",
                "db" => "password",
                "input" => "password",
                "attr" => ['class'=>'form-control']
            ],
            "role" => [
                "ad" => "Vəzifə",
                "db" => "role",
                "input" => "select",
                "relation" => config('config.role'),
                "relation_is" => "array",
                "selected" => 1,
                "attr" => ['class'=>'form-control']
            ],
            "youtube_id" => [
                "ad" => "Youtube ID",
                "db" => "youtube_id",
                "input" => "text",
                "attr" => ['class'=>'form-control','title' => 'youtube.com/watch?v=O5mcGeYQ74c<br>Bərabər işarəsindən sonra gələn hissə Youtube ID sayılır', 'placeholder' => 'Məs: O5mcGeYQ74c']
            ],
            "social_type" => [
                "ad" => "Şəbəkə",
                "db" => "type",
                "input" => "select",
                "relation" => config('config.social-network'),
                "relation_is" => "array",
                "selected" => 'facebook',
                "attr" => ['class'=>'form-control']
            ],
            "address" => [
                "ad" => "Ünvan",
                "db" => "address",
                "input" => "text",
                "attr" => ['class'=>'form-control']
            ],
            "logo" => [
                "ad" => "Loqo",
                "db" => "logo",
                "input" => "text",
                "attr" => ['id' => 'logo_png', 'class'=>'form-control', 'readonly' => ''],
                "design" => function($input){
                    $group_btn =
                        '<span class="input-group-btn">
                            <button class="btn btn-default popup_selector" type="button" data-inputid="logo_png" >
                                <i class="fa fa-cloud-upload"></i> Seçim et
                            </button>
                            <button class="btn btn-default clear_elfinder_picker" type="button" data-inputid="logo_png" >
                                <i class="fa fa-eraser"></i>
                            </button>
                        </span>';

                    return '<div class="input-group">'.$input.$group_btn.'</div>';
                }
            ],
            "popup" => [
                "ad" => "Popup",
                "db" => "popup",
                "input" => "checkbox",
                "value" => 1,
                "checked" => false,
                "attr" => ['class'=>'margin-left-zero'],
                "div_class" => "parent_div"
            ],
            "slider" => [
                "ad" => "Slider",
                "db" => "has_slider",
                "input" => "checkbox",
                "value" => 1,
                "checked" => false,
                "attr" => ['class'=>'margin-left-zero'],
                "div_class" => "parent_div"
            ],
            "partner" => [
                "ad" => "Əməkdaş",
                "db" => "has_partner",
                "input" => "checkbox",
                "value" => 1,
                "checked" => true,
                "attr" => ['class'=>'margin-left-zero'],
                "div_class" => "parent_div"
            ],
            "icon" => [
                "ad" => "İkon",
                "db" => "icon",
                "input" => "select",
                "relation" => config('config.icon-service'),
                "relation_is" => "array",
                "selected" => 1,
                "attr" => ['class'=>'form-control']
            ],
            "fb_app" => [
                "ad" => "Fb App Id",
                "db" => "fb_app_id",
                "input" => "text",
                "attr" => ['class'=>'form-control'],
            ],
            "languages" => [
                "ad" => "Başlıq",
                "db" => "title",
                "input" => "languageTab",
                "attr" => ['class'=>'form-control'],
            ],
            "keyword" => [
                "ad" => "Açar söz",
                "db" => "keyword",
                "input" => "text",
                "attr" => ['class'=>'form-control']
            ],
            "blog_page" => [
                "ad" => "Ana səhifədə xəbər lenti",
                "db" => "blog_page",
                "input" => "select",
                "relation" => [],
                "relation_is" => "array",
                "selected" => null,
                "attr" => ['class'=>'form-control', 'id' => 'blog_page']
            ],

            "corp_loans" => [
                "ad" => "Korporativ istiqraz",
                "db" => "corp_loans",
                "input" => "text",
                "attr" => ['class'=>'form-control']
            ],
            "gov_loans" => [
                "ad" => "Dövlət istiqrazları",
                "db" => "gov_loans",
                "input" => "text",
                "attr" => ['class'=>'form-control']
            ],
            "proportions2" => [
                "ad" => "Təkrar bazar (səhmlər)",
                "db" => "proportions",
                "input" => "text",
                "attr" => ['class'=>'form-control']
            ],
            "notes" => [
                "ad" => "Notlar",
                "db" => "notes",
                "input" => "text",
                "attr" => ['class'=>'form-control']
            ],
            "currency" => [
                "ad" => "Valyuta",
                "db" => "currency",
                "input" => "text",
                "attr" => ['class'=>'form-control']
            ],
            "buying" => [
                "ad" => "Alış",
                "db" => "buying",
                "input" => "text",
                "attr" => ['class'=>'form-control']
            ],
            "sales" => [
                "ad" => "Satış",
                "db" => "sales",
                "input" => "text",
                "attr" => ['class'=>'form-control']
            ],
            "loan_type" => [
                "ad" => "Növü",
                "db" => "type",
                "input" => "text",
                "attr" => ['class'=>'form-control']
            ],
            "ticker" => [
                "ad" => "Tiker",
                "db" => "ticker",
                "input" => "text",
                "attr" => ['class'=>'form-control']
            ],
            "price" => [
                "ad" => "Qiyməti",
                "db" => "price",
                "input" => "text",
                "attr" => ['class'=>'form-control']
            ],
            "introduction" => [
                "ad" => "Gəlirliliyi",
                "db" => "introduction1",
                "input" => "text",
                "attr" => ['class'=>'form-control']
            ],
            "birja" => [
                "ad" => "Satıldığı birja",
                "db" => "birja",
                "input" => "text",
                "attr" => ['class'=>'form-control']
            ],
            "asset" => [
                "ad" => "Asset",
                "db" => "name",
                "input" => "text",
                "attr" => ['class'=>'form-control']
            ],
            "capital" => [
                "ad" => "Capital",
                "db" => "capital",
                "input" => "text",
                "attr" => ['class'=>'form-control']
            ],
            "total" => [
                "ad" => "Total",
                "db" => "total",
                "input" => "text",
                "attr" => ['class'=>'form-control']
            ],
            "variance" => [
                "ad" => "Variance",
                "db" => "variance",
                "input" => "text",
                "attr" => ['class'=>'form-control']
            ],
        );
    }
}
