<?php
/**
 * Created by PhpStorm.
 * User: Rashad
 * Date: 2/28/17
 * Time: 22:20
 */

namespace App\Logic;

use Cache;
use Carbon\Carbon;

class Exchange
{

    public function list($type)
    {
        $currency = [];

        foreach(config('config.currency.'.$type) as $key => $name){

            list($from, $to) = explode('/', $name);

            $currency[] = $to;
        }

        return $currency;

    }



    public function get($type, $cachename, $endpoint, $source)
    {
        $access_key = '1e773dc8d04d436970813bf3f7c121cd';
        $currency = implode(',', $this->list($type));


        $currencyApi = Cache::remember($cachename, 60, function () use($currency, $access_key, $endpoint, $source){

            if(env('APP_ENV') == 'production'){
                $request = 'https';
            }
            else{
                $request = 'http';
            }


            $curl = $request.'://apilayer.net/api/'.$endpoint.'?access_key='.$access_key.'&currencies='.$currency.'&source='.$source;

            if($endpoint == 'historical'){
                $curl.= '&date='.Carbon::yesterday()->format('Y-m-d');
            }

            // Initialize CURL:
            $ch = curl_init($curl);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

            // Store the data:
            $json = curl_exec($ch);
            curl_close($ch);

            // Decode JSON response:
            $exchangeRates = json_decode($json, true);

            return $exchangeRates['quotes'];

        });


        return $currencyApi;

    }

}