<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Carbon\Carbon;

class BrokerTable extends Model
{
    use SoftDeletes;

    protected $table = 'broker_tables';
    protected $guarded = ['id'];
    protected $hidden = ['_token'];
    protected $dates = ['created_at', 'updated_at', 'deleted_at'];


    public function getCreatedAtAttribute($value)
    {
        return with(new Carbon($value))->format('d/m/Y, H:i');
    }


    public function getUpdatedAtAttribute($value)
    {
        return with(new Carbon($value))->format('d/m/Y, H:i');
    }


    public static $rules = [
        'category_id' => 'required|numeric',
        'corp_loans' => 'required',
        'gov_loans' => 'required',
        'proportions' => 'required',
        'notes' => 'required'
    ];


    public static $messages = [
        'category_id.required' => 'Xidmət növü seçilməyib.',
    ];
}
