<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Rekvisit extends Model
{
    protected $table = 'rekvisits';
    protected $guarded = ['id'];
    protected $hidden = ['_token'];
    protected $dates = ['created_at', 'updated_at'];

    public static $rules = [
        'registration_number' => 'required',
        'nominal_value' => 'required',
        'coupon' => 'required',
        'coupon_payment_az' => 'required',
        'coupon_payment_en' => 'required',
        'expiration_date' => 'required'
    ];

    public static $messages = [
        'registration_number.required' => 'Qeydiyyat nömrəsi seçilməyib',
        'nominal_value.required'       => 'Nominal dəyər   seçilməyib',
        'coupon.required'              => 'Kupon seçilməyib',
        'coupon_payment_az.required'   => 'Coupon Ödənişləri  Az seçilməyib',
        'coupon_payment_en.required'   => 'Coupon Ödənişləri  En seçilməyib',
        'expiration_date.required'     => 'Müddət seçilməyib'
    ];


    public function getCreatedAtAttribute($value)
    {
        return with(new Carbon($value))->format('d/m/Y, H:i');
    }

    public function getUpdatedAtAttribute($value)
    {
        return with(new Carbon($value))->format('d/m/Y, H:i');
    }
}
