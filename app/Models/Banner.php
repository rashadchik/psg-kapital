<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Carbon\Carbon;

class Banner extends Model
{
   use SoftDeletes;

    protected $table = 'banners';

    protected $guarded = ['id'];
    protected $hidden = ['_token'];
    protected $dates = ['created_at', 'updated_at', 'deleted_at'];

    public static $rules = [
        'category_id' => 'required',
        'image_original' => 'required',
        'pos_type' => 'required'
    ];

    public static $messages = [
        'category_id.required' => 'Səhifə seçilməyib.',
        'image_original.required' => 'Şəkil seçilməyib',
        'pos_type.required' => 'İstiqamət seçilməyib',
    ];

    public function getCreatedAtAttribute($value)
    {
        return with(new Carbon($value))->format('d/m/Y, H:i');
    }

    public function getUpdatedAtAttribute($value)
    {
        return with(new Carbon($value))->format('d/m/Y, H:i');
    }

}
