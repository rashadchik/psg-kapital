<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Carbon\Carbon;

class ServiceStat extends Model
{
    use SoftDeletes;

    protected $table = 'service_stats';
    protected $guarded = ['id'];
    protected $hidden = ['_token'];
    protected $dates = ['created_at', 'updated_at', 'deleted_at'];


    public function getCreatedAtAttribute($value)
    {
        return with(new Carbon($value))->format('d/m/Y, H:i');
    }


    public function getUpdatedAtAttribute($value)
    {
        return with(new Carbon($value))->format('d/m/Y, H:i');
    }

    public static function rules($id){

        return [
            'category_id' => 'required|numeric|unique:service_stats,category_id,'.$id,
        ];
    }


    public function setCustomersAttribute($value) {

        is_null($value) ? $this->attributes['customers'] = 0 : $this->attributes['customers'] = $value;

    }


    public function setTransactionsAttribute($value) {

        is_null($value) ? $this->attributes['transactions'] = 0 : $this->attributes['transactions'] = $value;

    }


    public function setLoansAttribute($value) {

        is_null($value) ? $this->attributes['loans'] = 0 : $this->attributes['loans'] = $value;

    }


    public function setProportionsAttribute($value) {

        is_null($value) ? $this->attributes['proportions'] = 0 : $this->attributes['proportions'] = $value;

    }


    public static $messages = [
        'category_id.required' => 'Xidmət növü seçilməyib.',
        'category_id.unique' => 'Bu xidmət növü artlq əlavə olunub'
    ];
}
