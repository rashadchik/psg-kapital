<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Carbon\Carbon;


class Reproduction extends Model
{
      use SoftDeletes;
 
    protected $table = 'reproductions';
    protected $guarded = ['id'];
    protected $hidden = ['_token'];
    protected $dates = ['created_at', 'updated_at', 'deleted_at'];
    

    public static $rules = [
        'lang_id' => 'required',
        'name' => 'required',
        'birja' => 'required',
        'type' => 'required',
        'ticker' => 'required',
    ];

    public static $messages = [
        'type.required' => 'Növ seçilməyib',
        'name.required' => 'Baza Aktivi seçilməyib',
        'lang_id.required' => 'Dil seçilməyib',
        'birja.required' => 'Birja seçilməyib',
        'ticker.required' => 'Tiker seçilməyib',
    ];

   public function getCreatedAtAttribute($value)
    {
        return with(new Carbon($value))->format('d/m/Y, H:i');
    }

    public function getUpdatedAtAttribute($value)
    {
        return with(new Carbon($value))->format('d/m/Y, H:i');
    }

}
