<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class MarketLoan extends Model
{ 

    use SoftDeletes;
    
    protected $table =      'market_loans';
    protected $guarded =    ['id'];
    protected $hidden =     ['_token'];
    protected $dates =      ['created_at', 'updated_at', 'deleted_at'];

        public static $rules = [
        'type' => 'required',
        'title_az' => 'required',
        'title_en' => 'required',
        'prise_difference1' => 'required',
        'prise_difference2' => 'required',
        'name_az' => 'required',
        'name_en' => 'required',
    ];

    public static $messages = [
        'type.required' => 'İstiqraz  seçilməyib',
        'title_az.required' => 'Şirkət indeksi az doldurulmayıb',
        'title_en.required' => 'Şirkət indeksi en doldurulmayıb',
        'prise_difference1.required' => 'Qiymət fərqi 1 doldurulmayıb',
        'prise_difference2.required' => 'Qiymət fərqi 2 doldurulmayıb',
        'name_az.required' => 'İstiqamət / Fyuçers (AZ) doldurulmayıb',
        'name_en.required' => 'İstiqamət / Fyuçers (EN) doldurulmayıb',
    ];

}
 