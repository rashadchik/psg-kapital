<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Carbon\Carbon;

class Strategy extends Model
{
    use SoftDeletes;
 
    protected $table = 'strategies';
    protected $guarded = ['id'];
    protected $hidden = ['_token'];
    protected $dates = ['created_at', 'updated_at', 'deleted_at'];
    

    public static $rules = [
        'name' => 'required',
        'capital' => 'required',
        'total' => 'required',
        'variance' => 'required',
    ];

    public static $messages = [
        'name.required' => 'Ad seçilməyib',
        'capital.required' => 'Capital seçilməyib',
        'total.required' => 'Total seçilməyib',
        'variance.required' => 'Variance seçilməyib',
    ];

   public function getCreatedAtAttribute($value)
    {
        return with(new Carbon($value))->format('d/m/Y, H:i');
    }

    public function getUpdatedAtAttribute($value)
    {
        return with(new Carbon($value))->format('d/m/Y, H:i');
    }

}
