<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
class StrategyChart2 extends Model
{
    protected $table = 'strategy_chart2s';
    protected $guarded = ['id'];
    protected $hidden = ['_token'];
    protected $dates = ['created_at', 'updated_at', 'deleted_at'];
    

    public static function rules($lang, $id = null){
        return [
            'lang_id' => 'required|unique:strategy_chart2s,lang_id,'.$id.',id,lang_id,'.$lang,
            'image_original' => 'required',
            'content' => 'required',
        ];
    }

    public function getCreatedAtAttribute($value)
    {
        return with(new Carbon($value))->format('d/m/Y, H:i');
    }

    public function getUpdatedAtAttribute($value)
    {
        return with(new Carbon($value))->format('d/m/Y, H:i');
    }
}
