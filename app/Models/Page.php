<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Carbon\Carbon;
use Mcamara\LaravelLocalization\Facades\LaravelLocalization;

class Page extends Model
{
    use SoftDeletes;

    protected $table = 'pages';
    protected $guarded = ['id'];
    protected $hidden =  ['_token'];
    protected $dates =   ['created_at', 'updated_at', 'deleted_at'];
    protected $parent = 'parent_id';


    public static function rules($lang, $id = null){

        return [
            'template_id' => 'required|numeric',
            'lang_id' => 'required',
            'name' => 'required|max:255',
            'slug' => 'unique:pages,slug,'.$id.',id,lang_id,'.$lang,
            'image_cover' => 'nullable',
            'image_original' => 'nullable',
            'description' => 'nullable',
            'summary' => 'nullable',
            'has_slider' => 'boolean',
            'has_partner' => 'boolean',
            'content' => 'nullable',
            'content_extra' => 'nullable',
            'forward_url' => 'nullable',
            'order' => 'numeric',
            'target' => 'boolean',
            'visible' => 'numeric',
            'youtube_id' => 'nullable',
            'featured' => 'boolean',
            'icon' => 'nullable',
            'meta_description' => 'nullable|max:160',
        ];
    }

    public static function categoryRules($lang, $id = null){

        return [
            'name' => 'required|max:255',
            'slug' => 'unique:pages,slug,'.$id.',id,lang_id,'.$lang,
            'image_original' => 'nullable',
            'description' => 'nullable',
            'summary' => 'nullable',
            'has_slider' => 'boolean',
            'has_partner' => 'boolean',
            'content' => 'nullable',
            'content_extra' => 'nullable',
            'forward_url' => 'nullable',
            'order' => 'numeric',
            'target' => 'boolean',
            'visible' => 'numeric',
            'youtube_id' => 'nullable',
            'featured' => 'boolean',
            'icon' => 'nullable',
            'meta_description' => 'nullable|max:160',
        ];
    }


    public static $vacancyRules = [
        'salary' => 'required_if:template_id,9',
        'end_date' => 'required_if:template_id,9'
    ];


    public static $messages = [
        'salary.required_if' => 'Əmək haqqı qeyd olunmayıb',
        'end_date.required_if' => 'Son tarix qeyd olunmayıb',

    ];

    public function getCreatedAtAttribute($value)
    {
        return with(new Carbon($value))->format('d/m/Y, H:i');
    }


    public function getUpdatedAtAttribute($value)
    {
        return with(new Carbon($value))->format('d/m/Y, H:i');
    }


    public function setMetaKeywordsAttribute($value) {

        $keywords = null;

        if(!is_null($value) && is_array($value))
        {
            $keywords = implode(",", $value);
        }

        $this->attributes['meta_keywords'] = $keywords;
    }



    //for reorder menu//
    public function buildPage($menu, $parentid = 0)
    {
        $result = null;
        foreach ($menu as $item)
            if ($item->parent_id == $parentid) {
                $result .= "<li class='dd-item nested-list-item' data-order='{$item->order}' data-id='{$item->id}'>
                <div class='dd-handle nested-list-handle'>
                    <span class='glyphicon glyphicon-move'></span>
                </div>
                <div class='nested-list-content'>{$item->name}
                </div>".$this->buildPage($menu, $item->id) . "</li>";
            }
        return $result ?  "\n<ol class=\"dd-list\">\n$result</ol>\n" : null;
    }



    public function getPage($items)
    {
        return $this->buildPage($items);
    }


    //for reorder menu//
    public function buildCategory($menu, $parentid = 0)
    {
        $result = null;
        foreach ($menu as $item)
            if ($item->parent_id == $parentid) {
                $result .= "<li data-order='{$item->order}' data-id='{$item->id}'>

                <div><h4>{$item->name}</h4>
                </div>".$this->buildPage($menu, $item->id) . "</li>";
            }
        return $result ?  "\n<ol class=\"dd-list\">\n$result</ol>\n" : null;
    }


    public function getCategory($items)
    {
        return $this->buildCategory($items);
    }


    public function scopeOrdered($query)
    {
        return $query->orderBy('order', 'asc');
    }


    public function scopeVisible($query)
    {
        return $query->where('visible', '<>', 0);
    }


    public function articles()
    {
        return $this->hasMany('App\Models\Article', 'category_id', 'id')
            ->leftJoin('galleries', function ($join) {
                $join->on('articles.relation_page', '=', 'galleries.category_id')->where('galleries.type', 6);
            })
            ->select('articles.id', 'articles.relation_page', 'articles.title', 'articles.slug', 'articles.summary', 'galleries.filename as image', 'articles.published_at')
            ->where('articles.status', 1)
            ->orderBy('articles.published_at', 'desc')
            ->orderBy('articles.id', 'desc')
            ->limit(4);
    }

    public function otherArticles($id)
    {
        return $this->hasMany('App\Models\Article', 'category_id', 'id')
            ->join('pages', 'pages.id', '=', 'articles.category_id')
            ->select('articles.title', 'articles.slug as article_slug', 'articles.published_at', 'pages.slug')
            ->where('articles.id', '<>', $id)
            ->where('articles.status', 1)
            ->whereNull('articles.deleted_at')
            ->orderBy('articles.published_at', 'desc')
            ->orderBy('articles.id', 'desc')
            ->limit(6);
    }

    public function menuChildren()
    {
        return $this->hasMany('App\Models\Page', 'parent_id', 'id')
            ->where('third_level', '<', 3)
            ->where('visible', 1)
            ->whereNotIn('template_id', [9, 11])
            ->select('id', 'parent_id', 'name', 'slug', 'target', 'template_id')
            ->with('menuChildren')
            ->ordered();
    }


    public function children()
    {
        return $this->hasMany('App\Models\Page', 'parent_id', 'id')->select('id', 'name', 'summary', 'slug', 'description', 'template_id', 'image_original')->visible()->ordered();
    }


    public function legalPhysical()
    {
        return $this->hasMany('App\Models\LegalPhysical', 'category_id', 'id')
            /*->leftJoin('galleries', function ($join) {
                $join->on('pages.relation_page', '=', 'galleries.category_id')->where('galleries.type', 5);
            })*/
            ->select('legal_physicals.type', 'legal_physicals.title');
    }


    public function brokerTable()
    {
        return $this->hasMany('App\Models\BrokerTable', 'category_id', 'relation_page');
    }


    public function services()
    {
        $lang = LaravelLocalization::getCurrentLocale();

        return $this->hasMany('App\Models\BrokerService', 'category_id', 'relation_page')->select("title_$lang as title");
    }

    public function details()
    {
        return $this->hasOne('App\Models\ServiceStat', 'category_id', 'relation_page');
    }


    public function galleries()
    {
        return $this->hasMany('App\Models\Gallery', 'category_id', 'relation_page')->where('type', 2)->orderBy('id', 'asc')->limit(10);
    }

    
    public function cover()
    {
        return $this->hasOne('App\Models\Gallery', 'category_id', 'relation_page')->where('type', 4)->select('filename as image');
    }


    public function vacancy()
    {
        return $this->hasMany('App\Models\Page', 'parent_id', 'id')
            ->leftJoin('vacancy_details', 'vacancy_details.vacancy_id', '=', 'pages.relation_page')
            ->select('pages.slug', 'pages.name', 'pages.description', 'vacancy_details.salary', 'vacancy_details.end_date')
            ->visible()
            ->ordered();
    }


    public function vacancyDetail()
    {
        return $this->hasOne('App\Models\VacancyDetail', 'vacancy_id', 'relation_page');
    }


    public function documents()
    {
        return $this->hasMany('App\Models\Document', 'category_id', 'id')->orderby('id', 'desc');
    }


    public function banners()
    {
        return $this->hasMany('App\Models\Banner', 'category_id', 'id')->limit(3);
    }
}
