<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
class MarketChart extends Model
{
    protected $guarded = ['id'];
    protected $hidden = ['_token'];
    protected $dates = ['created_at', 'updated_at', 'deleted_at'];


    public static $rules = [
        'published_at' => 'required|date',
        'type' => 'required',
        'percent' => 'required',
    ];

    public static $messages = [
        'published_at.required' => 'Tarix seçilməyib',
        'percent.required' => 'Faiz seçilməyib',
        'type.required' => 'İstiqraz seçilməyib',

    ];


    public function getCreatedAtAttribute($value)
    {
        return with(new Carbon($value))->format('d/m/Y, H:i');
    }

    public function getUpdatedAtAttribute($value)
    {
        return with(new Carbon($value))->format('d/m/Y, H:i');
    }
}
