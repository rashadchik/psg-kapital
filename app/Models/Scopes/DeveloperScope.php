<?php
/**
 * Created by PhpStorm.
 * User: Rashad
 * Date: 1/24/17
 * Time: 02:27
 */

namespace App\Models\Scopes;

use Illuminate\Database\Eloquent\Scope;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Auth\Guard;

class DeveloperScope implements Scope
{
    protected $auth;

    public function __construct(Guard $auth)
    {
        $this->auth = $auth;
    }


    public function apply(Builder $builder, Model $model)
    {
        return $builder->where('id', '<>', $this->auth->user()->id);
    }
}