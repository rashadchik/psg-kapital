<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Subscriber extends Model
{
    protected $table = 'subscribers';

    protected $fillable = ['email', 'status', 'full_name'];

    protected $dates = ['created_at', 'updated_at'];

    public function getCreatedAtAttribute($value)
    {
        return with(new Carbon($value))->format('d/m/Y, H:i');
    }

    public function getUpdatedAtAttribute($value)
    {
        return with(new Carbon($value))->format('d/m/Y, H:i');
    }

    public static function rules(){

        return [
            'full_name' => "nullable",
            'email' => 'required|email|unique:subscribers'
        ];
    }

}
