<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Carbon\Carbon;

class ReproductionClient extends Model
{
    use SoftDeletes;

    protected $table = 'reproduction_clients';
    protected $guarded = ['id'];
    protected $hidden = ['_token'];
    protected $dates = ['created_at', 'updated_at', 'deleted_at'];


    public function getCreatedAtAttribute($value)
    {
        return with(new Carbon($value))->format('d/m/Y, H:i');
    }


    public function getUpdatedAtAttribute($value)
    {
        return with(new Carbon($value))->format('d/m/Y, H:i');
    }

        public static $rules = [
        'lang_id' => 'required',
        'name' => 'required',
        'buyer' => 'required',
        'seller' => 'required',
    ];

    public static $messages = [
        'lang_id.required' => 'Dil seçilməyib',
        'name.required' => 'Ad seçilməyib',
        'buyer.required' => 'Alıcı seçilməyib',
        'seller.required' => 'Satıcı seçilməyib',
    ];


}
