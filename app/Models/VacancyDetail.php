<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class VacancyDetail extends Model
{
    protected $guarded = ['id'];


    public static $rules = [
            'salary' => 'required',
            'end_date' => 'required'
        ];



    public static $messages = [
        'salary.required' => 'Əmək haqqı qeyd olunmayıb',
        'end_date.required' => 'Son tarix qeyd olunmayıb',

    ];

}
