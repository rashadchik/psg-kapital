<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
class StrategyChart extends Model
{
    protected $table = 'strategies_charts';
    protected $guarded = ['id'];
    protected $hidden = ['_token'];
    protected $dates = ['created_at', 'updated_at', 'deleted_at'];
    

    public static $rules = [
        'year' => 'required',
        'type' => 'required',
        'percent' => 'required',
    ];

    public static $messages = [
        'year.required' => 'İl seçilməyib',
        'percent.required' => 'Faiz seçilməyib',
        'type.required' => 'İstiqraz seçilməyib',

    ];


    public function getCreatedAtAttribute($value)
    {
        return with(new Carbon($value))->format('d/m/Y, H:i');
    }

    public function getUpdatedAtAttribute($value)
    {
        return with(new Carbon($value))->format('d/m/Y, H:i');
    }
}
