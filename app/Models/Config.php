<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Config extends Model
{
    protected $table = 'configs';

    protected $guarded = ['id'];
    protected $hidden = ['_token'];
    protected $dates = ['created_at', 'updated_at'];


    public static $rules = [
            'company_name' => 'required',
            'email' => 'required|email',
            'contact_form_email' => 'nullable|email',
            'app_form_email' => 'nullable|email',
            'site_url' => 'required',
            'location' => 'nullable',
            'contact_phone' => 'nullable',
            'main_video' => 'nullable',
            'lang_id' => 'required',
            'google_api_key' => 'nullable',
    ];


    public static $catalogRule = [
        'brandbook' => 'required|mimes:pdf|max:100000'
    ];


    public static $contactRule = [
        'full_name' => 'required|min:3',
        'email' => 'required|email',
        //'phone' => 'required|numeric',
        //'phone-full' => 'required_with:phone',
        'subject' => 'required|min:3',
        'text' => 'required|min:30',
    ];

    public static $applyRule = [
        'full_name' => 'required|min:3',
        'email' => 'required|email',
        'phone' => 'required|numeric|digits_between:9,15',
    ];

    public static $popupRule = [
        'first_name' => 'required|min:3',
        'last_name' => 'required|min:3',
        'email' => 'required|email',
        'phone' => 'required|numeric|digits_between:9,15',
    ];

    public static $testRule = [
        'first_name' => 'required|min:3',
        'last_name' => 'required|min:3',
        'phone' => 'required|numeric|digits_between:9,15',
        'day' => 'required|numeric|between:1,31',
        'month' => 'required|numeric|between:1,12',
    ];

    public static $partnerRule = [
        'full_name' => 'required|min:3',
        'email' => 'required|email',
        'phone' => 'required|numeric|digits_between:9,15',
        'service' => 'nullable'
    ];

    public static $messages = [
        'brandbook.required' => 'Fayl seçilməyib',
        'brandbook.mimes' => 'Kataloq :values formatda yüklənməlidir',
    ];

}
