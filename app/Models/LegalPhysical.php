<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Carbon\Carbon;

class LegalPhysical extends Model
{
    use SoftDeletes;

    protected $table = 'legal_physicals';
    protected $guarded = ['id'];
    protected $hidden = ['_token'];
    protected $dates = ['created_at', 'updated_at', 'deleted_at'];


    public function getCreatedAtAttribute($value)
    {
        return with(new Carbon($value))->format('d/m/Y, H:i');
    }


    public function getUpdatedAtAttribute($value)
    {
        return with(new Carbon($value))->format('d/m/Y, H:i');
    }


    public static $rules = [
            'category_id' => 'required|numeric',
            'type' => 'required|in:1,2',
            'title' => 'required'
    ];


    public static $messages = [
        'category_id.required' => 'Xidmət növü seçilməyib.'
    ];
}
