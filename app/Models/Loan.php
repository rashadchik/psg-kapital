<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Carbon\Carbon;
class Loan extends Model
{   
    use SoftDeletes;

    protected $table = 'loans';
    protected $guarded = ['id'];
    protected $hidden = ['_token'];
    protected $dates = ['created_at', 'updated_at', 'deleted_at'];

     public static $rules = [
        'loans' => 'required',
        'name' => 'required',
        'type' => 'required',
        'price' => 'required',
        'introduction1' => 'required',
        'coupon' => 'required',
        'birja' => 'required',
        'title_az' => 'nullable',
        'title_en' => 'nullable',
    ];

    public static $messages = [
        'loans.required' => 'İstiqraz  seçilməyib',
        'name.required' => 'Ad  seçilməyib',
        'type.required' => 'İstiqraz növü  seçilməyib',
        'price.required' => 'Qiymət  seçilməyib',
        'introduction1.required' => 'Gəlir  seçilməyib',
        'coupon.required' => 'Kupon seçilməyib',
        'birja.required' => 'Birja seçilməyib',
    ];

    public function getCreatedAtAttribute($value)
    {
        return with(new Carbon($value))->format('d/m/Y, H:i');
    }


    public function getUpdatedAtAttribute($value)
    {
        return with(new Carbon($value))->format('d/m/Y, H:i');
    }

}