<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Question extends Model
{
    protected $table = 'quest';
    protected $guarded = ['id'];
    protected $hidden = ['_token'];
    protected $dates = ['created_at', 'updated_at', 'deleted_at'];

     public static $rules = [
        'title' => 'required',
        'image_original' => 'nullable',
    ];

    public function answer()
    {
        return $this->hasMany('App\Models\Question', 'parent_id', 'id')->select('id', 'title', 'parent_id')->orderBy('id', 'asc');
    }

}


 