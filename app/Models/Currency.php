<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Carbon\Carbon;
class Currency extends Model
{
    use SoftDeletes;
    protected $table = 'currencies';
    protected $guarded = ['id'];
    protected $hidden = ['_token'];
    protected $dates = ['created_at', 'updated_at', 'deleted_at'];


   public static $rules = [
        'currency' => 'required',
        'buying' => 'required',
        'sales' => 'required',
   ];

    public static $messages = [
        'currency.required' => 'Valyuta seçilməyib',
        'buying.required' => 'Alış seçilməyib',
        'sales.required' => 'Satış seçilməyib',
    ];


    public function getCreatedAtAttribute($value)
    {
        return with(new Carbon($value))->format('d/m/Y, H:i');
    }


    public function getUpdatedAtAttribute($value)
    {
        return with(new Carbon($value))->format('d/m/Y, H:i');
    }

}
