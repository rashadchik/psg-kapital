<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Carbon\Carbon;

class Stock extends Model
{
    use SoftDeletes;
 
    protected $table = 'stocks';
    protected $guarded = ['id'];
    protected $hidden = ['_token'];
    protected $dates = ['created_at', 'updated_at', 'deleted_at'];
    

    public static $rules = [
        'type' => 'required',
        'name' => 'required',
        'indicator1' => 'required',
        'indicator3' => 'required',
        'dividend' => 'required',
    ];

    public static $messages = [
        'type.required' => 'Səhm seçilməyib',
        'name.required' => 'Ad seçilməyib',
        'indicator1.required' => '1-illik göstərici seçilməyib',
        'indicator3.required' => '3-illik göstərici seçilməyib',
        'dividend.required' => 'Dividend seçilməyib',
    ];

   public function getCreatedAtAttribute($value)
    {
        return with(new Carbon($value))->format('d/m/Y, H:i');
    }

    public function getUpdatedAtAttribute($value)
    {
        return with(new Carbon($value))->format('d/m/Y, H:i');
    }

}
