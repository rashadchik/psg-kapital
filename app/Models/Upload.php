<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Upload extends Model
{
    protected $guarded =    ['id', 'created_at'];
    protected $hidden =     ['_token'];

    public function setUpdatedAtAttribute($value)
    {
        // Do nothing.
    }

    public static $rules = [
        'file' => 'mimes:jpeg,jpg,pdf|max:10240',
    ];
}
