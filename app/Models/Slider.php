<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Carbon\Carbon;

class Slider extends Model
{
    use SoftDeletes;

    protected $table = 'sliders';

    protected $guarded = ['id'];
    protected $hidden = ['_token'];
    protected $dates = ['created_at', 'updated_at', 'deleted_at'];

    public function getCreatedAtAttribute($value)
    {
        return with(new Carbon($value))->format('d/m/Y, H:i');
    }

    public function getUpdatedAtAttribute($value)
    {

        return with(new Carbon($value))->format('d/m/Y, H:i');
    }

    public static function rules(){

        return [
            'title' => 'nullable',
            'summary' => 'nullable',
            'image_original' => 'required',
            'link' => 'nullable',
            'lang_id' => 'required'
        ];
    }


    public static $partnerRules = [
        'title' => 'required',
        'image_original' => 'required',
    ];

    //for reorder//
    public function buildSlider($items)
    {
        $result = null;

        foreach ($items as $item)
        {
            $result .= "
            <li class='dd-item nested-list-item' data-order='{$item->order}' data-id='{$item->id}'>
                <div class='dd-handle nested-list-handle'>
                    <span class='glyphicon glyphicon-move'></span>
                </div>
                <div class='nested-list-content'>{$item->id}) {$item->title}</div>
            </li>";
        }

        return $result ?  "\n<ol class=\"dd-list\">\n$result</ol>\n" : null;
    }



    public function getSlider($items)
    {
        return $this->buildSlider($items);
    }

}
