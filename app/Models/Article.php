<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Carbon\Carbon;
use DB;
class Article extends Model
{
    use SoftDeletes;

    protected $table = 'articles';
    protected $guarded = ['id'];
    protected $hidden = ['_token'];
    protected $dates = ['created_at', 'updated_at', 'deleted_at'];

    public static function rules($id = null){

        return [
            'title' => "required|max:255",
            'slug' => 'unique:articles,slug,'.$id,
            'category_id' => 'required|numeric',
            'image_original' => 'nullable',
            'published_at' => 'required|date',
            'status' => 'boolean',
            'featured' => 'boolean',
            'summary' => 'nullable',
            'content' => 'nullable',
            'youtube_id' => 'nullable',
        ];
    }


    public function getCreatedAtAttribute($value)
    {
        return with(new Carbon($value))->format('d/m/Y, H:i');
    }


    public function getUpdatedAtAttribute($value)
    {
        return with(new Carbon($value))->format('d/m/Y, H:i');
    }


    public function page()
    {
        return $this->belongsTo('App\Models\Page', 'category_id');
    }


    public function scopeCondition($query, $keyword)
    {
        if($keyword == 1){
            return $query->where(function ($query) use ($keyword){
                $query->orWhere('articles.main_category_id', $keyword)
                    ->orWhere('articles.category_id', $keyword);
            });
        }
    }


    public function scopeSelectByYear($query, $year)
    {
        if(!is_null($year)){
            return $query->where(DB::raw('YEAR(articles.published_at)'), $year);
        }
        else{
            return $query;
        }
    }


    public function scopeSelectByMonth($query, $month)
    {
        if(!is_null($month)){
            return $query->where(DB::raw('MONTH(articles.published_at)'), $month);
        }
        else{
            return $query;
        }
    }


    public function image()
    {
        return $this->hasOne('App\Models\Gallery', 'category_id', 'relation_page')->where('type', 6);
    }


    public function galleries()
    {
        return $this->hasMany('App\Models\Gallery', 'category_id', 'relation_page')->where('type', 1)->orderBy('id', 'asc')->limit(20);
    }

}

