<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Carbon\Carbon;

class Media extends Model
{
    use SoftDeletes;

    protected $table =      'media';
    protected $guarded =    ['id'];
    protected $hidden =     ['_token'];
    protected $dates =      ['created_at', 'updated_at', 'deleted_at'];

    public function getCreatedAtAttribute($value)
    {
        return with(new Carbon($value))->format('d/m/Y, H:i');
    }

    public function getUpdatedAtAttribute($value)
    {
        return with(new Carbon($value))->format('d/m/Y, H:i');
    }


    public static function rules(){

        return [
            'lang_az' => 'nullable|max:255',
            'lang_en' => 'nullable|max:255',
            'lang_ru' => 'nullable|max:255',
            'youtube_id' => 'required',
            'doctor_id' => 'required',
            'image_original' => 'required',
        ];
    }
}
