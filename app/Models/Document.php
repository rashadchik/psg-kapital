<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Carbon\Carbon;
class Document extends Model
{
    use SoftDeletes;

    protected $table = 'documents';

    protected $guarded = ['id'];
    protected $hidden = ['_token'];
    protected $dates = ['created_at', 'updated_at', 'deleted_at'];

    public static $rules = [
        'category_id' => 'required',
        'document' => 'required',
        'year' => 'nullable',
        'summary' => 'required'
    ];

    public static $messages = [
        'category_id.required' => 'Kateqoriya seçilməyib.',
        //'year.required' => 'İl seçilməyib',
        'summary.required' => 'Məzmun boş saxlanılmamalıdır',
    ];

    public function getCreatedAtAttribute($value)
    {
        return with(new Carbon($value))->format('d/m/Y, H:i');
    }

    public function getUpdatedAtAttribute($value)
    {
        return with(new Carbon($value))->format('d/m/Y, H:i');
    }


}
