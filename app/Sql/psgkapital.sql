/*
 Navicat Premium Data Transfer

 Source Server         : localhost
 Source Server Type    : MySQL
 Source Server Version : 50634
 Source Host           : localhost
 Source Database       : psgkapital

 Target Server Type    : MySQL
 Target Server Version : 50634
 File Encoding         : utf-8

 Date: 11/08/2017 17:20:28 PM
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
--  Table structure for `articles`
-- ----------------------------
DROP TABLE IF EXISTS `articles`;
CREATE TABLE `articles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `category_id` int(10) unsigned NOT NULL,
  `main_category_id` int(10) unsigned DEFAULT NULL,
  `relation_page` int(10) unsigned DEFAULT NULL,
  `title` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(120) COLLATE utf8_unicode_ci NOT NULL,
  `summary` varchar(1500) COLLATE utf8_unicode_ci DEFAULT NULL,
  `content` text COLLATE utf8_unicode_ci,
  `status` tinyint(1) unsigned NOT NULL,
  `featured` tinyint(1) NOT NULL DEFAULT '0',
  `published_by` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `seen_count` int(10) unsigned NOT NULL,
  `published_at` date DEFAULT NULL,
  `youtube_id` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `articles_slug_unique` (`slug`),
  KEY `articles_category_id_index` (`category_id`),
  KEY `articles_main_category_id_index` (`main_category_id`),
  KEY `articles_status_index` (`status`),
  KEY `articles_published_at_index` (`published_at`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
--  Records of `articles`
-- ----------------------------
BEGIN;
INSERT INTO `articles` VALUES ('1', '9', '9', '1', '“Uğura inan, öz biznesinə başla” kitabı UNEC tələbələrinə tanıdılıb', 'ugura-inan-oz-biznesine-basla-kitabi-unec-telebelerine-tanidilib', null, null, '1', '0', 'Rəşad Ağayev', '0', '2017-11-07', null, '2017-11-07 17:04:50', '2017-11-07 17:04:50', null), ('2', '9', '9', '2', '“Uğura inan, öz biznesinə başla” kitabı UNEC tələbələrinə tanıdılıb', 'ugura-inan-oz-biznesine-basla-kitabi-unec-telebelerine-tanidilib_1', null, null, '1', '0', 'Rəşad Ağayev', '0', '2017-10-10', null, '2017-11-07 18:02:34', '2017-11-07 18:07:48', null);
COMMIT;

-- ----------------------------
--  Table structure for `broker_services`
-- ----------------------------
DROP TABLE IF EXISTS `broker_services`;
CREATE TABLE `broker_services` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `category_id` int(10) unsigned NOT NULL,
  `title_az` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `title_en` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `title_ru` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `broker_services_category_id_index` (`category_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
--  Records of `broker_services`
-- ----------------------------
BEGIN;
INSERT INTO `broker_services` VALUES ('2', '5', 'salam', 'hello', '', '2017-11-07 00:25:25', '2017-11-07 00:25:25', null);
COMMIT;

-- ----------------------------
--  Table structure for `broker_tables`
-- ----------------------------
DROP TABLE IF EXISTS `broker_tables`;
CREATE TABLE `broker_tables` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `category_id` int(10) unsigned NOT NULL,
  `corp_loans` varchar(63) COLLATE utf8_unicode_ci NOT NULL,
  `gov_loans` varchar(63) COLLATE utf8_unicode_ci NOT NULL,
  `proportions` decimal(7,3) NOT NULL,
  `notes` decimal(6,4) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `broker_tables_category_id_index` (`category_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
--  Records of `broker_tables`
-- ----------------------------
BEGIN;
INSERT INTO `broker_tables` VALUES ('1', '5', 'Apple', 'Swatch', '10.250', '10.6800', '2017-11-07 00:28:43', '2017-11-07 00:28:43', null);
COMMIT;

-- ----------------------------
--  Table structure for `configs`
-- ----------------------------
DROP TABLE IF EXISTS `configs`;
CREATE TABLE `configs` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `company_name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `contact_phone` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `contact_phone2` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `location` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `contact_form_email` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `hr_form_email` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `customer_form_email` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `site_url` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `logo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `sitemap` timestamp NULL DEFAULT NULL,
  `google_api_key` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `main_video` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `experience_count` int(10) unsigned NOT NULL DEFAULT '0',
  `main_video_cover` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `blog_page` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `lang_id` varchar(2) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'az',
  `brandbook` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
--  Records of `configs`
-- ----------------------------
BEGIN;
INSERT INTO `configs` VALUES ('1', 'PSG Kapital', '+(444)12 222 22 22', null, null, 'rashad@apamark.az', null, null, null, 'psgkapital.az', null, null, null, null, '0', null, '9', 'az', null, '2017-11-02 16:48:18', '2017-11-07 18:38:07');
COMMIT;

-- ----------------------------
--  Table structure for `customers`
-- ----------------------------
DROP TABLE IF EXISTS `customers`;
CREATE TABLE `customers` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `doctor_id` int(10) unsigned NOT NULL,
  `relation_page` int(10) unsigned NOT NULL,
  `full_name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `summary` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `image_original` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `order` tinyint(3) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `customers_doctor_id_index` (`doctor_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
--  Table structure for `dictionaries`
-- ----------------------------
DROP TABLE IF EXISTS `dictionaries`;
CREATE TABLE `dictionaries` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `lang_id` varchar(2) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'az',
  `keyword` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `content` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `type` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `dictionaries_keyword_unique` (`keyword`,`lang_id`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=116 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
--  Records of `dictionaries`
-- ----------------------------
BEGIN;
INSERT INTO `dictionaries` VALUES ('1', 'az', 'about_us', 'a', '0', '2017-10-16 15:25:51', '2017-10-27 00:31:40'), ('2', 'az', 'nothing_found_heading', 'Səhifə mövcud deyil', '0', '2017-10-16 15:25:51', '2017-10-27 00:39:53'), ('3', 'az', 'nothing_found_title', 'TƏƏSSÜF Kİ, AXTARDIĞINIZ SƏHİFƏ MÖVCUD DEYİL.', '0', '2017-10-16 15:25:51', '2017-10-27 00:40:09'), ('4', 'az', 'nothing_found_content', 'Axtardığınız məlumatları səhifəmizin üst hissəsində yerləşən MENYU vasitəsilə əldə edə və ya bizimlə əlaqə saxlaya bilərsiniz.', '0', '2017-10-16 15:25:51', '2017-10-27 00:39:33'), ('5', 'az', 'copyright', 'Bütün hüquqlar qorunur!', '0', '2017-10-16 15:25:51', '2017-10-27 00:29:45'), ('6', 'az', 'full_name', 'Ad, Soyad', '0', '2017-10-16 15:25:51', '2017-10-27 00:39:09'), ('7', 'az', 'subject', 'Mövzu', '0', '2017-10-16 15:25:51', '2017-10-27 00:40:51'), ('8', 'az', 'text', 'Məktub', '0', '2017-10-16 15:25:51', '2017-10-27 00:40:38'), ('9', 'az', 'address', 'Xətai .r, Afiyəddin cəlilov 26', '0', '2017-10-16 15:25:51', '2017-10-27 00:36:56'), ('10', 'az', 'date', 'Tarix', '0', '2017-10-16 15:25:51', '2017-10-27 00:37:54'), ('11', 'az', 'search', 'Axtarış', '0', '2017-10-16 15:25:51', '2017-10-27 00:41:52'), ('12', 'az', 'email_sent', 'Məktubunuz göndərildi, təşəkkür edirik', '0', '2017-10-16 15:25:51', '2017-10-27 00:38:57'), ('13', 'az', 'print', 'Çap üçün', '0', '2017-10-16 15:25:51', '2017-10-27 00:42:47'), ('14', 'az', 'send', 'Göndər', '0', '2017-10-16 15:25:51', '2017-10-27 00:41:36'), ('15', 'az', 'cancel', 'Ləğv et', '0', '2017-10-16 15:25:51', '2017-10-27 00:37:29'), ('16', 'az', 'az', 'Azərbaycan', '1', '2017-10-16 15:25:51', '2017-10-16 15:25:51'), ('17', 'az', 'en', 'English', '1', '2017-10-16 15:25:51', '2017-10-16 15:25:51'), ('18', 'az', 'ru', 'Русский', '1', '2017-10-16 15:25:51', '2017-10-16 15:25:51'), ('19', 'az', 'email', 'E-poçt', '0', '2017-10-16 15:25:51', '2017-10-27 00:38:42'), ('20', 'az', 'phone', 'Əlaqə nömrəsi', '0', '2017-10-16 15:25:51', '2017-10-27 00:40:22'), ('21', 'az', 'published', 'Dərc edildi', '0', '2017-10-16 15:25:51', '2017-10-27 00:42:34'), ('22', 'az', 'read_more', 'Ətraflı', '0', '2017-10-16 15:25:51', '2017-11-08 17:14:52'), ('23', 'az', 'gallery', '<strong>Foto</strong> qalereya', '1', '2017-10-16 15:25:51', '2017-10-16 15:25:51'), ('24', 'az', 'contact_us', 'BİZƏ MÜRACİƏT ET', '1', '2017-10-16 15:25:51', '2017-10-16 15:25:51'), ('25', 'az', 'send_by_email', 'Poçtla göndər', '0', '2017-10-16 15:25:51', '2017-10-27 00:41:17'), ('62', 'en', 'address', null, '0', '2017-10-27 00:36:56', '2017-10-27 00:36:56'), ('63', 'ru', 'address', null, '0', '2017-10-27 00:36:56', '2017-10-27 00:36:56'), ('64', 'tr', 'address', null, '0', '2017-10-27 00:36:56', '2017-10-27 00:36:56'), ('27', 'az', 'video_title', '<span>Reklam</span> filmlərimiz', '1', '2017-10-16 15:25:51', '2017-10-16 15:25:51'), ('28', 'az', 'video_summary', 'İzahlı videonu buradan izləyə bilərsiniz', '1', '2017-10-16 15:25:51', '2017-10-16 15:25:51'), ('29', 'az', 'contact_us_description', 'Aşağıdakı formanı doldurmaqla<br>bizə müraciət edə bilərsiniz', '1', '2017-10-16 15:25:51', '2017-10-16 15:25:51'), ('30', 'az', 'contact_us_summary', '* Biz sizinlə ən tez zamanda əlaqə saxlayacayıq', '1', '2017-10-16 15:25:51', '2017-10-16 15:25:51'), ('31', 'az', 'contact_us_button', 'MÜRACİƏT ET', '1', '2017-10-16 15:25:51', '2017-10-16 15:25:51'), ('32', 'az', 'subscribe', 'ABUNƏ OL', '1', '2017-10-16 15:25:51', '2017-10-16 15:25:51'), ('33', 'az', 'catalog', 'E-KATALOQ', '1', '2017-10-16 15:25:51', '2017-10-16 15:25:51'), ('35', 'az', 'subscribe_text', 'ATENA süd və süd məhsulları bazarına ilkin olaraq 32 növ məhsulla daxil olu', '1', '2017-10-16 15:25:51', '2017-10-16 15:25:51'), ('36', 'az', 'subscribe_title', '<h4>Atena xəbər və gündəlik yeniliklərinə</h4><h3>Abunə ol</h3>', '1', '2017-10-16 15:25:51', '2017-10-16 15:25:51'), ('37', 'az', 'expire_date', 'Saxlanma müddəti', '1', '2017-10-16 15:25:51', '2017-10-16 15:25:51'), ('38', 'az', 'category', 'Kateqoriya', '1', '2017-10-16 15:25:51', '2017-10-16 15:25:51'), ('39', 'az', 'day', 'gün', '1', '2017-10-16 15:25:51', '2017-10-16 15:25:51'), ('40', 'az', 'call_us', 'Bizə zəng et', '1', '2017-10-16 15:25:51', '2017-10-16 15:25:51'), ('41', 'az', 'contact_us_tab', 'Bizimlə əlaqə', '1', '2017-10-16 15:25:51', '2017-10-16 15:25:51'), ('42', 'az', 'customer_service_tab', 'Müştəri Xidmətləri', '1', '2017-10-16 15:25:51', '2017-10-16 15:25:51'), ('43', 'az', 'hr_tab', 'İnsan Resursları', '1', '2017-10-16 15:25:51', '2017-10-16 15:25:51'), ('44', 'az', 'work_hours', '<strong>İş</strong> saatları', '1', '2017-10-16 15:25:51', '2017-10-16 15:25:51'), ('45', 'az', 'subscribe_success', 'Yeniliklərə abunə oldunuz', '1', '2017-10-16 15:25:51', '2017-10-16 15:25:51'), ('46', 'az', 'go_back', 'Geriyə qayıt', '0', '2017-10-16 15:27:08', '2017-10-24 14:30:36'), ('47', 'az', 'search_article', 'Xəbəri ilinə görə axtarın', '1', '2017-10-17 23:35:28', '2017-10-17 23:35:28'), ('49', 'en', 'about_us', 'b', '0', '2017-10-18 15:03:56', '2017-10-27 00:31:40'), ('50', 'ru', 'about_us', 'c', '0', '2017-10-18 15:04:39', '2017-10-27 00:31:40'), ('51', 'az', 'similar_news', 'DİGƏR XƏBƏRLƏR', '0', '2017-10-24 14:26:45', '2017-10-24 14:26:45'), ('52', 'en', 'similar_news', 'DİGƏR XƏBƏRLƏR', '0', '2017-10-24 14:26:45', '2017-10-24 14:26:45'), ('53', 'ru', 'similar_news', 'DİGƏR XƏBƏRLƏR', '0', '2017-10-24 14:26:45', '2017-10-24 14:26:45'), ('54', 'tr', 'similar_news', 'DİGƏR XƏBƏRLƏR', '0', '2017-10-24 14:26:45', '2017-10-24 14:26:45'), ('55', 'en', 'go_back', 'Geriyə qayıt', '0', '2017-10-24 14:30:36', '2017-10-24 14:30:36'), ('56', 'ru', 'go_back', 'Geriyə qayıt', '0', '2017-10-24 14:30:36', '2017-10-24 14:30:36'), ('57', 'tr', 'go_back', 'Geriyə qayıt', '0', '2017-10-24 14:30:36', '2017-10-24 14:30:36'), ('58', 'en', 'copyright', 'All rights reserved!', '0', '2017-10-27 00:29:45', '2017-10-27 00:29:45'), ('59', 'ru', 'copyright', 'Все права защищены', '0', '2017-10-27 00:29:45', '2017-10-27 00:29:45'), ('60', 'tr', 'copyright', 'Bütün hüquqlar qorunur!', '0', '2017-10-27 00:29:45', '2017-10-27 00:29:45'), ('61', 'tr', 'about_us', null, '0', '2017-10-27 00:31:40', '2017-10-27 00:31:40'), ('65', 'en', 'cancel', 'Cancel', '0', '2017-10-27 00:37:29', '2017-10-27 00:37:29'), ('66', 'ru', 'cancel', 'Отмена', '0', '2017-10-27 00:37:29', '2017-10-27 00:37:29'), ('67', 'tr', 'cancel', 'Ləğv et', '0', '2017-10-27 00:37:29', '2017-10-27 00:37:29'), ('68', 'en', 'date', 'Date', '0', '2017-10-27 00:37:54', '2017-10-27 00:37:54'), ('69', 'ru', 'date', 'Дата', '0', '2017-10-27 00:37:54', '2017-10-27 00:37:54'), ('70', 'tr', 'date', 'Tarix', '0', '2017-10-27 00:37:54', '2017-10-27 00:37:54'), ('71', 'en', 'email', 'E-mail', '0', '2017-10-27 00:38:42', '2017-10-27 00:38:42'), ('72', 'ru', 'email', 'Эл. Почта', '0', '2017-10-27 00:38:42', '2017-10-27 00:38:42'), ('73', 'tr', 'email', 'E-poçt', '0', '2017-10-27 00:38:42', '2017-10-27 00:38:42'), ('74', 'en', 'email_sent', 'Məktubunuz göndərildi, təşəkkür edirik', '0', '2017-10-27 00:38:57', '2017-10-27 00:38:57'), ('75', 'ru', 'email_sent', 'Məktubunuz göndərildi, təşəkkür edirik', '0', '2017-10-27 00:38:57', '2017-10-27 00:38:57'), ('76', 'tr', 'email_sent', 'Məktubunuz göndərildi, təşəkkür edirik', '0', '2017-10-27 00:38:57', '2017-10-27 00:38:57'), ('77', 'en', 'full_name', 'Ad, Soyad', '0', '2017-10-27 00:39:09', '2017-10-27 00:39:09'), ('78', 'ru', 'full_name', 'Ad, Soyad', '0', '2017-10-27 00:39:09', '2017-10-27 00:39:09'), ('79', 'tr', 'full_name', 'Ad, Soyad', '0', '2017-10-27 00:39:09', '2017-10-27 00:39:09'), ('80', 'en', 'nothing_found_content', 'Axtardığınız məlumatları səhifəmizin üst hissəsində yerləşən MENYU vasitəsilə əldə edə və ya bizimlə əlaqə saxlaya bilərsiniz.', '0', '2017-10-27 00:39:33', '2017-10-27 00:39:33'), ('81', 'ru', 'nothing_found_content', 'Axtardığınız məlumatları səhifəmizin üst hissəsində yerləşən MENYU vasitəsilə əldə edə və ya bizimlə əlaqə saxlaya bilərsiniz.', '0', '2017-10-27 00:39:33', '2017-10-27 00:39:33'), ('82', 'tr', 'nothing_found_content', 'Axtardığınız məlumatları səhifəmizin üst hissəsində yerləşən MENYU vasitəsilə əldə edə və ya bizimlə əlaqə saxlaya bilərsiniz.', '0', '2017-10-27 00:39:33', '2017-10-27 00:39:33'), ('83', 'en', 'nothing_found_heading', 'Səhifə mövcud deyil', '0', '2017-10-27 00:39:53', '2017-10-27 00:39:53'), ('84', 'ru', 'nothing_found_heading', 'Səhifə mövcud deyil', '0', '2017-10-27 00:39:53', '2017-10-27 00:39:53'), ('85', 'tr', 'nothing_found_heading', 'Səhifə mövcud deyil', '0', '2017-10-27 00:39:53', '2017-10-27 00:39:53'), ('86', 'en', 'nothing_found_title', 'TƏƏSSÜF Kİ, AXTARDIĞINIZ SƏHİFƏ MÖVCUD DEYİL.', '0', '2017-10-27 00:40:09', '2017-10-27 00:40:09'), ('87', 'ru', 'nothing_found_title', 'TƏƏSSÜF Kİ, AXTARDIĞINIZ SƏHİFƏ MÖVCUD DEYİL.', '0', '2017-10-27 00:40:09', '2017-10-27 00:40:09'), ('88', 'tr', 'nothing_found_title', 'TƏƏSSÜF Kİ, AXTARDIĞINIZ SƏHİFƏ MÖVCUD DEYİL.', '0', '2017-10-27 00:40:09', '2017-10-27 00:40:09'), ('89', 'en', 'phone', 'Əlaqə nömrəsi', '0', '2017-10-27 00:40:22', '2017-10-27 00:40:22'), ('90', 'ru', 'phone', 'Əlaqə nömrəsi', '0', '2017-10-27 00:40:22', '2017-10-27 00:40:22'), ('91', 'tr', 'phone', 'Əlaqə nömrəsi', '0', '2017-10-27 00:40:22', '2017-10-27 00:40:22'), ('92', 'en', 'text', 'Məktub', '0', '2017-10-27 00:40:38', '2017-10-27 00:40:38'), ('93', 'ru', 'text', 'Məktub', '0', '2017-10-27 00:40:38', '2017-10-27 00:40:38'), ('94', 'tr', 'text', 'Məktub', '0', '2017-10-27 00:40:38', '2017-10-27 00:40:38'), ('95', 'en', 'subject', 'Mövzu', '0', '2017-10-27 00:40:51', '2017-10-27 00:40:51'), ('96', 'ru', 'subject', 'Mövzu', '0', '2017-10-27 00:40:51', '2017-10-27 00:40:51'), ('97', 'tr', 'subject', 'Mövzu', '0', '2017-10-27 00:40:51', '2017-10-27 00:40:51'), ('98', 'en', 'send_by_email', 'Poçtla göndər', '0', '2017-10-27 00:41:17', '2017-10-27 00:41:17'), ('99', 'ru', 'send_by_email', 'Poçtla göndər', '0', '2017-10-27 00:41:17', '2017-10-27 00:41:17'), ('100', 'tr', 'send_by_email', 'Poçtla göndər', '0', '2017-10-27 00:41:17', '2017-10-27 00:41:17'), ('101', 'en', 'send', 'Göndər', '0', '2017-10-27 00:41:36', '2017-10-27 00:41:36'), ('102', 'ru', 'send', 'Göndər', '0', '2017-10-27 00:41:36', '2017-10-27 00:41:36'), ('103', 'tr', 'send', 'Göndər', '0', '2017-10-27 00:41:36', '2017-10-27 00:41:36'), ('104', 'en', 'search', 'Axtarış', '0', '2017-10-27 00:41:52', '2017-10-27 00:41:52'), ('105', 'ru', 'search', 'Axtarış', '0', '2017-10-27 00:41:52', '2017-10-27 00:41:52'), ('106', 'tr', 'search', 'Axtarış', '0', '2017-10-27 00:41:52', '2017-10-27 00:41:52'), ('107', 'en', 'published', 'Dərc edildi', '0', '2017-10-27 00:42:34', '2017-10-27 00:42:34'), ('108', 'ru', 'published', 'Dərc edildi', '0', '2017-10-27 00:42:34', '2017-10-27 00:42:34'), ('109', 'tr', 'published', 'Dərc edildi', '0', '2017-10-27 00:42:34', '2017-10-27 00:42:34'), ('110', 'en', 'print', 'Çap üçün', '0', '2017-10-27 00:42:47', '2017-10-27 00:42:47'), ('111', 'ru', 'print', 'Çap üçün', '0', '2017-10-27 00:42:47', '2017-10-27 00:42:47'), ('112', 'tr', 'print', 'Çap üçün', '0', '2017-10-27 00:42:47', '2017-10-27 00:42:47'), ('113', 'en', 'read_more', 'Ətraflı', '0', '2017-10-27 00:43:05', '2017-11-08 17:14:52'), ('114', 'ru', 'read_more', 'Daha ətraflı', '0', '2017-10-27 00:43:05', '2017-10-27 00:43:05'), ('115', 'tr', 'read_more', 'Daha ətraflı', '0', '2017-10-27 00:43:05', '2017-10-27 00:43:05');
COMMIT;

-- ----------------------------
--  Table structure for `documents`
-- ----------------------------
DROP TABLE IF EXISTS `documents`;
CREATE TABLE `documents` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `category_id` int(10) unsigned NOT NULL,
  `document` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `documents_category_id_index` (`category_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
--  Table structure for `galleries`
-- ----------------------------
DROP TABLE IF EXISTS `galleries`;
CREATE TABLE `galleries` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `category_id` int(10) unsigned NOT NULL,
  `type` tinyint(3) unsigned NOT NULL,
  `filename` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `original_filename` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `galleries_category_id_index` (`category_id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
--  Records of `galleries`
-- ----------------------------
BEGIN;
INSERT INTO `galleries` VALUES ('1', '4', '4', '1c5662154722acdc9f7d09429602f4cf.png', 'textcover.png', '2017-11-02 16:53:52'), ('3', '1', '6', 'b942896563c211a022da0c9fc65e364a.png', 'news1.png', '2017-11-07 17:21:29'), ('4', '2', '6', '2204c24ba754f51313d3c580c67cc175.png', 'news2.png', '2017-11-07 18:02:43'), ('5', '1', '1', '2484f9a01f7af0535f58394e85b8cf00.png', 'news2.png', '2017-11-07 18:21:07'), ('6', '1', '1', '69a60814c0921246ded865d3bb2ed967.png', 'news3.png', '2017-11-07 18:21:08'), ('7', '1', '1', '6d3ff5fbbe2601f67b276cf963f5ab77.png', 'news1.png', '2017-11-07 18:21:08'), ('9', '5', '4', '12dcf5e0d6e67d07d24689a97a8c223b.png', 'cover-about.png', '2017-11-08 11:00:56'), ('11', '11', '4', '9c761b29d04d857e3056840c545bc237.png', 'cover1.png', '2017-11-08 11:08:43');
COMMIT;

-- ----------------------------
--  Table structure for `kivs`
-- ----------------------------
DROP TABLE IF EXISTS `kivs`;
CREATE TABLE `kivs` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `image_original` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `lang_az` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `lang_en` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `lang_ru` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `order` int(10) unsigned NOT NULL DEFAULT '1',
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
--  Table structure for `legal_physicals`
-- ----------------------------
DROP TABLE IF EXISTS `legal_physicals`;
CREATE TABLE `legal_physicals` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `category_id` int(10) unsigned NOT NULL,
  `type` tinyint(1) NOT NULL DEFAULT '1',
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `legal_physicals_category_id_index` (`category_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
--  Records of `legal_physicals`
-- ----------------------------
BEGIN;
INSERT INTO `legal_physicals` VALUES ('1', '4', '1', 'İdarə Heyətinin Sədrinin təyin olunması haqqında əmri (təsdiqli)', '2017-11-03 14:56:19', '2017-11-03 14:59:32', null), ('2', '4', '2', 'cEYHUN', '2017-11-03 16:07:25', '2017-11-03 16:07:25', null), ('3', '5', '1', 'Murad Kompas', '2017-11-03 17:47:57', '2017-11-03 17:47:57', null), ('4', '5', '2', 'Rəşad Ağayev', '2017-11-03 17:48:10', '2017-11-03 17:48:10', null);
COMMIT;

-- ----------------------------
--  Table structure for `media`
-- ----------------------------
DROP TABLE IF EXISTS `media`;
CREATE TABLE `media` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `doctor_id` int(10) unsigned NOT NULL,
  `lang_az` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `lang_en` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `lang_ru` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `youtube_id` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `image_original` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `order` tinyint(3) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `media_doctor_id_index` (`doctor_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
--  Table structure for `migrations`
-- ----------------------------
DROP TABLE IF EXISTS `migrations`;
CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=56 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
--  Records of `migrations`
-- ----------------------------
BEGIN;
INSERT INTO `migrations` VALUES ('22', '2014_10_12_000000_create_users_table', '1'), ('23', '2014_10_12_100000_create_password_resets_table', '1'), ('24', '2016_08_16_194021_create_pages_table', '1'), ('25', '2016_08_16_194022_create_articles_table', '1'), ('26', '2016_12_29_050641_create_galleries_table', '1'), ('27', '2017_04_16_145020_create_configs_table', '1'), ('28', '2017_04_16_170540_create_socials_table', '1'), ('29', '2017_04_17_111544_create_sliders_table', '1'), ('30', '2017_04_19_093941_create_subscribers_table', '1'), ('31', '2017_04_29_001453_create_dictionaries_table', '1'), ('32', '2017_05_01_104923_create_kivs_table', '1'), ('33', '2017_07_11_150517_create_videos_table', '1'), ('34', '2017_08_03_101223_create_media_table', '1'), ('35', '2017_08_07_171846_create_customers_table', '1'), ('36', '2017_08_09_174951_create_documents_table', '1'), ('37', '2017_09_04_203646_create_uploads_table', '1'), ('38', '2017_09_12_162404_create_vacancies_table', '1'), ('39', '2017_10_23_160432_create_portfolios_table', '1'), ('40', '2017_11_02_155843_create_service_stats_table', '1'), ('42', '2017_11_03_112017_create_legal_physicals_table', '2'), ('53', '2017_11_06_115422_create_broker_services_table', '3'), ('54', '2017_11_06_115422_create_broker_tables_table', '3'), ('55', '2017_11_08_142056_create_vacancy_details_table', '4');
COMMIT;

-- ----------------------------
--  Table structure for `pages`
-- ----------------------------
DROP TABLE IF EXISTS `pages`;
CREATE TABLE `pages` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `template_id` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `lang_id` varchar(2) COLLATE utf8_unicode_ci NOT NULL,
  `order` int(10) unsigned NOT NULL DEFAULT '1',
  `relation_page` int(10) unsigned DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `has_slider` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `has_partner` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `parent_id` int(10) unsigned DEFAULT NULL,
  `main_parent_id` int(10) unsigned DEFAULT NULL,
  `image_original` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `featured` tinyint(1) NOT NULL DEFAULT '0',
  `icon` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `visible` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `third_level` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `summary` varchar(1500) COLLATE utf8_unicode_ci DEFAULT NULL,
  `content` text COLLATE utf8_unicode_ci,
  `meta_description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `meta_keywords` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `target` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `forward_url` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `youtube_id` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `pages_slug_lang_id_unique` (`slug`,`lang_id`)
) ENGINE=InnoDB AUTO_INCREMENT=37 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
--  Records of `pages`
-- ----------------------------
BEGIN;
INSERT INTO `pages` VALUES ('1', '0', 'az', '1', '1', 'Ana səhifə', 'index', null, '0', '0', null, null, null, '0', null, '0', '0', null, null, null, null, '1', null, null, '2017-11-02 16:48:18', '2017-11-02 16:48:18', null), ('2', '0', 'az', '1', '2', 'Axtarışın nəticələri', 'search', null, '0', '0', null, null, null, '0', null, '0', '0', null, null, null, null, '1', null, null, '2017-11-02 16:48:18', '2017-11-02 16:48:18', null), ('3', '3', 'az', '1', '3', 'İnvestisiya xidmətləri', 'investisiya-xidmetleri', null, '0', '0', null, null, null, '0', null, '1', '0', null, null, null, null, '1', null, null, '2017-11-02 16:49:03', '2017-11-02 16:49:03', null), ('4', '4', 'az', '9', '4', 'Analysis & Consulting', 'analysis-consulting', null, '0', '0', '3', '3', '5ffbbcc863f277a01c05a51214851552.png', '0', null, '1', '1', '<h1><strong>M&uuml;əyyən edilmiş aktı i</strong>nvestisiya edir</h1>\r\n\r\n<p>Dəqiq quruluşa malik olmayan bu mətn latincada hələ XVI<br />\r\nəsrdə &ouml;z qəlib formasını almışdır.</p>', '<p><strong>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</strong></p>\r\n\r\n<p>It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>', null, null, '1', null, null, '2017-11-02 16:50:32', '2017-11-07 14:21:27', null), ('5', '6', 'az', '5', '5', 'Broker', 'broker', null, '0', '0', '3', '3', null, '0', null, '1', '1', null, null, null, null, '1', null, null, '2017-11-02 18:21:42', '2017-11-03 17:07:29', null), ('6', '3', 'en', '1', '3', 'Investition', 'investition', null, '0', '0', null, null, null, '0', null, '1', '0', null, null, null, null, '1', null, null, '2017-11-06 16:16:04', '2017-11-06 16:16:04', null), ('7', '6', 'en', '11', '5', 'Brochers', 'brochers', null, '0', '0', '6', '6', null, '0', null, '1', '1', null, null, null, null, '1', null, null, '2017-11-06 16:16:20', '2017-11-07 14:52:40', null), ('8', '7', 'az', '8', '8', 'Market Meyker', 'market-meyker', null, '0', '0', '3', '3', null, '0', null, '1', '1', null, null, null, null, '1', null, null, '2017-11-07 11:53:03', '2017-11-07 11:53:03', null), ('9', '2', 'az', '1', '9', 'Xəbərlər', 'xeberler', null, '0', '0', null, null, null, '0', null, '1', '0', 'Lorem İpsum &ndash; dizayn nəşrlərində istifadə olunan Dəqiq quruluşa malik olmayan bu mətn latincada<br />\r\nhələ XVI əsrdə &ouml;z qəlib formasını almışdır <strong>24 saat ərzində xidmət və dəstək.</strong>', null, null, null, '1', null, null, '2017-11-07 15:22:33', '2017-11-08 00:22:12', null), ('10', '0', 'az', '1', '10', 'Haqqımızda', 'haqqimizda', null, '0', '0', null, null, null, '0', null, '1', '0', null, null, null, null, '1', null, null, '2017-11-07 22:29:36', '2017-11-08 00:21:47', null), ('11', '1', 'az', '15', '11', 'PSG Kapital Haqqında', 'psg-kapital-haqqinda', null, '0', '0', '10', '10', null, '0', null, '1', '1', null, '<h3>&quot;PSG-Kapital İnvestisiya Şirkəti&quot; QSC 2012-ci ildə qurulmuşdur. Nizamnamə kapitalı 1 000 000 AZN-dir.</h3>\r\n\r\n<p>2014-c&uuml; ildə SOCAR şirkətlərindən biri olan &quot;Cənub Qaz Dəhlizi&quot; QSC-nin 101.12 milyon dollar həcmində istiqraz buraxılışında anderrayter kimi &ccedil;ıxış etmişdir.</p>\r\n\r\n<blockquote>2016-cı ildə &quot;PSG-Kapital İnvestisiya Şirkəti&quot; QSC SOCAR-ın 100 milyon dollar həcmində istiqraz yerləşdirməsində anderrayter qismində &ccedil;ıxış etmiş və hal-hazırda bu istiqrazların Bakı Fond Birjasında market meykerliyini həyata ke&ccedil;irir.</blockquote>\r\n\r\n<p>Şirkət Azərbaycanda İnvestisiya Xidmətləri və Portfelin İdarə edilməsi &uuml;zrə fəaliyyət g&ouml;stərir. Şirkətimiz fiziki və h&uuml;quqi şəxslərə geniş maliyyə xidmətləri təklif edir.</p>', null, null, '1', null, null, '2017-11-07 22:56:31', '2017-11-07 23:25:09', null), ('12', '0', 'az', '17', '12', 'Baxışımız', 'baxisimiz', null, '0', '0', '11', '10', null, '0', null, '1', '2', '<ul>\r\n	<li>Şirkətimizin m&uuml;ştəriləri arasında şəffaflığı təmin etməklə onların etibarını qazanmaq və şirkətə olan sadiqliklərini təmin etmək;</li>\r\n	<li>M&uuml;ştərilərlə m&uuml;ştərək iş birliyimizin uzunm&uuml;ddətli olması &uuml;&ccedil;&uuml;n davamlı araşdırmalar edir, yeni texnologiyalara yatırım edirik.</li>\r\n</ul>', null, null, null, '1', null, null, '2017-11-08 00:24:56', '2017-11-08 00:25:22', null), ('13', '11', 'az', '19', '13', 'Prinsiplərimiz', 'prinsiplerimiz', null, '0', '0', '11', '10', null, '0', null, '1', '2', 'Azərbaycanda se&ccedil;ilmiş investisiya şirkəti kimi sayılıb, m&uuml;ştərilərimizə d&uuml;nya səviyyəli investisiya təcr&uuml;bəsini m&uuml;kəmməl m&uuml;ştəri y&ouml;n&uuml;ml&uuml; xidmətlərlə təklif etmək, peşekar, məsuliyyətli və şəffaf şəkildə fəaliyyət g&ouml;stərməkdir.', null, null, null, '1', null, null, '2017-11-08 00:30:21', '2017-11-08 01:02:30', null), ('14', '11', 'az', '20', '14', 'sadasdasdasda', 'sadasdasdasda', null, '0', '0', '11', '10', null, '0', null, '1', '2', '<h1><strong>Lorem </strong>ipsum</h1>\r\n\r\n<p>Dəqiq quruluşa malik olmayan bu mətn latincada hələ XVI<br />\r\nəsrdə &ouml;z qəlib formasını almışdır.</p>', null, null, null, '1', null, null, '2017-11-08 01:06:58', '2017-11-08 01:06:58', null), ('15', '8', 'az', '1', '15', 'Vakansiyalar', 'vakansiyalar', null, '0', '0', null, null, null, '0', null, '1', '0', '<h1><strong>Lorem </strong>ipsum</h1>\r\n\r\n<p>Dəqiq quruluşa malik olmayan bu mətn latincada hələ XVI<br />\r\nəsrdə &ouml;z qəlib formasını almışdır.</p>', null, null, null, '1', null, null, '2017-11-08 12:45:21', '2017-11-08 12:45:21', null), ('23', '8', 'en', '1', '15', 'Vacancies', 'vacancies', null, '0', '0', null, null, null, '0', null, '1', '0', '<h1><strong>Lorem </strong>ipsum</h1>\r\n\r\n<p>Dəqiq quruluşa malik olmayan bu mətn latincada hələ XVI<br />\r\nəsrdə &ouml;z qəlib formasını almışdır.</p>', null, null, null, '1', null, null, '2017-11-08 15:29:53', '2017-11-08 15:30:19', null), ('35', '9', 'az', '24', '35', 'Maliyye', 'maliyye', '3-6 illik', '0', '0', '15', '15', null, '0', null, '1', '1', '<h1><strong>Lorem </strong>ipsum</h1>\r\n\r\n<p>Dəqiq quruluşa malik olmayan bu mətn latincada hələ XVI<br />\r\nəsrdə &ouml;z qəlib formasını almışdır.</p>', null, null, null, '1', null, null, '2017-11-08 16:15:09', '2017-11-08 17:01:15', null), ('36', '9', 'en', '23', '35', 'Finance', 'finance', '3-5 years', '0', '0', '23', '23', null, '0', null, '1', '1', '<h1><strong>Lorem </strong>ipsum</h1>\r\n\r\n<p>Dəqiq quruluşa malik olmayan bu mətn latincada hələ XVI<br />\r\nəsrdə &ouml;z qəlib formasını almışdır.</p>', null, null, null, '1', null, null, '2017-11-08 16:16:02', '2017-11-08 16:16:02', null);
COMMIT;

-- ----------------------------
--  Table structure for `password_resets`
-- ----------------------------
DROP TABLE IF EXISTS `password_resets`;
CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`),
  KEY `password_resets_token_index` (`token`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
--  Table structure for `portfolios`
-- ----------------------------
DROP TABLE IF EXISTS `portfolios`;
CREATE TABLE `portfolios` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `doctor_id` int(10) unsigned NOT NULL,
  `image_original` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `image_second` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `order` tinyint(3) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `portfolios_doctor_id_index` (`doctor_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
--  Table structure for `service_stats`
-- ----------------------------
DROP TABLE IF EXISTS `service_stats`;
CREATE TABLE `service_stats` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `category_id` int(10) unsigned NOT NULL,
  `customers` int(10) unsigned NOT NULL DEFAULT '0',
  `transactions` int(10) unsigned NOT NULL DEFAULT '0',
  `loans` int(10) unsigned NOT NULL DEFAULT '0',
  `proportions` int(10) unsigned NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `service_stats_category_id_index` (`category_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
--  Records of `service_stats`
-- ----------------------------
BEGIN;
INSERT INTO `service_stats` VALUES ('2', '5', '1000', '250', '0', '0', '2017-11-02 18:21:56', '2017-11-03 17:42:04', null);
COMMIT;

-- ----------------------------
--  Table structure for `sliders`
-- ----------------------------
DROP TABLE IF EXISTS `sliders`;
CREATE TABLE `sliders` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `image_original` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `summary` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `link` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `type` tinyint(1) NOT NULL DEFAULT '1',
  `order` tinyint(3) unsigned NOT NULL,
  `lang_id` varchar(2) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
--  Table structure for `socials`
-- ----------------------------
DROP TABLE IF EXISTS `socials`;
CREATE TABLE `socials` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `type` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `forward_url` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
--  Table structure for `subscribers`
-- ----------------------------
DROP TABLE IF EXISTS `subscribers`;
CREATE TABLE `subscribers` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `full_name` varchar(40) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `status` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `subscribers_email_unique` (`email`),
  KEY `subscribers_created_at_index` (`created_at`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
--  Table structure for `uploads`
-- ----------------------------
DROP TABLE IF EXISTS `uploads`;
CREATE TABLE `uploads` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `original_filename` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `filename` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `mime` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(16) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
--  Table structure for `users`
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `first_name` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  `last_name` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `role` tinyint(1) NOT NULL DEFAULT '2',
  `verified` tinyint(1) NOT NULL DEFAULT '0',
  `confirmation_code` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `api_token` varchar(60) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
--  Records of `users`
-- ----------------------------
BEGIN;
INSERT INTO `users` VALUES ('1', 'Rəşad', 'Ağayev', 'rashad@apamark.az', '$2y$10$pA5pm2vYMhxKt59v54S.Xe2fDWUACT16630RogpjWWmFETbBQYNFm', '1', '1', null, null, 'HzO3pQXsnwbLp3zUNITxGORKquspbhiLYAOD8Q7GPtGmH2EYKnHdo1TFC6V5', '2017-11-02 16:48:17', '2017-11-02 16:48:17', null), ('2', 'Huseyn', 'Huseynli', 'huseyn.h@code.edu.az', '$2y$10$lRSqbSbYRTiX.zxduYj5H.l8vH2jH46Iykpjey5A/nTt.2HWvMVdm', '1', '1', null, null, 'yVYoXPOZrfyrIeYqIDWMGRoAiNWSK79zm4jMCk5JO8BrUtoKJK1cOHWZNrRv', '2017-11-02 16:48:17', '2017-11-02 16:48:17', null), ('3', 'Emil', 'Alizade', 'emil@apamark.az', '$2y$10$cttQ8qgvkJ7t37ocM3spj.IYJ7xVTKmXjiXfdCsAs40TjxjsgIrIm', '1', '1', null, null, 'loctt6oQnle4pRJnWRpHwIHggaslA7nI0ZVhUHRNhGRRtvDHjBmUc90RmR9H', '2017-11-02 16:48:18', '2017-11-02 16:48:18', null);
COMMIT;

-- ----------------------------
--  Table structure for `vacancies`
-- ----------------------------
DROP TABLE IF EXISTS `vacancies`;
CREATE TABLE `vacancies` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `lang_id` varchar(2) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `summary` varchar(1500) COLLATE utf8_unicode_ci DEFAULT NULL,
  `published_at` date NOT NULL,
  `end_date` date NOT NULL,
  `order` int(10) unsigned NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
--  Table structure for `vacancy_details`
-- ----------------------------
DROP TABLE IF EXISTS `vacancy_details`;
CREATE TABLE `vacancy_details` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `vacancy_id` int(10) unsigned NOT NULL,
  `salary` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `end_date` date NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `vacancy_details_vacancy_id_unique` (`vacancy_id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
--  Records of `vacancy_details`
-- ----------------------------
BEGIN;
INSERT INTO `vacancy_details` VALUES ('4', '22', '500 - 1000 AZN', '2017-11-30', '2017-11-08 14:54:23', '2017-11-08 15:24:46'), ('5', '30', '500 - 1000 AZN', '2017-11-30', '2017-11-08 15:53:03', '2017-11-08 15:53:03'), ('6', '34', '500 - 1000 AZN', '2017-11-30', '2017-11-08 16:07:30', '2017-11-08 16:07:30'), ('7', '35', '500 - 1000 AZN', '2017-11-30', '2017-11-08 16:15:09', '2017-11-08 16:15:09');
COMMIT;

-- ----------------------------
--  Table structure for `videos`
-- ----------------------------
DROP TABLE IF EXISTS `videos`;
CREATE TABLE `videos` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `video` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `summary` varchar(1000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `link` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `order` tinyint(3) unsigned NOT NULL,
  `lang_id` varchar(2) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

SET FOREIGN_KEY_CHECKS = 1;
