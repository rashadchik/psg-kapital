<?php

namespace App\Http\Controllers;

use App\Models\Banner;
use Illuminate\Http\Request;
use App\DataTables\BannerDataTable;
use App\Logic\Input;
use App\Models\Page;
use App\Logic\MultiLanguageSelect;
use DB;
use Route;

class BannerController extends Controller
{
    protected $input, $requests;
    public $title, $route, $view;

    public function __construct(Request $request, Input $inputs)
    {
        $this->middleware('admin');

        $this->input = config('config.inputs.'.Route::currentRouteName());
        $this->requests = $request->except('_token', '_method');

        $this->title = "Bannerlər";
        $this->route = "banner";
        $this->view = "banner";

        view()->share('suitableInputs', $this->input);
        view()->share('allInputs', $inputs->allInputs());
        view()->share('title', $this->title);
        view()->share('route', $this->route);
    }

    public function index(BannerDataTable $dataTable)
    {
        $breadcrumb = $this->breadcrumb();
        $breadcrumb->addCrumb($this->title, route($this->route.'.index'));

        return $dataTable->render("app.$this->view.index", ["breadcrumb" => $breadcrumb->render()] );
    }

    public function trashed(BannerDataTable $dataTable)
    {
        $breadcrumb = $this->breadcrumb();
        $breadcrumb->addCrumb($this->title, route($this->route.'.index'));
        $breadcrumb->addCrumb(trans('locale.trash'));

        return $dataTable->trashed(true)->render("app.$this->view.index", ["breadcrumb" => $breadcrumb->render()] );
    }

    public function create()
    {
        $breadcrumb = $this->breadcrumb();
        $breadcrumb->addCrumb($this->title, route($this->route.'.index'));
        $breadcrumb->addCrumb(trans('locale.create'));

        $querySub = Page::whereIn('template_id', [17,18])
            ->where('third_level',2)
            ->select("id", "name", 'lang_id');

        $query = Page::whereIn('template_id', [5,6,7,4,19,23,24,25])
            ->where('third_level', 1)
            ->select("id", "name", 'lang_id')
            ->union($querySub)
            ->orderBy("id", "asc")
            ->get();


        $category = MultiLanguageSelect::multiLang($query);

        return view("app.$this->view.create", ["breadcrumb" => $breadcrumb->render(), "services" => $category]);
    }


    public function store(Request $request)
    {
        $this->requests['popup'] = $request->input('popup', 0);

        $request->validate(Banner::$rules, Banner::$messages);

        try{
            Banner::create($this->requests);
        }
        catch(\Exception $e){
            return redirect()->back()->withInput($request->input())->withErrors(["errors" => trans('locale.fail', ['number' => 1])]);
        }


        $request->session()->flash('message', "Əməliyyat uğurlu alındı");

        return redirect()->route($this->route.'.index');
    }


    public function edit($id)
    {
        $info = Banner::findOrFail($id);

        $querySub = Page::whereIn('template_id', [17,18])
            ->where('third_level',2)
            ->select("id", "name", 'lang_id');

        $query = Page::whereIn('template_id', [5,6,7,4,19,23,24,25])
            ->where('third_level', 1)
            ->select("id", "name", 'lang_id')
            ->union($querySub)
            ->orderBy("id", "asc")
            ->get();

        $category = MultiLanguageSelect::multiLang($query);


        $breadcrumb = $this->breadcrumb();
        $breadcrumb->addCrumb($this->title, route($this->route.'.index'));
        $breadcrumb->addCrumb(trans('locale.edit'));

        return view("app.$this->view.edit", ["info" => $info, "breadcrumb" => $breadcrumb->render(), 'category' => $category]);
    }


    public function update(Request $request, $id)
    {
        $this->requests['popup'] = $request->input('popup', 0);

        $find = Banner::findOrFail($id);


        $request->validate(Banner::$rules, Banner::$messages);

        try{
            foreach($this->requests as $key => $input){
                $find -> $key = $input;
            }

            $find -> save();
        }
        catch(\Exception $e){
            return redirect()->back()->withInput($request->input())->withErrors(["errors" => trans('locale.fail', ['number' => 1])]);
        }


        $request->session()->flash('message', trans('locale.update_success'));

        return redirect()->route($this->route.'.index');
    }

      public function trash($id)
    {
        $article = Banner::findOrFail($id);
        $article->delete();

        request()->session()->flash('message', "Məlumat silindi, qaytarılması mümkündür.");
        return redirect()->back();
    }


    public function restore($id)
    {
        $article = Banner::onlyTrashed()->findOrFail($id);
        $article->restore();

        request()->session()->flash('message', "Məlumat qaytarıldı");
        return redirect()->route($this->route.'.index');
    }


    public function destroy($id)
    {
        $info = Banner::onlyTrashed()->findOrFail($id);

        try{
            $info->forceDelete();
        }
        catch(\Exception $e){
            return redirect()->back()->withErrors(["errors" => trans('locale.fail', ['number' => 2])]);
        }


        request()->session()->flash('message', "Məlumat birdəfəlik silindi");
        return redirect()->back();
    }

}