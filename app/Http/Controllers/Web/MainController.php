<?php

namespace App\Http\Controllers\Web;

use App\Logic\Exchange;
use App\Logic\WebCache;
use App\Models\Banner;
use App\Models\Loan;
use App\Models\MarketChart;
use App\Models\MarketLoan;
use App\Models\Question;
use App\Models\Rekvisit;
use App\Models\Reproduction;
use App\Models\ReproductionClient;
use App\Models\Stock;
use App\Models\Strategy;
use App\Models\StrategyChart;
use App\Http\Controllers\Controller;
use App\Logic\DeepUrl;
use App\Logic\Menu;
use App\Models\Config;
use App\Models\Article;
use App\Models\Page;
use DB;
use Mcamara\LaravelLocalization\Facades\LaravelLocalization;
use App\Models\StrategyChart2;
class MainController extends Controller
{
    protected $lang, $deepUrl, $menu, $dictionary;

    public $config;

    public function __construct(DeepUrl $deepUrl, WebCache $webCache)
    {

        $this->middleware('web');

        $this->lang = LaravelLocalization::getCurrentLocale();
        $this->deepUrl = $deepUrl;
        $this->config = $webCache->getConfig();
        $this->dictionary = $webCache->getDictionary($this->lang);

        if(!request()->ajax())
        {
            $menu = Menu::getPages();

            $social = $webCache->getSocial();
            $currency = $webCache->getCurrency();
            $slider = $webCache->getSlider($this->lang);
            $partners = $webCache->getPartner($this->lang);


            view()->share('slider', $slider);
            view()->share('partners', $partners);
            view()->share('currency', $currency);
            view()->share('menu', $menu);
            view()->share('menuNum', 0);

            view()->share('social', $social);
            view()->share('webConfig', $this->config);

        }

        view()->share('dictionary', $this->dictionary);
        view()->share('lang', $this->lang);


    }


    public function showPage($category)
    {

        if($category == 'index')
        {
            return $this->index();
        }

        $result = $this->checkRoute($category);

        if($result == false)
        {
            return $this->error404();
        }


        $getPage = $result[0];

        $getParentPage = $result[1];


        if($getPage->slug == 'search')
        {
            return $this->search($getPage);
        }


        if(!is_null($getPage->forward_url))
        {
            return redirect()->to($getPage->forward_url);
        }


        if(is_null($getPage->parent_id) && $getPage->template_id != 8 && $getPage->template_id != 14)
        {
            $child = Page::where('parent_id', $getPage->id)->where('visible', 1)->ordered()->first();

            if($child)
            {
                $getPage = $child;
            }
        }


        if($getPage->template_id == 0){
            return $this->staticPage($getPage, $getParentPage);
        }
        if($getPage->template_id == 1){
            return $this->staticPage($getPage, $getParentPage);
        }
        elseif($getPage->template_id == 2){
            return $this->news($getPage, $getParentPage);
        }
        elseif($getPage->template_id == 3){
            return $this->staticPage($getPage, $getParentPage);
        }
        elseif($getPage->template_id == 4){
            return $this->service($getPage, $getParentPage, 'analysis');
        }
        elseif($getPage->template_id == 5){
            return $this->service($getPage, $getParentPage, 'underwriting');
        }
        elseif($getPage->template_id == 6){
            return $this->service($getPage, $getParentPage, 'broker');
        }
        elseif($getPage->template_id == 7){
            return $this->service($getPage, $getParentPage, 'market');
        }
        elseif($getPage->template_id == 8){
            return $this->vacancy($getPage, $getParentPage);
        }
        elseif($getPage->template_id == 9){
            return $this->vacancySingle($getPage, $getParentPage);
        }
        elseif($getPage->template_id == 10){
            return $this->licenses($getPage, $getParentPage);
        }
        elseif($getPage->template_id == 11){
            return $this->block($getParentPage);
        }
        elseif($getPage->template_id == 12){
            return $this->reports($getPage,$getParentPage);
        }
        elseif($getPage->template_id == 13){
            return $this->partnerSingle($getPage, $getParentPage);
        }
        elseif($getPage->template_id == 14){
            return $this->contact($getPage, $getParentPage);
        }
        elseif($getPage->template_id == 15){
            return $this->laws($getPage, $getParentPage);
        }
        elseif($getPage->template_id == 16){
            return $this->staticPage($getPage, $getParentPage);
        }
        elseif($getPage->template_id == 17){
            return $this->service($getPage, $getParentPage, 'portfolio' );
        }
        elseif($getPage->template_id == 27){
            return $this->service($getPage, $getParentPage, 'portfolio2');
        }
        elseif($getPage->template_id == 18){
            return $this->service($getPage, $getParentPage, 'fond'); //kapital muhafizeli fondlar
        }
        elseif($getPage->template_id == 19){
            return $this->service($getPage, $getParentPage, 'yanashma');
        }
        elseif($getPage->template_id == 20){
            return $this->plans($getPage, $getParentPage);
        }
        elseif($getPage->template_id == 21){
            return $this->staticPage($getPage, $getParentPage, 'director');
        }
        elseif($getPage->template_id == 22){
            return $this->investStrategy($getPage, $getParentPage);
        }
        elseif($getPage->template_id == 23){
            return $this->loans($getPage, $getParentPage);
        }
        elseif($getPage->template_id == 24){
            return $this->stocks($getPage, $getParentPage);
        }
        elseif($getPage->template_id == 25){
            return $this->reproduction($getPage, $getParentPage);
        }
        elseif($getPage->template_id == 26){
            return $this->risk($getPage, $getParentPage);
        }
        else{
            return $this->error404();
        }

    }


    public function showArticle($category, $slug)
    {
        $result = $this->checkRoute($category);

        if($result == false)
        {
            return $this->error404();
        }

        $getPage = $result[0];

        $getParentPage = $result[1];


        if($getPage->template_id == 2){
            return $this->articleSingle($getPage, $getParentPage, $slug);
        }
        else{
            return $this->error404();

        }

    }



    protected function checkRoute($category)
    {
        $getPage = Page::where('slug', $category)->where('lang_id', $this->lang)->first();

        /*$getPage = Page::leftJoin('galleries', function ($join) {
            $join->on('pages.relation_page', '=', 'galleries.category_id')->where('galleries.type', 4);
        })
            ->where('pages.slug', $category)
            ->where('pages.lang_id', $this->lang)
            ->select('pages.*', 'galleries.filename as image_cover')
            ->first();*/


        if(!$getPage)
        {
            return false;
        }

        if(!is_null($getPage->parent_id)){
            $getParentPage = Page::where('id', $getPage->main_parent_id)->first();
        }
        else{
            $getParentPage = $getPage;
        }

        if(!$getParentPage)
        {
            return false;
        }

        return [$getPage, $getParentPage];
    }



    public function index()
    {
        $getPage = Page::where('slug', 'index')->where('lang_id', $this->lang)->first();

        if(!$getPage)
        {
            return $this->error404();
        }

        $exchange = new Exchange;

        $exchangeEuroRate = $exchange->get(1, 'exchangeEuroRate', 'live', 'EUR');

        $exchangeGbpRate = $exchange->get(3, 'exchangeGbpRate', 'live', 'GBP');

        $exchangeRate = $exchange->get(2, 'exchangeRate', 'live', 'USD');

        $exchangeXauRate = $exchange->get(4, 'exchangeXauRate', 'live', 'XAU');

        $exchangeXagRate = $exchange->get(5, 'exchangeXagRate', 'live', 'XAG');

        $exchangeBtcRate = $exchange->get(6, 'exchangeBtcRate', 'live', 'BTC');



        $exchangeEuroHistoricalRate = $exchange->get(1, 'exchangeEuroHistoricalRate', 'historical', 'EUR');

        $exchangeGbpHistoricalRate = $exchange->get(3, 'exchangeGbpHistoricalRate', 'historical', 'GBP');

        $exchangeHistoricalRate = $exchange->get(2, 'exchangeHistoricalRate', 'historical', 'USD');

        $exchangeXauHistoricalRate = $exchange->get(4, 'exchangeXauHistoricalRate', 'historical', 'XAU');

        $exchangeXagHistoricalRate = $exchange->get(5, 'exchangeXagHistoricalRate', 'historical', 'XAG');

        $exchangeBtcHistoricalRate = $exchange->get(6, 'exchangeBtcHistoricalRate', 'historical', 'BTC');



        $exchanges = array_merge($exchangeEuroRate, $exchangeGbpRate, $exchangeRate, $exchangeXauRate, $exchangeXagRate, $exchangeBtcRate);

        $exchangeHistoricals = array_merge($exchangeEuroHistoricalRate, $exchangeGbpHistoricalRate, $exchangeHistoricalRate, $exchangeXauHistoricalRate, $exchangeXagHistoricalRate, $exchangeBtcHistoricalRate);

        $currencies = array_merge(config('config.currency.1'), config('config.currency.3'), config('config.currency.2'), config('config.currency.4'), config('config.currency.5'), config('config.currency.6'));



        $chart1 = [];
        $chart2 = [];

        $icon_keys = config('config.template-icon');

        $servicePages = Page::whereIn('template_id', array_keys($icon_keys))->where('lang_id', $this->lang)->orderBy('template_id', 'asc')->get();
        $newsPage = Page::where('relation_page', $this->config->blog_page)->where('lang_id', $this->lang)->first();

        $loans = MarketLoan::select("title_$this->lang as title", 'prise_difference1', 'prise_difference2', "name_$this->lang as name", 'type')->orderBy('id', 'asc')->get();

        $chart = MarketChart::select('published_at', 'percent', 'type' )->where('type', 1);

        $charts = MarketChart::select('published_at', 'percent', 'type' )->where('type', 2)->union($chart)->orderby('published_at', 'asc')->get();


        foreach ($charts as $data){
            if($data->type == 1){
                $chart1[] = ['date' => $data->published_at, 'value' => $data->percent];
            }
            else{
                $chart2[] = ['date' => $data->published_at, 'value' => $data->percent];
            }
        }



        return view('web.index', ['currencies' => $currencies, 'exchanges' => $exchanges, 'exchangeHistoricals' => $exchangeHistoricals, 'chart1' => json_encode($chart1), 'chart2' => json_encode($chart2), 'loans' => $loans, 'servicePage' => $servicePages, 'newsPage' => $newsPage, 'parentPage' => $getPage, 'page' => $getPage, 'title' => $getPage->name, 'menuWithoutSlug' => true]);
    }


    protected function news($getPage, $getParentPage)
    {
        $years = Article::where('status', 1)->select(DB::raw('YEAR(published_at) as year'))->groupBy(DB::raw('YEAR(published_at)'))->orderBy('published_at', 'desc')->pluck('year', 'year');


        if(!empty($years))
        {
            if(request()->has('year') && request()->get('year') != "undefined" && request()->get('year') > 0){
                $selectedYear = request()->get('year');
            }
            else{
                $selectedYear = $years->first();
            }
        }
        else{
            $selectedYear = null;
        }


        if(request()->has('month') && request()->get('month') != "undefined" && request()->get('month') > 0){
            $selectedMonth = request()->get('month');
        }
        else{
            $selectedMonth = null;
        }


        $news = $getPage->articles()->selectByYear($selectedYear)->selectByMonth($selectedMonth)->paginate(12);


        if (request()->ajax()) {
            $selectedYear = request()->get('year');
            $view = view('web.elements.ajax-news', ['page' => $getPage, 'news' => $news, 'thisYear' => $selectedYear] )->render();

            return response()->json(['html'=>$view]);
        }

        return view("web.news", ['page' => $getPage, 'parentPage' => $getParentPage, 'news' => $news, 'years' => $years, 'pageTitle' => $getPage->name, 'thisYear' => $selectedYear]);

    }


    protected function vacancy($getPage, $getParentPage)
    {
        $vacancies = $getPage->vacancy()->paginate(10);

        return view("web.vacancy", ['page' => $getPage, 'parentPage' => $getParentPage, 'vacancies' => $vacancies]);
    }


    protected function services($getPage, $getParentPage)
    {
        return view("web.services", ['page' => $getPage, 'parentPage' => $getParentPage, 'pageTitle' => $getPage->name]);
    }


    protected function licenses($getPage, $getParentPage)
    {
        return view("web.license", ['page' => $getPage, 'parentPage' => $getParentPage, 'pageTitle' => $getPage->name]);
    }


    protected function contact($getPage, $getParentPage)
    {
        $config = Config::findOrFail(1);

        return view("web.contact", ['page' => $getPage, 'parentPage' => $getParentPage, 'config' => $config, 'pageTitle' => $getPage->name]);
    }


    protected function reports($getPage, $getParentPage)
    {
        return view("web.reports", ['page' => $getPage, 'parentPage' => $getParentPage]);
    }


    protected function laws($getPage, $getParentPage)
    {
        return view("web.laws", ['page' => $getPage, 'parentPage' => $getParentPage]);
    }


    protected function plans($getPage, $getParentPage)
    {
        return view("web.plans", ['page' => $getPage, 'parentPage' => $getParentPage]);
    }


    protected function loans($getPage, $getParentPage)
    {
        $rekvisit = Rekvisit::select('*', "coupon_payment_$this->lang as coupon_payment")->first(1);

        $loans = Loan::orderBy('id', 'asc')->select('loans', 'birja', "title_$this->lang as title", 'name', 'type', 'price', 'introduction1', 'coupon')->get();

        return view("web.loan", ['page' => $getPage, 'parentPage' => $getParentPage, 'pageTitle' => $getPage->name, 'loans' => $loans, 'rekvisit' => $rekvisit] );
    }


    protected function stocks($getPage, $getParentPage)
    {
        $stocks = Stock::orderBy('id', 'asc')->select('type', 'name', 'indicator1', 'indicator3', 'dividend')->get();

        return view("web.stock", ['page' => $getPage, 'parentPage' => $getParentPage, 'pageTitle' => $getPage->name, 'stocks' => $stocks] );
    }


    protected function reproduction($getPage, $getParentPage)
    {
        $table1 = ReproductionClient::where('lang_id', $this->lang)->orderBy('id', 'asc')->select('name', 'buyer', 'seller')->get();

        $table2 = Reproduction::where('lang_id', $this->lang)->orderBy('id', 'asc')->select('type', 'name', 'birja', 'ticker')->get();

        return view("web.reproduction", ['page' => $getPage, 'parentPage' => $getParentPage, 'pageTitle' => $getPage->name, 'table1' => $table1, 'table2' => $table2] );
    }


    protected function risk($getPage, $getParentPage)
    {
        $exam = Question::where('lang_id', $this->lang)->whereNull('parent_id')->with('answer')->select('id', 'title', 'image_original')->orderBy('id', 'asc')->limit(10)->get();

        return view("web.risk", ['page' => $getPage, 'parentPage' => $getParentPage, 'exam' => $exam] );
    }


    protected function staticPage($getPage, $getParentPage, $view = 'static')
    {
        return view("web.$view", ['page' => $getPage, 'parentPage' => $getParentPage, 'pageTitle' => $getPage->name] );
    }


    protected function investStrategy($getPage, $getParentPage)
    {
        $chart1 = [];
        $chart2 = [];

        $strategy = Strategy::all();

        $chart =  StrategyChart::whereIn('type', [1, 2])->select('year', DB::raw('CASE WHEN type = 1 THEN percent END as value1'), DB::raw('CASE WHEN type = 2 THEN percent END as value2'), DB::raw('1 as chart') );

        $charts = StrategyChart::whereIn('type', [3, 4])->select('year', DB::raw('CASE WHEN type = 3 THEN percent END as value1'), DB::raw('CASE WHEN type = 4 THEN percent END as value2'), DB::raw('2 as chart') )->union($chart)->orderBy('year', 'asc')->get()->toArray();

        $filterChart = array_filter(array_map('array_filter', $charts));


        foreach ($filterChart as $key => $ch){
            if($ch['chart'] == 1)
            {
                $chart1[] = $ch;
            }
            else{
                $chart2[] = $ch;
            }
        }

        $barChart = StrategyChart2::where('lang_id', $this->lang)->first();

        return view("web.invest-strategy", ['page' => $getPage, 'parentPage' => $getParentPage, 'pageTitle' => $getPage->name, 'strategy' => $strategy, 'chart1' => json_encode($chart1), 'chart2' => json_encode($chart2), 'barChart' => $barChart] );
    }


    protected function partnerSingle($getPage, $getParentPage)
    {
        return view("web.partner", ['page' => $getPage, 'parentPage' => $getParentPage, 'pageTitle' => $getPage->name] );
    }


    protected function service($getPage, $getParentPage, $view)
    {

        if($getParentPage->template_id == 16 && $getPage->third_level == 2){
            $category = Page::findOrFail($getPage->parent_id);
        }
        else{
            $category = $getPage;
        }

        return view("web.$view", ['category' => $category, 'page' => $getPage, 'parentPage' => $getParentPage, 'pageTitle' => $getPage->name] );
    }


    protected function articleSingle($getPage, $getParentPage, $slug)
    {
        $getArticle = Article::where('slug', $slug)->first();

        if(!$getArticle)
        {
            return $this->error404();
        }

        return view('web.article-single', ['articleSingle' => true, 'article' => $getArticle, 'parentPage' => $getParentPage, 'page' => $getPage, 'menuWithoutSlug' => true]);
    }


    protected function vacancySingle($getPage, $getParentPage)
    {
        return view('web.vacancy-single', ['parentPage' => $getParentPage, 'page' => $getPage]);
    }




    public function search($getPage)
    {
        $keyword = strip_tags(request()->get('keyword'));

        if(trim($keyword) == '')
        {
            return redirect()->route('home');
        }


        $cat = Page::where('lang_id', $this->lang)
            ->visible()
            ->where(function ($query) use($keyword) {
                $query->orWhere('slug', 'like', '%' . $keyword . '%')
                    ->orWhere('name', 'like', '%' . $keyword . '%')
                    ->orWhere('summary', 'like', '%' . $keyword . '%')
                    ->orWhere('content', 'like', '%' . $keyword . '%');
            })
            ->select('id', 'slug', 'name', 'summary', 'content', 'slug as category', 'created_at as published_at', 'image_original as image', DB::raw('1 as type'));


        $results = Article::join('pages as c', 'c.id', '=', 'articles.category_id')
            ->leftJoin('galleries', function ($join) {
                $join->on('articles.relation_page', '=', 'galleries.category_id')->where('galleries.type', 6);
            })
            ->where('articles.status', 1)
            ->where('c.lang_id', $this->lang)
            ->where(function ($query) use($keyword) {
                $query->orWhere('articles.slug', 'like', '%' . $keyword . '%')
                    ->orWhere('articles.title', 'like', '%' . $keyword . '%')
                    ->orWhere('articles.summary', 'like', '%' . $keyword . '%')
                    ->orWhere('articles.content', 'like', '%' . $keyword . '%');
            })
            ->select('articles.id', 'articles.slug', 'articles.title as name', 'articles.summary', 'articles.content', 'c.slug as category', 'articles.published_at',  'galleries.filename as image', DB::raw('2 as type'))
            ->union($cat)
            ->orderBy('type', 'asc')
            ->orderBy('id', 'asc')
            ->get();


        return view('web.search', ['results' => $results, 'keyword' => $keyword, 'page' => $getPage, 'parentPage' => $getPage, 'pageTitle' => $getPage->name]);

    }

    protected function block($parentPage)
    {
        if($parentPage->template_id != 11){
            return redirect()->route('showPage', $parentPage->slug);
        }
        else{
            return redirect()->route('home');
        }
    }

    protected function error404()
    {
        return view('errors.503', ['menuWithoutSlug' => true]);
    }
}
