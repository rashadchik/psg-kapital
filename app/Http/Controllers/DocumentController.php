<?php
 
namespace App\Http\Controllers;

use App\Logic\MultiLanguageSelect;
use Illuminate\Http\Request;
use App\Models\Document;
use App\Models\Page;
use Illuminate\Support\Facades\Validator;
use App\DataTables\DocumentDataTable;
use App\Logic\Input;
use PhpParser\Comment\Doc;
use Route;
use DB;
 
class DocumentController extends Controller
{
    protected $input;
    public $title, $requests;

    public function __construct(Request $request, Input $inputs)
    {
        $this->middleware('admin', ['only' => ['trash', 'destroy']]);

        $this->title = "Sənədlər";

        $this->input = config('config.inputs.'.Route::currentRouteName());

        $this->requests = $request->except('_token', '_method');

        view()->share('suitableInputs', $this->input);
        view()->share('allInputs', $inputs->allInputs());
    }



    public function index(DocumentDataTable $dataTable)
    {
        $breadcrumb = $this->breadcrumb();
        $breadcrumb->addCrumb($this->title,route("document.index"));
        return $dataTable->render('app.document.index', ['title' => $this->title, "breadcrumb" => $breadcrumb->render()]);

    }



    public function trashed(DocumentDataTable $dataTable)
    {
        $breadcrumb = $this->breadcrumb();
        $breadcrumb->addCrumb($this->title, route("document.index"));
        $breadcrumb->addCrumb(trans('locale.trash'));
        return $dataTable->trashed(true)->render('app.document.index', ['title' => $this->title, "breadcrumb" => $breadcrumb->render()]);
    }



    public function create()
    {
        $query = Page::whereIn('template_id',  [12, 15, 20])
        ->orderBy("id", "asc")
        ->get();

        $category = MultiLanguageSelect::multiLang($query);

        $breadcrumb = $this->breadcrumb();
        $breadcrumb->addCrumb($this->title, route("document.index"));
        $breadcrumb->addCrumb(trans('locale.create'));

        return view('app.document.create', ["breadcrumb" => $breadcrumb->render(), "category" => $category]);
    }

 

    public function store(Request $request)
    {
        $request->validate(Document::$rules, Document::$messages);

        Page::findOrFail($request->input('category_id'));

        try{
            Document::create($this->requests);
        }
        catch(\Exception $e){
            return redirect()->back()->withInput($request->input())->withErrors(["errors" => trans('locale.fail', ['number' => 1])]);
        }

        $request->session()->flash('message', "Sənəd əlavə olundu");

        return redirect()->route('document.index');
    }



    public function edit($id)
    {
        $document = Document::findOrFail($id);

        $query = Page::whereIn('template_id',  [12, 15, 20])
            ->orderBy("id", "asc")
            ->get();

        $category = MultiLanguageSelect::multiLang($query);

        $breadcrumb = $this->breadcrumb();
        $breadcrumb->addCrumb($this->title, route("document.index"));
        $breadcrumb->addCrumb(trans('locale.edit'));

        return view('app.document.edit', ['info' => $document, "breadcrumb" => $breadcrumb->render(), 'category' => $category]);
    }



    public function update(Request $request, $id)
    {
        $doc = Document::findOrFail($id);

        Page::findOrFail($request->input('category_id'));

        $request->validate(Document::$rules, Document::$messages);

        foreach($this->requests as $key => $put){
            $doc->$key = $put;
        }

        $doc->save();

        $request->session()->flash('message', trans('locale.update_success'));
        return redirect()->route('document.index');
    }



    public function trash($id)
    {
        $member = Document::findOrFail($id);
        $member->delete();

        request()->session()->flash('message', "Sənəd silindi. Qaytarılması mümkündür.");
        return redirect()->back();
    }


    public function restore($id)
    {
        $member = Document::onlyTrashed()->findOrFail($id);
        $member->restore();

        request()->session()->flash('message', "Silinmiş sənəd qaytarıldı və aktiv olundu");
        return redirect()->route('document.index');
    }


    public function destroy($id)
    {
        $member = Document::onlyTrashed()->findOrFail($id);
        $member->forceDelete();

        request()->session()->flash('message', "Sənəd birdəfəlik silindi");
        return redirect()->back();
    }
}
