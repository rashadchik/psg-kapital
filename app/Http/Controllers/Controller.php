<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Creitive\Breadcrumbs\Breadcrumbs;


abstract class Controller extends BaseController
{

    use DispatchesJobs, ValidatesRequests;

    protected function breadcrumb()
    {
        $breadcrumbs = new Breadcrumbs();
        $breadcrumbs->setListElement('ul');
        //$breadcrumbs->setListItemCssClass('');
        $breadcrumbs->addCrumb(webConfig()->company_name, route('dashboard'));
        $breadcrumbs->setCssClasses("breadcrumb");
        $breadcrumbs->setDivider('');

        return $breadcrumbs;
    }


    protected function langMaxKey(){
        $lang = config('app.locales');

        return max(array_keys($lang));
    }

    protected function templateMaxKey(){
        $temp = config('config.template');

        return max(array_keys($temp));
    }


    protected function roleMaxKey(){
        $role = config('config.role');

        return max(array_keys($role));
    }


    protected function visibilityMaxKey(){
        $role = config('config.menu-visibility');

        return max(array_keys($role));
    }


    protected function protectDev($id){
        if(auth()->user()->id != 1){
            $protect = User::where('id', '<>', 1)->where('id', $id)->firstOrFail();
        }
        else{
            $protect = User::findOrFail($id);
        }

        return $protect;
    }


    protected function headers()
    {
        return array('Content-type'=> 'application/json; charset=utf-8');
    }


    protected function responseArray($error, $type, $msg, $close, $once=false, $reset=false)
    {
        return array("success"=>$error, "alert" => "label-$type", "msg" => $msg, "close" => $close, "once" => $once, "resetForm" => $reset);
    }


    protected function responseJson($response){
        return response()->json($response, 200, $this->headers(), JSON_FORCE_OBJECT, JSON_UNESCAPED_UNICODE);
    }
}
