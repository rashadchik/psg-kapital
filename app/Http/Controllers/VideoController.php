<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Video;
use Illuminate\Support\Facades\Validator;
use App\DataTables\VideoDataTable;
use App\Logic\Input;
use Route;
use DB;

class VideoController extends Controller
{
    protected $input, $image;
    public $title, $imageResize;

    public function __construct(Request $request, Input $inputs)
    {
        $this->middleware('admin', ['only' => ['trash', 'destroy']]);

        $this->title = "Video";

        $this->input = config('config.inputs.'.Route::currentRouteName());

        view()->share('suitableInputs', $this->input);
        view()->share('allInputs', $inputs->allInputs());
    }



    public function index(VideoDataTable $dataTable)
    {
        $breadcrumb = $this->breadcrumb();
        $breadcrumb->addCrumb($this->title);

        return $dataTable->render('app.video.index', ['title' => $this->title, "breadcrumb" => $breadcrumb->render()]);
    }



    public function trashed(VideoDataTable $dataTable)
    {
        $breadcrumb = $this->breadcrumb();
        $breadcrumb->addCrumb($this->title, route("video.index"));
        $breadcrumb->addCrumb(trans('locale.trash'));
        return $dataTable->trashed(true)->render('app.video.index', ['title' => $this->title, "breadcrumb" => $breadcrumb->render()]);
    }



    public function create()
    {
        $breadcrumb = $this->breadcrumb();
        $breadcrumb->addCrumb($this->title, route("video.index"));
        $breadcrumb->addCrumb(trans('locale.create'));

        return view('app.video.create', ["breadcrumb" => $breadcrumb->render()]);
    }



    public function store(Request $request)
    {
        $inputs = [
            'title' => $request->input('title'),
            'summary' => $request->input('summary'),
            'video' => $request->input('video'),
            'link' => $request->input('link'),
            'lang_id' => $request->input('lang_id'),
            'order' => Video::max('order')+1,
        ];

        $rules = array(
            'title' => 'nullable',
            'summary' => 'nullable',
            'video' => 'required',
            'link' => 'nullable',
            'lang_id' => 'required',
        );



        $validator = Validator::make($inputs, $rules);

        if ($validator->fails()) {
            return redirect()->back()->withInput($request->input())->withErrors(["errors" => $validator->errors()->all()]);
        }

        try{
            Video::create($inputs);
        }
        catch(\Exception $e){
            return redirect()->back()->withInput($request->input())->withErrors(["errors" => trans('locale.fail', ['number' => 1])]);
        }


        $request->session()->flash('message', "Video əlavə olundu");

        return redirect()->route('video.index');
    }



    public function edit($id)
    {
        $breadcrumb = $this->breadcrumb();
        $breadcrumb->addCrumb($this->title, route("video.index"));
        $breadcrumb->addCrumb(trans('locale.edit'));

        $slide = Video::findOrFail($id);
        return view('app.video.edit', ['info' => $slide, 'title' => $slide->description, 'id' => $id, "breadcrumb" => $breadcrumb->render()]);
    }



    public function update(Request $request, $id)
    {
        $member = Video::findOrFail($id);

        $inputs = [
            'title' => $request->input('title'),
            'summary' => $request->input('summary'),
            'video' => $request->input('video'),
            'link' => $request->input('link'),
            'lang_id' => $request->input('lang_id'),
        ];

        $rules = array(
            'title' => 'nullable',
            'summary' => 'nullable',
            'video' => 'required',
            'link' => 'nullable',
            'lang_id' => 'required',
        );


        $validator = Validator::make($inputs, $rules);

        if ($validator->fails()) {
            return redirect()->back()->withInput($request->input())->withErrors(["errors" => $validator->errors()->first()]);
        }

        foreach($inputs as $key => $put){
            $member->$key = $put;
        }

        $member->save();


        $request->session()->flash('message', trans('locale.update_success'));
        return redirect()->route('video.index');
    }



    public function copy($id, $lang_id)
    {

        DB::beginTransaction();


        $slider = Video::findOrFail($id);

        $copy = $slider->replicate();

        $copy->lang_id = $lang_id;

        $copy->save();


        DB::commit();

        request()->session()->flash('message', "Video üçün nüsxə yaradıldı");

        return redirect()->back();
    }



    public function trash($id)
    {
        $member = Video::findOrFail($id);
        $member->delete();

        request()->session()->flash('message', "Video silindi. Qaytarılması mümkündür.");
        return redirect()->back();
    }


    public function restore($id)
    {
        $member = Video::onlyTrashed()->findOrFail($id);
        $member->restore();

        request()->session()->flash('message', "Silinmiş video qaytarıldı və aktiv olundu");
        return redirect()->route('video.index');
    }


    public function destroy($id)
    {
        $member = Video::onlyTrashed()->findOrFail($id);
        $member->forceDelete();

        request()->session()->flash('message', "Video birdəfəlik silindi");
        return redirect()->back();
    }
}
