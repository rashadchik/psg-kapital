<?php

namespace App\Http\Controllers;

use App\Logic\ArticleImage;
use App\Models\Page;
use App\Logic\Slug;
use App\Logic\Input;
use Illuminate\Http\Request;
use App\DataTables\PageDataTable;
use DB;
use Route;

class PageController extends Controller
{
    protected $input, $image, $requests;
    public $title;

    public function __construct(ArticleImage $imageUpload, Request $request, Input $inputs)
    {
        $this->middleware('admin', ['only' => ['trash', 'orderPage', 'orderCategory', 'postOrder', 'destroy']]);


        $this->input = config('config.inputs.'.Route::currentRouteName());
        $this->requests = $request->except('_token', '_method', 'return', 'edit', 'apply_module');
        $this->image = $imageUpload;
        $this->title = "Menyular";

        if (in_array(strtolower($request->method()), ['put', 'post', 'patch', 'delete'])) {
            clearCache('pages');
        }

        view()->share('suitableInputs', $this->input);
        view()->share('allInputs', $inputs->allInputs());
    }


    public function index(PageDataTable $dataTable)
    {
        $breadcrumb = $this->breadcrumb();
        $breadcrumb->addCrumb($this->title);
        return $dataTable->render('app.page.index', ['title' => $this->title, "breadcrumb" => $breadcrumb->render()]);
    }


    public function trashed(PageDataTable $dataTable)
    {
        $breadcrumb = $this->breadcrumb();
        $breadcrumb->addCrumb($this->title, route("page.index"));
        $breadcrumb->addCrumb(trans('locale.trash'));
        return $dataTable->trashed(true)->render('app.page.index', ['title' => $this->title, "breadcrumb" => $breadcrumb->render()]);
    }


    public function create()
    {
        $breadcrumb = $this->breadcrumb();
        $breadcrumb->addCrumb($this->title, route("page.index"));
        $breadcrumb->addCrumb(trans('locale.create'));

        return view('app.page.create', ["breadcrumb" => $breadcrumb->render(), "route" => "page"]);
    }


    public function store(Request $request)
    {
        $this->requests['has_slider'] = $request->input('has_slider', 0);

        $request->validate(Page::rules($request->input('lang_id')));

        $this->requests['slug'] = Slug::slugify($request->input('slug'), $request->input('name'), 'pages', $request->input('lang_id') );

        DB::beginTransaction();


        try{
            $page = Page::create($this->requests);
        }
        catch(\Exception $e){
            DB::rollback();
            return redirect()->back()->withInput($request->input())->withErrors(["errors" => $e->getMessage()]);
        }


        if(!$request->has('relation_page')){
            try{
                $page->relation_page = $page->id;
            }
            catch(\Exception $e){
                DB::rollback();
                return redirect()->back()->withInput($request->input())->withErrors(["errors" => $e->getMessage()]);
            }
        }

        $page->save();

        DB::commit();

        $request->session()->flash('message', "Yeni səhifə yaradıldı");

        if($request->has('return'))
        {
            return redirect()->route('page.index');
        }
        else{
            if($request->has('edit')){
                return redirect()->back();
            }
            else{
                return redirect()->route('edit_page', $page->relation_page);
            }
        }
    }


    public function show($slug)
    {
        return $slug;
    }


    public function edit($id)
    {
        $relationPage = Page::where('relation_page', $id)->firstOrFail();

        $lang = request()->get('lang_id', $relationPage->lang_id);

        $page = Page::where('relation_page', $id)->where('lang_id', $lang)->first();

        $breadcrumb = $this->breadcrumb();
        $breadcrumb->addCrumb($this->title, route("page.index"));
        $breadcrumb->addCrumb($relationPage->name, route('edit_page', $id));
        $breadcrumb->addCrumb(trans('locale.page'));


        if($page)
        {
            $keys = array_filter(explode(",", $page->meta_keywords));

            return view('app.page.edit', ['lang' => $page->lang_id, 'info' => $page, 'relPage' => $relationPage, 'edit' => true, 'title' => $page->name, 'id' => $id, "breadcrumb" => $breadcrumb->render(), "keywords" => $keys, 'route' => 'page']);

        }
        else{

            return view('app.page.create', ['edit' => true, 'relPage' => $relationPage, "breadcrumb" => $breadcrumb->render(), 'id' => $id, 'lang' => $lang, 'title' => $relationPage->name, 'route' => 'page']);

        }

    }


    public function update(Request $request, $id)
    {
        $page = Page::findOrFail($id);

        $this->requests['has_slider'] = $request->input('has_slider', 0);

        $request->validate(Page::rules($request->input('lang_id'), $id));

        $this->requests['slug'] = Slug::slugify($request->input('slug'), $request->input('name'), 'pages', $request->input('lang_id') );

        #$this->requests['image_cover'] = $this->image->uploadImage('image_cover', $page, $this->resize(1), true);
        #$this->requests['image_original'] = $this->image->uploadImage('image_original', $page, $this->resize(2), true);


        DB::Begintransaction();

        try{
            foreach($this->requests as $key => $put){
                $page->$key = $put;
            }


            $original_lang_id = $page->getOriginal('lang_id');
            $original_template_id = $page->getOriginal('template_id');

            $page->save();
        }
        catch(\Exception $e){

            DB::rollback();
            return redirect()->back()->withInput($request->input())->withErrors(["errors" => $e->getMessage()]);
        }



        if($request->input('lang_id') != $original_lang_id)
        {

            try{
                Page::where('main_parent_id', $id)->update(['lang_id' => $request->input('lang_id')]);
            }
            catch(\Exception $e){
                DB::rollback();
                return redirect()->back()->withInput($request->input())->withErrors(["errors" => $e->getMessage()]);
            }

        }


        if($request->has('apply_module'))
        {
            try{
                Page::where('main_parent_id', $id)->update(['template_id' => $request->input('template_id')]);
            }
            catch(\Exception $e){
                DB::rollback();
                return redirect()->back()->withInput($request->input())->withErrors(["errors" => $e->getMessage()]);
            }

        }


        DB::commit();


        $request->session()->flash('message', trans('locale.update_success'));

        if($request->has('return'))
        {
            return redirect()->route('page.index');
        }
        else{
            return redirect()->back();

        }
    }


    public function trash($id)
    {
        $category = Page::findOrFail($id);
        $category->delete();

        request()->session()->flash('message', "Səhifə zibil qutusuna göndərildi");
        return redirect()->back();
    }


    public function restore($id)
    {
        $category = Page::onlyTrashed()->findOrFail($id);
        $category->restore();

        request()->session()->flash('message', "Səhifə zibil qutusundan qaytarıldı");
        return redirect()->back();
    }


    public function destroy($id)
    {
        $category = Page::onlyTrashed()->findOrFail($id);
        $category->forceDelete();

        request()->session()->flash('message', "Səhifə birdəfəlik silindi");
        return redirect()->back();
    }


    public function orderPage($lang)
    {
        $breadcrumb = $this->breadcrumb();
        $breadcrumb->addCrumb($this->title, route('page.index'));
        $breadcrumb->addCrumb("Ardıcıllıq");


        $items 	= Page::where('lang_id', $lang)->orderBy('order', 'asc')->whereNull('parent_id')->get();
        $menu 	= new Page;
        $menu   = $menu->getPage($items);

        $langs = config('app.locales');
        return view('app.page.order', ["breadcrumb" => $breadcrumb->render(), 'items'=>$items, 'menu'=>$menu, 'langs' => $langs, 'route' => 'order_page' ,'depth' => 1]);
    }


    public function orderCategory($lang)
    {
        $breadcrumb = $this->breadcrumb();
        $breadcrumb->addCrumb('Kateqoriyalar', route('category.index'));
        $breadcrumb->addCrumb("Ardıcıllıq");


        $items 	= Page::where('lang_id', $lang)->where('third_level', '<', 4)->whereNotIn('slug', ['index', 'search'])->orderBy('order', 'asc')->get();
        $menu 	= new Page;
        $menu   = $menu->getCategory($items);

        $langs = config('app.locales');
        return view('app.page.order', ["breadcrumb" => $breadcrumb->render(), 'items'=>$items, 'menu'=>$menu, 'langs' => $langs, 'route' => 'order_category', 'depth' => 4]);
    }


    public function postOrder(Request $request)
    {
        $source       = $request->input('source');
        $destination  = $request->input('destination') ? $request->input('destination') : null;

        $item             = Page::find($source);
        $item->parent_id  = $destination;
        $item->save();

        $ordering       = json_decode($request->input('order'));
        $rootOrdering   = json_decode($request->input('rootOrder'));

        if($ordering){
            foreach($ordering as $order=>$item_id){
                if($itemToOrder = Page::find($item_id)){
                    $itemToOrder->order = $order;
                    $itemToOrder->save();
                }
            }
        } else {
            foreach($rootOrdering as $order=>$item_id){
                if($itemToOrder = Page::find($item_id)){
                    $itemToOrder->order = $order;
                    $itemToOrder->save();
                }
            }
        }

        return 'ok ';
    }


    protected function resize($type)
    {
        if($type == 1)
        {
            $resizeImage = ['resize' => ['fit' => false, 'size' => [1920, 270]], 'thumb' => null ];
        }
        else{
            $resizeImage = ['resize' => ['fit' => false, 'size' => [720, 720]], 'thumb' => ['fit' => false, 'size' => [720, 720]] ];
        }


        return $resizeImage;
    }
}
