<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Logic\ArticleImage;
use App\Models\Slider;
use App\DataTables\PartnerDataTable;
use App\Logic\Input;
use Route;
use DB;

class PartnerController extends Controller
{
    protected $input, $image, $requests;
    public $title, $resizeImage;

    public function __construct(Request $request, ArticleImage $imageUpload, Input $inputs)
    {
        $this->middleware('admin', ['only' => ['trash', 'destroy']]);

        $this->image = $imageUpload;

        if (in_array(strtolower($request->method()), ['put', 'post', 'patch', 'delete'])) {
            clearCache('partner', false);
        }

        $this->title = "Tərəfdaşlar";
        $this->requests = $request->except('_token', '_method');
        $this->input = config('config.inputs.'.Route::currentRouteName());

        $this->resizeImage = ['resize' => ['fit' => false, 'size' => [190, 40]], 'thumb' => null ];

        view()->share('suitableInputs', $this->input);
        view()->share('allInputs', $inputs->allInputs());
    }



    public function index(PartnerDataTable $dataTable)
    {
        $breadcrumb = $this->breadcrumb();
        $breadcrumb->addCrumb($this->title);

        return $dataTable->render('app.partners.index', ['title' => $this->title, "breadcrumb" => $breadcrumb->render()]);
    }



    public function trashed(PartnerDataTable $dataTable)
    {
        $breadcrumb = $this->breadcrumb();
        $breadcrumb->addCrumb($this->title, route("partners.index"));
        $breadcrumb->addCrumb(trans('locale.trash'));
        return $dataTable->trashed(true)->render('app.partners.index', ['title' => $this->title, "breadcrumb" => $breadcrumb->render()]);
    }



    public function create()
    {
        $breadcrumb = $this->breadcrumb();
        $breadcrumb->addCrumb($this->title, route("partners.index"));
        $breadcrumb->addCrumb(trans('locale.create'));

        return view('app.partners.create', ["breadcrumb" => $breadcrumb->render()]);
    }



    public function store(Request $request)
    {

        $this->requests['type'] = 2;

        $this->requests['lang_id'] = 'az';


        $this->requests['order'] = Slider::max('order')+1;

        $request->validate(Slider::$partnerRules);


        DB::beginTransaction();


        try{
            $slider = Slider::create($this->requests);
        }
        catch(\Exception $e){
            DB::rollback();
            return redirect()->back()->withInput($request->input())->withErrors(["errors" => trans('locale.fail', ['number' => 1])]);
        }


        $uploadImage = $this->image->uploadImage('image_original',  $slider, $this->resizeImage);


        try{
            $slider->image_original = $uploadImage;
        }
        catch(\Exception $e){

            $this->image->delete($slider->id, $uploadImage);

            DB::rollback();
            return redirect()->back()->withInput($request->input())->withErrors(["errors" => $e->getMessage()]);
        }


        $slider->save();


        DB::commit();

        $request->session()->flash('message', "Logo əlavə olundu");

        return redirect()->route('partners.index');
    }



    public function edit($id)
    {
        $breadcrumb = $this->breadcrumb();
        $breadcrumb->addCrumb($this->title, route("partners.index"));
        $breadcrumb->addCrumb(trans('locale.edit'));

        $slide = Slider::findOrFail($id);
        return view('app.partners.edit', ['info' => $slide, 'title' => $slide->description, 'id' => $id, "breadcrumb" => $breadcrumb->render()]);
    }



    public function update(Request $request, $id)
    {
        $slider = Slider::findOrFail($id);

        $request->validate(Slider::$partnerRules);

        $this->requests['image_original'] = $this->image->uploadImage('image_original', $slider, $this->resizeImage, true);

        //dd($this->requests['image_original'] );

        foreach($this->requests as $key => $put){
            $slider->$key = $put;
        }

        $slider->save();


        $request->session()->flash('message', trans('locale.update_success'));
        return redirect()->route('partners.index');
    }


    public function order($lang)
    {
        $breadcrumb = $this->breadcrumb();
        $breadcrumb->addCrumb($this->title, route('partners.index'));
        $breadcrumb->addCrumb("Ardıcıllıq");


        $items 	= Slider::where('lang_id', $lang)->where('type', 2)->orderBy('order', 'asc')->get();

        $menu 	= new Slider;
        $menu   = $menu->getSlider($items);

        $langs = config('config.lang-dictionary');
        return view('app.page.order', ["breadcrumb" => $breadcrumb->render(), 'items'=>$items, 'menu'=>$menu, 'langs' => $langs, 'route' => 'order_slider', 'depth' => 2, 'post_order' => 'post_order_slider']);
    }


    public function postOrder(Request $request)
    {
        $source       = $request->input('source');
        $destination  = $request->input('destination') ? $request->input('destination') : null;

        $item             = Slider::find($source);
        $item->save();

        $ordering       = json_decode($request->input('order'));
        $rootOrdering   = json_decode($request->input('rootOrder'));

        if($ordering){
            foreach($ordering as $order=>$item_id){
                if($itemToOrder = Slider::find($item_id)){
                    $itemToOrder->order = $order;
                    $itemToOrder->save();
                }
            }
        } else {
            foreach($rootOrdering as $order=>$item_id){
                if($itemToOrder = Slider::find($item_id)){
                    $itemToOrder->order = $order;
                    $itemToOrder->save();
                }
            }
        }

        return 'ok ';
    }


    public function trash($id)
    {
        $member = Slider::findOrFail($id);
        $member->delete();

        request()->session()->flash('message', "Logo silindi. Qaytarılması mümkündür.");
        return redirect()->back();
    }


    public function restore($id)
    {
        $member = Slider::onlyTrashed()->findOrFail($id);
        $member->restore();

        request()->session()->flash('message', "Silinmiş logo qaytarıldı və aktiv olundu");
        return redirect()->route('slider.index');
    }


    public function destroy($id)
    {
        $member = Slider::onlyTrashed()->findOrFail($id);
        $member->forceDelete();

        request()->session()->flash('message', "Logo birdəfəlik silindi");
        return redirect()->back();
    }

}
