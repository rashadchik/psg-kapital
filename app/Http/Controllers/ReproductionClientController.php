<?php

namespace App\Http\Controllers;

use App\Models\ReproductionClient;
use Illuminate\Http\Request;
use App\DataTables\ReproductionClientDataTable;
use App\Logic\Input;
use DB;
use Route;

class ReproductionClientController extends Controller
{
    protected $input, $requests;
    public $title, $route, $view;

    public function __construct(Request $request, Input $inputs)
    {
        $this->middleware('admin', ['only' => ['trash', 'destroy']]);

        $this->input = config('config.inputs.'.Route::currentRouteName());
        $this->requests = $request->except('_token', '_method');

        $this->title = "Törəmə Alətləri";

        view()->share('suitableInputs', $this->input);
        view()->share('allInputs', $inputs->allInputs());
    }


    public function index(ReproductionClientDataTable $dataTable)
    {

        $breadcrumb = $this->breadcrumb();
        $breadcrumb->addCrumb($this->title, route('client.index'));

        return $dataTable->render("app.reproductionClient.index", ["breadcrumb" => $breadcrumb->render(),'title' =>  $this->title] );
    }


    public function trashed(ReproductionClientDataTable $dataTable)
    {
        $breadcrumb = $this->breadcrumb();
        $breadcrumb->addCrumb($this->title, route('client.index'));
        $breadcrumb->addCrumb(trans('locale.trash'));

        return $dataTable->trashed(true)->render("app.reproductionClient.index", ["breadcrumb" => $breadcrumb->render()] );
    }


    public function create()
    {
        $breadcrumb = $this->breadcrumb();
        $breadcrumb->addCrumb($this->title, route('client.index'));
        $breadcrumb->addCrumb(trans('locale.create'));



        return view("app.reproductionClient.create", ["breadcrumb" => $breadcrumb->render()]);
    }


    public function store(Request $request)
    {
        $request->validate(ReproductionClient::$rules, ReproductionClient::$messages);

        try{
            ReproductionClient::create($this->requests);
        }
        catch(\Exception $e){
            return redirect()->back()->withInput($request->input())->withErrors(["errors" => trans('locale.fail', ['number' => 1])]);
        }


        $request->session()->flash('message', "Əməliyyat uğurlu alındı");

        return redirect()->route('client.index');
    }



    public function edit($id)
    {
        $reproduction = ReproductionClient::findOrFail($id);
        $breadcrumb = $this->breadcrumb();
        $breadcrumb->addCrumb($this->title, route('client.index'));
        $breadcrumb->addCrumb(trans('locale.edit'));

        return view("app.reproductionClient.edit", ["info" => $reproduction, "breadcrumb" => $breadcrumb->render()]);
    }


    public function update(Request $request, $id)
    {
        $find = ReproductionClient::findOrFail($id);

        $request->validate(ReproductionClient::$rules, ReproductionClient::$messages);

        try{
            foreach($this->requests as $key => $input){
                $find -> $key = $input;
            }

            $find -> save();
        }
        catch(\Exception $e){
            return redirect()->back()->withInput($request->input())->withErrors(["errors" => trans('locale.fail', ['number' => 1])]);
        }


        $request->session()->flash('message', trans('locale.update_success'));

        return redirect()->route('client.index');
    }


    public function trash($id)
    {
        $reproduction = ReproductionClient::findOrFail($id);
        $reproduction->delete();

        request()->session()->flash('message', "Məlumat silindi, qaytarılması mümkündür.");
        return redirect()->back();
    }


    public function restore($id)
    {
        $reproduction = ReproductionClient::onlyTrashed()->findOrFail($id);
        $reproduction->restore();

        request()->session()->flash('message', "Məlumat qaytarıldı");
        return redirect()->route('client.index');
    }


    public function destroy($id)
    {
        $info = ReproductionClient::onlyTrashed()->findOrFail($id);

        try{
            $info->forceDelete();
        }
        catch(\Exception $e){
            return redirect()->back()->withErrors(["errors" => trans('locale.fail', ['number' => 2])]);
        }


        request()->session()->flash('message', "Məlumat birdəfəlik silindi");
        return redirect()->back();
    }
}
