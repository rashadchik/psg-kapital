<?php
 
namespace App\Http\Controllers;

use App\Models\Currency;
use Illuminate\Http\Request;
use App\DataTables\CurrencyDataTable;
use App\Logic\Input;
use Route;
use DB;
 
class CurrencyController extends Controller
{
    protected $input,$requests;
    public $title;

    public function __construct(Request $request, Input $inputs)
    {
        $this->middleware('admin', ['only' => ['trash', 'destroy']]);

        $this->title = "Valyutalar";

        $this->input = config('config.inputs.'.Route::currentRouteName());

        $this->requests = $request->except('_token', '_method');

        if (in_array(strtolower($request->method()), ['put', 'post', 'patch', 'delete'])) {
            clearCache('currency', false);
        }

        view()->share('suitableInputs', $this->input);
        view()->share('allInputs', $inputs->allInputs());
    }



    public function index(CurrencyDataTable $dataTable)
    {
        $breadcrumb = $this->breadcrumb();
        $breadcrumb->addCrumb($this->title,route("currency.index"));

        return $dataTable->render('app.currency.index', ['title' => $this->title, "breadcrumb" => $breadcrumb->render()]);

    }


    public function trashed(CurrencyDataTable $dataTable)
    {
        $breadcrumb = $this->breadcrumb();
        $breadcrumb->addCrumb($this->title, route("currency.index"));
        $breadcrumb->addCrumb(trans('locale.trash'));
        return $dataTable->trashed(true)->render('app.currency.index', ['title' => $this->title, "breadcrumb" => $breadcrumb->render()]);
    }


    public function create()
    {

        $breadcrumb = $this->breadcrumb();
        $breadcrumb->addCrumb($this->title, route("currency.index"));
        $breadcrumb->addCrumb(trans('locale.create'));

        return view('app.currency.create', ["breadcrumb" => $breadcrumb->render()]);
    }

  

    public function store(Request $request)
    {
        $request->validate(Currency::$rules, Currency::$messages);


        try{
            Currency::create($this->requests);
        }
        catch(\Exception $e){
            return redirect()->back()->withInput($request->input())->withErrors(["errors" => trans('locale.fail', ['number' => 1])]);
        }

        $request->session()->flash('message', " əlavə olundu");

        return redirect()->route('currency.index');
    }



    public function edit($id)
    {
        $currency = Currency::findOrFail($id);


        $breadcrumb = $this->breadcrumb();
        $breadcrumb->addCrumb($this->title, route("currency.index"));
        $breadcrumb->addCrumb(trans('locale.edit'));

        return view('app.currency.edit', ['info' => $currency, "breadcrumb" => $breadcrumb->render()]);
    }



    public function update(Request $request, $id)
    {
        $currency = Currency::findOrFail($id);


        $request->validate(Currency::$rules, Currency::$messages);

        try{
            foreach($this->requests as $key => $input){
                $currency -> $key = $input;
            }

            $currency -> save();
        }
        catch(\Exception $e){
            return redirect()->back()->withInput($request->input())->withErrors(["errors" => trans('locale.fail', ['number' => 1])]);
        }


        $request->session()->flash('message', trans('locale.update_success'));
        return redirect()->route('currency.index');
    }


    public function trash($id)
    {
        $member = Currency::findOrFail($id);
        $member->delete();

        request()->session()->flash('message', "Valyuta silindi. Qaytarılması mümkündür.");
        return redirect()->back();
    }

    public function restore($id)
    {
        $member = Currency::onlyTrashed()->findOrFail($id);
        $member->restore();

        request()->session()->flash('message', "Valyuta  qaytarıldı və aktiv olundu");
        return redirect()->route('currency.index');
    }


    public function destroy($id)
    {
        $member = Currency::onlyTrashed()->findOrFail($id);
        
        try{
            $member->forceDelete();
        }
        catch(\Exception $e){
            return redirect()->back()->withErrors(["errors" => trans('locale.fail', ['number' => 2])]);
        }
        request()->session()->flash('message', "Valyuta birdəfəlik silindi");
        return redirect()->back();
    }
}
