<?php

namespace App\Http\Controllers;

use App\Models\Strategy;
use Illuminate\Http\Request;
use App\DataTables\StrategiesDataTable;
use App\Logic\Input;
use DB;
use Route;

class StrategyController extends Controller
{
    protected $input, $requests;
    public $title, $route, $view;

    public function __construct(Request $request, Input $inputs)
    {
        $this->middleware('admin', ['only' => ['trash', 'destroy']]);

        $this->input = config('config.inputs.'.Route::currentRouteName());
        $this->requests = $request->except('_token', '_method');

        $this->title = "Strategiyalar";

        view()->share('suitableInputs', $this->input);
        view()->share('allInputs', $inputs->allInputs());
    }


    public function index(StrategiesDataTable $dataTable)
    {
        $breadcrumb = $this->breadcrumb();
        $breadcrumb->addCrumb($this->title, route('strategy.index'));

        return $dataTable->render("app.strategy.index", ["breadcrumb" => $breadcrumb->render(),'title' =>  $this->title] );
    }


    public function trashed(StrategiesDataTable $dataTable)
    {
        $breadcrumb = $this->breadcrumb();
        $breadcrumb->addCrumb($this->title, route('strategy.index'));
        $breadcrumb->addCrumb(trans('locale.trash'));

        return $dataTable->trashed(true)->render("app.strategy.index", ["breadcrumb" => $breadcrumb->render()] );
    }


    public function create()
    {
        $breadcrumb = $this->breadcrumb();
        $breadcrumb->addCrumb($this->title, route('strategy.index'));
        $breadcrumb->addCrumb(trans('locale.create'));



        return view("app.strategy.create", ["breadcrumb" => $breadcrumb->render()]);
    }


    public function store(Request $request)
    {

        $request->validate(Strategy::$rules, Strategy::$messages);


        try{
            Strategy::create($this->requests);
        }
        catch(\Exception $e){
            return redirect()->back()->withInput($request->input())->withErrors(["errors" => trans('locale.fail', ['number' => 1])]);
        }


        $request->session()->flash('message', "Əməliyyat uğurlu alındı");

        return redirect()->route('strategy.index');
    }



    public function edit($id)
    {
        $strategy = Strategy::findOrFail($id);
        $breadcrumb = $this->breadcrumb();
        $breadcrumb->addCrumb($this->title, route('strategy.index'));
        $breadcrumb->addCrumb(trans('locale.edit'));

        return view("app.strategy.edit", ["info" => $strategy, "breadcrumb" => $breadcrumb->render()]);
    }


    public function update(Request $request, $id)
    {
        $find = Strategy::findOrFail($id);

        $request->validate(Strategy::$rules, Strategy::$messages);

        try{
            foreach($this->requests as $key => $input){
                $find -> $key = $input;
            }

            $find -> save();
        }
        catch(\Exception $e){
            return redirect()->back()->withInput($request->input())->withErrors(["errors" => trans('locale.fail', ['number' => 1])]);
        }


        $request->session()->flash('message', trans('locale.update_success'));

        return redirect()->route('strategy.index');
    }


    public function trash($id)
    {
        $loan = Strategy::findOrFail($id);
        $loan->delete();

        request()->session()->flash('message', "Məlumat silindi, qaytarılması mümkündür.");
        return redirect()->back();
    }


    public function restore($id)
    {
        $loan = Strategy::onlyTrashed()->findOrFail($id);
        $loan->restore();

        request()->session()->flash('message', "Məlumat qaytarıldı");
        return redirect()->route('strategy.index');
    }


    public function destroy($id)
    {
        $info = Strategy::onlyTrashed()->findOrFail($id);

        try{
            $info->forceDelete();
        }
        catch(\Exception $e){
            return redirect()->back()->withErrors(["errors" => trans('locale.fail', ['number' => 2])]);
        }


        request()->session()->flash('message', "Məlumat birdəfəlik silindi");
        return redirect()->back();
    }
}
