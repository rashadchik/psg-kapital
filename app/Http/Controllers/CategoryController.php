<?php

namespace App\Http\Controllers;

use App\Logic\ArticleImage;
use App\Models\Page;
use Illuminate\Http\Request;
use App\DataTables\CategoryDataTable;
use App\Logic\Slug;
use App\Logic\Input;
use DB;
use Route;
use App\Models\VacancyDetail;
use App\Logic\MultiLanguageSelect;
class CategoryController extends Controller
{
    protected $input, $image, $requests;
    public $title;

    public function __construct(Articleimage $imageUpload, Request $request, Input $inputs)
    {
        $this->middleware('admin', ['only' => ['trash', 'destroy']]);

        $this->input = config('config.inputs.'.Route::currentRouteName());

        $this->image = $imageUpload;

        $this->title = 'Kateqoriyalar';
        $this->requests = $request->except('_token', '_method', 'category_id', 'return', 'edit', 'salary', 'end_date');


        if (in_array(strtolower($request->method()), ['put', 'post', 'patch', 'delete'])) {
            clearCache('pages');
        }


        view()->share('suitableInputs', $this->input);
        view()->share('allInputs', $inputs->allInputs());
    }



    public function index(CategoryDataTable $dataTable)
    {
        $breadcrumb = $this->breadcrumb();
        $breadcrumb->addCrumb($this->title);
        return $dataTable->render('app.category.index', ['title' => $this->title, "breadcrumb" => $breadcrumb->render()]);
    }


    public function trashed(CategoryDataTable $dataTable)
    {
        $breadcrumb = $this->breadcrumb();
        $breadcrumb->addCrumb($this->title, route("category.index"));
        $breadcrumb->addCrumb(trans('locale.trash'));
        return $dataTable->trashed(true)->render('app.category.index', ['title' => $this->title, "breadcrumb" => $breadcrumb->render()]);
    }


    public function create()
    {
        $breadcrumb = $this->breadcrumb();
        $breadcrumb->addCrumb($this->title, route("category.index"));
        $breadcrumb->addCrumb(trans('locale.create'));

        $query = Page::where('third_level', '<', 3)
            ->whereNotIn('slug', ['index', 'search'])
            ->select("id", "name", 'lang_id')
            ->orderBy("id", "asc")
            ->get();

        $category = MultiLanguageSelect::multiLang($query);


        return view('app.page.create',  ['category' => $category, "route" => "category", "breadcrumb" => $breadcrumb->render()]);
    }


    public function store(Request $request)
    {
        is_array($request->input('content_extra')) ? $content_extra = array_first(array_filter($request->input('content_extra'))) : $content_extra = $request->input('content_extra');

        $category = Page::find($request->input('category_id'));

        if(!$category){
            return redirect()->back()->withInput($request->input())->withErrors(["errors" => 'Kateqoriya seçilməyib.']);

        }


        if(!is_null($category->parent_id)){

            if($category->parent_id == $category->main_parent_id){
                $thirdLevel = 2;
            }
            else{
                $thirdLevel = 3;
            }

        }
        else{
            $thirdLevel = 1;
        }

        $this->requests['content_extra'] = $content_extra;
        $this->requests['lang_id'] = $category->lang_id;
        $this->requests['parent_id'] = $category->id;
        $this->requests['main_parent_id'] = is_null($category->parent_id) ? $category->id : $category->parent_id;
        $this->requests['order'] = Page::max('order')+1;
        $this->requests['third_level'] = $thirdLevel;





        if(!$request->has('relation_page')){
            $request->validate(array_merge(Page::categoryRules($category->lang_id), Page::$vacancyRules), Page::$messages);
        }
        else{
            $request->validate(Page::categoryRules($category->lang_id));
        }

        $this->requests['slug'] = Slug::slugify($request->input('slug'), $request->input('name'), 'pages', $category->lang_id);



        DB::beginTransaction();


        try{
            $page = Page::create($this->requests);
        }
        catch(\Exception $e){
            DB::rollback();
            return redirect()->back()->withInput($request->input())->withErrors(["errors" => $e->getMessage()]);
        }


        $uploadImage = $this->image->uploadImage('image_original',  $page, $this->resize($page->template_id));


        if(!$request->has('relation_page')){
            try{
                $page->relation_page = $page->id;
                $page->image_original = $uploadImage;
            }
            catch(\Exception $e){
                DB::rollback();
                return redirect()->back()->withInput($request->input())->withErrors(["errors" => $e->getMessage()]);
            }
        }

        $page->save();


        if($page->template_id == 9 && !$request->has('relation_page'))
        {
            try{
               VacancyDetail::create(['vacancy_id' => $page->id, 'salary' => $request->input('salary'), 'end_date' => $request->input('end_date')]);
            }
            catch(\Exception $e){
                DB::rollback();
                return redirect()->back()->withInput($request->input())->withErrors(["errors" => $e->getMessage()]);
            }
        }


        DB::commit();

        $request->session()->flash('message', "Yeni kateqoriya əlavə olundu");

        if($request->has('return'))
        {
            return redirect()->route('category.index');
        }
        else{
            if($request->has('edit')){
                return redirect()->back();
            }
            else{
                return redirect()->route('edit_category', $page->relation_page);
            }
        }
    }


    public function edit($id) //this is relation page
    {
        $relationPage = Page::where('relation_page', $id)->firstOrFail();

        $lang = request()->get('lang_id', $relationPage->lang_id);

        $page = Page::where('relation_page', $id)->where('lang_id', $lang)->first();

        $category = Page::where('third_level', '<', 3)
            ->where('id', '<>', $id)
            ->where('lang_id', $lang)
            ->whereNotIn('slug', ['index', 'search'])
            ->select("id", "name as text")
            ->orderBy("id", "asc")
            ->get()
            ->toJson();



        $breadcrumb = $this->breadcrumb();
        $breadcrumb->addCrumb($this->title, route("category.index"));
        $breadcrumb->addCrumb($relationPage->name, route('edit_category', $id));
        $breadcrumb->addCrumb(trans('locale.page'));



        if($page)
        {
            $keys = array_filter(explode(",", $page->meta_keywords));

            return view('app.page.edit', ['lang' => $page->lang_id, 'info' => $page, 'relPage' => $relationPage, 'edit' => true, "category" => $category, 'title' => $page->name, 'id' => $id, "breadcrumb" => $breadcrumb->render(), "keywords" => $keys, 'route' => 'category']);

        }
        else{

            return view('app.page.create', ['category' => $category, 'edit' => true, 'relPage' => $relationPage, "breadcrumb" => $breadcrumb->render(), 'id' => $id, 'lang' => $lang, 'title' => $relationPage->name, 'route' => 'category']);

        }

    }


    public function update(Request $request, $id)
    {

        is_array($request->input('content_extra')) ? $content_extra = array_first(array_filter($request->input('content_extra'))) : $content_extra = $request->input('content_extra');

        $page = Page::findOrFail($id);

        $category = Page::find($request->input('category_id'));

        if(!$category){
            return redirect()->back()->withInput($request->input())->withErrors(["errors" => 'Kateqoriya seçilməyib!']);
        }

        if(!is_null($category->parent_id)){

            if($category->parent_id == $category->main_parent_id){
                $thirdLevel = 2;
            }
            else{
                $thirdLevel = 3;
            }

        }
        else{
            $thirdLevel = 1;
        }

        $this->requests['content_extra'] = $content_extra;
        $this->requests['lang_id'] = $category->lang_id;
        $this->requests['parent_id'] = $category->id;
        $this->requests['main_parent_id'] = is_null($category->parent_id) ? $category->id : $category->parent_id;
        $this->requests['order'] = Page::max('order')+1;
        $this->requests['third_level'] = $thirdLevel;

        $request->validate(Page::categoryRules($category->lang_id, $id));

        $this->requests['slug'] = Slug::slugify($request->input('slug'), $request->input('name'), 'pages', $category->lang_id);
        $this->requests['image_original'] = $this->image->uploadImage('image_original', $page, $this->resize($page->template_id), true);


        try{
            foreach($this->requests as $key => $put){
                $page->$key = $put;
            }

            $page->save();
        }
        catch(\Exception $e){
            return redirect()->back()->withInput($request->input())->withErrors(["errors" => $e->getMessage()]);
        }


        $request->session()->flash('message', trans('locale.update_success'));

        if($request->has('return'))
        {
            return redirect()->route('category.index');
        }
        else{
            return redirect()->back();
        }
    }



    protected function resize($template_id)
    {

        $resizeImage = ['resize' => ['fit' => false, 'size' => [300, 300]], 'thumb' => null ];


        /*if($template_id == 4 || $template_id == 5 || $template_id == 6 || $template_id == 7)
        {
            $resizeImage = ['resize' => ['fit' => false, 'size' => [250, null]], 'thumb' => null ];
        }
        else{
            $resizeImage = ['resize' => ['fit' => true, 'size' => [555, 555]], 'thumb' => ['fit' => true, 'size' => [340, 340]] ];
        }*/

        return $resizeImage;
    }
}
