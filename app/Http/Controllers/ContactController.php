<?php

namespace App\Http\Controllers;

use App\Models\Page;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Models\Config;
use App\Models\Dictionary;
use App\Models\Question;
use App\Mail\ContactForm;
use App\Mail\Exam;
use Mail;
use Mcamara\LaravelLocalization\Facades\LaravelLocalization;

class ContactController extends Controller
{

    public $lang;

    public function __construct()
    {
        $this->lang = LaravelLocalization::getCurrentLocale();
    }


    public function contact(Request $request)
    {

        if(!$request->ajax()){
            return redirect()->route('home');
        }

        $config = Config::findOrFail(1);

        $dictionary = Dictionary::where('keyword', 'email_sent')->where('lang_id', $this->lang)->first();

        $content = $request->except('_token', '_method');

        if($request->has('page')){
            $page = Page::findOrFail($request->input('page'));

            $content['page'] = $page->name;

            $validator = Validator::make($content, Config::$applyRule);
        }
        elseif($request->has('popup')){

            $dictionary = Dictionary::where('keyword', 'popup_success_msg')->where('lang_id', $this->lang)->first();

            $validator = Validator::make($content, Config::$popupRule);
        }
        else{
            $validator = Validator::make($content, Config::$contactRule);
        }


        if ($validator->fails()) {

            $response = $this->responseArray(0, 'danger', $validator->errors()->first(), null);
            return $this->responseJson($response);
        }

        $email = $config->contact_form_email;


        try {
            Mail::to($email)->send(new ContactForm($content, 'contact', 'İstifadəçidən məktub'));

            $response = $this->responseArray(1, 'success', $dictionary->content, null, true, true);
            return $this->responseJson($response);
        } catch (\Exception $e) {
            $response = $this->responseArray(0, 'danger', $e->getMessage(), null);
            return $this->responseJson($response);
        }
    }



    public function test(Request $request)
    {
        $answers = $request->session()->get('answers');

        if(!$request->ajax()){
            return redirect()->route('home');
        }

        $config = Config::findOrFail(1);

        $dictionary = Dictionary::where('keyword', 'test_sent')->where('lang_id', $this->lang)->first();

        $content = $request->except('_token', '_method');

        $validator = Validator::make($content, Config::$testRule);


        if ($validator->fails()) {

            $response = $this->responseArray(0, 'danger', $validator->errors()->first(), null);
            return $this->responseJson($response);
        }

        if(!in_array($content['year'], range(date('Y') - 7, date('Y') - 100)))
        {
            $response = $this->responseArray(0, 'danger', "Error year field", null);
            return $this->responseJson($response);
        }

        $email = $config->contact_form_email;


        if (!$request->session()->has('answers')){
            $response = $this->responseArray(0, 'danger', "We can't retrieve your test result. Please, do test again and do not open any new window.", null);
            return $this->responseJson($response);
        }


        $result = Question::whereNotNull('parent_id')->whereIn('id', $answers)->sum('point');

        $questions = Question::whereIn('id', $answers)->pluck('parent_id')->toArray();


        $exam = Question::whereIn('id', $questions)->with('answer')->select('id', 'title', 'image_original')->orderBy('id', 'asc')->get();


        $content['point'] = $result;
        $content['answers'] = $answers;

        $pdf = \PDF::loadView('web.elements.exam',["exam" => $exam, 'content' => $content]);



        try {
            Mail::to($email)->send(new Exam($content, $pdf, 'Testin nəticəsi'));

            $response = $this->responseArray(1, 'success', $dictionary->content, null, true, false);
            return $this->responseJson($response);
        } catch (\Exception $e) {
            $response = $this->responseArray(0, 'danger', $e->getMessage(), null);
            return $this->responseJson($response);
        }
    }



    public function partner(Request $request)
    {

        if(!$request->ajax()){
            return redirect()->route('home');
        }

        $config = Config::findOrFail(1);

        $dictionary = Dictionary::where('keyword', 'email_sent')->where('lang_id', $this->lang)->first();

        $content = $request->except('_token', '_method');

        $page = Page::findOrFail($request->input('page'));

        $content['page'] = $page->name;


        $validator = Validator::make($content, Config::$partnerRule);


        if ($validator->fails()) {

            $response = $this->responseArray(0, 'danger', $validator->errors()->first(), null);
            return $this->responseJson($response);
        }

        $email = $config->contact_form_email;


        try {
            Mail::to($email)->send(new ContactForm($content, 'partner', 'Partnyorluq üçün müraciət'));

            $response = $this->responseArray(1, 'success', $dictionary->content, null, true, true);
            return $this->responseJson($response);
        } catch (\Exception $e) {
            $response = $this->responseArray(0, 'danger', $e->getMessage(), null);
            return $this->responseJson($response);
        }
    }
}
