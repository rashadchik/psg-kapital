<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\MarketChart;
use App\DataTables\MarketChartsDataTable;
use App\Logic\Input;
use DB;
use Route;

class MarketChartController extends Controller
{
    protected $input, $requests;
    public $title, $route, $view;

    public function __construct(Request $request, Input $inputs)
    {
        $this->middleware('admin');

        $this->input = config('config.inputs.'.Route::currentRouteName());
        $this->requests = $request->except('_token', '_method');

        $this->title = "Diagram";

        view()->share('suitableInputs', $this->input);
        view()->share('allInputs', $inputs->allInputs());
    }


    public function index(MarketChartsDataTable $dataTable)
    {

        $breadcrumb = $this->breadcrumb();
        $breadcrumb->addCrumb($this->title, route('charts.index'));

        return $dataTable->render("app.chart.index", ["breadcrumb" => $breadcrumb->render(),'title' =>  $this->title] );
    }

    public function create()
    {
        $breadcrumb = $this->breadcrumb();
        $breadcrumb->addCrumb($this->title, route('charts.index'));
        $breadcrumb->addCrumb(trans('locale.create'));

        return view("app.chart.create", ["breadcrumb" => $breadcrumb->render()]);
    }

    public function store(Request $request)
    {
        $request->validate(MarketChart::$rules, MarketChart::$messages);

        try{
            MarketChart::create($this->requests);
        }
        catch(\Exception $e){
            return redirect()->back()->withInput($request->input())->withErrors(["errors" => trans('locale.fail', ['number' => 1])]);
        }


        $request->session()->flash('message', "Əməliyyat uğurlu alındı");

        return redirect()->route('charts.index');
    }



    public function edit($id)
    {
        $chart = MarketChart::findOrFail($id);
        $breadcrumb = $this->breadcrumb();
        $breadcrumb->addCrumb($this->title, route('charts.index'));
        $breadcrumb->addCrumb(trans('locale.edit'));

        return view("app.chart.edit", ["info" => $chart, "breadcrumb" => $breadcrumb->render()]);
    }


    public function update(Request $request, $id)
    {
        $request->validate(MarketChart::$rules, MarketChart::$messages);

        $find = MarketChart::findOrFail($id);


        try{
            foreach($this->requests as $key => $input){
                $find -> $key = $input;
            }

            $find -> save();
        }
        catch(\Exception $e){
            return redirect()->back()->withInput($request->input())->withErrors(["errors" => trans('locale.fail', ['number' => 1])]);
        }


        $request->session()->flash('message', trans('locale.update_success'));

        return redirect()->route('charts.index');
    }


    public function destroy($id)
    {
        MarketChart::where('id', $id)->delete();

        request()->session()->flash('message', "Diagram silindi");

        return redirect()->route('charts.index');
    }
}
