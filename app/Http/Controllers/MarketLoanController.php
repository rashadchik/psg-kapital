<?php

namespace App\Http\Controllers;

use App\Models\MarketLoan;
use Illuminate\Http\Request;
use App\DataTables\MarketLoansDataTable;
use App\Logic\Input;
use DB;
use Route;

class MarketLoanController extends Controller
{
    protected $input, $requests;
    public $title, $route, $view;

    public function __construct(Request $request, Input $inputs)
    {
        $this->middleware('admin', ['only' => ['trash', 'destroy']]);

        $this->input = config('config.inputs.'.Route::currentRouteName());
        $this->requests = $request->except('_token', '_method');

        $this->title = "Istiqrazlar";

        view()->share('suitableInputs', $this->input);
        view()->share('allInputs', $inputs->allInputs());
    }


    public function index(MarketLoansDataTable $dataTable)
    {
        $breadcrumb = $this->breadcrumb();
        $breadcrumb->addCrumb($this->title, route('marketLoan.index'));
        return $dataTable->render("app.marketLoan.index", ["breadcrumb" => $breadcrumb->render(),'title' =>  $this->title] );
    }


    public function trashed(MarketLoansDataTable $dataTable)
    {
        $breadcrumb = $this->breadcrumb();
        $breadcrumb->addCrumb($this->title, route('loan.index'));
        $breadcrumb->addCrumb(trans('locale.trash'));

        return $dataTable->trashed(true)->render("app.loan.index", ["breadcrumb" => $breadcrumb->render()] );
    }


    public function create()
    {
        $breadcrumb = $this->breadcrumb();
        $breadcrumb->addCrumb($this->title, route('marketLoan.index'));
        $breadcrumb->addCrumb(trans('locale.create'));



        return view("app.marketLoan.create", ["breadcrumb" => $breadcrumb->render()]);
    }


    public function store(Request $request)
    {
        $request->validate(MarketLoan::$rules, MarketLoan::$messages);

        try{
            MarketLoan::create($this->requests);
        }
        catch(\Exception $e){
            return redirect()->back()->withInput($request->input())->withErrors(["errors" => trans('locale.fail', ['number' => 1])]);
        }


        $request->session()->flash('message', "Əməliyyat uğurlu alındı");

        return redirect()->route('marketLoan.index');
    }



    public function edit($id)
    {
        $MarketLoan = MarketLoan::findOrFail($id);
        $breadcrumb = $this->breadcrumb();
        $breadcrumb->addCrumb($this->title, route('marketLoan.index'));
        $breadcrumb->addCrumb(trans('locale.edit'));

        return view("app.marketLoan.edit", ["info" => $MarketLoan, "breadcrumb" => $breadcrumb->render()]);
    }


    public function update(Request $request, $id)
    {
        $find = MarketLoan::findOrFail($id);

        $request->validate(MarketLoan::$rules, MarketLoan::$messages);

        try{
            foreach($this->requests as $key => $input){
                $find -> $key = $input;
            }

            $find -> save();
        }
        catch(\Exception $e){
            return redirect()->back()->withInput($request->input())->withErrors(["errors" => trans('locale.fail', ['number' => 1])]);
        }


        $request->session()->flash('message', trans('locale.update_success'));

        return redirect()->route('marketLoan.index');
    }


    public function trash($id)
    {
        $find = MarketLoan::findOrFail($id);
        $find->delete();

        request()->session()->flash('message', "Məlumat silindi, qaytarılması mümkündür.");
        return redirect()->back();
    }


    public function restore($id)
    {
        $find = MarketLoan::onlyTrashed()->findOrFail($id);
        $find->restore();

        request()->session()->flash('message', "Məlumat qaytarıldı");
        return redirect()->route('marketLoan.index');
    }


    public function destroy($id)
    {
        $info = MarketLoan::onlyTrashed()->findOrFail($id);

        try{
            $info->forceDelete();
        }
        catch(\Exception $e){
            return redirect()->back()->withErrors(["errors" => trans('locale.fail', ['number' => 2])]);
        }


        request()->session()->flash('message', "Məlumat birdəfəlik silindi");
        return redirect()->back();
    }
}
