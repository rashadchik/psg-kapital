<?php

namespace App\Http\Controllers;

use App\Models\Subscriber;
use Illuminate\Http\Request;
use App\DataTables\SubscriberDataTable;
use Illuminate\Support\Facades\Validator;
use App\Models\Dictionary;
use Route;
use App\Logic\Input;
use Mcamara\LaravelLocalization\Facades\LaravelLocalization;
class SubscriberController extends Controller
{
    protected $input, $requests;
    public $title, $lang;

    public function __construct(Request $request, Input $inputs)
    {
        $this->middleware('admin', ['only' => ['destroy']]);

        $this->lang = LaravelLocalization::getCurrentLocale();
        $this->input = config('config.inputs.'.Route::currentRouteName());
        $this->requests = $request->except('_token', '_method');
        $this->title = "İzləyicilər";

        view()->share('suitableInputs', $this->input);
        view()->share('allInputs', $inputs->allInputs());
    }



    public function index(SubscriberDataTable $dataTable)
    {
        $breadcrumb = $this->breadcrumb();
        $breadcrumb->addCrumb($this->title);

        return $dataTable->render('app.subscriber.index', ['title' => $this->title, "breadcrumb" => $breadcrumb->render()]);
    }



    public function pending(SubscriberDataTable $dataTable)
    {
        $breadcrumb = $this->breadcrumb();
        $breadcrumb->addCrumb($this->title, route('subscriber.index'));
        $breadcrumb->addCrumb(trans('locale.pending'));
        return $dataTable->pending(true)->render('app.subscriber.index', ['title' => $this->title, "breadcrumb" => $breadcrumb->render()]);
    }



    public function create()
    {
        $breadcrumb = $this->breadcrumb();
        $breadcrumb->addCrumb($this->title, route("subscriber.index"));
        $breadcrumb->addCrumb(trans('locale.create'));

        return view('app.subscriber.create', ["breadcrumb" => $breadcrumb->render()]);
    }



    public function store(Request $request)
    {
        $inputs = [
            'full_name' => $request->input('full_name'),
            'email' => $request->input('email'),
            'status' => auth()->check() ? 1 : 0
        ];


        $validator = Validator::make($inputs, Subscriber::rules());

        if ($validator->fails()) {

            if($request->ajax()){
                $response = $this->responseArray(0, 'danger', $validator->errors()->first(), null);
                return $this->responseJson($response);
            }else{
                return redirect()->back()->withInput($request->input())->withErrors(["errors" => $validator->errors()->first()]);
            }

        }


        Subscriber::create($inputs);


        if($request->ajax())
        {

            $dictionary = Dictionary::where('keyword', 'subscribe_success')->where('lang_id', $this->lang)->first();
            
            $response = $this->responseArray(1, 'success', $dictionary->content, null, false, true);
            return $this->responseJson($response);
        }
        else{
            $request->session()->flash('message', "Yeni izləyici əlavə olundu");
            return redirect()->back();
        }
    }



    public function approve($id)
    {
        $user = Subscriber::findOrFail($id);
        $user->status = 1;
        $user->save();

        request()->session()->flash('message', "İzləyicinin istəyi qəbul olundu");
        return redirect()->back();
    }



    public function destroy($id)
    {
        $user = Subscriber::findOrFail($id);
        $user->delete();

        request()->session()->flash('message', "İzləyici siyahıdan silindi");
        return redirect()->back();
    }
}
