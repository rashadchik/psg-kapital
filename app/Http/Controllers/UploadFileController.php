<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Upload;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\File;
use DB;
use Illuminate\Support\Facades\Storage;

class UploadFileController extends Controller
{
    public function store(Request $request)
    {
        $token = $request->input('token');
        $file = $request->file('file');

        $validation = ['file' => $file];

        $validator = Validator::make($validation, Upload::$rules);


        if ($validator->fails() || !$request->has('token')) {

            return response()->json([
                'error' => true,
                'message' => $validator->messages()->first(),
                'code' => 400
            ], 400);

        }


        DB::beginTransaction();


        try{

            $fileName = $file->hashName();
            $file->store('uploads');

            Upload::create(['original_filename' => $file->getClientOriginalName(), 'filename' => $fileName, 'token' => $token, 'mime' => $file->getMimeType() ]);

        }
        catch(\Exception $e){
            DB::rollback();

            return response()->json([
                'error' => true,
                'message' => $e->getMessage(),
                'code' => 400
            ], 400);

        }


        DB::commit();

        return response()->json([
            'error' => false,
            'code'  => 200
        ], 200);

    }



    public function destroy(Request $request)
    {

        $originalName = $request->input('filename');
        $token = $request->input('token');

        $file = Upload::where('original_filename', $originalName)
            ->where('token', $token)
            ->first();


        if(empty($file))
        {
            return response()->json([
                'error' => true,
                'code'  => 400
            ], 400);

        }

        Storage::delete("uploads/$file->filename");
        $file->delete();


        return response()->json([
            'error' => false,
            'code'  => 200
        ], 200);
    }
}
