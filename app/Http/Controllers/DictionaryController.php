<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Dictionary;
use App\DataTables\DictionaryDataTable;
use App\Logic\Input;
use Route;
use DB;
class DictionaryController extends Controller
{
    public $title;
    protected $input, $requests;

    public function __construct(Request $request, Input $inputs)
    {
        $this->title = "Lüğət";

        if (in_array(strtolower($request->method()), ['put', 'patch', 'post'])) {
            clearCache('dictionary');
        }

        $this->input = config('config.inputs.'.Route::currentRouteName());

        $this->requests = $request->except('_token', '_method');

        view()->share('suitableInputs', $this->input);
        view()->share('allInputs', $inputs->allInputs());
    }


    public function index(DictionaryDataTable $dataTable)
    {
        $breadcrumb = $this->breadcrumb();
        $breadcrumb->addCrumb($this->title);


        return $dataTable->render('app.dictionary.index', ['title' => $this->title, "breadcrumb" => $breadcrumb->render()]);
    }


    public function create()
    {
        $breadcrumb = $this->breadcrumb();
        $breadcrumb->addCrumb($this->title, route('dictionary.index'));
        $breadcrumb->addCrumb(trans('locale.create'));

        return view('app.dictionary.create', ['title' => $this->title, "breadcrumb" => $breadcrumb->render() ]);
    }


    public function edit($id)
    {
        $breadcrumb = $this->breadcrumb();
        $breadcrumb->addCrumb($this->title, route('dictionary.index'));
        $breadcrumb->addCrumb(trans('locale.edit'));

        $word = Dictionary::findOrFail($id);

        $words = Dictionary::where('keyword', $word->keyword)->pluck('content', 'lang_id');


        return view('app.dictionary.edit', ['title' => $this->title, "breadcrumb" => $breadcrumb->render(), 'info' => $word, 'words' => $words]);
    }


    public function store(Request $request)
    {
        $inputs = $request->input('content');
        $keyword = $request->input('keyword');
        $editor = $request->input('editor');
        if($editor == ""){
            $editor =0;
        }

        $request->validate(Dictionary::$rules, Dictionary::$messages);


        DB::beginTransaction();


        foreach($inputs as $lang => $input){

            try{
                Dictionary::create(['keyword' => $keyword, 'content' => $input, 'lang_id' => $lang, 'type' => $lang,'editor' => $editor]);
            }
            catch(\Exception $e){
                DB::rollback();

                return redirect()->back()->withInput($request->input())->withErrors(["errors" => $e->getMessage()]);
            }

        }


        DB::commit();

        return redirect()->route('dictionary.index');
    }


    public function update(Request $request, $id)
    {
        $word = Dictionary::findOrFail($id);

        $inputs = $request->input('content');

        $request->validate(['content' => 'nullable']);

        $find = Dictionary::where('keyword', $word->keyword)->pluck('lang_id')->toArray();


        DB::beginTransaction();


        foreach($inputs as $lang => $input){

            if(in_array($lang, $find))
            {
                try{
                    Dictionary::where('keyword', $word->keyword)->where('lang_id', $lang)->update(['content' => $input]);
                }
                catch(\Exception $e){
                    DB::rollback();
                    return redirect()->back()->withInput($request->input())->withErrors(["errors" => $e->getMessage()]);
                }
            }
            else{
                try{
                    Dictionary::create(['keyword' => $word->keyword, 'content' => $input, 'lang_id' => $lang]);
                }
                catch(\Exception $e){
                    DB::rollback();

                    return redirect()->back()->withInput($request->input())->withErrors(["errors" => $e->getMessage()]);
                }
            }

        }


        DB::commit();


        $request->session()->flash('message', trans('locale.update_success'));

        return redirect()->route('dictionary.index');

    }
}
