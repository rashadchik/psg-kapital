<?php

namespace App\Http\Controllers;

use App\Logic\Input;
use Illuminate\Http\Request;
use App\Models\Config;
use App\Models\Page;
use App\Logic\ArticleImage;
use Cache;
use Route;

class ConfigController extends Controller
{
    protected $input, $image, $requests;
    public $resizeImage;

    public function __construct(Request $request, Input $inputs, ArticleImage $imageUpload)
    {
        $this->middleware('admin', ['only' => ['edit', 'update']]);

        $this->requests = $request->except('_token', '_method');
        $this->resizeImage = ['resize' => ['fit' => false, 'size' => [720, 720]], 'thumb' => ['fit' => false, 'size' => [720, 720]] ];
        $this->image = $imageUpload;
        $this->input = config('config.inputs.'.Route::currentRouteName());

        if (in_array(strtolower($request->method()), ['put', 'post', 'patch', 'delete'])) {
            Cache::forget('webConfig', false);
        }

        view()->share('suitableInputs', $this->input);
        view()->share('allInputs', $inputs->allInputs());
    }

    public function show($id)
    {
        $config = Config::findOrFail($id);

        $blog_page = Page::find($config->blog_page);

        if($blog_page)
        {
            $config->blog_page = $blog_page->name;
        }
        else{
            $config->blog_page = "Seçilməyib";
        }

        $title = "Konfiqurasiya";
        $breadcrumb = $this->breadcrumb();
        $breadcrumb->addCrumb('Tənzimləmələr', route('show_config', 1));
        $breadcrumb->addCrumb($title);

        return view('app.settings.config.show', ["breadcrumb" => $breadcrumb->render(), "title" => $title, 'show' => $config, 'id' => $id]);
    }


    public function edit($id)
    {
        $config = Config::findOrFail($id);

        $category = Page::where('lang_id', 'az')
            ->where('template_id', 2)
            ->select("relation_page as id", "name as text")
            ->ordered()
            ->get()
            ->toJson();

        return view('app.settings.config.edit', ['info' => $config, 'title' => trans('locale.edit'), 'id' => $id, 'category' => $category]);
    }

    public function update(Request $request, $id)
    {
        $config = Config::findOrFail($id);

        $request->validate(Config::$rules);

        foreach($this->requests as $key => $put){
            $config->$key = $put;
        }

        $config->save();

        $request->session()->flash('message', trans('locale.update_success'));
        return redirect()->back();
    }
}
