<?php

namespace App\Http\Controllers;

use App\Models\BrokerService;
use Illuminate\Http\Request;
use App\DataTables\ServiceDataTable;
use App\Logic\Input;
use App\Models\Page;
use DB;
use Route;

class ServiceController extends Controller
{
    protected $input, $requests;
    public $title, $route, $view;

    public function __construct(Request $request, Input $inputs)
    {
        $this->middleware('admin', ['only' => ['trash', 'destroy']]);

        $this->input = config('config.inputs.'.Route::currentRouteName());
        $this->requests = $request->except('_token', '_method');

        $this->title = "Xidmət və üstünlüklər";
        $this->route = "service";
        $this->view = "service";

        view()->share('suitableInputs', $this->input);
        view()->share('allInputs', $inputs->allInputs());
        view()->share('title', $this->title);
        view()->share('route', $this->route);
    }


    public function index(ServiceDataTable $dataTable)
    {
        $breadcrumb = $this->breadcrumb();
        $breadcrumb->addCrumb($this->title, route($this->route.'.index'));

        return $dataTable->render("app.$this->view.index", ["breadcrumb" => $breadcrumb->render()] );
    }


    public function trashed(ServiceDataTable $dataTable)
    {
        $breadcrumb = $this->breadcrumb();
        $breadcrumb->addCrumb($this->title, route($this->route.'.index'));
        $breadcrumb->addCrumb(trans('locale.trash'));

        return $dataTable->trashed(true)->render("app.$this->view.index", ["breadcrumb" => $breadcrumb->render()] );
    }


    public function create()
    {
        $breadcrumb = $this->breadcrumb();
        $breadcrumb->addCrumb($this->title, route($this->route.'.index'));
        $breadcrumb->addCrumb(trans('locale.create'));


        $category = Page::whereIn('template_id',  [6, 17, 18, 19, 27])
            ->select("relation_page as id", "name as text")
            ->groupBy('relation_page')
            ->orderBy("id", "asc")
            ->get()
            ->toJson();

        return view("app.$this->view.create", ["breadcrumb" => $breadcrumb->render(), "services" => $category]);
    }


    public function store(Request $request)
    {

        $request->validate(BrokerService::$rules, BrokerService::$messages);

        Page::where('id', $request->input('category_id'))->firstOrFail();


        try{
            BrokerService::create($this->requests);
        }
        catch(\Exception $e){
            return redirect()->back()->withInput($request->input())->withErrors(["errors" => trans('locale.fail', ['number' => 1])]);
        }


        $request->session()->flash('message', "Əməliyyat uğurlu alındı");

        return redirect()->route($this->route.'.index');
    }



    public function edit($id)
    {
        $info = BrokerService::findOrFail($id);

        $category = Page::whereIn('template_id',  [6, 17, 18, 19, 27])
            ->select("relation_page as id", "name as text")
            ->groupBy('relation_page')
            ->orderBy("id", "asc")
            ->get()
            ->toJson();


        $breadcrumb = $this->breadcrumb();
        $breadcrumb->addCrumb($this->title, route($this->route.'.index'));
        $breadcrumb->addCrumb(trans('locale.edit'));

        return view("app.$this->view.edit", ["info" => $info, "breadcrumb" => $breadcrumb->render(), 'category' => $category]);
    }


    public function update(Request $request, $id)
    {
        $find = BrokerService::findOrFail($id);

        $request->validate(BrokerService::$rules, BrokerService::$messages);

        try{
            foreach($this->requests as $key => $input){
                $find -> $key = $input;
            }

            $find -> save();
        }
        catch(\Exception $e){
            return redirect()->back()->withInput($request->input())->withErrors(["errors" => trans('locale.fail', ['number' => 1])]);
        }


        $request->session()->flash('message', trans('locale.update_success'));

        return redirect()->route($this->route.'.index');
    }


    public function trash($id)
    {
        $article = BrokerService::findOrFail($id);
        $article->delete();

        request()->session()->flash('message', "Məlumat silindi, qaytarılması mümkündür.");
        return redirect()->back();
    }


    public function restore($id)
    {
        $article = BrokerService::onlyTrashed()->findOrFail($id);
        $article->restore();

        request()->session()->flash('message', "Məlumat qaytarıldı");
        return redirect()->route($this->route.'.index');
    }


    public function destroy($id)
    {
        $info = BrokerService::onlyTrashed()->findOrFail($id);

        try{
            $info->forceDelete();
        }
        catch(\Exception $e){
            return redirect()->back()->withErrors(["errors" => trans('locale.fail', ['number' => 2])]);
        }


        request()->session()->flash('message', "Məlumat birdəfəlik silindi");
        return redirect()->back();
    }
}
