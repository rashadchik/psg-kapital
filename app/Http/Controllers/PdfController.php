<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Page;
class PdfController extends Controller
{


    public function show($slug)
    {
        $content = Page::where('slug', $slug)->firstOrFail();

        $pdfForm = \PDF::loadView('web.pdf.show', ["content" => $content]);

        return $pdfForm->setPaper('a4')->stream();

    }
}
