<?php

namespace App\Http\Controllers;

use App\Logic\ArticleImage;
use App\Models\Question;
use Illuminate\Http\Request;
use App\DataTables\QuestionDataTable;
use App\Logic\Input;
use DB;
use Route;

class QuestionController extends Controller
{
    protected $input, $image, $requests;
    public $title, $route, $view;

    public function __construct(ArticleImage $imageUpload, Request $request, Input $inputs)
    {
        $this->middleware('admin')->except(['result']);

        $this->input = config('config.inputs.'.Route::currentRouteName());
        $this->requests = $request->except('_token', '_method');
        $this->image = $imageUpload;

        $this->title = "Suallar";

        view()->share('suitableInputs', $this->input);
        view()->share('allInputs', $inputs->allInputs());
    }


    public function index(QuestionDataTable $dataTable)
    {
        $breadcrumb = $this->breadcrumb();
        $breadcrumb->addCrumb($this->title, route('question.index'));

        return $dataTable->render("app.question.index", ["breadcrumb" => $breadcrumb->render(),'title' =>  $this->title] );
    }


    public function result(Request $request)
    {
        $answers = $request->input('answer');

        if(empty($answers)){
            $result = 0;
            $response = $this->responseArray(0, 'success', "", null, false, false);
        }
        else{
            $result = Question::whereNotNull('parent_id')->whereIn('id', $answers)->sum('point');
            $response = $this->responseArray(1, 'success', $result, null, true, false);


            $request->session()->flash('answers', $answers);

        }

        return $this->responseJson($response);

    }



    public function create()
    {
        $breadcrumb = $this->breadcrumb();
        $breadcrumb->addCrumb($this->title, route('question.index'));
        $breadcrumb->addCrumb(trans('locale.create'));



        return view("app.question.create", ["breadcrumb" => $breadcrumb->render()]);
    }


    public function store(Request $request)
    {
        $request->validate(Question::$rules);

        DB::beginTransaction();


        try{
            $question = Question::create($this->requests);
        }
        catch(\Exception $e){
            DB::rollback();
            return redirect()->back()->withInput($request->input())->withErrors(["errors" => $e->getMessage()]);
        }

        $uploadImage = $this->image->uploadImage('image_original',  $question, $this->resize());


        try{
            $question->image_original = $uploadImage;
        }
        catch(\Exception $e){

            $this->image->delete($question->id, $uploadImage);

            DB::rollback();
            return redirect()->back()->withInput($request->input())->withErrors(["errors" => $e->getMessage()]);
        }

        DB::commit();


        $request->session()->flash('message', "Əməliyyat uğurlu alındı");

        return redirect()->route('question.index');
    }



    public function edit($id)
    {
        $question = Question::findOrFail($id);
        $breadcrumb = $this->breadcrumb();
        $breadcrumb->addCrumb($this->title, route('question.index'));
        $breadcrumb->addCrumb(trans('locale.edit'));

        return view("app.question.edit", ["info" => $question, "breadcrumb" => $breadcrumb->render()]);
    }


    public function update(Request $request, $id)
    {
        $request->validate(Question::$rules);

        $find = Question::findOrFail($id);

        $this->requests['image_original'] = $this->image->uploadImage('image_original', $find, $this->resize(), true);


        try{
            foreach($this->requests as $key => $input){
                $find -> $key = $input;
            }

            $find -> save();
        }
        catch(\Exception $e){
            return redirect()->back()->withInput($request->input())->withErrors(["errors" => trans('locale.fail', ['number' => 1])]);
        }


        $request->session()->flash('message', trans('locale.update_success'));

        return redirect()->route('question.index');
    }


    public function destroy($id)
    {
        Question::where('id', $id)->orWhere('parent_id', $id)->delete();

        request()->session()->flash('message', "Sual silindi");

        return redirect()->route('question.index');
    }


    protected function resize()
    {
        $resizeImage = ['resize' => ['fit' => false, 'size' => [1140, null]], 'thumb' => null ];

        return $resizeImage;
    }
}
