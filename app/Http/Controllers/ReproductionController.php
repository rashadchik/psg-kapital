<?php

namespace App\Http\Controllers;

use App\Models\Reproduction;
use Illuminate\Http\Request;
use App\DataTables\ReproductionDataTable;
use App\Logic\Input;
use DB;
use Route;

class ReproductionController extends Controller
{
    protected $input, $requests;
    public $title, $route, $view;

    public function __construct(Request $request, Input $inputs)
    {
        $this->middleware('admin', ['only' => ['trash', 'destroy']]);

        $this->input = config('config.inputs.'.Route::currentRouteName());
        $this->requests = $request->except('_token', '_method');

        $this->title = "Törəmə Alətləri";

        view()->share('suitableInputs', $this->input);
        view()->share('allInputs', $inputs->allInputs());
    }


    public function index(ReproductionDataTable $dataTable)
    {
        $breadcrumb = $this->breadcrumb();
        $breadcrumb->addCrumb($this->title, route('reproduction.index'));

        return $dataTable->render("app.reproduction.index", ["breadcrumb" => $breadcrumb->render(),'title' =>  $this->title] );
    }


    public function trashed(ReproductionDataTable $dataTable)
    {
        $breadcrumb = $this->breadcrumb();
        $breadcrumb->addCrumb($this->title, route('reproduction.index'));
        $breadcrumb->addCrumb(trans('locale.trash'));

        return $dataTable->trashed(true)->render("app.reproduction.index", ["breadcrumb" => $breadcrumb->render()] );
    }


    public function create()
    {
        $breadcrumb = $this->breadcrumb();
        $breadcrumb->addCrumb($this->title, route('reproduction.index'));
        $breadcrumb->addCrumb(trans('locale.create'));



        return view("app.reproduction.create", ["breadcrumb" => $breadcrumb->render()]);
    }


    public function store(Request $request)
    {
        $request->validate(Reproduction::$rules, Reproduction::$messages);

        try{
            Reproduction::create($this->requests);
        }
        catch(\Exception $e){
            return redirect()->back()->withInput($request->input())->withErrors(["errors" => trans('locale.fail', ['number' => 1])]);
        }


        $request->session()->flash('message', "Əməliyyat uğurlu alındı");

        return redirect()->route('reproduction.index');
    }



    public function edit($id)
    {
        $reproduction = Reproduction::findOrFail($id);
        $breadcrumb = $this->breadcrumb();
        $breadcrumb->addCrumb($this->title, route('reproduction.index'));
        $breadcrumb->addCrumb(trans('locale.edit'));

        return view("app.reproduction.edit", ["info" => $reproduction, "breadcrumb" => $breadcrumb->render()]);
    }


    public function update(Request $request, $id)
    {
        $find = Reproduction::findOrFail($id);

        $request->validate(Reproduction::$rules, Reproduction::$messages);

        try{
            foreach($this->requests as $key => $input){
                $find -> $key = $input;
            }

            $find -> save();
        }
        catch(\Exception $e){
            return redirect()->back()->withInput($request->input())->withErrors(["errors" => trans('locale.fail', ['number' => 1])]);
        }


        $request->session()->flash('message', trans('locale.update_success'));

        return redirect()->route('reproduction.index');
    }


    public function trash($id)
    {
        $reproduction = Reproduction::findOrFail($id);
        $reproduction->delete();

        request()->session()->flash('message', "Məlumat silindi, qaytarılması mümkündür.");
        return redirect()->back();
    }


    public function restore($id)
    {
        $reproduction = Reproduction::onlyTrashed()->findOrFail($id);
        $reproduction->restore();

        request()->session()->flash('message', "Məlumat qaytarıldı");
        return redirect()->route('reproduction.index');
    }


    public function destroy($id)
    {
        $info = Reproduction::onlyTrashed()->findOrFail($id);

        try{
            $info->forceDelete();
        }
        catch(\Exception $e){
            return redirect()->back()->withErrors(["errors" => trans('locale.fail', ['number' => 2])]);
        }


        request()->session()->flash('message', "Məlumat birdəfəlik silindi");
        return redirect()->back();
    }
}
