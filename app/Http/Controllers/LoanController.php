<?php

namespace App\Http\Controllers;

use App\Models\Loan;
use Illuminate\Http\Request;
use App\DataTables\LoansDataTable;
use App\Logic\Input;
use DB;
use Route;

class LoanController extends Controller
{
    protected $input, $requests;
    public $title, $route, $view;

    public function __construct(Request $request, Input $inputs)
    {
        $this->middleware('admin', ['only' => ['trash', 'destroy']]);

        $this->input = config('config.inputs.'.Route::currentRouteName());
        $this->requests = $request->except('_token', '_method');

        $this->title = "Istiqrazlar";

        view()->share('suitableInputs', $this->input);
        view()->share('allInputs', $inputs->allInputs());
    }


    public function index(LoansDataTable $dataTable)
    {
        $breadcrumb = $this->breadcrumb();
        $breadcrumb->addCrumb($this->title, route('loan.index'));

        return $dataTable->render("app.loan.index", ["breadcrumb" => $breadcrumb->render(),'title' =>  $this->title] );
    }


    public function trashed(LoansDataTable $dataTable)
    {
        $breadcrumb = $this->breadcrumb();
        $breadcrumb->addCrumb($this->title, route('loan.index'));
        $breadcrumb->addCrumb(trans('locale.trash'));

        return $dataTable->trashed(true)->render("app.loan.index", ["breadcrumb" => $breadcrumb->render()] );
    }


    public function create()
    {
        $breadcrumb = $this->breadcrumb();
        $breadcrumb->addCrumb($this->title, route('loan.index'));
        $breadcrumb->addCrumb(trans('locale.create'));



        return view("app.loan.create", ["breadcrumb" => $breadcrumb->render()]);
    }


    public function store(Request $request)
    {

        $request->validate(Loan::$rules, Loan::$messages);


        try{
            Loan::create($this->requests);
        }
        catch(\Exception $e){
            return redirect()->back()->withInput($request->input())->withErrors(["errors" => trans('locale.fail', ['number' => 1])]);
        }


        $request->session()->flash('message', "Əməliyyat uğurlu alındı");

        return redirect()->route('loan.index');
    }



    public function edit($id)
    {
        $loan = Loan::findOrFail($id);
        $breadcrumb = $this->breadcrumb();
        $breadcrumb->addCrumb($this->title, route('loan.index'));
        $breadcrumb->addCrumb(trans('locale.edit'));

        return view("app.loan.edit", ["info" => $loan, "breadcrumb" => $breadcrumb->render()]);
    }


    public function update(Request $request, $id)
    {
        $find = Loan::findOrFail($id);

        $request->validate(Loan::$rules, Loan::$messages);

        try{
            foreach($this->requests as $key => $input){
                $find -> $key = $input;
            }

            $find -> save();
        }
        catch(\Exception $e){
            return redirect()->back()->withInput($request->input())->withErrors(["errors" => trans('locale.fail', ['number' => 1])]);
        }


        $request->session()->flash('message', trans('locale.update_success'));

        return redirect()->route('loan.index');
    }


    public function trash($id)
    {
        $loan = Loan::findOrFail($id);
        $loan->delete();

        request()->session()->flash('message', "Məlumat silindi, qaytarılması mümkündür.");
        return redirect()->back();
    }


    public function restore($id)
    {
        $loan = Loan::onlyTrashed()->findOrFail($id);
        $loan->restore();

        request()->session()->flash('message', "Məlumat qaytarıldı");
        return redirect()->route('loan.index');
    }


    public function destroy($id)
    {
        $info = Loan::onlyTrashed()->findOrFail($id);

        try{
            $info->forceDelete();
        }
        catch(\Exception $e){
            return redirect()->back()->withErrors(["errors" => trans('locale.fail', ['number' => 2])]);
        }


        request()->session()->flash('message', "Məlumat birdəfəlik silindi");
        return redirect()->back();
    }
}
