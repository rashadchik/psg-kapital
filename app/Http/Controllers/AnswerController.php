<?php

namespace App\Http\Controllers;

use App\Models\Question;
use Illuminate\Http\Request;
use App\DataTables\AnswerDataTable;
use App\Logic\Input;
use DB;
use Route;
use App\Logic\MultiLanguageSelect;
class AnswerController extends Controller
{
    protected $input, $requests;
    public $title, $route, $view;

    public function __construct(Request $request, Input $inputs)
    {
        $this->middleware('admin');

        $this->input = config('config.inputs.'.Route::currentRouteName());
        $this->requests = $request->except('_token', '_method');

        $this->title = "Cavablar";

        view()->share('suitableInputs', $this->input);
        view()->share('allInputs', $inputs->allInputs());
    }


    public function index(AnswerDataTable $dataTable)
    {
        $breadcrumb = $this->breadcrumb();
        $breadcrumb->addCrumb($this->title, route('answer.index'));

        return $dataTable->render("app.answer.index", ["breadcrumb" => $breadcrumb->render(),'title' =>  $this->title] );
    }



    public function create()
    {
        $breadcrumb = $this->breadcrumb();
        $breadcrumb->addCrumb($this->title, route('answer.index'));
        $breadcrumb->addCrumb(trans('locale.create'));
        
        $query = Question::whereNull('parent_id')
            ->select("id", "title as name", 'lang_id')
            ->orderBy("id", "asc")
            ->get();

        $category = MultiLanguageSelect::multiLang($query);

        return view("app.answer.create", ["answers" => $category, "route" => "answer", "breadcrumb" => $breadcrumb->render()]);
    }


    public function store(Request $request)
    {
        $request->validate(Question::$rules);

        $question = Question::findOrFail($request->input('parent_id'));

        $this->requests['lang_id'] = $question->lang_id;

        try{
            Question::create($this->requests);
        }
        catch(\Exception $e){
            return redirect()->back()->withInput($request->input())->withErrors(["errors" => $e->getMessage()]);
        }


        $request->session()->flash('message', "Əməliyyat uğurlu alındı");

        return redirect()->route('answer.index');
    }



    public function edit($id)
    {
        $breadcrumb = $this->breadcrumb();
        $breadcrumb->addCrumb($this->title, route('answer.index'));
        $breadcrumb->addCrumb(trans('locale.edit'));

        $info = Question::findOrFail($id);

        $query = Question::whereNull('parent_id')
            ->select("id", "title as name", 'lang_id')
            ->orderBy("id", "asc")
            ->get();

        $category = MultiLanguageSelect::multiLang($query);

        return view('app.answer.edit', [ 'info' => $info, "answers" => $category, "breadcrumb" => $breadcrumb->render(), 'route' => 'answer']);
    }


    public function update(Request $request, $id)
    {
        $request->validate(Question::$rules);

        $question = Question::findOrFail($request->input('parent_id'));

        $this->requests['lang_id'] = $question->lang_id;

        $find = Question::findOrFail($id);

        try{
            foreach($this->requests as $key => $input){
                $find -> $key = $input;
            }

            $find -> save();
        }
        catch(\Exception $e){
            return redirect()->back()->withInput($request->input())->withErrors(["errors" => $e->getMessage()]);
        }


        $request->session()->flash('message', trans('locale.update_success'));

        return redirect()->route('answer.index');
    }


    public function destroy($id)
    {
        $answer = Question::findOrFail($id);
        $answer->delete();
        request()->session()->flash('message', "Cavab silindi");
        return redirect()->route('answer.index');

    }
}
