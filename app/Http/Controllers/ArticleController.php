<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\DataTables\ArticleDataTable;
use App\Models\Article;
use App\Models\Page;
use Illuminate\Support\Facades\File;
use App\Logic\Slug;
use App\Logic\ArticleImage;
use App\Logic\Input;
use App\Logic\MultiLanguageSelect;
use DB;
use Mail;
use Route;


class ArticleController extends Controller
{
    protected $input, $image, $requests, $resizeImage2;
    public $title;

    public function __construct(ArticleImage $imageUpload, Input $inputs, Request $request)
    {
        $this->middleware('admin', ['only' => ['trash', 'destroy']]);

        $this->image = $imageUpload;

        $this->input = config('config.inputs.'.Route::currentRouteName());
        $this->requests = $request->except('_token', '_method','return', 'edit');
        $this->title = "Xəbərlər";

        //$this->resizeImage2 = ['resize' => ['fit' => true, 'size' => [263, 378]], 'thumb' => null ];


        view()->share('suitableInputs', $this->input);
        view()->share('allInputs', $inputs->allInputs());
    }


    public function index(ArticleDataTable $dataTable)
    {
        $cat = $this->search();

        $breadcrumb = $this->breadcrumb();
        $breadcrumb->addCrumb($this->title, route("article.index"));
        return $dataTable->render('app.article.index', ['title' => $this->title, "breadcrumb" => $breadcrumb->render()], compact('cat'));
    }


    public function featured(ArticleDataTable $dataTable)
    {
        $cat = $this->search();

        $breadcrumb = $this->breadcrumb();
        $breadcrumb->addCrumb($this->title, route("article.index"));
        $breadcrumb->addCrumb(trans('locale.featured'));

        return $dataTable->featured(1)->render('app.article.index', ['title' => $this->title, "breadcrumb" => $breadcrumb->render()], compact('cat'));
    }

    public function trashed(ArticleDataTable $dataTable)
    {
        $cat = $this->search();

        $breadcrumb = $this->breadcrumb();
        $breadcrumb->addCrumb($this->title, route("article.index"));
        $breadcrumb->addCrumb(trans('locale.trash'));

        return $dataTable->trashed(true)->render('app.article.index', ['title' => $this->title, "breadcrumb" => $breadcrumb->render()], compact('cat'));
    }


    public function create()
    {
        $breadcrumb = $this->breadcrumb();
        $breadcrumb->addCrumb($this->title, route("article.index"));
        $breadcrumb->addCrumb(trans('locale.create'));


        $query = Page::where('template_id', 2)
            ->select("id", "name", 'lang_id')
            ->orderBy("id", "asc")
            ->get();

        $category = MultiLanguageSelect::multiLang($query);


        return view('app.article.create', ["category" => $category, "route" => "article", "breadcrumb" => $breadcrumb->render()]);
    }


    public function store(Request $request)
    {
        $category = Page::find($request->input('category_id'));

        $request->validate(Article::rules());

        $this->requests['featured'] = $request->input('featured', 0);
        $this->requests['main_category_id'] = is_null($category->parent_id) ? $category->id : $category->parent_id;
        $this->requests['published_by'] = auth()->user()->first_name.' '.auth()->user()->last_name;


        $this->requests['slug'] = Slug::slugify($request->input('slug'), $request->input('title'), 'articles', $category->lang_id );

        DB::beginTransaction();


        try{
            $article = Article::create($this->requests);
        }
        catch(\Exception $e){
            DB::rollback();
            return redirect()->back()->withInput($request->input())->withErrors(["errors" => $e->getMessage()]);
        }


        if(!$request->has('relation_page')){
            try{
                $article->relation_page = $article->id;
            }
            catch(\Exception $e){
                DB::rollback();
                return redirect()->back()->withInput($request->input())->withErrors(["errors" => $e->getMessage()]);
            }
        }


        $article->save();

        DB::commit();

        $request->session()->flash('message', "Xəbər əlavə edildi");

        if($request->has('return'))
        {
            return redirect()->route('article.index');
        }
        else{
            if($request->has('edit')){
                return redirect()->back();
            }
            else{
                return redirect()->route('edit_article', $article->relation_page);
            }
        }
    }



    public function edit($id)
    {

        $relationPage = Article::join('pages', 'pages.id', '=', 'articles.category_id')->where('articles.relation_page', $id)->select('articles.title', 'pages.lang_id', 'pages.template_id')->firstOrFail();

        $lang = request()->get('lang_id', $relationPage->lang_id);

        $page = Article::join('pages', 'pages.id', '=', 'articles.category_id')->where('articles.relation_page', $id)->where('pages.lang_id', $lang)->select('articles.*', 'pages.lang_id')->first();

        $category = Page::where('template_id', 2)
            ->where('lang_id', $lang)
            ->select("id", "name as text")
            ->orderBy("id", "asc")
            ->get()
            ->toJson();


        $breadcrumb = $this->breadcrumb();
        $breadcrumb->addCrumb($this->title, route("article.index"));
        $breadcrumb->addCrumb($relationPage->title, route('edit_article', $id));
        $breadcrumb->addCrumb(trans('locale.page'));


        if($page)
        {
            return view('app.article.edit', ['lang' => $page->lang_id, 'info' => $page, 'relPage' => $relationPage, 'edit' => true, 'editArticle' => true, "category" => $category, 'title' => $page->title, 'id' => $id, "breadcrumb" => $breadcrumb->render(), 'route' => 'article']);
        }
        else{
            return view('app.article.create', ['category' => $category, 'relPage' => $relationPage, 'edit' => true, 'editArticle' => true, "breadcrumb" => $breadcrumb->render(), 'id' => $id, 'lang' => $lang, 'title' => $relationPage->title, 'route' => 'article']);
        }

    }


    public function update(Request $request, $id)
    {
        $find = Article::findOrFail($id);

        $request->validate(Article::rules($find->id));

        $category = Page::findOrFail($request->input('category_id'));


        $this->requests['featured'] = $request->input('featured', 0);
        $this->requests['main_category_id'] = is_null($category->parent_id) ? $category->id : $category->parent_id;


        $this->requests['slug'] = Slug::slugify($request->input('slug'), $request->input('title'), 'articles', $category->lang_id );
        //$this->requests['image_original'] = $this->image->uploadImage('image_original', $find, $this->resize($category->template_id), true);
        //$this->requests['image_second'] = $this->image->uploadImage('image_second', $find, $this->resizeImage2, true);



        DB::beginTransaction();


        try{
            foreach($this->requests as $key => $input){
                $find -> $key = $input;
            }

            $find -> save();
        }
        catch(\Exception $e){
            DB::rollback();
            return redirect()->back()->withInput($request->input())->withErrors(["errors" => trans('locale.fail', ['number' => 1])]);
        }


        DB::commit();

        $request->session()->flash('message', trans('locale.update_success'));

        return redirect()->route('article.index');
    }


    public function trash($id)
    {
        $article = Article::findOrFail($id);
        $article->delete();

        request()->session()->flash('message', "Xəbər zibil qutusuna göndərildi");
        return redirect()->back();
    }


    public function restore($id)
    {
        $article = Article::onlyTrashed()->findOrFail($id);
        $article->restore();

        request()->session()->flash('message', "Xəbər zibil qutusundan qaytarıldı");
        return redirect()->route('article.index');
    }


    public function destroy($id)
    {
        $article = Article::onlyTrashed()->findOrFail($id);

        DB::beginTransaction();


        try{
            $article->forceDelete();
        }
        catch(\Exception $e){
            DB::rollback();
            return redirect()->back()->withErrors(["errors" => trans('locale.fail', ['number' => 2])]);
        }


        DB::commit();

        request()->session()->flash('message', "Xəbər birdəfəlik silindi");
        return redirect()->back();
    }


    protected function search()
    {
        if(!request()->ajax())
        {
            $cat = Page::where('template_id', 2)->whereNotNull('parent_id')->orderBy('id')->pluck('name', 'id');
            $cat->prepend('Kateqoriya üzrə', 0);

            return $cat;
        }
    }


    /*protected function resize($template_id)
    {
        if($template_id == 2){
            $resizeImage = ['resize' => ['fit' => false, 'size' => [900, null]], 'thumb' => ['fit' => true, 'size' => [431, 364]] ];

        }
        else{
            $resizeImage = ['resize' => ['fit' => false, 'size' => [500, null]], 'thumb' => ['fit' => true, 'size' => [432, 431]] ];

        }

        return $resizeImage;
    }*/

}
