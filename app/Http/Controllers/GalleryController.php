<?php

namespace App\Http\Controllers;

use App\Models\Gallery;
use Illuminate\Http\Request;
use App\Logic\ImageRepository;
use Illuminate\Support\Facades\File;
use App\Models\Page;
use App\Models\Article;

class GalleryController extends Controller
{
    protected $image;
    public $maxFileSize;

    public function __construct(Request $request, ImageRepository $imageRepository)
    {
        $this->maxFileSize = 10;
        $this->image = $imageRepository;

    }


    public function index($relPageId, $type)
    {
        $id = $relPageId;
        $description = config("config.galleryPlugins.$type.title");
        $route = config("config.galleryPlugins.$type.route");
        $dbType = config("config.galleryPlugins.$type.type");

        if($route == 'article'){
            $title = ['Xəbərlər', $description];

            $page = Article::join('pages', 'pages.id', '=', 'articles.category_id')->where('articles.id', $id)->select('articles.*', 'pages.template_id')->firstOrfail();

            $name = $page->title;

        }
        else{
            $title = ['Səhifələr', $description];

            $page = Page::findOrfail($id);

            $name = $page->name;

            if(is_null($page->parent_id)){
                $route = 'page';

            }
            else{
                $route = 'category';
            }
        }


        $icon_dir = $this->image->iconPath($id);


        $gallery = Gallery::where('category_id', $id)->where('type', $dbType)->count();

        $breadcrumb = $this->breadcrumb();

        if(is_array($title))
        {
            $breadcrumb->addCrumb($title[0], route("$route.index"));
            $breadcrumb->addCrumb($name, route("edit_$route", $id));

            $breadcrumb->addCrumb($title[1]);
        }
        else{
            $breadcrumb->addCrumb($title);
        }


        if($gallery == 0){
            return view('app.article.gallery.create', ['galleryPage' => true, 'id' => $id, 'type' => $type, 'title' => $name, 'maxFileSize' => $this->maxFileSize, "breadcrumb" => $breadcrumb->render(), 'edit' => true, 'route' => $route, 'info' => $page, 'relPage' => $page]);
        }
        else{
            return view('app.article.gallery.edit', ['galleryPage' => true, 'id' => $id, 'type' => $type, 'title' => $name, 'maxFileSize' => $this->maxFileSize, 'dir' => $icon_dir, "breadcrumb" => $breadcrumb->render(), 'edit' => true, 'route' => $route, 'info' => $page, 'relPage' => $page]);
        }
    }


    public function store(Request $request, $id, $type)
    {
        $resizeImage = null;
        $maxImageCount = config("config.galleryPlugins.$type.maxFile");
        $route = config("config.galleryPlugins.$type.route");
        $dbType = config("config.galleryPlugins.$type.type");

        if($route == 'page'){

            $page = Page::findOrfail($id);

        }
        else{

            $page = Article::leftJoin('pages', 'pages.id', '=', 'articles.category_id')->where('articles.id', $id)->firstOrfail();
        }

        $galleryCount = Gallery::where('category_id', $id)->where('type', $type)->count();


        if($galleryCount < $maxImageCount){
            $resizeImage = config("config.galleryPlugins.$type.image");
        }
        else{
            return response()->json([
                'error' => true,
                'message' => $maxImageCount.' ədəddən artıq şəkil yüklənə bilməz.',
                'code' => 300
            ], 500);

        }

        $photo = $request->all();

        $response = $this->image->upload($photo, $id, $dbType, $resizeImage);
        return $response;
    }


    public function getImages($id, $type)
    {

        $dbType = config("config.galleryPlugins.$type.type");


        $images = Gallery::where('category_id', $id)->where('type', $dbType)->get(['filename', 'category_id', 'original_filename']);

        $imageAnswer = [];

        foreach ($images as $image) {
            $imageAnswer[] = [
                'id' => $image->id,
                'original' => $image->original_filename,
                'server' => $image->filename,
                'size' => File::size($this->image->originalPathPhoto($id, $image->filename))
            ];
        }

        return response()->json([
            'images' => $imageAnswer
        ]);
    }


    public function destroy(Request $request, $article_id, $type)
    {
        $dbType = config("config.galleryPlugins.$type.type");
        $filename = $request->input('filename');

        if(!$filename)
        {
            return 0;
        }

        $response = $this->image->delete( $filename, $article_id, $dbType);

        return $response;
    }


    public function getDownload($id, $filename)
    {
        $path = $this->image->originalPathPhoto($id, $filename);

        $file= public_path(). "/$path";

        $headers = array(
            'Content-Type: image/jpg',
        );

        return response()->download($file, $filename, $headers);
    }
}
