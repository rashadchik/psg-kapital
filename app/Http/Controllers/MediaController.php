<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\DataTables\MediaDataTable;
use App\Models\Media;
use App\Models\Page;
use App\Logic\ArticleImage;
use DB;
use Mail;
use Route;
use App\Logic\Input;
class MediaController extends Controller
{
    protected $input, $image, $requests, $resizeImage;
    public $title;

    public function __construct(ArticleImage $imageUpload, Input $inputs, Request $request)
    {
        $this->middleware('admin', ['only' => ['trash', 'destroy']]);

        $this->image = $imageUpload;
        $this->input = config('config.inputs.'.Route::currentRouteName());
        $this->title = "Media";
        $this->requests = $request->except('_token', '_method');
        $this->resizeImage = ['resize' => ['fit' => true, 'size' => [360, 200]], 'thumb' => null ];

        view()->share('suitableInputs', $this->input);
        view()->share('allInputs', $inputs->allInputs());
    }


    public function index(MediaDataTable $dataTable)
    {
        $breadcrumb = $this->breadcrumb();
        $breadcrumb->addCrumb($this->title, route("media.index"));

        return $dataTable->render('app.media.index', ['title' => $this->title, "breadcrumb" => $breadcrumb->render()]);
    }


    public function featured(MediaDataTable $dataTable)
    {
        $breadcrumb = $this->breadcrumb();
        $breadcrumb->addCrumb($this->title, route("media.index"));
        $breadcrumb->addCrumb(trans('locale.featured'));

        return $dataTable->featured(1)->render('app.media.index', ['title' => $this->title, "breadcrumb" => $breadcrumb->render()]);
    }

    public function trashed(MediaDataTable $dataTable)
    {
        $breadcrumb = $this->breadcrumb();
        $breadcrumb->addCrumb($this->title, route("media.index"));
        $breadcrumb->addCrumb(trans('locale.trash'));

        return $dataTable->trashed(true)->render('app.media.index', ['title' => $this->title, "breadcrumb" => $breadcrumb->render()]);
    }


    public function create()
    {
        $breadcrumb = $this->breadcrumb();
        $breadcrumb->addCrumb($this->title, route("media.index"));
        $breadcrumb->addCrumb(trans('locale.create'));

        $category = Page::where('template_id', 3)
            ->where('third_level', 1)
            ->select('relation_page as id', 'name as text')
            ->orderBy("relation_page", "asc")
            ->groupBy('relation_page')
            ->get()
            ->toJson();

        return view('app.media.create', ["category" => $category, "breadcrumb" => $breadcrumb->render()]);
    }


    public function store(Request $request)
    {
        $request->validate(Media::rules());

        DB::beginTransaction();

        Page::findOrFail($request->input('doctor_id'));

        try{
            $media = Media::create($this->requests);
        }
        catch(\Exception $e){
            DB::rollback();
            return redirect()->back()->withInput($request->input())->withErrors(["errors" => $e->getMessage()]);
        }


        $uploadImage = $this->image->uploadImage('image_original', $media, $this->resizeImage);

        try{
            $media->image_original = $uploadImage;
        }
        catch(\Exception $e) {

            $this->image->delete($media->id, $uploadImage);

            DB::rollback();
            return redirect()->back()->withInput($request->input())->withErrors(["errors" => $e->getMessage()]);

        }

        $media->save();

        DB::commit();

        $request->session()->flash('message', "Media əlavə edildi");

        return redirect()->route('media.index');
    }



    public function edit($id)
    {
        $info = Media::findOrFail($id);

        $breadcrumb = $this->breadcrumb();
        $breadcrumb->addCrumb($this->title, route("media.index"));
        $breadcrumb->addCrumb(trans('locale.edit'));

        $category = Page::where('template_id', 3)
            ->where('third_level', 1)
            ->select('relation_page as id', 'name as text')
            ->orderBy("relation_page", "asc")
            ->groupBy('relation_page')
            ->get()
            ->toJson();


        return view('app.media.edit', ["category" => $category, "info" => $info, "breadcrumb" => $breadcrumb->render()]);
    }


    public function update(Request $request, $id)
    {
        $find = Media::findOrFail($id);

        $request->validate(Media::rules());

        Page::findOrFail($request->input('doctor_id'));

        $this->requests['image_original'] = $this->image->uploadImage('image_original', $find, $this->resizeImage, true);


        try{
            foreach($this->requests as $key => $input){
                $find -> $key = $input;
            }

            $find -> save();
        }
        catch(\Exception $e){
            return redirect()->back()->withInput($request->input())->withErrors(["errors" => trans('locale.fail', ['number' => 1])]);
        }


        $request->session()->flash('message', trans('locale.update_success'));

        return redirect()->route('media.index');
    }


    public function trash($id)
    {
        $article = Media::findOrFail($id);
        $article->delete();

        request()->session()->flash('message', "Media silindi, qaytarılması mümkündür.");
        return redirect()->back();
    }


    public function restore($id)
    {
        $article = Media::onlyTrashed()->findOrFail($id);
        $article->restore();

        request()->session()->flash('message', "Media qaytarıldı");
        return redirect()->route('media.index');
    }


    public function destroy($id)
    {
        $article = Media::onlyTrashed()->findOrFail($id);


        try{
            $article->forceDelete();
        }
        catch(\Exception $e){
            DB::rollback();
            return redirect()->back()->withErrors(["errors" => trans('locale.fail', ['number' => 2])]);
        }


        request()->session()->flash('message', "Media birdəfəlik silindi");
        return redirect()->back();
    }
}
