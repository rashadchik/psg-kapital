<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Cache;
use App\Models\Category;

class DashboardController extends Controller
{

    public function index()
    {

        $breadcrumb = $this->breadcrumb();
        //return view('app.index', ["breadcrumb" => $breadcrumb->render()]);

        return redirect()->route('article.index');
    }

}
