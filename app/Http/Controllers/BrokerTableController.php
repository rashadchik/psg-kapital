<?php

namespace App\Http\Controllers;

use App\Models\BrokerTable;
use Illuminate\Http\Request;
use App\DataTables\BrokerTableDataTable;
use App\Logic\Input;
use App\Models\Page;
use DB;
use Route;

class BrokerTableController extends Controller
{
    protected $input, $requests;
    public $title, $route, $view;

    public function __construct(Request $request, Input $inputs)
    {
        $this->middleware('admin', ['only' => ['trash', 'destroy']]);

        $this->input = config('config.inputs.'.Route::currentRouteName());
        $this->requests = $request->except('_token', '_method');

        $this->title = "Broker üçün cədvəl";
        $this->route = "brokerTable";
        $this->view = "service";

        view()->share('suitableInputs', $this->input);
        view()->share('allInputs', $inputs->allInputs());
        view()->share('title', $this->title);
        view()->share('route', $this->route);
    }


    public function index(BrokerTableDataTable $dataTable)
    {
        $breadcrumb = $this->breadcrumb();
        $breadcrumb->addCrumb($this->title, route($this->route.'.index'));

        return $dataTable->render("app.$this->view.index", ["breadcrumb" => $breadcrumb->render()] );
    }


    public function trashed(BrokerTableDataTable $dataTable)
    {
        $breadcrumb = $this->breadcrumb();
        $breadcrumb->addCrumb($this->title, route($this->route.'.index'));
        $breadcrumb->addCrumb(trans('locale.trash'));

        return $dataTable->trashed(true)->render("app.$this->view.index", ["breadcrumb" => $breadcrumb->render()] );
    }


    public function create()
    {
        $breadcrumb = $this->breadcrumb();
        $breadcrumb->addCrumb($this->title, route($this->route.'.index'));
        $breadcrumb->addCrumb(trans('locale.create'));


        $category = Page::where('template_id',  6)
            ->where('third_level', 1)
            ->select("relation_page as id", "name as text")
            ->groupBy('relation_page')
            ->orderBy("id", "asc")
            ->get()
            ->toJson();

        return view("app.$this->view.create", ["breadcrumb" => $breadcrumb->render(), "services" => $category]);
    }


    public function store(Request $request)
    {

        $request->validate(BrokerTable::$rules, BrokerTable::$messages);

        Page::where('id', $request->input('category_id'))->firstOrFail();


        try{
            BrokerTable::create($this->requests);
        }
        catch(\Exception $e){
            return redirect()->back()->withInput($request->input())->withErrors(["errors" => trans('locale.fail', ['number' => 1])]);
        }


        $request->session()->flash('message', "Əməliyyat uğurlu alındı");

        return redirect()->route($this->route.'.index');
    }



    public function edit($id)
    {
        $info = BrokerTable::findOrFail($id);

        $category = Page::where('template_id',  6)
            ->where('third_level', 1)
            ->select("relation_page as id", "name as text")
            ->groupBy('relation_page')
            ->orderBy("id", "asc")
            ->get()
            ->toJson();


        $breadcrumb = $this->breadcrumb();
        $breadcrumb->addCrumb($this->title, route($this->route.'.index'));
        $breadcrumb->addCrumb(trans('locale.edit'));

        return view("app.$this->view.edit", ["info" => $info, "breadcrumb" => $breadcrumb->render(), 'category' => $category]);
    }


    public function update(Request $request, $id)
    {
        $find = BrokerTable::findOrFail($id);

        $request->validate(BrokerTable::$rules, BrokerTable::$messages);

        try{
            foreach($this->requests as $key => $input){
                $find -> $key = $input;
            }

            $find -> save();
        }
        catch(\Exception $e){
            return redirect()->back()->withInput($request->input())->withErrors(["errors" => trans('locale.fail', ['number' => 1])]);
        }


        $request->session()->flash('message', trans('locale.update_success'));

        return redirect()->route($this->route.'.index');
    }


    public function trash($id)
    {
        $article = BrokerTable::findOrFail($id);
        $article->delete();

        request()->session()->flash('message', "Məlumat silindi, qaytarılması mümkündür.");
        return redirect()->back();
    }


    public function restore($id)
    {
        $article = BrokerTable::onlyTrashed()->findOrFail($id);
        $article->restore();

        request()->session()->flash('message', "Məlumat qaytarıldı");
        return redirect()->route($this->route.'.index');
    }


    public function destroy($id)
    {
        $info = BrokerTable::onlyTrashed()->findOrFail($id);

        try{
            $info->forceDelete();
        }
        catch(\Exception $e){
            return redirect()->back()->withErrors(["errors" => trans('locale.fail', ['number' => 2])]);
        }


        request()->session()->flash('message', "Məlumat birdəfəlik silindi");
        return redirect()->back();
    }
}
