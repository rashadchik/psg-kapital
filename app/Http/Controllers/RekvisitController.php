<?php

namespace App\Http\Controllers;

use App\Logic\Input;
use Illuminate\Http\Request;
use App\Models\Rekvisit;
use Route;

class RekvisitController extends Controller
{
    protected $input,$requests;

    public function __construct(Request $request, Input $inputs)
    {
        $this->middleware('admin', ['only' => ['edit', 'update']]);

        $this->requests = $request->except('_token', '_method');
        $this->input = config('config.inputs.'.Route::currentRouteName());

        view()->share('suitableInputs', $this->input);
        view()->share('allInputs', $inputs->allInputs());
    }

    public function show($id)
    {
        $rekvisit = Rekvisit::findOrFail($id);

        $title = "Rekvisitlər";
        $breadcrumb = $this->breadcrumb();
        $breadcrumb->addCrumb('rekvisit', route('show_rekvisit', 1));
        $breadcrumb->addCrumb($title);

        return view('app.rekvisit.show', ["breadcrumb" => $breadcrumb->render(), "title" => $title, 'show' => $rekvisit, 'id' => $id]);
    }


    public function edit($id)
    {
        $rekvisit = Rekvisit::findOrFail($id);

        return view('app.rekvisit.edit', ['info' => $rekvisit, 'title' => trans('locale.edit'), 'id' => $id]);
    }

    public function update(Request $request, $id)
    {
        $rekvisit = Rekvisit::findOrFail($id);

         $request->validate(Rekvisit::$rules, Rekvisit::$messages);

        foreach($this->requests as $key => $put){
            $rekvisit->$key = $put;
        }

        $rekvisit->save();

        $request->session()->flash('message', trans('locale.update_success'));
        return redirect()->back();
    }
}
