<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Config;
use Cache;
use App\Logic\ImageUpload;

class LogoController extends Controller
{
    public function __construct(Request $request, ImageUpload $logoUpload)
    {
        $this->middleware('admin', ['only' => ['update']]);

        $this->imageResize = [300, 300];

        $this->image = $logoUpload;

        view()->share('inputsArray', config('inputs'));

        if (in_array(strtolower($request->method()), ['put', 'post', 'patch', 'delete'])) {
            Cache::forget('webConfig');
        }

        $this->title = "Logo";
    }


    public function index()
    {
        $logo = Config::findOrFail(1);

        $breadcrumb = $this->breadcrumb();
        $breadcrumb->addCrumb('Tənzimləmələr', route('show_config', 1));
        $breadcrumb->addCrumb($this->title);

        return view('app.settings.logo.index', ["breadcrumb" => $breadcrumb->render(), "title" => $this->title, 'logo' => $logo]);
    }


    public function update(Request $request, $id)
    {
        $logo = Config::findOrFail(1);

        $logo_original = $logo->getOriginal('logo');


        if($request->has('logo') && ($request->input('logo') != $logo_original)){

            $startUpload = $this->image->upload($request->input('logo'), $this->imageResize);

            if($startUpload == false){
                return redirect()->back()->withInput($request->input())->withErrors(["errors" => trans('locale.upload_image_error')]);
            }
            else{
                $logo->logo = $startUpload;
            }
        }

        $logo->save();

        $request->session()->flash('message', trans('locale.update_success'));

        return redirect()->route('logo_index');

    }
}
