<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Config;
use DB;
use Carbon\Carbon;

class SiteMapController extends Controller
{
    public $title;

    public function __construct(Request $request)
    {
        $this->middleware('admin');

        $this->title = "Sitemap";
    }


    public function index()
    {
        $config = Config::findOrFail(1);

        $breadcrumb = $this->breadcrumb();
        $breadcrumb->addCrumb($this->title);

        return view('app.sitemap.index', ["breadcrumb" => $breadcrumb->render(), "title" => $this->title, "config" => $config]);
    }


    public function store(Request $request)
    {
        $config = Config::findOrFail(1);

        $page = DB::table('pages as p')
            ->leftJoin('galleries', function ($join) {
                $join->on('p.relation_page', '=', 'galleries.category_id')->where('galleries.type', 4);
            })
            ->select('p.id', 'p.relation_page', 'p.slug', 'p.lang_id', 'galleries.filename as image', 'p.name', 'p.updated_at', DB::raw('1 as type'))
            ->orderBy('id', 'desc');

        $category = DB::table('articles as a')
            ->join('pages as c', 'c.id', '=', 'a.category_id')
            ->leftJoin('galleries', function ($join) {
                $join->on('a.relation_page', '=', 'galleries.category_id')->where('galleries.type', 6);
            })
            ->select('a.id', 'a.relation_page', 'a.slug', 'c.lang_id', 'galleries.filename as image', 'a.title as name', 'a.updated_at',  DB::raw('2 as type'))
            ->orderBy('a.id', 'desc')
            ->union($page)
            ->get();


        $sitemap = app()->make("sitemap");


        foreach ($category as $post)
        {
            $post->slug == 'index' ? $slug = '' : $slug = $post->slug;


            if(!is_null($post->image))
            {
                $images = [
                    ['url' => asset('files/cache/images/'.$post->relation_page.'/'.$post->image), 'title' => $post->name, 'caption' => $post->name],
                ];
            }
            else{
                $images = 0;
            }


            $sitemap->add(url("/$post->lang_id/".$slug),  Carbon::parse($post->updated_at), '1.0', 'monthly', $images);
        }

        $sitemap->store('xml', 'sitemap');

        $config->sitemap = Carbon::now();
        $config->save();

        $request->session()->flash('message', trans('locale.store_success'));

        return redirect()->back();
    }
}
