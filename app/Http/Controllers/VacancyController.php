<?php

namespace App\Http\Controllers;

use App\Models\Page;
use Illuminate\Http\Request;
use App\Models\VacancyDetail;
use App\Models\Config;
use App\Models\Dictionary;
use Illuminate\Support\Facades\Validator;
use App\Logic\Input;
use Route;
use DB;
use Cache;
use Mail;
use App\Mail\VacancyForm;
use Mcamara\LaravelLocalization\Facades\LaravelLocalization;

class VacancyController extends Controller
{
    protected $input;
    public $title, $lang;


    public function __construct(Request $request, Input $inputs)
    {
        $this->middleware('admin', ['only' => ['trash', 'destroy']]);

        $this->lang = LaravelLocalization::getCurrentLocale();

        $this->title = "Kateqoriyalar";

        $this->input = config('config.inputs.'.Route::currentRouteName());

        view()->share('suitableInputs', $this->input);
        view()->share('allInputs', $inputs->allInputs());
    }




    public function apply(Request $request, $id)
    {
        $vacancy = Page::findOrFail($id);

        $config = Config::findOrFail(1);

        $dictionary = Dictionary::where('keyword', 'cv_sent')->where('lang_id', $this->lang)->first();

        $inputs = array(
            'resume' => $request->file('resume'),
            'page' => $vacancy->name

        );

        $rules = array(
            'resume' => 'required|mimes:doc,docx,pdf|max:10240',
        );


        $validator = Validator::make($inputs, $rules);

        if ($validator->fails()) {

            $response = $this->responseArray(0, 'danger', $validator->errors()->first(), null);
            return $this->responseJson($response);
        }


        try {
            Mail::to($config->hr_form_email)->send(new VacancyForm($inputs, $request->file('resume'), 'Vakansiya üçün müraciət var'));

            $response = $this->responseArray(1, 'success', $dictionary->content, null, true, true);
            return $this->responseJson($response);
        } catch (\Exception $e) {
            $response = $this->responseArray(0, 'danger', trans('locale.error_send_message'), null);
            return $this->responseJson($response);
        }
    }


    public function edit($relPageId)
    {
        $id = $relPageId;

        $page = Page::findOrFail($id);

        $vacancy = VacancyDetail::where('vacancy_id', $id)->firstOrFail();

        $breadcrumb = $this->breadcrumb();
        $breadcrumb->addCrumb($this->title, route("category.index"));
        $breadcrumb->addCrumb($page->name, route('edit_category', $id));
        $breadcrumb->addCrumb('Əmək haqqı və son tarix');

        return view('app.vacancy.edit', ['id' => $id,  'title' => $page->name, "breadcrumb" => $breadcrumb->render(), 'edit' => true, 'info' => $vacancy, 'route' => 'category', 'relPage' => $page, 'dynamicPage' => true]);
    }



    public function update(Request $request, $id)
    {
        $vacancy = VacancyDetail::findOrFail($id);

        $request->validate(VacancyDetail::$rules, VacancyDetail::$messages);

        $vacancy->salary = $request->input('salary');
        $vacancy->end_date = $request->input('end_date');

        $vacancy->save();

        $request->session()->flash('message', trans('locale.update_success'));
        return redirect()->back();
    }

}
