<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\StrategyChart2;
use App\DataTables\StrategyChart2sDataTable;
use App\Logic\Input;
use DB;
use Route;

class StrategyChart2Controller extends Controller
{
   protected $input, $requests;
    public $title, $route, $view;

    public function __construct(Request $request, Input $inputs)
    {
        $this->middleware('admin');

        $this->input = config('config.inputs.'.Route::currentRouteName());
        $this->requests = $request->except('_token', '_method');

        $this->title = "Diagram";

        view()->share('suitableInputs', $this->input);
        view()->share('allInputs', $inputs->allInputs());
    }
 

    public function index(StrategyChart2sDataTable $dataTable)
    {
        $breadcrumb = $this->breadcrumb();
        $breadcrumb->addCrumb($this->title, route('chart2.index'));

        return $dataTable->render("app.strategy_chart2.index", ["breadcrumb" => $breadcrumb->render(),'title' =>  $this->title] );
    }

    public function create()
    {
        $breadcrumb = $this->breadcrumb();
        $breadcrumb->addCrumb($this->title, route('chart2.index'));
        $breadcrumb->addCrumb(trans('locale.create'));

        return view("app.strategy_chart2.create", ["breadcrumb" => $breadcrumb->render()]);
    }

    public function store(Request $request)
    {
       $request->validate(StrategyChart2::rules($request->input('lang_id')));
        
        try{
            StrategyChart2::create($this->requests);
        }
        catch(\Exception $e){
            return redirect()->back()->withInput($request->input())->withErrors(["errors" => trans('locale.fail', ['number' => 1])]);
        }


        $request->session()->flash('message', "Əməliyyat uğurlu alındı");

        return redirect()->route('chart2.index');
    }



    public function edit($id)
    {
        $chart = StrategyChart2::findOrFail($id);
        $breadcrumb = $this->breadcrumb();
        $breadcrumb->addCrumb($this->title, route('chart2.index'));
        $breadcrumb->addCrumb(trans('locale.edit'));

        return view("app.strategy_chart2.edit", ["info" => $chart, "breadcrumb" => $breadcrumb->render()]);
    }


    public function update(Request $request, $id)
    {

        $find = StrategyChart2::findOrFail($id);

        $request->validate(StrategyChart2::rules($request->input('lang_id'), $id));



        try{
            foreach($this->requests as $key => $input){
                $find -> $key = $input;
            }

            $find -> save();
        }
        catch(\Exception $e){
            return redirect()->back()->withInput($request->input())->withErrors(["errors" => trans('locale.fail', ['number' => 1])]);
        }


        $request->session()->flash('message', trans('locale.update_success'));

        return redirect()->route('chart2.index');
    }


    public function destroy($id)
    {
        StrategyChart2::where('id', $id)->delete();

        request()->session()->flash('message', "Diagram silindi");

        return redirect()->route('chart2.index');
    }
}
