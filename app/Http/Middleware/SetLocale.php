<?php

namespace App\Http\Middleware;

use Closure;
use App\Logic\WebCache;

class SetLocale
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $config = WebCache::getConfig();

        if($request->path() == '/' && $config->lang_id != 'az')
        {
            return redirect()->to($config->lang_id);
        }

        return $next($request);
    }
}
