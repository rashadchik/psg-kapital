<?php

namespace App\Http\Middleware;

use Closure;

class filterStr
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (!in_array(strtolower($request->method()), ['put', 'post', 'patch'])) {
            return $next($request);
        }

        function ucfirst_utf8($str)
        {
            return mb_substr(mb_strtoupper($str, 'utf-8'), 0, 1, 'utf-8') . mb_substr($str, 1, mb_strlen($str)-1, 'utf-8');
        }

        $input = $request->only(['first_name', 'last_name', 'email']);

        $onlyLower = ['email', 'type']; //doesn't work

        array_walk_recursive($input, function(&$input) use ($request, $onlyLower){

            $value = array_search($input, $request->input());

            $input = mb_strtolower($input);

            if(!in_array($value, $onlyLower)){
                $input = ucfirst_utf8($input);
            }

        });


        $request->merge($input);
        return $next($request);
    }
}
