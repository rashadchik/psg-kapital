<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Models\Upload;


class Exam extends Mailable
{
    use Queueable, SerializesModels;

    public $content, $pdf, $subject;

    public function __construct($content, $pdf, $subject)
    {
        $this->content = $content;
        $this->pdf = $pdf;
        $this->subject = $subject;
    }

    public function build()
    {
        $send = $this->replyTo($this->content['email'])
            ->subject($this->subject)
            ->markdown("emails.exam")
            ->with('content', $this->content)
            ->attachData($this->pdf->output(), $this->content['first_name'].' '.$this->content['last_name']."-exam.pdf", ['mime' => 'application/pdf'] );

        return $send;
    }
}
