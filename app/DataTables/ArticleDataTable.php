<?php

namespace App\DataTables;

use App\Models\Article;
use Yajra\DataTables\Services\DataTable;
use Yajra\DataTables\EloquentDataTable;
use App\Logic\userAction;

class ArticleDataTable extends DataTable
{
    protected $trashed, $featured;

    public function trashed($trash) {
        $this->trashed = $trash;
        return $this;
    }

    public function featured($featured = false) {
        $this->featured = $featured;
        return $this;
    }

    public function dataTable($query)
    {
        $dataTable = new EloquentDataTable($query);

        return $dataTable
            ->editColumn('published_at', function($post) {
                return filterDate($post->published_at);
            })
            ->editColumn('title', function($post) {
                return str_limit($post->title, $limit = 50, $end = '...');
            })
            ->editColumn('image', function($post) {
                if(!is_null($post->image)){
                    return '<img src='.asset("files/cache/images/$post->relation_page/thumb/".$post->image).' style="width:200px">';
                }
                else{
                    return 'Yüklənməyib';
                }
            })
            ->editColumn('status', function($post) {
                if($post->featured == 1){
                    $featured = '<span class="text-warning text-center">'.icon('star').'</span>';
                }
                else{
                    $featured = null;
                }
                return label(config("config.label.".$post->status), config("config.article-status.".$post->status)).' '.$featured;
            })
            ->rawColumns(['status', 'image', 'action'])
            ->addColumn('action', function($row) {
                return view( 'widgets.action-article', ['route' => 'article', 'page' => $row, 'deleted' => $this->trashed])->render();
            });
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\User $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(Article $model)
    {
        $query = $model->newQuery()
            ->leftJoin('pages', 'pages.id', '=', 'articles.category_id')
            ->leftJoin('galleries', function ($join) {
                $join->on('articles.relation_page', '=', 'galleries.category_id')->where('galleries.type', 6);
            })
            ->select($this->getColumnsData());


        if($this->featured == true)
        {
            $query->where('articles.featured', 1);
        }


        if ($this->request()->has('title')) {
            $title = $this->request()->get('title');

            $query->where('articles.title', 'like', "%{$title}%");
        }

        if ($this->request()->has('categoryId') && $this->request()->get('categoryId') != 0) {
            $query->where('articles.category_id', $this->request()->get('categoryId'));
        }

        if($this->trashed == true){
            $query->onlyTrashed();
        }
        else{
            if(array_key_exists(auth()->user()->dt_view, config('app.locales'))){
                $query->where('pages.lang_id', auth()->user()->dt_view);
            }
        }


        return $query;
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->addAction(['width' => userAction::showAction() == true ? '145px' : '120px', 'orderable' => false, 'searchable' => false, 'class' => 'all', 'title' => 'Əməliyyat'])
            ->parameters($this->getBuilderParameters());
    }

    /**
     * Get columns.
     *
     * @return array
     */


    protected function getColumns()
    {
        return [
            ['data' => 'id', 'name' => 'articles.id', 'title' => 'ID', 'searchable' => false, 'visible' => false],
            ['data' => 'published_at', 'name' => 'articles.published_at', 'title' => 'Tarix', 'searchable' => false],
            ['data' => 'title', 'name' => 'articles.title', 'title' => 'Başlıq','orderable' => false],
            ['data' => 'name', 'name' => 'pages.name', 'title' => 'Kateqoriya', 'searchable' => false],
            ['data' => 'lang_id', 'name' => 'pages.lang_id', 'title' => 'Dil', 'searchable' => false],
            ['data' => 'status', 'name' => 'articles.status', 'title' => 'Status', 'searchable' => false],
            ['data' => 'slug', 'name' => 'articles.slug', 'title' => 'Slug','orderable' => false, 'searchable' => false, 'class' => 'none'],
            ['data' => 'published_by', 'name' => 'articles.published_by', 'title' => 'Müəllif','orderable' => false, 'searchable' => false, 'class' => 'none'],
            ['data' => 'image', 'name' => 'galleries.filename as image', 'title' => 'Şəkil', 'orderable' => false, 'searchable' => false, 'class' => 'none'],
            ['data' => 'created_at', 'name' => 'articles.created_at', 'title' => 'Yaradıldı','orderable' => false, 'searchable' => false, 'class' => 'none'],
            ['data' => 'updated_at', 'name' => 'articles.updated_at', 'title' => 'Yenilənib','orderable' => false, 'searchable' => false, 'class' => 'none'],
        ];
    }


    protected function getColumnsData()
    {
        $extraColumns = ['articles.relation_page', 'pages.slug as category_slug'];

        $columns = [];

        foreach($this->getColumns() as $column)
        {
            $columns[] = $column['name'];
        }

        return array_merge($columns, $extraColumns);
    }


    protected function getBuilderParameters()
    {
        return [
            'responsive' => true,
            'filter' => false,
            'order' => [ [1,'desc'] ],
            'lengthMenu' => [10,25,50],
            'drawCallback' => 'function() {
                $("#articles_length").prependTo($("#dataTables_length_box"));
            }',
        ];
    }


    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'testdatatable_' . time();
    }
}
