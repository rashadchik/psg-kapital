<?php

namespace App\DataTables;

use App\Models\Rekvisit;
use Yajra\DataTables\Services\DataTable;
use Yajra\DataTables\EloquentDataTable;
use App\Logic\userAction;

class RekvisitDataTable extends DataTable
{
    protected $trashed = false;

    public function trashed($trash) {
        $this->trashed = $trash;
        return $this;
    }


     public function dataTable($query)
    {
        $dataTable = new EloquentDataTable($query);

        return $dataTable
        ->addColumn('action', function($row) {
                return view( 'widgets.action-datatable', ['route' => 'rekvisit', 'id' => $row->id, 'deleted' =>false,'trash' =>false,'destroy' =>false])->render();
            })
            ->rawColumns(['action']);
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\User $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(Rekvisit $model)
    {
        $query = $model->newQuery()->select($this->getsColumnsData());
        if($this->trashed == true){
            $query->onlyTrashed();
        }
        return $query;
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
   public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->addAction(['width' => userAction::showAction() == true ? '145px' : '120px', 'orderable' => false, 'searchable' => false, 'class' => 'all', 'title' => 'Əməliyyat'])
            ->parameters($this->getBuilderParameters());
    }

    /**
     * Get columns.
     *
     * @return array
     */


    protected function getColumns()
    {

        return [
            ['data' => 'id','title' => 'ID', 'searchable' => false, 'visible' => false],
            ['data' => 'registration_number','title' => 'Qeydiyyat nömrəsi', 'orderable' => false],
            ['data' => 'nominal_value','title' => 'Nominal dəyər', 'orderable' => false],
            ['data' => 'coupon','title' => 'Kupon', 'orderable' => false],
            ['data' => 'coupon_payment_az','title' => 'Coupon Ödənişləri  Az', 'orderable' => false],
            ['data' => 'coupon_payment_en','title' => 'Coupon Ödənişləri  En', 'orderable' => false],
            ['data' => 'time','title' => 'Müddət', 'orderable' => false],
            ['data' => 'created_at','title' => 'Yaradıldı', 'searchable' => false, 'class' => 'none'],
            ['data' => 'updated_at','title' => 'Yenilənib', 'searchable' => false, 'class' => 'none'],
        ];
    }


    protected function getsColumnsData()
    {
        $columns = [];

        foreach($this->getColumns() as $column)
        {
            $columns[] = $column['data'];
        }

        return $columns; 
    }


    protected function getBuilderParameters()
    {
        return [
            'processing' => true,
            'responsive' => true,
            'filter' => true,
            'order' => [ [1,'asc'] ],
            'lengthMenu' => [10,25,50]
        ];
    }


    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'questiondatatable_' . time();
    }
}
