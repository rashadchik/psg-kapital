<?php

namespace App\DataTables;

use App\Models\Dictionary;
use Yajra\DataTables\Services\DataTable;
use Yajra\DataTables\EloquentDataTable;

class DictionaryDataTable extends DataTable
{

    public function dataTable($query)
    {
        $dataTable = new EloquentDataTable($query);

        return $dataTable
            ->editColumn('content', function($post) {
                return str_limit(strip_tags($post->content), $limit = 150, $end = '...');
            })
            ->addColumn('action', 'widgets.action-dictionary');

    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\User $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(Dictionary $model)
    {
        $query = $model->newQuery()->where('lang_id', 'az')->where('type', 0)->select($this->getsColumnsData());

        return $query;
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->addAction(['width' => '30px', 'orderable' => false, 'searchable' => false, 'class' => 'all', 'title' => ''])
            ->parameters($this->getBuilderParameters());
    }

    /**
     * Get columns.
     *
     * @return array
     */


    protected function getColumns()
    {
        return [
            ['data' => 'id', 'title' => 'ID', 'searchable' => false, 'visible' => false],
            ['data' => 'keyword', 'title' => 'Açar söz'],
            ['data' => 'content', 'title' => 'Text'],
            ['data' => 'updated_at', 'title' => 'Yenilənib', 'searchable' => false]
        ];
    }


    protected function getsColumnsData()
    {
        $columns = [];

        foreach($this->getColumns() as $column)
        {
            $columns[] = $column['data'];
        }

        return $columns;
    }


    protected function getBuilderParameters()
    {
        return [
            'processing' => true,
            'responsive' => true,
            'filter' => true,
            'order' => [ [1,'asc'] ],
            'lengthMenu' => [25,50,100]
        ];
    }


    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'dicdatatable_' . time();
    }
}
