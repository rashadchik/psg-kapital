<?php

namespace App\DataTables;

use App\Models\Slider;
use Yajra\DataTables\Services\DataTable;
use App\Logic\userAction;
use Yajra\DataTables\EloquentDataTable;

class PartnerDataTable extends DataTable
{
    protected $trashed;

    public function trashed($trash) {
        $this->trashed = $trash;
        return $this;
    }


    public function dataTable($query)
    {
        $dataTable = new EloquentDataTable($query);

        return $dataTable
            ->addColumn('action', 'widgets.action-slider')
            ->editColumn('image_original', function($slider) {
                return '<img src="'.asset("files/cache/images/".$slider->id."/".$slider->image_original).'" class="img-responsive">';
            })

            ->addColumn('action', function($row) {
                return view( 'widgets.action-datatable', ['route' => 'partner', 'id' => $row->id, 'deleted' => $this->trashed])->render();
            })
            ->rawColumns(['image_original', 'action']);
    }



    public function query(Slider $model)
    {
        $query = $model->newQuery()->where('type', 2)->select($this->getColumnsData());

        if($this->trashed == true){
            $query->onlyTrashed();
        }

        return $query;
    }



    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->addAction(['width' => userAction::showAction() == true ? '90px' : '60px', 'orderable' => false, 'searchable' => false, 'title' => ' '])
            ->parameters($this->getBuilderParameters());
    }



    protected function getColumns()
    {
        return [
            ['data' => 'id', 'title' => 'ID', 'visible' => false, 'searchable' => false],
            ['data' => 'image_original', 'title' => 'Şəkil', 'searchable' => false, 'orderable' => false],
            ['data' => 'title', 'title' => 'Başlıq', 'orderable' => false],
            ['data' => 'created_at', 'title' => 'Yaradıldı', 'searchable' => false],
            ['data' => 'updated_at', 'title' => 'Yenilənib', 'searchable' => false],
        ];
    }


    protected function getColumnsData()
    {
        $extraColumns = ['deleted_at'];

        $columns = [];

        foreach($this->getColumns() as $column)
        {
            $columns[] = $column['data'];
        }

        return array_merge($columns, $extraColumns);
    }



    protected function getBuilderParameters()
    {
        return [
            'processing' => false,
            'responsive' => true,
            'filter' => true,
            'order' => [ [0,'desc'] ],
            'lengthMenu' => [10,25]
        ];
    }



    protected function filename()
    {
        return 'partnerdatatable_' . time();
    }
}
