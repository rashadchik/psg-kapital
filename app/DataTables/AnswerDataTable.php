<?php

namespace App\DataTables;

use App\Models\Question;
use Yajra\DataTables\Services\DataTable;
use Yajra\DataTables\EloquentDataTable;
use App\Logic\userAction;

class AnswerDataTable extends DataTable
{
    protected $trashed = false;

    public function trashed($trash) {
        $this->trashed = $trash;
        return $this;
    }


     public function dataTable($query)
    {
        $dataTable = new EloquentDataTable($query);

        return $dataTable
        ->addColumn('action', function($row) {
                return view( 'widgets.action-datatable', ['route' => 'answer', 'id' => $row->id, 'deleted' =>false,'trash' =>false])->render();
            })
            ->editColumn('point', function($post) {
                return $post->point.' xal';
            })
            ->rawColumns([ 'action']);
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\User $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(Question $model)
    {
        $query = $model->newQuery()
            ->leftJoin('quest as question', 'question.id', '=', 'quest.parent_id')
            ->whereNotNull('quest.parent_id')
            ->select($this->getColumnsData());

        if($this->trashed == true){
            $query->onlyTrashed();
        }

        return $query;
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
   public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->addAction(['width' => userAction::showAction() == true ? '145px' : '120px', 'orderable' => false, 'searchable' => false, 'class' => 'all', 'title' => 'Əməliyyat'])
            ->parameters($this->getBuilderParameters());
    }

    /**
     * Get columns.
     *
     * @return array
     */


    protected function getColumns()
    {

        return [
            ['data' => 'id', 'name' => 'quest.id', 'title' => 'ID', 'searchable' => false, 'visible' => false],
            ['data' => 'question', 'name' => 'question.title as question', 'title' => 'Sual', 'searchable' => false],
            ['data' => 'title', 'name' => 'quest.title', 'title' => 'Cavab', 'orderable' => false],
            ['data' => 'lang_id', 'name' => 'quest.lang_id', 'title' => 'Dil', 'searchable' => false],
            ['data' => 'point', 'name' => 'quest.point', 'title' => 'Xal', 'orderable' => false],
            ['data' => 'created_at', 'name' => 'quest.created_at','title' => 'Yaradıldı', 'searchable' => false, 'class' => 'none'],
            ['data' => 'updated_at', 'name' => 'quest.updated_at','title' => 'Yenilənib', 'searchable' => false, 'class' => 'none'],
        ];
    }


    protected function getColumnsData()
    {
        $columns = [];

        foreach($this->getColumns() as $column)
        {
            $columns[] = $column['name'];
        }

        return $columns;
    }


    protected function getBuilderParameters()
    {
        return [
            'processing' => true,
            'responsive' => true,
            'filter' => true,
            'order' => [ [1, 'desc'] ],
            'lengthMenu' => [10,25,50]
        ];
    }


    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'questiondatatable_' . time();
    }
}
