<?php

namespace App\DataTables;

use App\Models\Question;
use Yajra\DataTables\Services\DataTable;
use Yajra\DataTables\EloquentDataTable;
use App\Logic\userAction;

class QuestionDataTable extends DataTable
{
    protected $trashed = false;

    public function trashed($trash) {
        $this->trashed = $trash;
        return $this;
    }

     public function dataTable($query)
    {
        $dataTable = new EloquentDataTable($query);

        return $dataTable
            ->editColumn('image_original', function($slider) {
                return '<img src="'.asset("files/cache/images/".$slider->id."/".$slider->image_original).'" class="img-responsive">';
            })
            ->addColumn('action', function($row) {
            return  view( 'widgets.action-datatable', ['route' => 'question', 'id' => $row->id, 'deleted' =>false, 'trash' =>false])->render();
        })
        ->rawColumns(['image_original', 'action']);

    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\User $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(Question $model)
    {
        $query = $model->newQuery()->where('parent_id', null)->select($this->getsColumnsData());
        if($this->trashed == true){
            $query->onlyTrashed();
        }
        return $query;
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
   public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->addAction(['width' => userAction::showAction() == true ? '145px' : '120px', 'orderable' => false, 'searchable' => false, 'class' => 'all', 'title' => 'Əməliyyat'])
            ->parameters($this->getBuilderParameters());
    }

    /**
     * Get columns.
     *
     * @return array
     */


    protected function getColumns()
    {

        return [
            ['data' => 'id','title' => 'ID', 'searchable' => false, 'visible' => false],
            ['data' => 'title','title' => 'Sual', 'orderable' => false],
            ['data' => 'lang_id', 'title' => 'Dil', 'searchable' => false],
            ['data' => 'image_original', 'title' => 'Şəkil','searchable' => false, 'class' => 'none'],
            ['data' => 'created_at','title' => 'Yaradıldı', 'searchable' => false, 'class' => 'none'],
            ['data' => 'updated_at','title' => 'Yenilənib', 'searchable' => false, 'class' => 'none'],
        ];
    }


    protected function getsColumnsData()
    {
        $columns = [];

        foreach($this->getColumns() as $column)
        {
            $columns[] = $column['data'];
        }

        return $columns; 
    }


    protected function getBuilderParameters()
    {
        return [
            'processing' => true,
            'responsive' => true,
            'filter' => true,
            'order' => [ [1,'asc'] ],
            'lengthMenu' => [10,25]
        ];
    }


    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'questiondatatable_' . time();
    }
}
