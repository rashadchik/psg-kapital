<?php

namespace App\DataTables;

use Yajra\DataTables\Services\DataTable;
use App\Models\MarketLoan;
use App\Logic\userAction;
use Yajra\DataTables\EloquentDataTable;

class MarketLoansDataTable extends DataTable
{
    protected $trashed;

    public function trashed($trash) {
        $this->trashed = $trash;
        return $this;
    }

     public function dataTable($query)
    {
        $dataTable = new EloquentDataTable($query);
 
        return $dataTable
           ->editColumn('type', function($post) {
                return config("config.loanMarketType.$post->type");
            })
            ->addColumn('action', function($row) {
                return view( 'widgets.action-datatable', ['route' => 'marketLoan', 'id' => $row->id, 'deleted' => $this->trashed])->render();
            })
            ->rawColumns(['action', 'type']);

    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\User $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(MarketLoan $model)
    {
        $query = $model->newQuery()->select($this->getsColumnsData());
        if($this->trashed == true){
            $query->onlyTrashed();
        }
        return $query;
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
   public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->addAction(['width' => userAction::showAction() == true ? '145px' : '120px', 'orderable' => false, 'searchable' => false, 'class' => 'all', 'title' => 'Əməliyyat'])
            ->parameters($this->getBuilderParameters());
    }

    /**
     * Get columns.
     *
     * @return array
     */


    protected function getColumns()
    {

        return [
            ['data' => 'id','title' => 'ID', 'searchable' => false, 'visible' => false],
            ['data' => 'type','title' => 'Istiqraz novü', 'orderable' => false],
            ['data' => 'title_az','title' => 'Qiymətli kağız (AZ)', 'orderable' => false],
            ['data' => 'prise_difference1','title' => 'Qiymət / Bağlanış qiymət'],
            ['data' => 'prise_difference2','title' => 'Gəlirlilik / Açılış qiyməti'],
            ['data' => 'name_az','title' => 'İstiqamət / Fyuçers (AZ)'],
            ['data' => 'title_en','title' => 'Qiymətli kağız (EN)', 'class' => 'none'],
            ['data' => 'name_en','title' => 'İstiqamət / Fyuçers (EN)', 'class' => 'none'],
            ['data' => 'created_at','title' => 'Yaradıldı', 'searchable' => false, 'class' => 'none'],
            ['data' => 'updated_at','title' => 'Yenilənib', 'searchable' => false, 'class' => 'none'],
        ];
    }


    protected function getsColumnsData()
    {
        $columns = [];

        foreach($this->getColumns() as $column)
        {
            $columns[] = $column['data'];
        }

        return $columns; 
    }


    protected function getBuilderParameters()
    {
        return [
            'processing' => true,
            'responsive' => true,
            'filter' => true,
            'order' => [ [1,'asc'] ],
            'lengthMenu' => [10,25,50]
        ];
    }


    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'marketloandatatable' . time();
    }
}
