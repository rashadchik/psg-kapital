<?php 

namespace App\DataTables;

use App\Models\Document;
use Yajra\DataTables\Services\DataTable;
use Yajra\DataTables\EloquentDataTable;
use App\Logic\userAction;

class DocumentDataTable extends DataTable
{
    protected $trashed;

    public function trashed($trash) {
        $this->trashed = $trash;
        return $this;
    }

    public function dataTable($query)
    {
        $dataTable = new EloquentDataTable($query);

        return $dataTable
            ->editColumn('document', function($row) {
                return '<a href="'.asset($row->document).'" target="_blank">Sənədə bax</a>';
            })
            ->addColumn('action', function($row) {
                return view( 'widgets.action-datatable', ['route' => 'document', 'id' => $row->id, 'deleted' => $this->trashed])->render();
            })
            ->rawColumns(['document', 'action']);
    }


    public function query(Document $model)
    {
        $query = $model->newQuery()
            ->leftJoin('pages', 'pages.id', '=', 'documents.category_id')
            ->select($this->getColumnsData());
            
        if($this->trashed == true){
            $query->onlyTrashed();
        }

        return $query;

    }



    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->addAction(['width' => userAction::showAction() == true ? '145px' : '120px', 'orderable' => false, 'searchable' => false, 'class' => 'all', 'title' => 'Əməliyyat'])
            ->parameters($this->getBuilderParameters());
    }



    protected function getColumns()
    {
        return [
            ['data' => 'id', 'name' => 'documents.id', 'title' => 'ID', 'visible' => false, 'searchable' => false],
            ['data' => 'name', 'name' => 'pages.name', 'title' => 'Kateqoriya', 'searchable' => false],
            ['data' => 'year', 'name' => 'documents.year', 'title' => 'Year'],
            ['data' => 'document','name' => 'documents.document', 'title' => 'Sənəd', 'orderable' => false, 'searchable' => false],
            ['data' => 'summary', 'name' => 'documents.summary', 'title' => 'Məzmun', 'orderable' => false, 'class' => 'none'],
            ['data' => 'created_at', 'name' => 'documents.created_at','title' => 'Yaradıldı','orderable' => false, 'searchable' => false, 'class' => 'none'],
            ['data' => 'updated_at', 'name' => 'documents.updated_at', 'title' => 'Yenilənib','orderable' => false, 'searchable' => false, 'class' => 'none'],
        ];
    }


    protected function getColumnsData()
    {
        $columns = [];

        foreach($this->getColumns() as $column)
        {
            $columns[] = $column['name'];
        }

        return $columns;
    }


    protected function getBuilderParameters()
    {
        return [
            'processing' => true,
            'responsive' => true,
            'filter' => true,
            'order' => [ [0,'desc'] ],
            'lengthMenu' => [10,25,50,100]
        ];
    }



    protected function filename()
    {
        return 'documentdatatable_' . time();
    }
}
