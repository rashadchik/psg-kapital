<?php

namespace App\DataTables;

use App\Models\Banner;
use Yajra\DataTables\Services\DataTable;
use Yajra\DataTables\EloquentDataTable;
use App\Logic\userAction;

class BannerDataTable extends DataTable
{
    protected $trashed;

    public function trashed($trash) {
        $this->trashed = $trash;
        return $this;
    }


     public function dataTable($query)
    {
        $dataTable = new EloquentDataTable($query);

        return $dataTable
            ->editColumn('image_original', function($post) {
                return '<img src="'.asset($post->image_original).'" class="img-responsive">';
            })
            ->editColumn('pos_type', function($post) {
                return config("config.position.$post->pos_type");
            })
           ->editColumn('document', function($post) {

               if(!is_null($post->document))
               {
                    return '<a href="'.asset($post->document).'" target="_blank">Fayla bax</a>';
               }
               else{
                   return 'Fayl yüklənməyib';
               }
            })
            ->addColumn('action', function($row) {
            return view( 'widgets.action-datatable', ['route' => 'banner', 'id' => $row->id, 'deleted' => $this->trashed])->render();
            })
            ->rawColumns(['image_original', 'document', 'action']);
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\User $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(Banner $model)
    {
        $query = $model->newQuery()
            ->leftJoin('pages', 'pages.id', '=', 'banners.category_id')
            ->select($this->getsColumnsData());
        if($this->trashed == true){
            $query->onlyTrashed();
        }
        return $query;
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
   public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->addAction(['width' => userAction::showAction() == true ? '145px' : '120px', 'orderable' => false, 'searchable' => false, 'class' => 'all', 'title' => 'Əməliyyat'])
            ->parameters($this->getBuilderParameters());
    }

    /**
     * Get columns.
     *
     * @return array
     */


    protected function getColumns()
    {

        return [
            ['data' => 'id', 'name' => 'banners.id', 'title' => 'ID', 'searchable' => false, 'visible' => false],
            ['data' => 'page', 'name' => 'pages.name as page', 'title' => 'Sehifə', 'orderable' => false],
            ['data' => 'pos_type', 'name' => 'banners.pos_type', 'title' => 'İstiqamət', 'orderable' => false],
            ['data' => 'document', 'name' => 'banners.document', 'title' => 'Pdf', 'orderable' => false],
            ['data' => 'created_at', 'name' => 'banners.created_at', 'title' => 'Yaradıldı', 'searchable' => false],
            ['data' => 'updated_at', 'name' => 'banners.updated_at', 'title' => 'Yenilənib', 'searchable' => false],
            ['data' => 'image_original', 'name' => 'banners.image_original', 'title' => 'Şəkil', 'orderable' => false, 'searchable' => false, 'class' => 'none']
        ];
    }


    protected function getsColumnsData()
    {
        $columns = [];

        foreach($this->getColumns() as $column)
        {
            $columns[] = $column['name'];
        }

        return $columns; 
    }


    protected function getBuilderParameters()
    {
        return [
            'processing' => true,
            'responsive' => true,
            'filter' => true,
            'order' => [ [0,'desc'] ],
            'lengthMenu' => [10,25,50]
        ];
    }


    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'bannerdatatable_' . time();
    }
}
