<?php

namespace App\DataTables;

use App\Models\ServiceStat;
use Yajra\DataTables\Services\DataTable;
use Yajra\DataTables\EloquentDataTable;

use App\Logic\userAction;
class ServiceStatDataTable extends DataTable
{
    protected $trashed;

    public function trashed($trash) {
        $this->trashed = $trash;
        return $this;
    }


    public function dataTable($query)
    {
        $dataTable = new EloquentDataTable($query);

        return $dataTable
            ->addColumn('action', function($row) {
                return view( 'widgets.action-datatable', ['route' => 'serviceStat', 'id' => $row->id, 'deleted' => $this->trashed])->render();
            });
    }

    /**
     * Get the query object to be processed by dataTables.
     *
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Query\Builder|\Illuminate\Support\Collection
     */
    public function query(ServiceStat $model)
    {
        $query = $model->newQuery()
            ->leftJoin('pages', 'pages.relation_page', '=', 'service_stats.category_id')
            ->where('pages.third_level', 1)
            ->select($this->getColumnsData())
            ->groupBy('pages.relation_page');

        if($this->trashed == true){
            $query->onlyTrashed();
        }

        return $query;
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\Datatables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->addAction(['width' => userAction::showAction() == true ? '80' : '80', 'orderable' => false, 'searchable' => false, 'class' => 'all', 'title' => 'Əməliyyat'])
            ->parameters($this->getBuilderParameters());

    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            ['data' => 'id', 'name' => 'service_stats.id', 'title' => 'ID', 'searchable' => false, 'visible' => false],
            ['data' => 'name', 'name' => 'pages.name', 'title' => 'Xidmət', 'orderable' => false],
            ['data' => 'customers', 'name' => 'service_stats.customers', 'title' => 'Müştəri sayı'],
            ['data' => 'transactions', 'name' => 'service_stats.transactions', 'title' => 'Əqd sayı'],
            ['data' => 'loans', 'name' => 'service_stats.loans', 'title' => 'İstiqrazlar'],
            ['data' => 'proportions', 'name' => 'service_stats.proportions', 'title' => 'Səhmlər'],
            ['data' => 'created_at', 'name' => 'service_stats.created_at', 'title' => 'Yaradıldı', 'searchable' => false, 'class' => 'none'],
            ['data' => 'updated_at', 'name' => 'service_stats.updated_at', 'title' => 'Yenilənib', 'searchable' => false, 'class' => 'none'],
        ];
    }


    protected function getColumnsData()
    {
        $extraColumns = [];

        $columns = [];

        foreach($this->getColumns() as $column)
        {
            $columns[] = $column['name'];
        }

        return array_merge($columns, $extraColumns);
    }


    protected function getBuilderParameters()
    {
        return [
            'responsive' => true,
            'order' => [ [1,'desc'] ],
            'lengthMenu' => [5,10],
        ];
    }

    protected function filename()
    {
        return 'service_statsdatatable_' . time();
    }
}
