<?php

namespace App\DataTables;

use App\Models\Media;
use Yajra\DataTables\Services\DataTable;
use Yajra\DataTables\EloquentDataTable;
use App\Logic\userAction;

class MediaDataTable extends DataTable
{
    protected $trashed, $featured;

    public function trashed($trash) {
        $this->trashed = $trash;
        return $this;
    }

    public function featured($featured = false) {
        $this->featured = $featured;
        return $this;
    }

    public function dataTable($query)
    {
        $dataTable = new EloquentDataTable($query);


        return $dataTable
            ->editColumn('youtube_id', function($row) {
                return '<img src="'.asset("files/cache/images/$row->id/$row->image_original").'" width="180px">';
            })
            ->addColumn('action', function($row) {
                return view( 'widgets.action-datatable', ['route' => 'media', 'id' => $row->id, 'deleted' => $this->trashed])->render();
            })
            ->rawColumns(['youtube_id', 'action']);
    }

    /**
     * Get the query object to be processed by dataTables.
     *
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Query\Builder|\Illuminate\Support\Collection
     */
    public function query(Media $model)
    {
        $query = $model->newQuery()->leftJoin('pages', 'pages.id', '=', 'media.doctor_id')->select($this->getColumnsData());

        if($this->trashed == true){
            $query->onlyTrashed();
        }

        return $query;
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\Datatables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
                    ->columns($this->getColumns())
                    ->minifiedAjax()
                    ->addAction(['width' => userAction::showAction() == true ? '80' : '80', 'orderable' => false, 'searchable' => false, 'class' => 'all', 'title' => 'Əməliyyat'])
                    ->parameters($this->getBuilderParameters());

    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            ['data' => 'id', 'name' => 'media.id','title' => 'ID', 'searchable' => false, 'visible' => false],
            ['data' => 'name', 'name' => 'pages.name','title' => 'Media', 'searchable' => false],
            ['data' => 'youtube_id', 'name' => 'media.youtube_id','title' => 'Youtube', 'orderable' => false],
            ['data' => 'created_at', 'name' => 'media.created_at','title' => 'Yaradıldı', 'searchable' => false, 'class' => 'none'],
            ['data' => 'updated_at', 'name' => 'media.updated_at','title' => 'Yenilənib', 'searchable' => false, 'class' => 'none'],
        ];
    }


    protected function getColumnsData()
    {
        $extraColumns = ['media.image_original', 'media.deleted_at'];

        $columns = [];

        foreach($this->getColumns() as $column)
        {
            $columns[] = $column['name'];
        }

        return array_merge($columns, $extraColumns);
    }


    protected function filename()
    {
        return 'mediadatatable_' . time();
    }
}
