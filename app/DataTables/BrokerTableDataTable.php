<?php

namespace App\DataTables;

use App\Models\BrokerTable;
use Yajra\DataTables\Services\DataTable;
use Yajra\DataTables\EloquentDataTable;

use App\Logic\userAction;
class BrokerTableDataTable extends DataTable
{
    protected $trashed;

    public function trashed($trash) {
        $this->trashed = $trash;
        return $this;
    }


    public function dataTable($query)
    {
        $dataTable = new EloquentDataTable($query);

        return $dataTable
            ->addColumn('action', function($row) {
                return view( 'widgets.action-datatable', ['route' => 'brokerTable', 'id' => $row->id, 'deleted' => $this->trashed])->render();
            });
    }

    /**
     * Get the query object to be processed by dataTables.
     *
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Query\Builder|\Illuminate\Support\Collection
     */
    public function query(BrokerTable $model)
    {
        $query = $model->newQuery()
            ->leftJoin('pages', 'pages.relation_page', '=', 'broker_tables.category_id')
            ->where('pages.template_id', 6)
            ->groupBy('broker_tables.id')
            ->select($this->getColumnsData());

        if($this->trashed == true){
            $query->onlyTrashed();
        }

        return $query;
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\Datatables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->addAction(['width' => userAction::showAction() == true ? '80' : '80', 'orderable' => false, 'searchable' => false, 'class' => 'all', 'title' => 'Əməliyyat'])
            ->parameters($this->getBuilderParameters());

    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            ['data' => 'id', 'name' => 'broker_tables.id', 'title' => 'ID', 'searchable' => false, 'visible' => false],
            ['data' => 'name', 'name' => 'pages.name', 'title' => 'Xidmət', 'orderable' => false],
            ['data' => 'corp_loans', 'name' => 'broker_tables.corp_loans', 'title' => 'Korp. istiqraz'],
            ['data' => 'gov_loans', 'name' => 'broker_tables.gov_loans', 'title' => 'Dövlət. istiqrazları'],
            ['data' => 'proportions', 'name' => 'broker_tables.proportions', 'title' => 'Təkrar bazar'],
            ['data' => 'notes', 'name' => 'broker_tables.notes', 'title' => 'Notlar'],
            ['data' => 'created_at', 'name' => 'broker_tables.created_at', 'title' => 'Yaradıldı', 'searchable' => false, 'class' => 'none'],
            ['data' => 'updated_at', 'name' => 'broker_tables.updated_at', 'title' => 'Yenilənib', 'searchable' => false, 'class' => 'none'],
        ];
    }


    protected function getColumnsData()
    {
        $extraColumns = [];

        $columns = [];

        foreach($this->getColumns() as $column)
        {
            $columns[] = $column['name'];
        }

        return array_merge($columns, $extraColumns);
    }


    protected function getBuilderParameters()
    {
        return [
            'responsive' => true,
            'order' => [ [1,'desc'] ],
            'lengthMenu' => [10, 25, 50],
        ];
    }

    protected function filename()
    {
        return 'broker_tabledatatable_' . time();
    }
}
