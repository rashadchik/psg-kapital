<?php

namespace App\DataTables;

use App\Models\StrategyChart2;
use Yajra\DataTables\Services\DataTable;
use Yajra\DataTables\EloquentDataTable;
use App\Logic\userAction;

class StrategyChart2sDataTable extends DataTable
{
    protected $trashed = false;

    public function trashed($trash) {
        $this->trashed = $trash;
        return $this;
    }


     public function dataTable($query)
    {
        $dataTable = new EloquentDataTable($query);

        return $dataTable
         ->editColumn('image_original', function($post) {
                if(!is_null($post->image_original)){
                    return '<img src="'.asset($post->image_original).'" style="width:200px">';
                }
                else{
                    return 'Yüklənməyib';
                }
            })
           ->addColumn('action', function($row) {
            return  view( 'widgets.action-datatable', ['route' => 'str_chart2', 'id' => $row->id, 'deleted' =>false,'trash' =>false])->render();
        })
            ->rawColumns(['image_original', 'action','content']);
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\User $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(StrategyChart2 $model)
    {
        $query = $model->newQuery()->select($this->getsColumnsData());
        if($this->trashed == true){
            $query->onlyTrashed();
        }
        return $query;
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
   public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->addAction(['width' => userAction::showAction() == true ? '145px' : '120px', 'orderable' => false, 'searchable' => false, 'class' => 'all', 'title' => 'Əməliyyat'])
            ->parameters($this->getBuilderParameters());
    }

    /**
     * Get columns.
     *
     * @return array
     */


    protected function getColumns()
    {

        return [
            ['data' => 'id','title' => 'ID', 'searchable' => false, 'visible' => false],
            ['data' => 'lang_id','title' => 'Dil', 'searchable' => false],
            ['data' => 'title','title' => 'Başlıq', 'searchable' => false],
            ['data' => 'content','title' => 'Məzmun'],
            ['data' => 'image_original','title' => 'Şəkil','class' => 'none'],
            ['data' => 'created_at','title' => 'Yaradıldı', 'searchable' => false, 'class' => 'none'],
            ['data' => 'updated_at','title' => 'Yenilənib', 'searchable' => false, 'class' => 'none'],
        ];
    }


    protected function getsColumnsData()
    {
        $columns = [];

        foreach($this->getColumns() as $column)
        {
            $columns[] = $column['data'];
        }

        return $columns; 
    }


    protected function getBuilderParameters()
    {
        return [
            'processing' => true,
            'responsive' => true,
            'filter' => true,
            'order' => [ [1,'asc'] ],
            'lengthMenu' => [10,25,50]
        ];
    }


    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'strategychart2datatable_' . time();
    }
}
