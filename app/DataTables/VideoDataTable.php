<?php

namespace App\DataTables;

use App\Models\Video;
use Yajra\Datatables\Services\DataTable;
use App\Logic\userAction;
class VideoDataTable extends DataTable
{
    protected $trashed;

    public function trashed($trash) {
        $this->trashed = $trash;
        return $this;
    }


    public function ajax()
    {
        return $this->datatables
            ->eloquent($this->query())
            ->addColumn('action', 'widgets.action-video')
            ->make(true);
    }



    public function query()
    {
        $query = Video::query()
            ->select('id', 'title', 'lang_id', 'summary', 'link', 'created_at', 'updated_at', 'deleted_at');

        if($this->trashed == true){
            $query->onlyTrashed();
        }

        return $this->applyScopes($query);
    }



    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->ajax('')
            ->addAction(['width' => userAction::showAction() == true ? '90px' : '60px', 'orderable' => false, 'searchable' => false, 'title' => ' '])
            ->parameters($this->getBuilderParameters());
    }



    protected function getColumns()
    {
        return [
            ['data' => 'id', 'title' => 'ID', 'visible' => false, 'searchable' => false],
            ['data' => 'title', 'title' => 'Başlıq', 'orderable' => false],
            ['data' => 'link', 'title' => 'URL', 'orderable' => false],
            ['data' => 'lang_id', 'title' => 'Dil', 'searchable' => false],
            ['data' => 'summary', 'title' => 'Qısa məzmun', 'searchable' => false, 'orderable' => false, 'class' => 'none'],
            ['data' => 'created_at', 'title' => 'Yaradıldı','orderable' => false, 'searchable' => false, 'class' => 'none'],
            ['data' => 'updated_at', 'title' => 'Yenilənib','orderable' => false, 'searchable' => false, 'class' => 'none'],
        ];
    }


    protected function getBuilderParameters()
    {
        return [
            'processing' => true,
            'responsive' => true,
            'filter' => true,
            'order' => [ [0,'desc'] ],
            'lengthMenu' => [5,10]
        ];
    }



    protected function filename()
    {
        return 'videodatatable_' . time();
    }
}
