<?php

namespace App\DataTables;

use Yajra\DataTables\Services\DataTable;
use App\Models\Page;
use App\Logic\userAction;
use Yajra\DataTables\EloquentDataTable;

class PageDataTable extends DataTable
{
    protected $trashed;

    public function trashed($trash) {
        $this->trashed = $trash;
        return $this;
    }



    public function dataTable($query)
    {
        $dataTable = new EloquentDataTable($query);

        return $dataTable
            ->editColumn('visible', function($post) {
                return label(config("config.menu-visibility-label.$post->visible"), config("config.menu-visibility.$post->visible"));
            })

            ->editColumn('has_slider', function($post) {
                return label(config("config.label.$post->has_slider"), icon(config("config.yes-no.$post->has_slider")));
            })
            ->editColumn('has_partner', function($post) {
                return label(config("config.label.$post->has_partner"), icon(config("config.yes-no.$post->has_partner")));
            })
            ->editColumn('template_id', function($post) {
                return config("config.template.$post->template_id");
            })
            ->addColumn('action', function($row) {
                return view( 'widgets.action-page', ['route' => 'page', 'page' => $row, 'deleted' => $this->trashed])->render();
            })
            ->rawColumns(['visible', 'action', 'has_slider', 'has_partner', 'template_id', 'summary']);
    }



    public function query(Page $model)
    {
        $query = $model->newQuery()
            ->whereNull('parent_id')
            ->select($this->getColumnsData());


        if($this->trashed == true){
            $query->onlyTrashed();
        }
        else{
            if(array_key_exists(auth()->user()->dt_view, config('app.locales'))){
                $query->where('lang_id', auth()->user()->dt_view);
            }
        }

        return $query;
    }



    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->addAction(['width' => '120px', 'orderable' => false, 'searchable' => false, 'visible' => userAction::showAction($this->trashed), 'class' => 'all', 'title' => 'Əməliyyat'])
            ->parameters($this->getBuilderParameters());
    }



    protected function getColumns()
    {
        return [
            ['data' => 'id', 'title' => 'ID', 'searchable' => false],
            ['data' => 'name', 'title' => 'Ad'],
            ['data' => 'slug', 'title' => 'Slug'],
            ['data' => 'lang_id', 'title' => 'Dil', 'searchable' => false],
            ['data' => 'visible', 'title' => 'Status', 'searchable' => false],
            ['data' => 'template_id', 'title' => 'Modul', 'searchable' => false, 'class' => 'none'],
            ['data' => 'summary', 'title' => 'Qısa məzmun', 'searchable' => false, 'class' => 'none'],
            ['data' => 'created_at', 'title' => 'Yaradıldı','orderable' => false, 'searchable' => false, 'class' => 'none'],
            ['data' => 'updated_at', 'title' => 'Yenilənib','orderable' => false, 'searchable' => false, 'class' => 'none'],
        ];
    }



    protected function getColumnsData()
    {
        $extraColumns = ['relation_page'];

        $columns = [];

        foreach($this->getColumns() as $column)
        {
            $columns[] = $column['data'];
        }

        return array_merge($columns, $extraColumns);
    }



    protected function getBuilderParameters()
    {
        return [
            'processing' => true,
            'responsive' => true,
            'filter' => true,
            'order' => [ [0,'desc'] ],
            'lengthMenu' => [25,50]
        ];
    }
}
