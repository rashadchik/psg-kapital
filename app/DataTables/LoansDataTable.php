<?php

namespace App\DataTables;

use App\Models\Loan;
use Yajra\DataTables\Services\DataTable;
use Yajra\DataTables\EloquentDataTable;
use App\Logic\userAction;

class LoansDataTable extends DataTable
{
    protected $trashed;

    public function trashed($trash) {
        $this->trashed = $trash;
        return $this;
    }

     public function dataTable($query)
    {
        $dataTable = new EloquentDataTable($query);

        return $dataTable
            ->addColumn('action', function($row) {
                return view( 'widgets.action-datatable', ['route' => 'loan', 'id' => $row->id, 'deleted' => $this->trashed])->render();
            })
            ->rawColumns(['loan', 'action']);

    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\User $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(Loan $model)
    {
        $query = $model->newQuery()->select($this->getsColumnsData());
        if($this->trashed == true){
            $query->onlyTrashed();
        }
        return $query;
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
   public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->addAction(['width' => userAction::showAction() == true ? '145px' : '120px', 'orderable' => false, 'searchable' => false, 'class' => 'all', 'title' => 'Əməliyyat'])
            ->parameters($this->getBuilderParameters());
    }

    /**
     * Get columns.
     *
     * @return array
     */


    protected function getColumns()
    {

        return [
            ['data' => 'id','title' => 'ID', 'searchable' => false, 'visible' => false],
            ['data' => 'loans','title' => 'Istiqraz novü', 'orderable' => false],
            ['data' => 'name','title' => 'Istiqraz adı', 'orderable' => false],
            ['data' => 'type','title' => 'Növü'],
            ['data' => 'price','title' => 'Qiyməti'],
            ['data' => 'introduction1','title' => 'İllik gəlir'],
            ['data' => 'coupon','title' => 'Kupon'],
            ['data' => 'birja','title' => 'Satıldığı birja'],
            ['data' => 'title_az','title' => 'Ölkə'],
            ['data' => 'created_at','title' => 'Yaradıldı', 'searchable' => false, 'class' => 'none'],
            ['data' => 'updated_at','title' => 'Yenilənib', 'searchable' => false, 'class' => 'none'],
        ];
    }


    protected function getsColumnsData()
    {
        $columns = [];

        foreach($this->getColumns() as $column)
        {
            $columns[] = $column['data'];
        }

        return $columns; 
    }


    protected function getBuilderParameters()
    {
        return [
            'processing' => true,
            'responsive' => true,
            'filter' => true,
            'order' => [ [1,'asc'] ],
            'lengthMenu' => [10,25,50]
        ];
    }


    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'loandatatable_' . time();
    }
}
