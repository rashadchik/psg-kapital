<?php

namespace App\DataTables;

use App\Models\Stock;
use Yajra\DataTables\Services\DataTable;
use Yajra\DataTables\EloquentDataTable;
use App\Logic\userAction;

class StockDataTable extends DataTable
{
    protected $trashed;

    public function trashed($trash) {
        $this->trashed = $trash;
        return $this;
    }

     public function dataTable($query)
    {
        $dataTable = new EloquentDataTable($query);

        return $dataTable
            ->editColumn('type', function($post) {
                return config("config.stockType.$post->type");
            })
            ->addColumn('action', function($row) {
                return view( 'widgets.action-datatable', ['route' => 'stock', 'id' => $row->id, 'deleted' => $this->trashed])->render();
            })
            ->rawColumns(['action']);

    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\User $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(Stock $model)
    {
        $query = $model->newQuery()->select($this->getsColumnsData());
        if($this->trashed == true){
            $query->onlyTrashed();
        }
        return $query;
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
   public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->addAction(['width' => userAction::showAction() == true ? '145px' : '120px', 'orderable' => false, 'searchable' => false, 'class' => 'all', 'title' => 'Əməliyyat'])
            ->parameters($this->getBuilderParameters());
    }

    /**
     * Get columns.
     *
     * @return array
     */


    protected function getColumns()
    {

        return [
            ['data' => 'id','title' => 'ID', 'searchable' => false, 'visible' => false],
            ['data' => 'type','title' => 'Səhm novü', 'orderable' => false],
            ['data' => 'name','title' => 'Səhm', 'orderable' => false],
            ['data' => 'indicator1','title' => '1-illik göstərici'],
            ['data' => 'indicator3','title' => '3-illik göstərici'],
            ['data' => 'dividend','title' => 'Dividend'],
            ['data' => 'created_at','title' => 'Yaradıldı', 'searchable' => false, 'class' => 'none'],
            ['data' => 'updated_at','title' => 'Yenilənib', 'searchable' => false, 'class' => 'none'],
        ];
    }


    protected function getsColumnsData()
    {
        $columns = [];

        foreach($this->getColumns() as $column)
        {
            $columns[] = $column['data'];
        }

        return $columns; 
    }


    protected function getBuilderParameters()
    {
        return [
            'processing' => true,
            'responsive' => true,
            'filter' => true,
            'order' => [ [1,'asc'] ],
            'lengthMenu' => [10,25,50]
        ];
    }


    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'stockdatatable_' . time();
    }
}
