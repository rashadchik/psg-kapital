<?php

namespace App\DataTables;

use App\Models\BrokerService;
use Yajra\DataTables\Services\DataTable;
use Yajra\DataTables\EloquentDataTable;
use App\Logic\userAction;

class ServiceDataTable extends DataTable
{
    protected $trashed;

    public function trashed($trash) {
        $this->trashed = $trash;
        return $this;
    }


    public function dataTable($query)
    {
        $dataTable = new EloquentDataTable($query);

        return $dataTable
            ->addColumn('action', function($row) {
                return view( 'widgets.action-datatable', ['route' => 'service', 'id' => $row->id, 'deleted' => $this->trashed])->render();
            });
    }

    /**
     * Get the query object to be processed by dataTables.
     *
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Query\Builder|\Illuminate\Support\Collection
     */
    public function query(BrokerService $model)
    {
        $query = $model->newQuery()
            ->leftJoin('pages', 'pages.relation_page', '=', 'broker_services.category_id')
            ->groupBy('broker_services.id')
            ->select($this->getColumnsData());

        if($this->trashed == true){
            $query->onlyTrashed();
        }

        return $query;
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\Datatables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->addAction(['width' => userAction::showAction() == true ? '80' : '80', 'orderable' => false, 'searchable' => false, 'class' => 'all', 'title' => 'Əməliyyat'])
            ->parameters($this->getBuilderParameters());

    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            ['data' => 'id', 'name' => 'broker_services.id', 'title' => 'ID', 'searchable' => false, 'visible' => false],
            ['data' => 'name', 'name' => 'pages.name', 'title' => 'Xidmət', 'orderable' => false, 'width' => '180px'],
            ['data' => 'title_az', 'name' => 'broker_services.title_az', 'title' => 'AZ'],
            ['data' => 'title_en', 'name' => 'broker_services.title_en', 'title' => 'EN'],
            ['data' => 'created_at', 'name' => 'broker_services.created_at', 'title' => 'Yaradıldı', 'searchable' => false, 'class' => 'none'],
            ['data' => 'updated_at', 'name' => 'broker_services.updated_at', 'title' => 'Yenilənib', 'searchable' => false, 'class' => 'none'],
        ];
    }


    protected function getColumnsData()
    {
        $extraColumns = [];

        $columns = [];

        foreach($this->getColumns() as $column)
        {
            $columns[] = $column['name'];
        }

        return array_merge($columns, $extraColumns);
    }


    protected function getBuilderParameters()
    {
        return [
            'responsive' => true,
            'order' => [ [1,'desc'] ],
            'lengthMenu' => [10, 25, 50],
        ];
    }

    protected function filename()
    {
        return 'broker_servicedatatable_' . time();
    }
}
