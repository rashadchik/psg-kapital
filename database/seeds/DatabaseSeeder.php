<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\User;
use App\Models\Config;
use App\Models\Page;
use App\Models\Rekvisit;

class DatabaseSeeder extends Seeder
{

    public function run()
    {
        Model::unguard();

        $this->call('UserSeeder');
        $this->call('ConfigSeeder');
        $this->call('PageSeeder');
        $this->call('RekvisitSeeder');

        $path = 'app/Sql/dictionary.sql';
        DB::unprepared(file_get_contents($path));
        $this->command->info('Dictionary table seeded!');

        Model::reguard();
    }

}

class UserSeeder extends Seeder
{
    public function run()
    {
        User::create([
            "first_name" => "Rəşad",
            "last_name" => "Ağayev",
            "email" => "rashad@apamark.az",
            "password" => bcrypt("agent007"),
            "verified" => 1,
            "role" => 1,
            "api_token" => str_random(60)
        ]);

        User::create([
            "first_name" => "Huseyn",
            "last_name" => "Huseynli",
            "email" => "huseyn.h@code.edu.az",
            "password" => bcrypt("atena123"),
            "verified" => 1,
            "role" => 1,
            "api_token" => str_random(60)
        ]);


        User::create([
            "first_name" => "Emil",
            "last_name" => "Alizade",
            "email" => "emil@apamark.az",
            "password" => bcrypt("emil12345"),
            "verified" => 1,
            "role" => 1,
            "api_token" => str_random(60)
        ]);
    }

}

class ConfigSeeder extends Seeder
{
    public function run()
    {
        Config::create([
            "company_name" => "PSG Kapital",
            "contact_phone" => "+(444)12 222 22 22",
            "email" => "rashad@apamark.az",
            "site_url" => "psgkapital.az",
            "location" => "40.4093101, 49.8945632",
            "google_api_key" => "",
            "lang_id" => "az",

        ]);
    }

}

class PageSeeder extends Seeder
{
    public function run()
    {
        Page::create([
            "template_id" => 0,
            "lang_id" => "az",
            "relation_page" => 1,
            "name" => "Ana səhifə",
            "slug" => "index",
            "visible" => 0,
            "target" => 1
        ]);


        Page::create([
            "template_id" => 0,
            "lang_id" => "az",
            "relation_page" => 2,
            "name" => "Axtarışın nəticələri",
            "slug" => "search",
            "visible" => 0,
            "target" => 1
        ]);

    }

}


class RekvisitSeeder extends Seeder
{
    public function run()
    {
        Rekvisit::create([
            "registration_number" => "AZ2002009465",
            "nominal_value" => 1000,
            "coupon" => 9,
            "coupon_payment_az" => "İldə iki dəfə",
            "coupon_payment_en" => "İldə iki dəfə",
            "expiration_date" => "01.03.2019",

        ]);
    }

}
