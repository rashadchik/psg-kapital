<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMediaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('media', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('doctor_id')->index();
            $table->string('lang_az', 255)->nullable();
            $table->string('lang_en', 255)->nullable();
            $table->string('lang_ru', 255)->nullable();
            $table->string('youtube_id', 20);
            $table->string('image_original', 255);
            $table->unsignedTinyInteger('order');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('media');
    }
}
