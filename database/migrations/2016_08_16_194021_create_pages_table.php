<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pages', function(Blueprint $table)
        {
            $table->increments('id');
            $table->boolean('template_id')->unsigned()->default(1);
            $table->string('lang_id', 2);
            $table->integer('order')->unsigned()->default(1);
            $table->unsignedInteger('relation_page')->nullable();
            $table->string('name', 255);
            $table->string('slug', 255);
            $table->string('description', 255)->nullable();
            $table->boolean('has_slider')->unsigned()->default(0);
            $table->boolean('has_partner')->unsigned()->default(0);
            $table->integer('parent_id')->unsigned()->nullable();
            $table->integer('main_parent_id')->unsigned()->nullable();
            //$table->string('image_cover', 255)->nullable();
            $table->string('image_original', 255)->nullable();
            $table->boolean('featured')->default(false);
            $table->string('icon', 20)->nullable();
            $table->boolean('visible')->unsigned()->default(0);
            $table->boolean('third_level')->unsigned()->default(0);
            $table->string('summary', 1500)->nullable();
            $table->text('content')->nullable();
            $table->string('content_extra', 1500)->nullable();
            $table->string('meta_description', 255)->nullable();
            $table->string('meta_keywords', 255)->nullable();
            $table->boolean('target')->unsigned()->default(1);
            $table->string('forward_url', 255)->nullable();
            $table->string('youtube_id', 20)->nullable();
            $table->nullableTimestamps();
            $table->softDeletes();

            $table->unique(['slug', 'lang_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('pages');
    }
}
