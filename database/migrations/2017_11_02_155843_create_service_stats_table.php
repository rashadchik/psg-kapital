<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateServiceStatsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('service_stats', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('category_id')->unique();
            $table->unsignedInteger('customers')->default(0);
            $table->unsignedInteger('transactions')->default(0);
            $table->string('loans', 32)->default(0);
            $table->string('proportions', 32)->default(0);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('service_stats');
    }
}
