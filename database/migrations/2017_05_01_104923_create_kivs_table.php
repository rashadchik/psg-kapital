<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateKivsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('kivs', function (Blueprint $table) {
            $table->increments('id');
            $table->string('image_original', 255);
            $table->string('lang_az', 100);
            $table->string('lang_en', 100);
            $table->string('lang_ru', 100);
            $table->string('description', 255)->nullable();
            $table->integer('order')->unsigned()->default(1);
            $table->boolean('status')->default(1);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('kivs');
    }
}
