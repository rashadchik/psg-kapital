<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRekvisitsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rekvisits', function (Blueprint $table) {
            $table->increments('id');
            $table->string('registration_number');
            $table->integer('nominal_value');
            $table->integer('coupon');
            $table->string('coupon_payment_az');
            $table->string('coupon_payment_en');
            $table->string('expiration_date', 20);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rekvisits');
    }
}
