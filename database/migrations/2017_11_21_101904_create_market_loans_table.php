<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMarketLoansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('market_loans', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedTinyInteger('type')->index();
            $table->string('title_az',255);
            $table->string('title_en',255)->nullable();
            $table->string('prise_difference1', 20);
            $table->string('prise_difference2',20);
            $table->string('name_az', 20);
            $table->string('name_en', 20);
            $table->timestamps();
            $table->softDeletes();
        });
    } 

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('market_loans');
    }
}
