<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSlidersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sliders', function (Blueprint $table) {
            $table->increments('id');
            $table->string('image_original', 255);
            $table->string('title', 255)->nullable();
            $table->string('summary', 255)->nullable();
            $table->string('link', 255)->nullable();
            $table->boolean('type')->default(1);
            $table->tinyInteger('order')->unsigned();
            $table->string('lang_id', 2);
            $table->nullableTimestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sliders');
    }
}
