let loading = "Please wait...<img src='../css/images/loading.gif'>";

$.fn.select2.defaults.set( "theme", "bootstrap" );


$(function() {

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $('.datepicker').datetimepicker({
        format: 'YYYY-MM-DD',
        useCurrent:false
    });

    $('[data-toggle="tooltip"]').tooltip();

    $('[data-toggle="popover"]').popover();

    $('#side-menu').metisMenu();


});

//Loads the correct sidebar on window load,
//collapses the sidebar on window resize.
// Sets the min-height of #page-wrapper to window size
$(function() {
    $(window).bind("load resize", function() {
        topOffset = 50;
        width = (this.window.innerWidth > 0) ? this.window.innerWidth : this.screen.width;
        if (width < 768) {
            $('div.navbar-collapse').addClass('collapse');
            topOffset = 100; // 2-row-menu
        } else {
            $('div.navbar-collapse').removeClass('collapse');
        }

        height = ((this.window.innerHeight > 0) ? this.window.innerHeight : this.screen.height) - 1;
        height = height - topOffset;
        if (height < 1) height = 1;
        if (height > topOffset) {
            $("#page-wrapper").css("min-height", (height) + "px");
        }
    });

    var url = window.location;
    var element = $('ul.nav a').filter(function() {
        return this.href == url || url.href.indexOf(this.href) == 0;
    }).addClass('active').parent().parent().addClass('in').parent();
    if (element.is('li')) {
        element.addClass('active');
    }
});



$(document).ready(function(){


    $("body").on('click', '.open-modal-dialog', function (e){
        e.preventDefault();
        modal($(this).attr('data-title'), $(this).attr('data-active'), $(this).attr('data-action'), $(this).attr('second-modal'), $(this).attr('data-large'));
    });


    function modal(title,active,action,modal,large) {
        if(modal == true){
            var mBody = "#secondModal";
            $('#mySecondModal').modal('show');
        }
        else{
            var mBody = "#modalBody";
            $('#myModal').modal('show');
        }
        if(large == true){
            $( ".modal-dialog" ).addClass( "modal-lg" );
        }
        else{
            $( ".modal-dialog" ).removeClass( "modal-lg" );
        }

        $.ajax({
            url: action,
            type: "GET",
            timeout: 20000,
            data: {"title": title, "status": active},

            beforeSend: function() {
                $(mBody).html(loading);
            },
            success: function(data){
                $(mBody).html(data);
            },
            error: function() {
                $(mBody).html('<h3>Unexpected Error!</h3>');
            }
        });
    }

    $('#modal-confirm').on('show.bs.modal', function(e) {
        $(this).find('.warning-modal').attr('action', $(e.relatedTarget).data('action'));
    });


    $("#ajaxForm").on("submit",function (event){

        alert('sa');
        event.preventDefault();
        sbmButton($(this));
    });

    function sbmButton(action){

        if (action.data('submitted') === true) {
            return false;
        }
        else
        {
            $.ajax({
                url:  action.attr('action'),
                type: action.attr('method'),
                timeout: 20000,
                headers: { 'X-CSRF-Token' : $('meta[name="csrf-token"]').attr('content') },
                data: action.serialize(),
                dataType: "json",

                beforeSend: function(response) {
                    $("#loadingButton").button('loading');
                },

                success: function(response){
                    $("#loadingButton").button('reset');

                    if(response.success == 1) {
                        if(response.once == true)
                        {
                            $('input[type=submit]').attr('disabled',true);
                            action.data('submitted', true);
                        }

                        if(response.draw)
                        {
                            if(typeof response.draw =='object')
                            {
                                $(response.draw[0]).DataTable().draw( false );
                                $(response.draw[1]).DataTable().draw( false );
                            }
                            else{
                                $(response.draw).DataTable().draw( false );
                            }

                        }

                        if(response.redirect)
                        {
                            window.location = response.redirect;
                        }


                        if(response.resetForm == true){
                            $('#ajaxForm')[0].reset();
                        }

                        if(response.close)
                        {
                            $(response.close).modal("toggle");
                        }
                    }

                    $(".msgBox").html('<div class="alert '+response.alert+'">'+response.msg+'</div>');
                },

                error: function(res){
                    $("#loadingButton").button('reset');
                    $(".msgBox").html('<h3>Unexpected Error!</h3>');
                }
            });
        }
    }



});
