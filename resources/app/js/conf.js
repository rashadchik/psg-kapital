
$(function() {

    var telInput = $("#phone"),
        errorMsg = $("#error-msg"),
        validMsg = $("#valid-msg");

    // initialise plugin
    telInput.intlTelInput({
        preferredCountries: ['az'],
        utilsScript: "vendor/int-number/utils.js"
    });

    var reset = function() {
        telInput.removeClass("error");
        errorMsg.addClass("hide");
        validMsg.addClass("hide");
    };

    // on blur: validate
    telInput.blur(function() {
        reset();
        if ($.trim(telInput.val())) {
            if (telInput.intlTelInput("isValidNumber")) {
                validMsg.removeClass("hide");
                $("#hidden").val($("#phone").intlTelInput("getNumber"));

            } else {
                telInput.addClass("error");
                errorMsg.removeClass("hide");
            }
        }
    });

    // on keyup / change flag: reset
    telInput.on("keyup change", reset);


    $('.form__cell').on('keydown', '#phone', function(e){-1!==$.inArray(e.keyCode,[46,8,9,27,13,110,190])||/65|67|86|88/.test(e.keyCode)&&(!0===e.ctrlKey||!0===e.metaKey)||35<=e.keyCode&&40>=e.keyCode||(e.shiftKey||48>e.keyCode||57<e.keyCode)&&(96>e.keyCode||105<e.keyCode)&&e.preventDefault()});


    function preventNumberInput(e) {
        var keyCode = (e.keyCode ? e.keyCode : e.which);
        if (keyCode > 47 && keyCode < 58) {
            e.preventDefault();
        }
    }

    $('.alphaonly').keypress(function(e) {
        preventNumberInput(e);
    });

});



$("#ajax-form").on("submit",function (event){

    event.preventDefault();
    ajaxForm($(this));
});

function ajaxForm(action){

    if (action.data('submitted') === true) {
        return false;
    }
    else
    {
        $.ajax({
            url:  action.attr('action'),
            type: action.attr('method'),
            timeout: 20000,
            //data: action.serialize(),
            data: new FormData(action[0]),
            dataType: "json",
            processData: false,
            contentType: false,

            beforeSend: function(response) {
                $("#loadingButton").button('loading');
            },

            success: function(response){

                $("#loadingButton").button('reset');

                if(response.success == 1) {

                    if(response.once == true)
                    {
                        $('input[type=submit]').attr('disabled',true);
                        action.data('submitted', true);
                    }

                    if(response.resetForm == true){
                        $('#ajax-form')[0].reset();
                    }

                    if(response.close)
                    {
                        $(response.close).modal("toggle");
                    }


                    $("#divForm").toggle();
                    $("#successBox").show();
                    $("#errorBox").remove();
                    $(".modal-footer").remove();
                    $("#successMessage").html(response.msg);
                }
                else{
                    $("#errorBox").show();
                    $("#msgBox").html(response.msg);
                }

            },

            error: function(res){
                $("#loadingButton").button('reset');
                $("#errorBox").show();
                $("#msgBox").html('Unexpected Error!');
            }
        });
    }
}



$("#contact-form").on("submit",function (event){

    event.preventDefault();
    contactForm($(this));
});

function contactForm(action){

    if (action.data('submitted') === true) {
        return false;
    }
    else
    {
        $.ajax({
            url:  action.attr('action'),
            type: action.attr('method'),
            timeout: 20000,
            data: action.serialize(),
            dataType: "json",
            beforeSend: function(response) {
                $("#loadingButton").button('loading');
            },

            success: function(response){

                $("#loadingButton").button('reset');

                if(response.success == 1) {

                    $('input[type=submit]').attr('disabled',true);

                    action.data('submitted', true);

                    $("#divForm").toggle();
                    $("#successBox").show();
                    $("#errorBox").remove();
                    $("#successMessage").html(response.msg);
                }
                else{
                    $("#errorBox").show();
                    $("#msgBox").html(response.msg);
                }

            },

            error: function(res){
                $("#loadingButton").button('reset');
                $("#errorBox").show();
                $("#msgBox").html('Unexpected Error!');
            }
        });
    }
}


$("#subscribe-form").on("submit",function (event){
    event.preventDefault();
    subscribe($(this));
});

function subscribe(action){

    if (action.data('submitted') === true) {
        return false;
    }
    else
    {
        $.ajax({
            url:  action.attr('action'),
            type: action.attr('method'),
            timeout: 20000,
            data: action.serialize(),
            dataType: "json",

            beforeSend: function(response) {
                $("#loadingSubButton").button('loading');
            },

            success: function(response){

                $("#loadingSubButton").button('reset');

                if(response.success == 1) {

                    $('#subscribe-form')[0].reset();
                }

                $(".subscribeBox").html('<span class="label '+response.alert+'">'+response.msg+'</span>');
            },

            error: function(res){
                $("#loadingSubButton").button('reset');
                $(".subscribeBox").html('<h3>Unexpected Error!</h3>');
            }
        });
    }
}



var requestSent = false;

$(".open-modal-dialog").on("click",function (event){
    event.preventDefault();

    if(requestSent) {return;}
    modal($(this).attr('data-title'), $(this).attr('data-action'));

});



function modal(title,action) {

    requestSent = true;

    $('#myModal').modal('show');

    $.ajax({
        url: action,
        type: "GET",
        timeout: 20000,
        beforeSend: function() {
            $("#modalBody").html('<img src="/images/loading.svg" width="50px">');
        },
        success: function(data){
            $("#modalBody").html(data);
            requestSent = false;

        },
        error: function() {
            $("#modalBody").html('<h3>Whoops! Something went wrong!</h3>');
            requestSent = false;

        }
    });
}




var page = 1;

$(window).scroll(function() {
    if($(window).scrollTop() + $(window).height() >= $(document).height()) {
        if(requestSent) {return;}
        page++;
        loadMoreData(page);
    }
});

function loadMoreData(page){

    requestSent = true;

    $.ajax(
        {
            url: '?year={{$thisYear}}&page=' + page,
            type: "get",
            beforeSend: function()
            {
                $('.ajax-loading').show();
            }
        })
        .done(function(data)
        {
            $('.ajax-loading').hide();


            if(data.html == ""){
                return;
            }
            else{
                $("#post-news").append(data.html);
            }

            requestSent = false;

        })
        .fail(function(jqXHR, ajaxOptions, thrownError)
        {
            $('.ajax-loading').html("{{ trans('locale.whops') }}");
            requestSent = false;
        });
}



