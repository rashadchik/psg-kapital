<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'home_page' => 'Əsas səhifə',
    'upload_image_error' => 'Server error while uploading image',
    'fail' => 'Əməliyyat baş tutmadı. Transaction: #:number',
    'page' => 'Səhifələr',
    'create' => 'Əlavə et',
    'edit' => 'Düzəliş et',
    'delete' => 'Sil',
    'save' => 'Yadda saxla',
    'next' => 'İrəli',
    'update_success' => 'Dəyişikliklər qeydə alındı',
    'store_success' => 'Əməliyyat uğurlu alındı',
    'active' => 'Aktiv',
    'pending' => 'Gözləmədə',
    'blocked' => 'Bloklanmış',
    'featured' => 'Slider',
    'trash' => "Silinmiş",
    'all' => 'Hamı',
    'restore' => 'Aktiv et',
    'show' => 'Göstər',
    'order' => 'Ardıcıllıq',
    'site_by' => 'Bu bir :site məhsuludur',
    'error_send_message' => 'Məktubun göndərilməsi baş tutmadı',
    'cancel' => 'İmtina',
    'whops' => 'Whoops! Something went wrong!',
    'invalid_number' => 'Nömrə düzgün deyil',
    'search_result' => "<span class='searchresult__key'>:keyword</span> sözü üzrə <span class='searchresult__key'>:count</span> nəticə tapıldı"
];
