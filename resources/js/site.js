$(window).load(function(){
    //EqualHeights
    if ($('.news_list article').length){
        var resizeTimer;
        $( window ).resize(function() {
            var width = $(window).width();
            clearTimeout(resizeTimer);
            resizeTimer = setTimeout(eqHeightGrey, 500);
        });
        eqHeightGrey()
    }

    function eqHeightGrey(){
        var width = $(window).width();
        $('.news_list article').removeAttr('style');
        if (width < 480){
            $('.news_list article').height("auto");
        }else{
            $('.news_list article').equalHeights();
        }
    }

    if ($('.about_blocks article').length){
        var resizeTimer;
        $( window ).resize(function() {
            var width = $(window).width();
            clearTimeout(resizeTimer);
            resizeTimer = setTimeout(eqHeightGrey1, 500);
        });
        eqHeightGrey1()
    }

    function eqHeightGrey1(){
        var width = $(window).width();
        $('.about_blocks article').removeAttr('style');
        if (width < 767){
            $('.about_blocks article').height("auto");
        }else{
            $('.about_blocks article').equalHeights();
        }
    }

    if ($('.reports article').length){
        var resizeTimer;
        $( window ).resize(function() {
            var width = $(window).width();
            clearTimeout(resizeTimer);
            resizeTimer = setTimeout(eqHeightGrey2, 500);
        });
        eqHeightGrey2()
    }

    function eqHeightGrey2(){
        var width = $(window).width();
        $('.reports article').equalHeights();

    }
});

$(function(){

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $('.form__cell').on('keydown', '#phone', function(e){-1!==$.inArray(e.keyCode,[46,8,9,27,13,110,190])||/65|67|86|88/.test(e.keyCode)&&(!0===e.ctrlKey||!0===e.metaKey)||35<=e.keyCode&&40>=e.keyCode||(e.shiftKey||48>e.keyCode||57<e.keyCode)&&(96>e.keyCode||105<e.keyCode)&&e.preventDefault()});


    function preventNumberInput(e) {
        var keyCode = (e.keyCode ? e.keyCode : e.which);
        if (keyCode > 47 && keyCode < 58) {
            e.preventDefault();
        }
    }

    $('.alphaonly').keypress(function(e) {
        preventNumberInput(e);
    });

    $(".open_popup").click(function(){
        $(".popup").stop(true, true).fadeIn("slow");
    });
    $(".popup span.closed").click(function(){
        $(".popup").stop(true, true).fadeOut("slow");
    });


	//MobileMenu

    $(".mobile_menu li.drop>a").click(function(){
        if($(this).hasClass("active")){
            $(this).stop(true, true).removeClass("active");
            $(this).siblings("ul").stop(true, true).slideUp("show");
        }
        else{
            $(this).stop(true, true).addClass("active");
            $(this).siblings("ul").stop(true, true).slideDown("show");
        }
        return false;
    });

	$(".menu_btn").click(function(){
		if($(this).hasClass("closed")){
			$(".mobile_menu").stop(true, true).hide("normal");
			$(this).stop(true, true).removeClass("closed");
			$("body").stop(true, true).removeClass("noscroll");
		}
		else{
			$("body").stop(true, true).addClass("noscroll");
			$(this).stop(true, true).addClass("closed");
			$(".mobile_menu").stop(true, true).show("normal");
		}
		return false
	});
	
	$(".mobile_menu li.back a").click(function(){
		$(this).parent().parent().stop(true, true).removeClass("show");
		return false
	});
	
	$(".mobile_menu li.sub>a").click(function(){
		$(this).siblings("ul").stop(true, true).addClass("show");
		return false
	});
	
	//Search
	$(".search_m_btn,.search_btn").click(function(){
		if($(this).hasClass("active")){
			$(this).stop(true, true).removeClass("active");
			$(".search_block").stop(true, true).slideUp("slow");
		}
		else{
			$(this).stop(true, true).addClass("active");
			$(".search_block").stop(true, true).slideDown("slow");
			$(".search_block input").focus();
		}
		return false;
	});
	
	$(document).keyup(function(e) {
		if (e.keyCode === 27) {
			$(".search_m_btn,.search_btn").stop(true, true).removeClass("active");
			$(".search_block").stop(true, true).slideUp("slow");
		}
	});
	
	$(document).click(function(e){
		if (!$(e.target).is(".search_block, .search_block *")) {
			$(".search_m_btn,.search_btn").stop(true, true).removeClass("active");
			$(".search_block").stop(true, true).slideUp("slow");
		}
	});
	
	//Tabs
	$(".tabnav a:first,.pi_tab_nav a:first,.main_tab_nav a:first").addClass("active");
	$(".tabnav a,.pi_tab_nav a,.main_tab_nav a").click(function(){
		var indis = $(this).index();
		$(".tabnav a,.pi_tab_nav a,.main_tab_nav a").removeClass("active");
		$(this).addClass("active");
		$(".tab_block,.pi_tab_panel,.main_tab_panel").hide();
		$(".tab_block:eq("+indis+"),.pi_tab_panel:eq("+indis+"),.main_tab_panel:eq("+indis+")").fadeIn();
		return false;
	});
	
	//Slider
	/* $("#slider").owlCarousel({
		dots: true,
		nav : false,
		touchDrag  : false,
		mouseDrag  : false,
		autoplay: true,
		autoplayHoverPause: true,
		autoplayTimeout: 5000,
		autoplaySpeed: 500,
		loop: true,
		animateIn: 'fadeIn',
		animateOut: 'fadeOut',
		items: 1,
		margin: 0,
		smartSpeed: 450,
	}); */
	
	//Valyuta
	$("#valyuta").owlCarousel({
		dots: false,
		nav : false,
		autoplay: true,
		autoplayTimeout: 5000,
		autoplayHoverPause: true,
		loop: true,
		items: 2,
		margin: 30,
		smartSpeed: 450,
		responsive : {
			// breakpoint from 0 up
			0 : {
				slideBy: 1,
				items: 1
			},
			// breakpoint from 480 up
			500 : {
				slideBy: 3,
				items: 3
			},
			// breakpoint from 768 up
			768 : {
				items: 3
			},
			// breakpoint from 992 up
			992 : {
				items: 4
			},
			// breakpoint from 1200 up
			1200 : {
				items: 5
			}
		}
	});
	
	//ServicesCarousel
	$("#services_carousel").owlCarousel({
		dots: false,
		nav : true,
		autoplay: false,
		autoplayTimeout: 5000,
		autoplayHoverPause: true,
		loop: false,
		items: 2,
		margin: 0,
		smartSpeed: 450,
		responsive : {
			// breakpoint from 0 up
			0 : {
				slideBy: 1,
				items: 1
			},
			// breakpoint from 480 up
			480 : {
				slideBy: 2,
				items: 2
			},
			// breakpoint from 768 up
			768 : {
				items: 3
			},
			// breakpoint from 992 up
			992 : {
				items: 3
			},
			// breakpoint from 1200 up
			1200 : {
				items: 4
			}
		}
	});
	
	//PartnersCarousel
	$("#partners_carousel").owlCarousel({
		dots: false,
		nav : false,
		autoplay: true,
		autoplayTimeout: 5000,
		autoplayHoverPause: true,
		loop: true,
		items: 2,
		margin: 0,
		smartSpeed: 450,
		responsive : {
			// breakpoint from 0 up
			0 : {
				slideBy: 1,
				items: 1
			},
			// breakpoint from 480 up
			480 : {
				slideBy: 2,
				items: 2
			},
			// breakpoint from 768 up
			768 : {
				items: 3
			},
			// breakpoint from 992 up
			992 : {
				items: 5
			},
			// breakpoint from 1200 up
			1200 : {
				items: 6
			}
		}
	});
	
	//Gallery
	if( $(".news_gallery figure").length ){
		$(".news_gallery figure").magnificPopup({
			delegate: "a.zoom",
			type: "image",
			tLoading: "Loading image #%curr%...",
			mainClass: "mfp-img-mobile",
			gallery: {
				enabled: true,
				navigateByImgClick: false,
				preload: [0,1] // Will preload 0 - before current, and 1 after the current image
			},
			image: {
				tError: '<a href="%url%">The image #%curr%</a> could not be loaded.',				
			}
		});
	}

    if( $(".licenses figure").length ){
        $(".licenses figure").magnificPopup({
            delegate: "a.zoom",
            type: "image",
            tLoading: "Loading image #%curr%...",
            mainClass: "mfp-img-mobile",
            gallery: {
                enabled: true,
                navigateByImgClick: false,
                preload: [0,1] // Will preload 0 - before current, and 1 after the current image
            },
            image: {
                tError: '<a href="%url%">The image #%curr%</a> could not be loaded.',
            }
        });
    }

	//FAQAccordion
	$(".faq .head").on("click", function(){
		if($(this).hasClass("active")){
			$(this).stop().removeClass("active");
			$(this).siblings(".body").stop().slideUp();
		}
		else{
			$(".faq .head").stop().removeClass("active");
			$(this).stop().addClass("active");
			$(".faq .body").stop().slideUp();
			$(this).siblings(".body").stop().slideDown();
		}
	});
	
});

setInterval(function () {
	var a = $(".cur_col .active");
	aN = a.next()
	aNi = aN.index()
	if (aN.length > 0) {
		$(".cur_col").css("top", -aNi * 20)
		a.removeClass("active");
		aN.addClass("active");
	} else {

		$(".cur_col").each(function () {
			var t = $(this);
			all = t.find("p.active").prevAll()
			all.remove()
			$(".cur_col").removeClass("animate")
			$(".cur_col").css("top", 0)
			t.append(all)
			setTimeout(function () {
				$(".cur_col").addClass("animate")
			}, 500)
		})
	}
}, 2500)





function ajaxForm(action){

    if (action.data('submitted') === true) {
        return false;
    }
    else
    {
        $.ajax({
            url:  action.attr('action'),
            type: action.attr('method'),
            timeout: 20000,
            headers: { 'X-CSRF-Token' : $('meta[name="csrf-token"]').attr('content') },
            //data: action.serialize(),
            data: new FormData(action[0]),
            dataType: "json",
            processData: false,
            contentType: false,

            beforeSend: function(response) {
                $("#loadingButton").button('loading');
            },

            success: function(response){

                $("#loadingButton").button('reset');

                if(response.success == 1) {

                    if(response.once == true)
                    {
                        $('button[type=submit]').attr('disabled',true);
                        action.data('submitted', true);
                    }

                    if(response.resetForm == true){
                        $('#'+action.attr('id'))[0].reset();
                    }

                    if(response.close)
                    {
                        $(response.close).modal("toggle");
                    }


                    $("#divForm").slideUp(100);
                    $("#successBox").show();
                    $("#successMessage").show().prepend(response.msg);
                }

                $("#msgBox").show().html(response.msg);
            },

            error: function(res){
                $("#loadingButton").button('reset');
                $("#msgBox").show().html('Unexpected Error!');
            }
        });
    }
}

function testForm(action){

    if (action.data('submitted') === true) {
        return false;
    }
    else
    {
        $.ajax({
            url:  action.attr('action'),
            type: action.attr('method'),
            timeout: 20000,
            headers: { 'X-CSRF-Token' : $('meta[name="csrf-token"]').attr('content') },
            data: action.serialize(),
            dataType: "json",


            beforeSend: function(response) {
                $("#button_test").button('loading');
            },

            success: function(response){

                $("#button_test").button('reset');

                if(response.success == 1) {

                    if(response.once == true)
                    {
                        $('#button_test').attr('disabled',true);
                        action.data('submitted', true);
                    }

                }

                $("#div_msg").html(response.msg);

            },

            error: function(res){
                $("#button_test").button('reset');
                $("#div_msg").html('Unexpected Error!');
            }
        });
    }
}



function popupForm(action){

    if (action.data('submitted') === true) {
        return false;
    }
    else
    {
        $.ajax({
            url:  action.attr('action'),
            type: action.attr('method'),
            timeout: 20000,
            headers: { 'X-CSRF-Token' : $('meta[name="csrf-token"]').attr('content') },
            data: action.serialize(),
            dataType: "json",

            beforeSend: function(response) {
                $("#button").button('loading');
            },

            success: function(response){

                $("#button").button('reset');

                if(response.success == 1) {

                    if(response.once == true)
                    {
                        $('button[type=submit]').attr('disabled',true);
                        action.data('submitted', true);
                    }

                    if(response.resetForm == true){
                        $('#contact_form')[0].reset();
                    }

                    $(".form").slideUp(300);
                    $("#success_box").show();
                    $("#success_msg").prepend(response.msg);
                    $("#error_msg").hide();
                }
                else{
                    $("#error_msg").show().html(response.msg);
                }
            },

            error: function(res){
                $("#button").button('reset');
                $("#error_msg").show().html('Unexpected Error!');
            }
        });
    }
}



$("#exam_form").on("submit",function (event){

    event.preventDefault();
    examForm($(this));
});


function examForm(action){

    if (action.data('submitted') === true) {
        return false;
    }
    else
    {
        $.ajax({
            url:  action.attr('action'),
            type: action.attr('method'),
            timeout: 20000,
            headers: { 'X-CSRF-Token' : $('meta[name="csrf-token"]').attr('content') },
            data: action.serialize(),
            dataType: "json",


            beforeSend: function(response) {
                $("#result").button('loading');
            },

            success: function(response){

                $("#result").button('reset');

                if(response.success == 1) {

                    if(response.once == true)
                    {
                        $('#result').attr('disabled',true);
                        action.data('submitted', true);
                    }

                    if(response.resetForm == true){
                        $('#exam_form')[0].reset();
                    }

                    $("#successBox").show();
                    $("#answerBox").show();
                    $("#buttonBox").slideUp(300);
                    $("#successMsg").html(response.msg);
                }
                else{
                    $("#msgBox").html(response.msg);
                }

            },

            error: function(res){
                $("#result").button('reset');
                $("#msgBox").html('Unexpected Error!');
            }
        });
    }
}


