<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
    <h4 class="modal-title">@yield('title') {!! isset($subtitle) ? '<span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span> '.$subtitle : ''!!}</h4>
</div>

@if(isset($error))
    @include('widgets.alert', ['class' => 'danger', 'msg' => $error])
    {{die()}}
@endif

@if(isset($form))
    @yield('form')
@endif

<div class="modal-body">
    <div class="col-md-12">
        <div class="row">
            @yield('content')
        </div>
    </div>
    <div class="clearfix"></div>
</div>

<div class="modal-footer">

    {!! Form::button(trans('locale.cancel'), ['class' => 'btn btn-danger', "data-dismiss"=>"modal"]) !!}
    @yield('button')

</div>

@if(isset($form))
    {!! Form::close() !!}
@endif

@stack('scripts')
