@extends('planes.web')
@section('layout')

    @include('web.elements.popup')

    @include('web.elements.search')

    @include('web.elements.mobile-menu')
    
    @include('web.elements.header')

    @yield('content')

    @include('web.elements.footer')

@endsection