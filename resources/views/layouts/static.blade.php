@extends('layouts.web')
@section('content')

    {!! Breadcrumbs::render('web', $parentPage, $page, false) !!}

    @yield('static')

@endsection


