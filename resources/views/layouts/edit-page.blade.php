@extends('layouts.app')

@section('content')

    <style>
        .select2-results__message {
            display: none !important;
        }

    @if(isset($type))

    .dz-image{
        width:{{ config("config.galleryPlugins.$type.image.dz-width") }}px !important;
        height:auto !important;
    }

    @endif


    </style>

    @if(isset($edit))
        <ul class="nav nav-tabs">
            <li role="presentation" {{ activeUrl(route("edit_$route", $id)) }}>
                <a href="{{ route("edit_$route", $id) }}"><i class="fa fa-file"></i> {{ trans('locale.page') }}</a>
            </li>

            @if(isset($info) && ($info->template_id == 9 || $relPage->template_id == 9))
                <li role="presentation" {{ activeUrl(route("edit_vacancy", $id)) }}>
                    <a href="{{ route("edit_vacancy", $id) }}"><i class="fa fa-table"></i> Əmək haqqı və son tarix</a>
                </li>
            @endif

            @if($relPage->slug != 'index' && array_key_exists($relPage->template_id, config("config.modulePlugin.$route")))

                @foreach( config("config.modulePlugin.".$route.".".$relPage->template_id) as $plugin)

                    <li role="presentation" {{ activeUrl(route('gallery_index', [$id,  $plugin ])) }}>
                        <a href="{{ route('gallery_index', [$id,  $plugin ]) }}"><i class="fa fa-{{ config("config.galleryPlugins.$plugin.icon") }}"></i> {{ config("config.galleryPlugins.$plugin.title") }}</a>
                    </li>

                @endforeach

            @endif


        </ul>

        <div class="tab-content">
            <div class="tab-pane in active"><br>

                @if(!isset($galleryPage) && !isset($dynamicPage))
                    <ul class="nav nav-tabs">

                        @foreach(config("app.locales") as $key => $locale)
                            <li role="presentation" @if($key == $lang) class="active" @endif>
                                <a href="{{ route("edit_$route", $id) }}?lang_id={{ $key }}">{{ $locale }}</a>
                            </li>
                        @endforeach

                    </ul>

                    <div class="tab-content"><br>

                        <div class="tab-pane in active">

                            @yield('edit')

                        </div>

                    </div>
                @else

                    @yield('edit')

                @endif

            </div>
        </div>
    @else
        @yield('edit')
    @endif

@endsection

@push('scripts')

    <script src="{{ asset('vendor/ckeditor/ckeditor.js')}}"></script>

    <script>

        $(function() {

            val = $('#template').find("option:selected").val();


            if(val)
            {
                $('.hide'+val).hide();
                $("div[class^='show'],div[class*=' show']").hide();
                $('.show'+val).show();



                $('#template ').on('change', function() {
                    $('.form-group').show();
                    $("div[class^='show'],div[class*=' show']").hide();
                    var value = $(this).find("option:selected").val();
                    $('.hide'+value).hide();
                    $('.show'+value).show();
                })
            }

        });


        @if(isset($category))
            $('#category_list').select2({
                data: {!! $category !!}
            });
            @if(isset($info))
                $('#category_list').val({{ isset($editArticle) ? $info->category_id : $info->parent_id }}).trigger("change");
            @endif
        @endif




        $('#meta_keywords').select2({
            @if(isset($keywords)) data: {!! json_encode($keywords) !!}, @endif
            tags: true,
            maximumSelectionLength: 10,

        }).on("change", function(e) {
            var isNew = $(this).find('[data-select2-tag="true"]');
            if(isNew.length){
                isNew.replaceWith('<option selected value="'+isNew.val()+'">'+isNew.val()+'</option>');
            }
        });


        @if(isset($keywords))

        $('#meta_keywords').val({!! json_encode($keywords) !!}).trigger('change');

        @endif



        @if(!isset($editor) || $editor == true)

            @if($route != 'article')

            CKEDITOR.replace('ck-editor-mini', { customConfig: '{{ asset('vendor/ckeditor/config-mini.js?v=2') }}' });

            @endif


            CKEDITOR.replace('ck-editor', { customConfig: '{{ asset('vendor/ckeditor/config.js?v=6') }}' });


            @if($route == 'category')

            CKEDITOR.replace('ck-editor-extra', { customConfig: '{{ asset('vendor/ckeditor/config-mini.js?v=2') }}' });

            @endif

        @endif

    </script>

    @include('web.elements.elfinder-selector')

@endpush
