@extends('planes.app')

<!-- Main Content -->
@section('layout')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">

            @section ('register_panel_title','Email ünvanınızı daxil edin')
            @section ('register_panel_body')

            @if (session('status'))
                <div class="alert alert-success">
                    {{ session()->get('status') }}
                </div>
            @endif

            {!! Form::open(['url'=>'/password/email', 'method'=>'POST', "class" => "form-horizontal", "role" => "form"]) !!}

                <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                    {!! Form::label("email", "E-Mail", ['class' => 'col-md-4 control-label']) !!}

                    <div class="col-md-6">
                        {!! Form::email("email", old('email'), ["class" => "form-control", "autofocus"]) !!}

                        @if ($errors->has('email'))
                            <span class="help-block">
                                <strong>{{ $errors->first('email') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-md-6 col-md-offset-4">
                        {!! Form::button("Göndər", ["class" => "btn btn-primary", "type" => "submit"]) !!}
                    </div>
                </div>

            {!! Form::close() !!}

            @endsection

            @include('widgets.panel', array('as'=>'register', 'header'=>true))

        </div>
    </div>
</div>
@endsection
