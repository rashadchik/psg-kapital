@extends ('layouts.app', ['panel' => true])
@section ('page_heading', $title)
@section ('content')

    @if(!is_null($config->sitemap))
        <div class="alert alert-success">
            Son yenilənmə: {{$config->sitemap}}
        </div>
    @endif


    {!! Form::open(['url'=>route('sitemap.store'), 'method'=>'POST', 'class' => 'text-center']) !!}

    @include('widgets.form-submit', ['text' => 'Yenilə', 'class' => 'success', 'icon' => 'refresh'])

    {!! Form::close() !!}

@endsection
