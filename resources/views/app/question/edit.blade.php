@extends ('layouts.app', ['script' => true])
@section ('page_heading', 'Düzəliş')


@section('content')

    <style>
        .select2-results__message {
            display: none !important;
        }
    </style>

    <div class="col-lg-10 col-lg-offset-1">
        {!! Form::open(['url'=>route("update_question", $info->id), 'method'=>'PATCH']) !!}

        @include('widgets.form-large')

        @include('widgets.form-submit', ['text' => trans('locale.save'), 'class' => 'success', 'icon' => 'save'])

        {!! Form::close() !!}
    </div>

@endsection

@push('scripts')

    @include('web.elements.elfinder-selector')

@endpush