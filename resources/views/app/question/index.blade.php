@extends ('layouts.app', ['panel' => true, 'create' => "create_question", 'title' => trans('locale.create')])
@section ('page_heading', 'Suallar')

@section ('content')
    @include('widgets.modal-confirm')

    {!! $dataTable->table(['class' => 'table table-striped table-hover', 'id' => 'datatable', 'width' => '100%']) !!}

@endsection

@push('scripts')

{!! $dataTable->scripts() !!}

@endpush
