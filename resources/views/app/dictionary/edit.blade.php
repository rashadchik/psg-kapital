@extends(request()->ajax() ? 'layouts.modal' : 'layouts.app', ['form' => true, 'script' => true] )
@section('title', $title)
@section('class', 'col-md-6 col-md-offset-3')
@section('form', Form::open(array('route'=>['update_dictionary', $info['id']], 'method'=>'PATCH', 'class'=>'form-horizontal', 'id' => 'ajaxForm')))

@section('button', Form::button(icon('save').trans('locale.save'), ['class' => 'btn btn-success', 'type' => 'submit']))


@section('content')

	@include('widgets.lang-tab', ['input' => 'textarea','editor' => $info->editor, 'info' => $words, 'multiRow' => true, 'name' => 'content'])

@endsection


@push('scripts')


<script src="{{asset(mix('js/script.js'))}}"></script>
<script src="{{ asset('vendor/ckeditor/ckeditor.js')}}"></script>

<script>
    CKEDITOR.replace('ck-editor-mini-az', { customConfig: '{{ asset('vendor/ckeditor/config-mini.js?v=2') }}' });

    CKEDITOR.replace('ck-editor-mini-en', { customConfig: '{{ asset('vendor/ckeditor/config-mini.js?v=2') }}' });
</script>

@endpush