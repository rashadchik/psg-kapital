@extends ('layouts.app')
@section ('page_heading', $title)

@section ('content')

    {!! $dataTable->table(['class' => 'table table-striped table-hover', 'id' => 'dictionary', 'width' => '100%']) !!}

@endsection

@push('scripts')

{!! $dataTable->scripts() !!}

@endpush
