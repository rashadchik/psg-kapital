@extends ('layouts.edit-page', ['script' => true])
@section ('page_heading', $info->title)

@section('edit')

    @include('widgets.modal-confirm', ['trash' => true])

    <div class="col-lg-10 col-lg-offset-1">
        {!! Form::open(['url'=>route('update_article', $info->id), 'method'=>'PATCH', 'files' => true]) !!}

        @include('widgets.form-large')

        @include('widgets.form-submit', ['id' => $info->id, 'text' => trans('locale.save'), 'class' => 'success', 'icon' => 'save', 'return' => true, 'del' => true, 'route' => 'article'])

        {!! Form::close() !!}
    </div>

@endsection