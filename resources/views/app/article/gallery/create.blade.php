@extends ('layouts.edit-page', ['script' => true])
@section ('page_heading', $title)

@section('edit')


    <div class="col-lg-10 col-lg-offset-1">
        {!! Form::open(['url'=>route('gallery_store', [$id, $type]), 'method'=>'POST', 'class' => 'dropzone', 'files'=>true, 'id'=>'real-dropzone']) !!}

        <div class="dz-message">

        </div>

        <div class="fallback">
            <input name="file" type="file" multiple />
        </div>

        <div class="dropzone-previews" id="dropzonePreview"></div>

        <h4 style="text-align: center;color:#428bca;">Şəkilləri bura sürüşdürün  <span class="glyphicon glyphicon-hand-down"></span></h4>

        @include('widgets.dropzone-template')

        {!! Form::hidden('csrf-token', csrf_token(), ['id' => 'csrf-token']) !!}

        {!! Form::close() !!}

    </div>

@endsection

@push('scripts')

<script src="{{ asset("js/dropzone.js") }}" type="text/javascript"></script>

@include('web.elements.dropzone', ['maxFile' => config("config.galleryPlugins.$type.maxFile") ])

@endpush