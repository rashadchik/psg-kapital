@extends ('layouts.edit-page', ['script' => true])
@section ('page_heading', $title)

@section('edit')

    <div class="col-lg-10 col-lg-offset-1">
        {!! Form::open(['url'=>route('gallery_store', [$id, $type]), 'method'=>'POST', 'class' => 'dropzone', 'files'=>true, 'id'=>'real-dropzone']) !!}

        <div class="dz-message">

        </div>

        <div class="fallback">
            <input name="file" type="file" multiple />
        </div>

        <div class="dropzone-previews" id="dropzonePreview"></div>

        <h4 style="text-align: center;color:#428bca;">Şəkilləri buraya sürüşdürün  <span class="glyphicon glyphicon-hand-down"></span></h4>

        @include('widgets.dropzone-template')


        {!! Form::close() !!}

    </div>

@endsection

@push('scripts')

<script src="{{ asset("js/dropzone.js") }}" type="text/javascript"></script>

<script>

    var photo_counter = 0;
    Dropzone.options.realDropzone = {

        uploadMultiple: false,
        parallelUploads: 3,
        maxFiles: '{{ config("config.galleryPlugins.$type.maxFile") }}',
        maxFilesize: '{{$maxFileSize}}',
        previewsContainer: '#dropzonePreview',
        previewTemplate: document.querySelector('#preview-template').innerHTML,
        addRemoveLinks: true,
        dictRemoveFile: 'Sil',
        dictFileTooBig: 'Image is bigger than {{$maxFileSize}} MB',

        // The setting up of the dropzone
        init:function() {

            // Add server images
            var myDropzone = this;

            $.get('{!! route('gallery_get', [$id, $type]) !!}', function(data) {


                $.each(data.images, function (key, value) {

                    var file = {name: value.original, size: value.size};
                    myDropzone.options.addedfile.call(myDropzone, file);
                    myDropzone.options.thumbnail.call(myDropzone, file, '{!! asset($dir) !!}/' + value.server);
                    myDropzone.emit("complete", file);
                    photo_counter++;
                    $("#photoCounter").text( "(" + photo_counter + ")");
                });
            });

            this.on("removedfile", function(file) {

                $.ajax({
                    type: 'POST',
                    url: '{!! route('gallery_destroy', [$id, $type]) !!}',
                    data: {filename: file.name, _token: '{{csrf_token()}}', _method: 'DELETE'},
                    dataType: 'html',
                    success: function(data){
                        var rep = JSON.parse(data);
                        if(rep.code == 200)
                        {
                            photo_counter--;
                            $("#photoCounter").text( "(" + photo_counter + ")");
                        }

                    }
                });

            } );
        },
        error: function(file, response) {
            if($.type(response) === "string")
                var message = response; //dropzone sends it's own error messages in string
            else
                var message = response.message;
            file.previewElement.classList.add("dz-error");
            _ref = file.previewElement.querySelectorAll("[data-dz-errormessage]");
            _results = [];
            for (_i = 0, _len = _ref.length; _i < _len; _i++) {
                node = _ref[_i];
                _results.push(node.textContent = message);
            }
            return _results;
        },
        success: function(file,done) {
            photo_counter++;
            $("#photoCounter").text( "(" + photo_counter + ")");
        }
    }


</script>
@endpush