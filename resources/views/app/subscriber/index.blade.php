@extends ('layouts.app', ['panel' => true, 'create' => 'create_subscriber', 'title' => trans('locale.create'), 'modal' => true])
@section ('page_heading', $title)

@section ('content')
    @include('widgets.modal-confirm')

    <ul class="nav nav-tabs">
        <li role="presentation" {{ activeUrl(route('subscriber.index')) }}><a href="{{route('subscriber.index')}}">{{trans('locale.all')}}</a></li>
        <li role="presentation" {{ activeUrl(route('pending_subscriber')) }}><a href="{{route('pending_subscriber')}}">{{trans('locale.pending')}}</a></li>
    </ul>

    <div class="tab-content">
        <div class="tab-pane active"><br>
            {!! $dataTable->table(['class' => 'table table-striped table-hover', 'id' => 'subscriber', 'width' => '100%']) !!}
        </div>
    </div>
@endsection

@push('scripts')

{!! $dataTable->scripts() !!}

@endpush
