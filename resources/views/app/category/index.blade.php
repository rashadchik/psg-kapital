@extends ('layouts.app', ['panel' => true, 'create' => 'create_category', 'title' => trans('locale.create')])
@section ('page_heading', $title)

@section ('content')
    @include('widgets.modal-confirm')

    <ul class="nav nav-tabs">
        <li role="presentation" {{ activeUrl(route('category.index')) }}><a href="{{route('category.index')}}">{{trans('locale.active')}}</a></li>
        <li role="presentation" {{ activeUrl(route('trashed_category')) }}><a href="{{route('trashed_category')}}">{{trans('locale.trash')}}</a></li>
        @if(auth()->user()->role == 1)
        <li role="presentation" {{ activeUrl(route('order_category', 'az')) }}><a href="{{route('order_category', 'az')}}">{{trans('locale.order')}}</a></li>
        @endif
    </ul>

    <div class="tab-content">
        <div class="tab-pane active"><br>
            {!! $dataTable->table(['class' => 'table table-striped table-hover', 'id' => 'categories', 'width' => '100%']) !!}
        </div>
    </div>
@endsection

@push('scripts')

{!! $dataTable->scripts() !!}

@endpush
