@extends ('layouts.app', ['panel' => true, 'create' => "create_$route", 'title' => trans('locale.create')])
@section ('page_heading', $title)

@section ('content')
    @include('widgets.modal-confirm')

    <ul class="nav nav-tabs">
        <li role="presentation" {{ activeUrl(route("$route.index")) }}><a href="{{ route("$route.index") }}">{{trans('locale.active')}}</a></li>
        <li role="presentation" {{ activeUrl(route("trashed_$route")) }}><a href="{{ route("trashed_$route") }}">{{trans('locale.trash')}}</a></li>
    </ul>

    <div class="tab-content">
        <div class="tab-pane active"><br>
            {!! $dataTable->table(['class' => 'table table-striped table-hover', 'id' => 'datatable', 'width' => '100%']) !!}
        </div>
    </div>
@endsection

@push('scripts')

{!! $dataTable->scripts() !!}

@endpush
