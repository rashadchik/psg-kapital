@extends ('layouts.app', ['panel' => true, 'create' => 'create_media', 'title' => trans('locale.create')])
@section ('page_heading', $title)
@section ('content')

    @include('widgets.modal-confirm')

    <ul class="nav nav-tabs">
        <li role="presentation" {{ activeUrl(route('media.index')) }}><a href="{{route('media.index')}}">{{trans('locale.active')}}</a></li>
        <li role="presentation" {{ activeUrl(route('trashed_media')) }}><a href="{{route('trashed_media')}}">{{trans('locale.trash')}}</a></li>
    </ul>

    <div class="tab-content">
        <div class="tab-pane active"><br>
            {!! $dataTable->table(['class' => 'table table-striped table-hover', 'id' => 'media', 'width' => '100%']) !!}
        </div>
    </div>

@endsection

@push('scripts')

    {!! $dataTable->scripts() !!}

@endpush
