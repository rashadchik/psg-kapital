@extends(request()->ajax() ? 'layouts.modal' : 'layouts.app', ['form' => true, 'script' => true] )
@section('title', 'Yeni istifadəçi')
@section('class', 'col-md-6 col-md-offset-3')
@section('form', Form::open(array('route'=>'store_user', 'method'=>'POST', 'class'=>'form-horizontal')))
@section('button', Form::button(trans('locale.create'), ['class' => 'btn btn-primary', 'type' => 'submit']))

@section('content')

    @include('widgets.create-form')

@endsection

@push('scripts')

@endpush