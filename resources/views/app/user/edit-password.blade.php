@extends(request()->ajax() ? 'layouts.modal' : 'layouts.app', ['form' => true, 'script' => true] )
@section('title', $title)
@section('class', 'col-md-6 col-md-offset-3')
@section('form', Form::open(array('route'=>['update_user_pass'], 'method'=>'POST', 'class'=>'form-horizontal')))
@section('button', Form::button('Şifrəni dəyiş', ['class' => 'btn btn-primary', 'type' => 'submit']))

@section('content')


<div class="form-group">
    {!! Form::label(null, 'Şifrə', ['class'=>'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::password("current_password", ['class'=>'col-md-6 form-control']) !!}
    </div>
</div>

<div class="form-group">
    {!! Form::label(null, "Yeni şifrə", ['class'=>'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::password("password", ['class'=>'col-md-6 form-control']) !!}
    </div>
</div>

<div class="form-group">
    {!! Form::label(null, "Yeni şifrənin təsdiqi", ['class'=>'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::password("password_confirmation", ['class'=>'col-md-6 form-control']) !!}
    </div>
</div>


@endsection


