@extends ('layouts.app')
@section ('page_heading', 'Profil')
@section ('content')

    <div class="col-md-6 col-md-offset-3">

            @if (Auth::user()->verified == 0)
                @include('widgets.alert', array('class'=>'danger', 'message'=>"Please check your email and confirm your account."))
            @endif

        {!! Form::open(['url'=>route('update_user', auth()->user()->id), 'method'=>'PATCH', 'class'=>'form-horizontal' ]) !!}

            <div class="form-group">
                {!! Form::label(null, 'Ad', ['class'=>'col-md-4 control-label']) !!}
                <div class="col-md-6">
                    {!! Form::text("first_name", Auth::user()->first_name, ['class'=>'col-md-6 form-control']) !!}
                </div>
            </div>

            <div class="form-group">
                {!! Form::label(null, "Soyad", ['class'=>'col-md-4 control-label']) !!}
                <div class="col-md-6">
                    {!! Form::text("last_name", Auth::user()->last_name, ['class'=>'col-md-6 form-control']) !!}
                </div>
            </div>

            <div class="form-group">
                {!! Form::label(null, "Email", ['class'=>'col-md-4 control-label']) !!}
                <div class="col-md-6">
                    {!! Form::email("email", Auth::user()->email, ['class'=>'col-md-6 form-control']) !!}
                </div>
            </div>

            <div class="form-group">
                {!! Form::label(null, "Cədvəl", ['class'=>'col-md-4 control-label']) !!}
                <div class="col-md-6">
                    {!! Form::select("dt_view", array_merge([0 => 'Bütün dillər'], config('app.locales')), Auth::user()->dt_view, ['class'=>'col-md-6 form-control']) !!}
                    <p class="help-block">Xəbərlər, səhifələr və kateqoriyalar üçün nəzərdə tutulub.</p>
                </div>
            </div>

            <div class="form-group">
                {!! Form::button("Şifrəni dəyiş", ['class'=>'btn btn-sm btn-danger btn-block open-modal-dialog','data-action'=>route('edit_pass')]) !!}
            </div>

            <div class="form-group">
                {!! Form::submit("Profili yenilə", ["class" => "btn btn-lg btn-success btn-block"]) !!}
            </div>

        {!! Form::close() !!}
    </div>

@endsection