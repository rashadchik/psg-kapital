@extends ('layouts.app', ['script' => true])
@section ('page_heading', 'Düzəliş')

@section('content')

    <div class="col-lg-10 col-lg-offset-1">
        {!! Form::open(['url'=>route("update_$route", $info->id), 'method'=>'PATCH']) !!}

            @include('widgets.form-large')

            @include('widgets.form-submit', ['text' => trans('locale.save'), 'class' => 'success', 'icon' => 'save'])

        {!! Form::close() !!}
    </div>

@endsection


@push('scripts')

    <script>

        $('#category_list').select2({
            data: {!! $answers !!}
        });

        $('#category_list').val({{$info->parent_id}}).trigger("change");

    </script>

@endpush

