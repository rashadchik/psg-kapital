@extends ('layouts.app', ['panel' => true, 'create' => "create_answer", 'title' => trans('locale.create')])
@section ('page_heading', 'Cavablar')

@section ('content')
    @include('widgets.modal-confirm')

    {!! $dataTable->table(['class' => 'table table-striped table-hover', 'id' => 'datatable', 'width' => '100%']) !!}

@endsection

@push('scripts')

{!! $dataTable->scripts() !!}

@endpush
