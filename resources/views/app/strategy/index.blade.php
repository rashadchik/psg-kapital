@extends ('layouts.app', ['panel' => true, 'create' => "create_strategy", 'title' => trans('locale.create')])
@section ('page_heading', 'Strategiyalar')

@section ('content')
    @include('widgets.modal-confirm')

    <ul class="nav nav-tabs">
        <li role="presentation" {{ activeUrl(route("strategy.index")) }}><a href="{{ route("strategy.index") }}">{{trans('locale.active')}}</a></li>
        <li role="presentation" {{ activeUrl(route("trashed_strategy")) }}><a href="{{ route("trashed_strategy") }}">{{trans('locale.trash')}}</a></li>
    </ul>

    <div class="tab-content">
        <div class="tab-pane active"><br>
            {!! $dataTable->table(['class' => 'table table-striped table-hover', 'id' => 'datatable', 'width' => '100%']) !!}
        </div>
    </div>
@endsection

@push('scripts')

{!! $dataTable->scripts() !!}

@endpush
