@extends ('layouts.edit-page', ['script' => true])
@section ('page_heading', $info->name)

@section('edit')

    @include('widgets.modal-confirm', ['trash' => true])

    <div class="col-lg-10 col-lg-offset-1">
        {!! Form::open(['url'=>route("update_$route", $info->id), 'method'=>'PATCH']) !!}

            @include('widgets.form-large')

            @if($route == 'page')
                {!! Form::hidden('lang_id', $lang) !!}
            @endif

            @include('widgets.form-submit', ['id' => $info->id, 'text' => trans('locale.save'), 'class' => 'success', 'icon' => 'save', 'return' => true, 'del' => true, 'route' => 'page'])

        {!! Form::close() !!}
    </div>

@endsection