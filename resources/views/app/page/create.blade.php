@extends ('layouts.edit-page', ['script' => true])
@section ('page_heading', isset($title) ? $title : 'Yeni səhifə')

@section('edit')

    <div class="col-lg-10 col-lg-offset-1">
        {!! Form::open(['url'=>route("store_$route"), 'method'=>'POST']) !!}
        
            @include('widgets.form-large')

            @if(isset($id))
                {!! Form::hidden('relation_page', $id) !!}
            @endif

            @if($route == 'page' && isset($title))
                {!! Form::hidden('lang_id', $lang) !!}
            @endif

            @include('widgets.form-submit', ['text' => isset($title) ? trans('locale.save') : trans('locale.next'), 'class' => 'success', 'icon' => isset($title) ? 'save' : 'arrow-right', 'return' => true])

        {!! Form::close() !!}
    </div>

@endsection