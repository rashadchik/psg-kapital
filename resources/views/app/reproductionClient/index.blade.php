@extends ('layouts.app', ['panel' => true, 'create' => "create_client", 'title' => trans('locale.create')])
@section ('page_heading', 'Istiqrazlar')

@section ('content')
    @include('widgets.modal-confirm')

    <ul class="nav nav-tabs">
        <li role="presentation" {{ activeUrl(route("client.index")) }}><a href="{{ route("client.index") }}">{{trans('locale.active')}}</a></li>
        <li role="presentation" {{ activeUrl(route("trashed_client")) }}><a href="{{ route("trashed_client") }}">{{trans('locale.trash')}}</a></li>
    </ul>

    <div class="tab-content">
        <div class="tab-pane active"><br>
            {!! $dataTable->table(['class' => 'table table-striped table-hover', 'id' => 'datatable', 'width' => '100%']) !!}
        </div>
    </div>
@endsection

@push('scripts')

{!! $dataTable->scripts() !!}

@endpush
