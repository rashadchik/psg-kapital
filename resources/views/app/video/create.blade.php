@extends ('layouts.app', ['script' => true])
@section ('page_heading', 'Yeni video')

@section('content')

    <div class="col-lg-10 col-lg-offset-1">
        {!! Form::open(['url'=>route('store_video'), 'method'=>'POST' ]) !!}

            @include('widgets.form-large')

            @include('widgets.form-submit', ['text' => trans('locale.create'), 'class' => 'success', 'icon' => 'save'])

        {!! Form::close() !!}

    </div>

@endsection

@push('scripts')

@include('web.elements.elfinder-selector', ['video' => true])

@endpush