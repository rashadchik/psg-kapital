@extends(request()->ajax() ? 'layouts.modal' : 'layouts.app', ['form' => true, 'script' => true] )
@section('title', $title)
@section('class', 'col-md-6 col-md-offset-3')
@section('form', Form::open(array('route'=>['update_config', $id], 'method'=>'PATCH', 'class'=>'form-horizontal', 'id' => 'ajaxForm')))

@section('button', Form::button(icon('save', trans('locale.save')), ['class' => 'btn btn-success', 'type' => 'submit']))

@section('content')

    @include('widgets.edit-form')

@endsection

@push('scripts')

    <script>

        $('#blog_page').select2({
            data: {!! $category !!}
        });

        $('#blog_page').val({{$info->blog_page}}).trigger("change");

    </script>

    @include('web.elements.elfinder-selector')
@endpush