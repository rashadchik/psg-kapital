@extends ('layouts.app')
@section ('page_heading', $title)

@section ('content')

    <div class="row">
        <div class="col-md-8 col-md-offset-1">

            @section('settings')
                <table class="table table-bordered table-striped" style="table-layout: fixed; word-wrap: break-word;">

                    @include('widgets.view-form')

                    @if(auth()->user()->role == 1)
                    <tr>
                        <td colspan="2" align="center">
                            <a href="{{route('edit_config', $id)}}" data-action="{{route('edit_config', $id)}}" class="btn btn-primary btn-lg open-modal-dialog">{!! icon('edit', trans('locale.edit')) !!}</a>
                        </td>
                    </tr>
                    @endif
                    
                </table>
            @endsection

            @include('widgets.settings-panel')
        </div>
    </div>

@endsection