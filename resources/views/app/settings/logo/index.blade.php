@extends ('layouts.app')
@section ('page_heading', $title)

@section ('content')

    @include('widgets.modal-confirm')

    <div class="row">
        <div class="col-md-6 col-md-offset-3">

            @section('settings')

                <div class="col-md-12 text-center" style="margin-bottom:30px">
                    <img src="{{asset("files/cache/images/$logo->logo")}}" alt="{{$logo->company_name}}">
                </div>

                @if(auth()->user()->role == 1)
                {!! Form::open(['url'=>route('update_logo', $logo->id), 'method'=>'PATCH']) !!}

                @include('widgets.form-large')

                @include('widgets.form-submit', ['text' => 'Upload', 'class' => 'success', 'icon' => 'upload'])

                {!! Form::close() !!}
                @endif

            @endsection

            @include('widgets.settings-panel')
        </div>
    </div>

@endsection

@push('scripts')

    @include('web.elements.elfinder-selector')

@endpush