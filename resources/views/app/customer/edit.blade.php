@extends ('layouts.edit-page', ['script' => true])
@section ('page_heading', $info->title)

@section('edit')

    <div class="col-lg-10 col-lg-offset-1">
        {!! Form::open(['url'=>route('update_customer', $info->id), 'method'=>'PATCH', 'files' => true]) !!}

        @include('widgets.form-large')

        @include('widgets.form-submit', ['text' => trans('locale.save'), 'class' => 'success', 'icon' => 'save'])

        {!! Form::close() !!}
    </div>

@endsection