@extends ('layouts.app', ['panel' => true, 'create' => 'create_customer', 'title' => trans('locale.create')])
@section ('page_heading', $title)

@section ('content')
    @include('widgets.modal-confirm')

    <ul class="nav nav-tabs">
        <li role="presentation" {{ activeUrl(route('customer.index')) }}><a href="{{route('customer.index')}}">{{trans('locale.active')}}</a></li>
        <li role="presentation" {{ activeUrl(route('trashed_customer')) }}><a href="{{route('trashed_customer')}}">{{trans('locale.trash')}}</a></li>
    </ul>

    <div class="tab-content">
        <div class="tab-pane active"><br>

            <div class="dataTables_wrapper">

                <div class="pull-right">
                    @include('widgets.customer-search')
                </div>
            </div>

            {!! $dataTable->table(['class' => 'table table-striped table-hover', 'id' => 'customers', 'width' => '100%']) !!}
        </div>
    </div>
@endsection

@push('scripts')

    {!! $dataTable->scripts() !!}

    <script>
        $('#search-form').on('submit', function(e) {
            $('#customers').DataTable().draw(false);
            e.preventDefault();
        });

        $("#customers").on('preXhr.dt', function(e, settings, data) {
            data.categoryId = $("#category_id").val();
        });


    </script>
@endpush
