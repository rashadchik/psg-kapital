@extends ('layouts.app', ['script' => true])
@section ('page_heading', 'Düzəliş')

@section('content')

    <div class="col-lg-10 col-lg-offset-1">
        {!! Form::open(['url'=>route("update_str_chart2", $info->id), 'method'=>'PATCH']) !!}

            @include('widgets.form-large')

            @include('widgets.form-submit', ['text' => trans('locale.save'), 'class' => 'success', 'icon' => 'save'])

        {!! Form::close() !!}
    </div>

@endsection

@push('scripts')
    <script src="{{ asset('vendor/ckeditor/ckeditor.js')}}"></script>
    <script>
        CKEDITOR.replace('ck-editor', { customConfig: '{{ asset('vendor/ckeditor/config-mini.js?v=4') }}' });
    </script>

    @include('web.elements.elfinder-selector', ['files' => ['image'] ])

@endpush

