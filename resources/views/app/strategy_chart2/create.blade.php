@extends ('layouts.app', ['script' => true])
@section ('page_heading', "Əlavə et")

@section('content')


    <div class="col-lg-10 col-lg-offset-1">
        {!! Form::open(['url'=>route("store_str_chart2"), 'method'=>'POST']) !!}
        
        @include('widgets.form-large')

        @include('widgets.form-submit', ['text' => trans('locale.create'), 'class' => 'success', 'icon' => 'save'])

        {!! Form::close() !!}
    </div>

@endsection

@push('scripts')
    <script src="{{ asset('vendor/ckeditor/ckeditor.js')}}"></script>
    <script>
	CKEDITOR.replace('ck-editor', { customConfig: '{{ asset('vendor/ckeditor/config-mini.js?v=4') }}' });
    </script>

    @include('web.elements.elfinder-selector', ['files' => ['image'] ])

@endpush
