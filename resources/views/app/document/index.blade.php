@extends ('layouts.app', ['panel' => true, 'create' => 'create_document', 'title' => trans('locale.create')])
@section ('page_heading', $title)

@section ('content')
    @include('widgets.modal-confirm')

    <ul class="nav nav-tabs">
        <li role="presentation" {{ activeUrl(route('document.index')) }}><a href="{{route('document.index')}}">{{trans('locale.active')}}</a></li>
        <li role="presentation" {{ activeUrl(route('trashed_document')) }}><a href="{{route('trashed_document')}}">{{trans('locale.trash')}}</a></li>
    </ul>

    <div class="tab-content">
        <div class="tab-pane active"><br>
            {!! $dataTable->table(['class' => 'table table-striped table-hover', 'id' => 'document', 'width' => '100%']) !!}
        </div>
    </div>
@endsection

@push('scripts')

    {!! $dataTable->scripts() !!}

@endpush