@extends ('layouts.app', ['script' => true])
@section ('page_heading', 'Yeni sənəd')

@section('content')

    <div class="col-lg-10 col-lg-offset-1">
        {!! Form::open(['url'=>route('store_document'), 'method'=>'POST' ]) !!}

            @include('widgets.form-large')

            @include('widgets.form-submit', ['text' => trans('locale.create'), 'class' => 'success', 'icon' => 'save'])

        {!! Form::close() !!}

    </div>

@endsection

@push('scripts')

    <script>

        $('#category_list').select2({
            data: {!! $category !!}
        });

    </script>

    @include('web.elements.elfinder-selector', ['files' => ['application/pdf', 'application/msword', 'application/vnd.openxmlformats-officedocument.wordprocessingml.document'] ])

@endpush
