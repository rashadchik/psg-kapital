@extends ('layouts.app', ['script' => true])
@section('page_heading', 'Düzəliş')

@section('content')

    <div class="col-lg-10 col-lg-offset-1">
        {!! Form::open(['url'=>route('update_document', $info->id), 'method'=>'PATCH', 'files' => true]) !!}

        @include('widgets.form-large')

        @include('widgets.form-submit', ['text' => trans('locale.save'), 'class' => 'success', 'icon' => 'save'])

        {!! Form::close() !!}
    </div>

@endsection

@push('scripts')

<script>

    $('#category_list').select2({
        data: {!! $category !!}
    });

    $('#category_list').val({{$info->category_id}}).trigger("change");

</script>

@include('web.elements.elfinder-selector', ['files' => ['application/pdf', 'application/msword', 'application/vnd.openxmlformats-officedocument.wordprocessingml.document'] ])

@endpush