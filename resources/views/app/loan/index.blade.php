@extends ('layouts.app', ['panel' => true, 'create' => "create_loan", 'title' => trans('locale.create')])
@section ('page_heading', 'Istiqrazlar')

@section ('content')
    @include('widgets.modal-confirm')

    <ul class="nav nav-tabs">
        <li role="presentation" {{ activeUrl(route("loan.index")) }}><a href="{{ route("loan.index") }}">{{trans('locale.active')}}</a></li>
        <li role="presentation" {{ activeUrl(route("trashed_loan")) }}><a href="{{ route("trashed_loan") }}">{{trans('locale.trash')}}</a></li>
    </ul>

    <div class="tab-content">
        <div class="tab-pane active"><br>
            {!! $dataTable->table(['class' => 'table table-striped table-hover', 'id' => 'datatable', 'width' => '100%']) !!}
        </div>
    </div>
@endsection

@push('scripts')

{!! $dataTable->scripts() !!}

@endpush
