@extends ('layouts.app')
@section ('page_heading', $title)

@section ('content')

    <div class="row">
        <div class="col-md-6 col-md-offset-3">

            @section('settings')

            <table class="table table-bordered table-striped" style="table-layout: fixed; word-wrap: break-word;">

                @include('widgets.view-form')

                @if(auth()->user()->role == 1)
                <tr>
                    <td colspan="2" align="center">
                        <a href="{{route('edit_rekvisit', $id)}}" data-action="{{route('edit_rekvisit', $id)}}" class="btn btn-primary btn-lg open-modal-dialog">{!! icon('edit', trans('locale.edit')) !!}</a>
                    </td>
                </tr>
                @endif

            </table>

            @endsection

            @include('widgets.settings-panel')

        </div>
    </div>

@endsection