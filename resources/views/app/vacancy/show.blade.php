@extends('layouts.modal', ['form' => true])
@section('title', $vacancy->name)
@section('class', 'col-md-6 col-md-offset-3')
@section('form', Form::open(array('route'=>['apply', $id], 'method'=>'POST', 'class'=>'form-inline', 'id' => 'ajax-form', 'files' => true)))

@section('button', Form::button($dictionary['send'][$lang], ['id' => 'loadingButton', 'data-loading-text' => g_icon('refresh', null, 'spinning'), 'autocomplete' => 'off', 'type' => 'submit', 'class' => 'btn btn-success']))


@section('content')

    <div class="modal-thank-message" style="display:none" id="successBox">
        <h2><i class="fa fa-check-circle"></i></h2>
        <p id="successMessage"></p>
    </div>

    <div class="modal-warn-message" style="display:none" id="errorBox">
        <h2><i class="fa fa-warning"></i></h2>
        <p id="msgBox"></p>
    </div>

    <p>
        {{ $vacancy->summary }}
    </p>

    <div id="divForm">

        <div class="form-group">
            {!! Form::text('full_name', null, ["class" => "form-control alphaonly", "placeholder" => $dictionary['full_name'][$lang] ]) !!}
        </div>

        <div class="form-group">
            {!! Form::text('email', null, ["class" => "form-control", "placeholder" => $dictionary['email'][$lang] ]) !!}
        </div>

        <div class="form-group">
            <label class="btn-bs-file btn  btn-primary">
                CV faylı seçin
                {!! Form::file('resume') !!}
            </label>
            <input type="text" class="form-control" readonly="">
        </div>
    </div>

@endsection

@push('scripts')
    <script src="{{asset(mix('js/script.js'))}}"></script>

    <script>

        // We can attach the `fileselect` event to all file inputs on the page
        $(document).on('change', ':file', function() {
            var input = $(this),
                numFiles = input.get(0).files ? input.get(0).files.length : 1,
                label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
            input.trigger('fileselect', [numFiles, label]);
        });


        // We can watch for our custom `fileselect` event like this
        $(document).ready( function() {
            $(':file').on('fileselect', function(event, numFiles, label) {

                var input = $(this).parents('.form-group').find(':text'),
                    log = numFiles > 1 ? numFiles + ' files selected' : label;

                if( input.length ) {
                    input.val(log);
                } else {
                    if( log ) alert(log);
                }

            });
        });

    </script>
@endpush

