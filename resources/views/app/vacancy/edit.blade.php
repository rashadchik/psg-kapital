@extends ('layouts.edit-page', ['script' => true, 'editor' => false])
@section('page_heading', $relPage->name)

@section('edit')

    <div class="col-lg-10 col-lg-offset-1">
        {!! Form::open(['url'=>route('update_vacancy', $info->id), 'method'=>'PATCH', 'files' => true]) !!}

        @include('widgets.form-large')

        @include('widgets.form-submit', ['text' => trans('locale.save'), 'class' => 'success', 'icon' => 'save'])

        {!! Form::close() !!}
    </div>

@endsection