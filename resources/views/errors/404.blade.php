@extends ('planes.404')

@section ('404')

    <!-- Error404 Begin -->

    <section class="error404 text-center">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <h1>Error 404</h1>
                    <img src="{{ asset('images/404.png') }}">
                </div>
            </div>
        </div>
    </section>

    <!-- Error404 End -->

@endsection
