@extends ('layouts.web', ['page_heading' => $dictionary['nothing_found_heading']])

@section ('content')

    <!-- Error404 Begin -->

    <section class="error404 text-center">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <h1>{{  $dictionary['nothing_found_heading'] }}</h1>
                    <img src="{{ asset('images/404.png') }}" alt="{{ $webConfig->company_name }}">
                    <h1>{{  $dictionary['nothing_found_title'] }}</h1>
                    <p>{!! $dictionary['nothing_found_content'] !!}</p>
                </div>
            </div>
        </div>
    </section>

    <!-- Error404 End -->

@endsection
