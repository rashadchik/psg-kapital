{!! Form::open(array('method'=>'POST','id'=>'search-form', 'class'=>'form-inline', 'style' => 'margin-bottom:10px')) !!}

<div class="form-group hidden-xs">

    <div id="search_field" class="dataTables_filter pull-left"></div>

    {!!Form::select("category_id", $cat, null, ["class" => "form-control", "id" => "category_id"])!!}

    {!!Form::button(icon('search'), ["class" => "btn btn-primary", 'type' => 'submit'])!!}
</div>

{!! Form::close() !!}
