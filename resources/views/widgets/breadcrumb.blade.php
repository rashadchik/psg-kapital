@if ($breadcrumbs)

    @foreach ($breadcrumbs as $breadcrumb)

        @if (!$breadcrumb->last)
            <a href="{{ $breadcrumb->url }}" title="{{ $breadcrumb->title }}">{{ $breadcrumb->title }}</a>
            <span class="arrow"><i class="fa fa-angle-right" aria-hidden="true"></i></span>
        @else
            <span class="current">{{ $breadcrumb->title }}</span>
        @endif

    @endforeach

@endif