
@if(!isset($panel) )

	<ul class="nav nav-tabs">
	    <li role="presentation" {{ activeUrl(route('show_config', 1)) }}><a href="{{route('show_config', 1)}}">Konfiqurasiya</a></li>
	    <li role="presentation" {{ activeUrl(route('social.index')) }}><a href="{{route('social.index')}}">Sosial şəbəkə</a></li>
		<li role="presentation" {{ activeUrl(route('show_rekvisit', 1)) }}><a href="{{route('show_rekvisit', 1)}}">İstiqraz üçün rekvizitlər</a></li>

	</ul>
@endif

<div class="tab-content">
    <div class="tab-pane active"><br>
        @yield('settings')
    </div>
</div>