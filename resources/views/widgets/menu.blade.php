@foreach($menu as $cat)
    @if(is_null($cat->parent_id))
        <tr class="success" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse{{$cat->id}}" aria-expanded="false" aria-controls="#collapse{{$cat->id}}">
            <td>{{$cat->name}}</td>
            <td>{{$cat->slug}}</td>
            <td>{{$cat->created_at}}</td>
        </tr>
        @foreach($menu as $subCat)
            @if($cat->id == $subCat->parent_id)
                <td colspan="100%">
                    <div id="collapse{{$cat->id}}" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading{{$cat->id}}">
                        <table class="table table-striped" style="margin-bottom:0px">
                            @foreach($menu as $subcat)
                                @if($cat->id == $subcat->parent_id)
                                    <tr class="info" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse{{$subcat->id}}" aria-expanded="true" aria-controls="#collapse{{$subcat->id}}">
                                        <td>{{$subcat->name}}</td>
                                        <td>{{$subcat->slug}}</td>
                                        <td>
                                            @include('widgets.labels', ["class" => config("config.label1.$subcat->visible.1"), "value" => config("config.label1.$subcat->visible.0")])
                                        </td>
                                    </tr>
                                    @foreach($menu as $subsubCat)
                                        @if($subCat->id == $subsubCat->parent_id)
                                            <tr style="background-color: #fff">
                                                <td colspan="100%">
                                                    <div id="collapse{{$subcat->id}}" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading{{$subcat->id}}">
                                                        <table class="table table-striped" style="margin-bottom:0px">
                                                            @foreach($menu as $subsubcat)
                                                                @if($subcat->id == $subsubcat->parent_id)
                                                                    <tr class="danger">
                                                                        <td>{{$subsubcat->name}}</td>
                                                                        <td>{{$subsubcat->slug}}</td>
                                                                        <td>
                                                                            @include('widgets.labels', ["class" => config("config.label1.$subsubcat->visible.1"), "value" => config("config.label1.$subsubcat->visible.0")])
                                                                        </td>
                                                                    </tr>
                                                                @endif
                                                            @endforeach
                                                        </table>
                                                    </div>
                                                </td>
                                            </tr>
                                        @endif
                                    @endforeach
                                @endif
                            @endforeach
                        </table>
                    </div>
                </td>
            @endif
        @endforeach
    @endif
@endforeach