<div class="form-group col-md-12" style="border-top:1px solid #eee"><br>
    {!! Form::button(icon($icon).$text, ['class' => "btn btn-$class", 'type' => 'submit']) !!}

    @if(isset($return))
        {!! Form::button(icon('save').'Yadda saxla və bağla', ['class' => "btn btn-primary", 'type' => 'submit', 'name' => 'return']) !!}
    @endif

    @if(auth()->user()->role == 1 && isset($del))

        <a href="#" class="btn btn-danger" data-action="{{ route("trash_$route", $id) }}" data-toggle="modal" data-target="#modal-confirm">
            <i class="fa fa-trash"></i> Sil
        </a>

    @endif

    @if(isset($edit))
        {!! Form::hidden('edit', true) !!}
    @endif

    <a href="javascript: history.back()" class="btn btn-warning">Geri qayıt</a>

</div>