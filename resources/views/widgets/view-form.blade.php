@foreach($suitableInputs as $input)

    <tr class="{{isset($allInputs[$input]['div_class']) ? $allInputs[$input]['div_class'] : ''}}">
        <td>
            {!! Form::label(null, $allInputs[$input]['ad']) !!}
        </td>

        <td class="{{$allInputs[$input]['input'] == 'checkbox' ? 'checkbox' : ''}}">
            {!! viewInputs($allInputs[$input], $show) !!}
        </td>
    </tr>

@endforeach