@if($status == 0)
    <a href="#" class="btn btn-xs btn-default" onclick="event.preventDefault(); document.getElementById('approve-{{$id}}').submit();">
        <i class="fa fa-check-circle"></i>
    </a>

    {!! Form::open(['method' => 'PATCH', 'url' => route('approve_subscriber', $id), 'id' => "approve-$id", 'style' => 'display:none']) !!}
    {!! Form::close() !!}
@endif

<a href="#" class="btn btn-xs btn-danger" data-action="{{route('destroy_subscriber', $id)}}" data-toggle="modal" data-target="#modal-confirm"><i class="fa fa-remove"></i></a>