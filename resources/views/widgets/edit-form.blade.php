@foreach($suitableInputs as $input)

    <div class="form-group {{isset($allInputs[$input]['div_class']) ? $allInputs[$input]['div_class'] : ''}}">
        {!! Form::label(null, $allInputs[$input]['ad'], array('class' => 'col-md-4 control-label')) !!}

        <div class="col-md-6 {{$allInputs[$input]['input'] == 'checkbox' ? 'checkbox' : ''}}">
            @if(!isset($allInputs[$input]['design']))
                {!! editInputs($allInputs[$input], $info) !!}
            @else
                {!!call_user_func($allInputs[$input]['design'], editInputs($allInputs[$input], $info))!!}
            @endif
        </div>
    </div>

@endforeach