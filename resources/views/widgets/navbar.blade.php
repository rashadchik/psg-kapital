<!-- Navigation -->
<nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
  <div class="navbar-header">
    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
      <span class="sr-only">Toggle navigation</span>
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
    </button>
    <a class="navbar-brand" href="{{ route ('dashboard') }}" style="padding-top:5px !important;">
      <img src="{{asset('images/logotype.png')}}" height="40px" alt="{{ webConfig()->company_name }}">
    </a>
  </div>
  <!-- /.navbar-header -->

  <ul class="nav navbar-top-links navbar-right">
    <li><a href="{{ url ('/') }}" target="_blank">Sayt</a></li>
    <li class="dropdown">
      <a class="dropdown-toggle" data-toggle="dropdown" href="#">
        <i class="fa fa-user fa-fw"></i>  {{auth()->user()->first_name}} <i class="fa fa-caret-down"></i>
      </a>
      <ul class="dropdown-menu dropdown-user">
        <li><a href="{{ route ('show_user') }}"><i class="fa fa-user fa-fw"></i> Profil</a>
        </li>
        <li class="divider"></li>
        <li>
          <a href="#" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
            <i class="fa fa-sign-out fa-fw"></i> Çıx
          </a>

          <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
            {{ csrf_field() }}
          </form>
        </li>
      </ul>
      <!-- /.dropdown-user -->
    </li>
    <!-- /.dropdown -->
  </ul>
  <!-- /.navbar-top-links -->

  <div class="navbar-default sidebar" role="navigation">
    <div class="sidebar-nav navbar-collapse">
      <ul class="nav" id="side-menu">

        <li {{ activeUrl(route('slider.index')) }}>
          <a href="{{ route ('slider.index') }}"><i class="fa fa-image fa-fw"></i> Slider</a>
        </li>

        <li {{ activeUrl(route('article.index')) }}>
          <a href="{{ route ('article.index') }}"><i class="fa fa-newspaper-o fa-fw"></i> Xəbərlər</a>
        </li>

        <li>
          <a href="#"><i class="fa fa-file fa-fw"></i> Səhifələr<span class="fa arrow"></span></a>
          <ul class="nav nav-second-level">
            <li {{ activeUrl(route('page.index')) }}><a href="{{ route ('page.index') }}"><i class="fa fa-list fa-fw"></i> Menyular</a></li>
            <li {{ activeUrl(route('category.index')) }}><a href="{{ route ('category.index') }}"><i class="fa fa-list-ul fa-fw"></i> Kateqoriyalar</a></li>
          </ul>
        </li>

        <li {{ activeUrl(route('serviceStat.index')) }}>
          <a href="{{ route ('serviceStat.index') }}"><i class="fa fa-balance-scale fa-fw"></i> Əqd</a>
        </li>

        <li {{ activeUrl(route('brokerTable.index')) }}>
          <a href="{{ route ('brokerTable.index') }}"><i class="fa fa-table fa-fw"></i> Broker cədvəli</a>
        </li>

        <li {{ activeUrl(route('service.index')) }}>
          <a href="{{ route ('service.index') }}"><i class="fa fa-briefcase fa-fw"></i> Xidmət və üstünlüklər</a>
        </li>

        <li {{ activeUrl(route('legalPhysical.index')) }}>
          <a href="{{ route ('legalPhysical.index') }}"><i class="fa fa-users fa-fw"></i> Hüquqi və fiziki şəxslər</a>
        </li>

        <li {{ activeUrl(route('document.index')) }}>
            <a href="{{ route ('document.index') }}"><i class="fa fa-file fa-fw"></i> Sənədlər</a>
        </li>

        <li {{ activeUrl(route('currency.index')) }}>
            <a href="{{ route ('currency.index') }}"><i class="fa fa-usd fa-fw"></i> Valyutalar</a>
        </li>

        <li>
          <a href="#"><i class="fa fa-file fa-fw"></i> İnvestisiya strategiyaları<span class="fa arrow"></span></a>
          <ul class="nav nav-second-level">
            <li {{ activeUrl(route('strategy.index')) }}><a href="{{ route ('strategy.index') }}"><i class="fa fa-table fa-fw"></i> Strategiyalar</a></li>
            <li {{ activeUrl(route('chart.index')) }}><a href="{{ route ('chart.index') }}"><i class="fa fa-line-chart fa-fw"></i> Diaqram</a></li>
            <li {{ activeUrl(route('chart2.index')) }}><a href="{{ route ('chart2.index') }}"><i class="fa fa-bar-chart fa-fw"></i> Şəkilli diaqram</a></li>
          </ul>
        </li>

        <li {{ activeUrl(route('banner.index')) }}>
          <a href="{{ route ('banner.index') }}"><i class="fa fa-picture-o fa-fw"></i> Bannerlər</a>
        </li>

        <li>
          <a href="#"><i class="fa fa-list-ul fa-fw"></i> Test<span class="fa arrow"></span></a>
          <ul class="nav nav-second-level">
            <li {{ activeUrl(route('question.index')) }}><a href="{{ route ('question.index') }}"><i class="fa fa-question fa-fw"></i> Suallar</a></li>
            <li {{ activeUrl(route('answer.index')) }}><a href="{{ route ('answer.index') }}"><i class="fa fa-check fa-fw"></i> Cavablar</a></li>
          </ul>
        </li>


        <li {{ activeUrl(route('reproduction.index')) }}>
          <a href="#"><i class="fa fa-money fa-fw"></i> Torəmə Alətləri<span class="fa arrow"></span></a>
          <ul class="nav nav-second-level">
             <li>
                <a href="{{ route ('client.index') }}"><i class="fa fa-money fa-fw"></i> Müştəri</a>
             </li>
              <li>
                <a href="{{ route ('reproduction.index') }}"><i class="fa fa-money fa-fw"></i> Baza Aktivliyi</a>
             </li>
          </ul>

        </li>

        <li {{ activeUrl(route('stock.index')) }}>
          <a href="{{ route ('stock.index') }}"><i class="fa fa-money fa-fw"></i> Səhmlər</a>
        </li>


        <li {{ activeUrl(route('loan.index')) }}>
          <a href="{{ route ('loan.index') }}"><i class="fa fa-money fa-fw"></i> İstiqrazlar</a>
        </li>

        <li>
          <a href="#"><i class="fa fa-list fa-fw"></i> Bazar<span class="fa arrow"></span></a>
          <ul class="nav nav-second-level">
            <li {{ activeUrl(route('marketLoan.index')) }}><a href="{{ route ('marketLoan.index') }}"><i class="fa fa-money fa-fw"></i> Bazar Istiqrazları</a></li>
            <li {{ activeUrl(route('charts.index')) }}><a href="{{ route ('charts.index') }}"><i class="fa fa-line-chart fa-fw"></i> Diaqramlar</a></li>
          </ul>
        </li>


        <li {{ activeUrl(route('dictionary.index')) }}>
          <a href="{{ route ('dictionary.index') }}"><i class="fa fa-text-height fa-fw"></i> Lüğət</a>
        </li>

        <li {{ activeUrl(route('partners.index')) }}>
          <a href="{{ route ('partners.index') }}"><i class="fa fa-bandcamp fa-fw"></i> Tərəfdaşlar</a>
        </li>

        @if(auth()->user()->role == 1)
        <li {{ activeUrl(route('user.index')) }}>
          <a href="{{ route ('user.index') }}"><i class="fa fa-user fa-fw"></i> İstifadəçilər</a>
        </li>

        <li  {{ activeUrl(url('manager/filemanager')) }}>
          <a href="{{ url ('manager/filemanager') }}"><i class="fa fa-folder fa-fw"></i> File Manager</a>
        </li>

        <li>
          <a href="#"><i class="fa fa-list fa-fw"></i> Tənzimləmələr<span class="fa arrow"></span></a>
          <ul class="nav nav-second-level">
              <li  {{ activeUrl(route('show_config', 1)) }}>
                <a href="{{ route ('show_config', 1) }}"><i class="fa fa-cogs fa-fw"></i> Konfiqurasiya</a>
              </li>
              <li  {{ activeUrl(route('sitemap.index')) }}>
                <a href="{{ route ('sitemap.index') }}"><i class="fa fa-map fa-fw"></i> Sitemap</a>
              </li>
          </ul>
        </li>

        @endif

      </ul>
    </div>
    <!-- /.sidebar-collapse -->
  </div>
  <!-- /.navbar-static-side -->
</nav>
