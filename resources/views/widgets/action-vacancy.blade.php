@if(is_null($deleted_at))
    <a href="{{route('edit_vacancy', $id)}}" class="btn btn-xs btn-primary">{!! icon('edit') !!}</a>

    @if(auth()->user()->role == 1)
    <a href="#" class="btn btn-xs btn-danger" onclick="event.preventDefault(); document.getElementById('soft-delete-{{$id}}').submit();">
        <i class="fa fa-trash"></i>
    </a>

    {!! Form::open(['method' => 'PATCH', 'url' => route('trash_vacancy', $id), 'id' => "soft-delete-$id", 'style' => 'display:none']) !!}
    {!! Form::close() !!}
    @endif

    @include('widgets.action-duplicate', ['route' => 'vacancy'])

@else
    <a href="#" class="btn btn-xs btn-default" onclick="event.preventDefault(); document.getElementById('restore-delete-{{$id}}').submit();">
        {!! icon('repeat') !!}
    </a>

    {!! Form::open(['method' => 'PATCH', 'url' => route('restore_vacancy', $id), 'id' => "restore-delete-$id", 'style' => 'display:none']) !!}
    {!! Form::close() !!}

    @if(auth()->user()->role == 1)
    <a href="#" class="btn btn-xs btn-danger" data-action="{{route('destroy_vacancy', $id)}}" data-toggle="modal" data-target="#modal-confirm">
        {!! icon('remove') !!}
    </a>
    @endif
@endif