@foreach($suitableInputs as $input)

    <div class="form-group col-md-12 @if(isset($allInputs[$input]['hide'])) {{ $allInputs[$input]['hide'] }} @elseif(isset($allInputs[$input]['show']))  {{ $allInputs[$input]['show'] }} @endif">
        @if($allInputs[$input]['input'] == 'languageTab')

            @include('widgets.lang-tab', ['input' => 'text', 'name' => $allInputs[$input]['db'], 'title' => $allInputs[$input]['ad']])

        @else
            @if($allInputs[$input]['input'] == 'checkbox')
                <div class="checkbox">
                    <label>
                        {!! Form::checkbox($allInputs[$input]['db'], $allInputs[$input]['value'], isset($info) && $info[$allInputs[$input]['db']] == $allInputs[$input]['value'] ? $info[$allInputs[$input]['db']] : $allInputs[$input]['checked']) !!}
                        <b>{{$allInputs[$input]['ad']}}</b>
                    </label>
                </div>
            @else
                {!! Form::label(null, $allInputs[$input]['ad']) !!}

                @if(!isset($allInputs[$input]['design']))
                    @if(isset($info))
                        {!! editInputs($allInputs[$input], $info) !!}
                    @else
                        {!! createInputs($allInputs[$input]) !!}
                    @endif
                @else
                    @if(isset($info))
                        {!!call_user_func($allInputs[$input]['design'], editInputs($allInputs[$input], $info))!!}
                    @else
                        {!!call_user_func($allInputs[$input]['design'], createInputs($allInputs[$input]))!!}
                    @endif
                @endif

            @endif
        @endif


        @if(isset($allInputs[$input]['attr']['title']))
            <p class="help-block">{!! $allInputs[$input]['attr']['title'] !!}</p>
        @endif

    </div>

@endforeach