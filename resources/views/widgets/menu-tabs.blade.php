<div class="col-md-12 col-md-offset-0">

    <ul class="nav nav-tabs">
        <li role="presentation" {{ activeUrl(route('dashboard')) }}><a href="{{route('dashboard')}}">Active</a></li>
        <li role="presentation" {{ activeUrl(route('deletedMenu')) }}><a href="{{route('deletedMenu')}}">Deleted</a></li>

    </ul>

    <div class="tab-content">
        <div class="tab-pane active">
            <br>
            @yield('tab-content')
        </div>
    </div>
</div>