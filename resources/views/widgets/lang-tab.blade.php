<div class="clearfix"></div>

<label for="{{ $name }}">{{ $title }}</label>

<ul class="nav nav-tabs">

    @foreach(config("app.locales") as $key => $locale)
        <li role="presentation" @if($loop->first) class="active" @endif>
            <a href="#{{ $name.'_'.$key }}" data-toggle="tab">{{$locale}}</a>
        </li>
    @endforeach

</ul>

<div class="tab-content">

    @foreach(config("app.locales") as $key => $locale)
        <div id="{{ $name.'_'.$key }}" class="tab-pane fade @if($loop->first) in active @endif">

            <div class="form-group">
                <div class="col-md-12">

                    @if(!isset($multiRow))
                        {!! Form::$input($name.'_'.$key, isset($info) ? $info[$name.'_'.$key] : null, ['class' => 'form-control']) !!}
                    @else
                        @if(isset($editor))
                           {!! Form::$input("content[$key]", isset($info[$key]) ? $info[$key] : null, ['class' => 'form-control ck-editor' , 'id' => $editor == 1 ? 'ck-editor-mini-'.$key : 'no-edit']) !!}
                        @else
                            {!! Form::$input("content[$key]", isset($info[$key]) ? $info[$key] : null, ['class' => 'form-control ck-editor']) !!}
                        @endif
                    @endif

                </div>
            </div>

        </div>
    @endforeach

</div>