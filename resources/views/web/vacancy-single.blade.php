@extends ('layouts.web', ['page_heading' => $page->name])

@section ('content')

    @include('web.elements.breadcrumb')

    <!-- VacanciesDetail Begin -->
    <section class="vacancies_detail">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    @include('web.elements.page-cover2')
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12">
                    <h1 class="pull-left">{{ $page->name }}</h1>
                </div>
                <div class="col-xs-12">
                    <span class="price">@if($page->vacancyDetail->salary ==0) {{ $dictionary['salary_agreement'] or 'Razılaşma yolu ilə' }} @endif</span>
                </div>
            </div>
            <div class="row">
                <div class="col-md-5 col-sm-6 col-xs-12">
                    <nav class="list">
                        <h4>{{ $dictionary["vacancy_single_information"] or "İş barədə məlumat" }}</h4>
                        {!! $page->summary !!}
                    </nav>
                </div>
                <div class="col-md-1 hidden-sm hidden-xs"></div>
                <div class="col-md-5 col-sm-6 col-xs-12">
                    <nav class="list">
                        <h4>{{ $dictionary["vacancy_single_requirement"] or "Namizədə tələblər" }}</h4>
                        {!! $page->content !!}
                    </nav>
                </div>
            </div>
            <div class="row margin-top-30">
                <div class="col-md-2 col-sm-3">
                    <div class="cv_send">
                        <h4>{{ $dictionary["vacancy_single_cv_title"] or "Öz CV-ni göndər" }}</h4>
                        <p>{{ $dictionary["vacancy_single_cv_desc"] or "Sizin təcrübə daha yaxşı performans üçün ən yaxşı yol CV yükləməkdir" }}</p>
                    </div>
                </div>

                <div class="col-md-4 col-sm-6 col-md-offset-1 cv_form" id="divForm">

                    {!! Form::open(['url'=>route('apply', $page->id), 'method'=>'POST', 'id' => 'vacancy_form']) !!}

                        <div class="col-md-6">
                            <div class="fileupload row">
                                {{ $dictionary['download'] or 'Yüklə' }}

                                {!! Form::file('resume', ['class' => 'uploadimage']) !!}
                            </div>
                        </div>

                        <div class="col-md-6">
                            {!! Form::button($dictionary['send'], ['id' => 'loadingButton', 'data-loading-text' => g_icon('refresh', null, 'spinning'), 'autocomplete' => 'off', 'type' => 'submit']) !!}
                        </div>

                    {!! Form::close() !!}

                    <p class="fileupload_text">{{ $dictionary["vacancy_single_cv_format"] or "CV-nizi PDF və ya Word formatında yükləyin" }}</p>

                    <p id="msgBox" class="fileupload_text" style="color:#c23934; display:none"></p>
                </div>

                <h4 id="successMessage" style="display: none" class="text-center"></h4>

            </div>
        </div>
    </section>
    <!-- Vacancies End -->

@endsection


@push('scripts')

    <script>
        $("#vacancy_form").on("submit",function (event){
            event.preventDefault();
            ajaxForm($(this));
        });

    </script>

@endpush