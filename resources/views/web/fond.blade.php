@extends ('layouts.web', ['page_heading' => $page->name, 'page_image' => !is_null($page->image_original) ? asset("files/cache/images/$page->id/$page->image_original") : ""])

@section ('content')

    @include('web.elements.breadcrumb')

    @include('web.elements.content', ['form' => true])

    @include('web.elements.page-service', ['text' => isset($dictionary['fond_service']) ? $dictionary['fond_service'] : 'Xidmət növləri'])

    <section class="bgwhite padding-bottom-50">
        <!-- PINumune Begin -->
        <div class="pi_numune">
            <div class="container">
                <div class="row">

                    @if($page->children->count())

                        @foreach($page->children as $child)

                            @if($loop->first)
                                <div class="col-md-3 col-xs-12">
                                    <h2>{{ $child->name }}</h2>
                                    {!! $child->summary !!}
                                </div>
                            @else
                                <div class="col-md-3 col-sm-4 col-xs-12">
                                    <div class="block">
                                        <p><strong>{{ config("config.variants.$loop->iteration") }})</strong> {{ $child->name }}</p>
                                        <p>{!! $child->summary !!}</p>
                                    </div>
                                </div>
                            @endif

                        @endforeach

                    @endif

                </div>
            </div>
        </div>
        <!-- PINumune End -->
        <div class="container">
            <div class="row">
                <div class="col-md-4 pi_qeyd">
                    @if(isset($dictionary["fond_desc_1"]))
                    <p class="size-16">{{ $dictionary["fond_desc_1"] }}</p>
                    @endif

                    @if(isset($dictionary["fond_desc_2"]))
                    <p class="size-14">{!! $dictionary['fond_desc_2'] !!}</p>
                    @endif
                </div>
                <!-- Banner Begin -->
                <div class="col-md-8 call_banner">
                    @include('web.elements.banner', ['type' => 2])
                </div>
                <!-- Banner End -->
            </div>
        </div>

    </section>


@endsection
