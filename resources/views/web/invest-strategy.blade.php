@extends ('layouts.web', ['page_heading' => $page->name])

@section ('content')

    @include('web.elements.breadcrumb')

    @include('web.elements.content', ['class1' => 'col-md-9', 'class2' => 'col-md-3'])

    <section class="bgwhite">
        <div class="container">
            <div class="row">

                <div class="col-xs-12">
                    <!-- ChartBlock -->
                    <div class="chart_block_full">
                        <p>{!! $dictionary['diagram1_text'] or "Bu qrafikdə  ənənəvi strategiyanın əsas komponentləri olan istiqraz və səhmlərin  20 il ərzindəki gəlirliliyin görə bilərsiniz. Mənfəət bəzi illərdə çox yüksək və bəzilərində isə aşağı ola bilər. Aktivlərin hər ikisini portfelə daxil etməklə ümumi portfel dəyişkənliyini artırmaq mümkündür." !!} </p>

                        <div class="chart_head clearfix">
                            <ul class="list-unstyled">
                                <li class="clr1">{{ $dictionary['loan'] or 'İstiqraz' }}</li>
                                <li class="clr2">{{ $dictionary['int_stocks'] or 'Beynəlxalq səhmlər' }}</li>
                            </ul>
                        </div>

                        <div id="chartdiv" class="imgchart"></div>

                    </div>
                </div>

            </div>
        </div>
    </section>

    @if($barChart)

    <section class="chart_other">
        <div class="container">
            <div class="row">
                <div class="col-md-5 col-md-offset-1 col-sm-7">
                    <div class="block">
                        <h2>{{ $barChart->title }}</h2>
                        <img src="{{ asset($barChart->image_original) }}" alt="{{ $barChart->title }}">
                    </div>
                </div>
                <div class="col-md-4 col-md-offset-1 col-sm-5">
                    <div class="blockquote">
                        <p>{!! $barChart->content !!}</p>
                    </div>
                </div>
            </div>
        </div>
    </section>

    @endif

    <section class="bgwhite">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">

                    @foreach($page->children as $child)
                        <div class="pi_text_block @if(!$loop->first) padding-top-0 @endif">

                            <h2 class="title">{{ $child->name }}</h2>
                            <div class="text">
                                {!! $child->summary !!}
                            </div>
                        </div>
                    @endforeach

                </div>
                <div class="col-md-7">
                    <div class="chart_block_small">
                        <div id="chartdiv1" class="imgchart"></div>
                        <div class="chart_head clearfix">
                            <ul class="list-unstyled">
                                <li class="clr1">{{ $dictionary['potfolio'] or 'Portfolio' }}</li>
                                <li class="clr2">{{ $dictionary['sp500'] or 'S&P 500' }}</li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-md-5">
                    <div class="data_table margin-top-0">
                        <table class="text-center">
                            <thead>
                            <tr>
                                <th>{!! $dictionary['asset'] or "Asset" !!}</th>
                                <th>{!! $dictionary['capital'] or "Capital Allocation" !!}</th>
                                <th>{!! $dictionary['total'] or "Total Return" !!}</th>
                                <th>{!! $dictionary['variance'] or "Variance" !!}</th>
                            </tr>
                            </thead>
                            <tbody>

                            @foreach($strategy as $str)
                                <tr>
                                    <td data-title="{!! $dictionary['asset'] or "Asset" !!}">{{ $str->name }}</td>
                                    <td data-title="{!! $dictionary['capital'] or "Capital Allocation" !!}">{{ $str->capital + 0}}%</td>
                                    <td data-title="{!! $dictionary['total'] or "Total Return" !!}">{{ $str->total + 0}}%</td>
                                    <td data-title="{!! $dictionary['variance'] or "Variance" !!}">{{ $str->variance + 0}}</td>
                                </tr>
                            @endforeach

                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="chat_text col-xs-12">
                    <p>{!! $dictionary['diagram3_text'] or "Bu qrafikdə  ənənəvi strategiyanın əsas komponentləri olan istiqraz və səhmlərin  20 il ərzindəki gəlirliliyin görə bilərsiniz. Mənfəət bəzi illərdə çox yüksək və bəzilərində isə aşağı ola bilər. Aktivlərin hər ikisini portfelə daxil etməklə ümumi portfel dəyişkənliyini artırmaq mümkündür." !!} </p>
                    <span>{{ $dictionary['strategy_note'] or 'Gəlirliliyi hesablayarkən vergilər və komissiyalar nəzərə alınmamışdır. Risk portfelin müvafiq müddət ərzindəki dəyişikənliyini ölçür.' }}</span>
                </div>
            </div>
        </div>
    </section>

@endsection


@push('scripts')
    <script>

        var chart = AmCharts.makeChart("chartdiv", {
            "type": "serial",
            "theme": "light",
            "marginTop":0,
            "marginRight": 80,
            "dataProvider": {!! $chart1 !!},
            "valueAxes": [{
                "axisAlpha": 0,
                "position": "left"
            }],

            "graphs": [{
                "id": "g1",
                "balloonText": "[[category]]<br><b><span style='font-size:14px;'>[[value]]%</span></b>",
                "bullet": "round",
                "bulletSize": 9,
                "lineColor": "#0090d0",
                "lineThickness": 3,
                "negativeLineColor": "#0090d0",
                "type": "smoothedLine",
                "valueField": "value1"
            }, {
                "id": "g2",
                "balloonText": "[[category]]<br><b><span style='font-size:14px;'>[[value]]%</span></b>",
                "bullet": "round",
                "bulletSize": 9,
                "lineColor": "#dd4cdd",
                "lineThickness": 3,
                "negativeLineColor": "#dd4cdd",
                "type": "smoothedLine",
                "valueField": "value2"
            }],
            "chartScrollbar": {
                "graph":"g1",
                "gridAlpha":0,
                "color":"#888888",
                "scrollbarHeight":55,
                "backgroundAlpha":0,
                "selectedBackgroundAlpha":0.1,
                "selectedBackgroundColor":"#888888",
                "graphFillAlpha":0,
                "autoGridCount":true,
                "selectedGraphFillAlpha":0,
                "graphLineAlpha":0.2,
                "graphLineColor":"#c2c2c2",
                "selectedGraphLineColor":"#888888",
                "selectedGraphLineAlpha":1

            },
            "chartCursor": {
                "categoryBalloonDateFormat": "YYYY",
                "cursorAlpha": 0,
                "valueLineEnabled":true,
                "valueLineBalloonEnabled":true,
                "valueLineAlpha":0.5,
                "fullWidth":true
            },
            "dataDateFormat": "YYYY",
            "categoryField": "year",
            "categoryAxis": {
                "minPeriod": "YYYY",
                "parseDates": true,
                "minorGridAlpha": 0.1,
                "minorGridEnabled": true
            },
            "export": {
                "enabled": false
            }
        });


        var chart = AmCharts.makeChart("chartdiv1", {
            "type": "serial",
            "theme": "light",
            "marginTop":0,
            "marginRight": 80,
            "dataProvider": {!! $chart2 !!},
            "valueAxes": [{
                "axisAlpha": 0,
                "position": "left"
            }],

            "graphs": [{
                "id": "g1",
                "balloonText": "[[category]]<br><b><span style='font-size:14px;'>[[value]]</span></b>",
                "bullet": "round",
                "bulletSize": 9,
                "lineColor": "#0090d0",
                "lineThickness": 3,
                "negativeLineColor": "#0090d0",
                "type": "smoothedLine",
                "valueField": "value1"
            }, {
                "id": "g2",
                "balloonText": "[[category]]<br><b><span style='font-size:14px;'>[[value]]</span></b>",
                "bullet": "round",
                "bulletSize": 9,
                "lineColor": "#dd4cdd",
                "lineThickness": 3,
                "negativeLineColor": "#dd4cdd",
                "type": "smoothedLine",
                "valueField": "value2"
            }],
            "chartScrollbar": {
                "graph":"g1",
                "gridAlpha":0,
                "color":"#888888",
                "scrollbarHeight":55,
                "backgroundAlpha":0,
                "selectedBackgroundAlpha":0.1,
                "selectedBackgroundColor":"#888888",
                "graphFillAlpha":0,
                "autoGridCount":true,
                "selectedGraphFillAlpha":0,
                "graphLineAlpha":0.2,
                "graphLineColor":"#c2c2c2",
                "selectedGraphLineColor":"#888888",
                "selectedGraphLineAlpha":1

            },
            "chartCursor": {
                "categoryBalloonDateFormat": "YYYY",
                "cursorAlpha": 0,
                "valueLineEnabled":true,
                "valueLineBalloonEnabled":true,
                "valueLineAlpha":0.5,
                "fullWidth":true
            },
            "dataDateFormat": "YYYY",
            "categoryField": "year",
            "categoryAxis": {
                "minPeriod": "YYYY",
                "parseDates": true,
                "minorGridAlpha": 0.1,
                "minorGridEnabled": true
            },
            "export": {
                "enabled": false
            }
        });
    </script>

@endpush
