@extends ('layouts.web', ['page_heading' => $page->name, 'page_image' => !is_null($page->image_original) ? asset("files/cache/images/$page->id/$page->image_original") : ""])

@section ('content')

    @include('web.elements.breadcrumb')

    @include('web.elements.content', ['class1' => 'col-md-9 col-xs-12', 'class2' => 'col-md-3 col-xs-12', 'banner' => true])

    @include('web.elements.page-service', ['text' => $page->description])

@endsection
