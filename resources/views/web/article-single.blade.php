@extends ('layouts.web', ['page_heading' => $article->title, 'page_description' => $article->title, 'page_image' => $article->image ? asset('files/cache/images/'.$article->relation_page.'/thumb/'.$article->image->filename) : ''  ])

@section ('content')

    @include('web.elements.breadcrumb', ['articleSingle' => $article])
    <!-- NewsDetail Begin -->
    <section class="news_detail">
        <div class="container">
            <div class="row">
                <div class="col-md-9">
                    <!-- NewsContent -->
                    <div class="news_detail__content">

                        @if($article->image)
                            <figure>
                                <img src="{!! asset('files/cache/images/'.$article->relation_page.'/'.$article->image->filename) !!}" alt="{{ $article->title }}" class="center-block">
                            </figure>
                        @endif

                        <h1 class="title">{{ $article->title }}</h1>
                        <div class="details clearfix">
                            <div class="pull-left">
                                <span><i class="fa fa-clock-o" aria-hidden="true"></i>{{ mediaFullDate($article->published_at) }}</span>
                            </div>
                        </div>
                        <div class="text">
                            {!! $article->content !!}
                        </div>
                    </div>

                    @if($article->galleries->count())
                    <!-- NewsGallery -->
                    <div class="news_gallery">
                        <h2>{{ $dictionary['gallery'] or 'Qalereya'}}</h2>
                        <div class="row">
                            @include('web.elements.gallery', ['gallery' => $article, 'gallery_title' => $article->title])
                        </div>
                    </div>
                    @endif

                    <!-- Share -->
                    <div class="share clearfix">
                        <a href="https://www.facebook.com/sharer/sharer.php?u={{ request()->url() }}&amp;src=sdkpreparse" title="{{ $article->title }}" class="facebook"><i class="fa fa-facebook" aria-hidden="true"></i></a>
                        <a href="https://twitter.com/home?status={{ request()->url() }}" title="{{ $article->title }}" class="twitter"><i class="fa fa-twitter" aria-hidden="true"></i></a>
                        <a href="https://www.linkedin.com/cws/share?url={{ request()->url() }}" title="{{ $article->title }}" class="linkedin"><i class="fa fa-linkedin" aria-hidden="true"></i></a>
                    </div>
                </div>
                <div class="col-md-3">

                    <!-- SimilarNews -->
                    <div class="similar_news">
                        <h4>{{ $dictionary['similar_news'] }}</h4>
                        <ul class="list-unstyled">

                            @foreach($page->otherArticles($article->id)->get() as $other)
                                <li>
                                    <span>{{ blogDate($other->published_at) }}</span>
                                    <h2><a href="{{ route('showArticle', [$other->slug, $other->article_slug]) }}" title="{{ $other->title }}">{{ $other->title }}</a></h2>
                                </li>
                            @endforeach
                        </ul>
                    </div>

                </div>
            </div>
        </div>
    </section>
    <!-- NewsDetail End -->

@endsection