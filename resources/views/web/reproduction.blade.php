@extends ('layouts.web', ['page_heading' => $page->name, 'page_image' => !is_null($page->image_original) ? asset("files/cache/images/$page->id/$page->image_original") : ""])

@section ('content')

    @include('web.elements.breadcrumb')

    @include('web.elements.content', ['class1' => 'col-md-12', 'class2' => 'col-md-3 col-xs-12', 'class4' => 'col-md-3 col-sm-4 col-xs-5', 'class5' => 'col-md-9 col-sm-8 col-xs-7', 'withoutRightSide' => true])

    <section class="bgwhite padding-30">
        <div class="container">
            <div class="row">

                <div class="col-md-8 col-xs-12">

                    @if($page->children->count())
                        <div class="ta_block">
                            <div class="row">

                                @foreach($page->children as $child)
                                    <div class="col-sm-6">
                                        <article>
                                            <h2>{{ $child->name }}</h2>

                                            {!! $child->summary !!}
                                        </article>
                                    </div>
                                @endforeach

                            </div>
                        </div>
                    @endif

                    @if($table1->count() > 0)
                        <div class="data_table pull-left margin-bottom-60">
                            <table class="text-center">
                                <thead>
                                <tr>
                                    <th>&nbsp;</th>
                                    <th>{{ $dictionary['reproduction_buyer'] or "Alıcı" }}</th>
                                    <th>{{ $dictionary['reproduction_seller'] or "Satıcı" }}</th>
                                </tr>
                                </thead>
                                <tbody>

                                @foreach($table1 as $t1)
                                    <tr>
                                        <td><strong>{{ $t1->name }}</strong></td>
                                        <td data-title="{{ $dictionary['reproduction_buyer'] or "Alıcı" }}">{{ $t1->buyer }}</td>
                                        <td data-title="{{ $dictionary['reproduction_seller'] or "Satıcı" }}">{{ $t1->seller }}</td>
                                    </tr>
                                @endforeach

                                </tbody>
                            </table>
                        </div>
                    @endif



                    @if($table2->count() > 0)
                        <div class="data_table pull-left">
                            <table class="text-center">
                                <thead>
                                <tr>
                                    <th>{{ $dictionary['reproduction_base'] or "Baza Aktivi" }}</th>
                                    <th>{{ $dictionary['reproduction_birja'] or "Birja" }}</th>
                                    <th>{{ $dictionary['reproduction_type'] or "Növü" }}</th>
                                    <th>{{ $dictionary['reproduction_tiker'] or "Tiker" }}</th>
                                </tr>
                                </thead>
                                <tbody>

                                @foreach($table2 as $t2)
                                    <tr>
                                        <td data-title="{{ $dictionary['reproduction_base'] or "Baza Aktivi" }}">{{ $t2->name }}</td>
                                        <td data-title="{{ $dictionary['reproduction_birja'] or "Birja" }}">{{ $t2->birja }}</td>
                                        <td data-title="{{ $dictionary['reproduction_type'] or "Növü" }}">{{ $t2->type }}</td>
                                        <td data-title="{{ $dictionary['reproduction_tiker'] or "Tiker" }}">{{ $t2->ticker }}</td>
                                    </tr>
                                @endforeach

                                </tbody>
                            </table>
                        </div>
                    @endif

                </div>

                <div class="col-md-4 col-xs-12">
                    <!-- Application Begin -->
                    @include('web.elements.application_form', ['bg' => 'bggrey'])
                    <!-- Application End -->

                    @if(isset($dictionary['reproduction_note']) && $dictionary['reproduction_note'] != '')
                    <div class="ta_qeyd">
                        <p>{!! $dictionary['reproduction_note'] !!}</p>
                    </div>
                    @endif

                    @include('web.elements.banner', ['type' => 1, 'class' => 'margin-top-30'])

                </div>
            </div>

        </div>
    </section>

@endsection
