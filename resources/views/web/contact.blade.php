@extends ('layouts.web', ['page_heading' => $page->name])

@section ('content')

    @include('web.elements.breadcrumb')

    <section class="contact_map">
        <div class="container-fluid">
            <div class="row">
                <div id="map"></div>
            </div>
        </div>
    </section>

    <!-- Contact Begin -->

    <section class="contact_three_block">
        <div class="container">
            <div class="row">
                <div class="col-md-4">
                    <div class="block">
                        <span><i class="fa fa-phone" aria-hidden="true"></i></span>
                        <div class="text">
                            <h4>{!! $dictionary['call_us'] or "Bizə zəng et" !!}</h4>
                            <p>{{ $config->contact_phone }}</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="block">
                        <span><i class="fa fa-map-marker" aria-hidden="true"></i></span>
                        <div class="text">
                            <p>{{ $dictionary['address'] }}</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="block">
                        <span><i class="fa fa-envelope" aria-hidden="true"></i></span>
                        <div class="text">
                            <h4>{!! $dictionary['write_to_us'] or "Bizə yaz" !!}</h4>
                            <p>{{ $config->email }}</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- ContactThreeBlocks End -->
    <!-- ContactTab Begin -->
    <section class="contact_tab">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <!-- TabNav -->
                    <nav class="tabnav contact_tab__nav clearfix">
                        <a href="#" title="">{!! $dictionary['address1'] or "Baş ofis" !!}</a>
                        <a href="#" title="">{!! $dictionary['address2'] or "ASAN xidmət ofisləri" !!}</a>
                    </nav>
                </div>
                <!-- Tabcontent -->
                <div class="tab_content col-xs-12">
                    <div class="row">
                        <div class="tab_block">
                            <!-- ContactForm -->
                            <div class="contact_form col-md-7 clearfix">
                                <h1>{!! $dictionary['contact_us'] or "<strong>Bizimlə</strong> əlaqə saxla" !!}</h1>

                                <div id="divForm">

                                    {!! Form::open(['url'=>route('contact'), 'method'=>'POST', 'id' => 'form']) !!}

                                    <span class="span">
                                        {!! Form::text('full_name', null, ["class" => "alphaonly", "placeholder" => $dictionary['full_name'] ]) !!}
                                    </span>
                                    <span class="span">
                                        {!! Form::text('email', null, ["placeholder" => $dictionary['email'] ]) !!}
                                    </span>
                                    <span class="span">
                                        {!! Form::text('subject', null, ["placeholder" => $dictionary['subject'] ]) !!}
                                    </span>
                                    <span class="span">
                                        {!! Form::textarea('text', null, ["placeholder" => $dictionary['text'] ]) !!}
                                    </span>

                                    {!! Form::button($dictionary['send'], ['id' => 'loadingButton', 'data-loading-text' => g_icon('refresh', null, 'spinning'), 'autocomplete' => 'off', 'type' => 'submit']) !!}

                                    {!! Form::close() !!}

                                    <h5 id="msgBox" style="color:#c23934"></h5>

                                </div>

                                <h3 id="successMessage" style="display: none"></h3>

                            </div>
                            <!-- ContactInfo -->
                            <div class="contact_info col-md-5">
                                <div class="block">
                                    <h4>{!! $dictionary['our_office'] or "Bizim ofis" !!}</h4>
                                    <p>{!! $dictionary['address'] or "A. Gayibov 10Q, SDN Business Centre 3rd floors AZ1029" !!}</p>
                                    <p>{!! $dictionary['country'] or "Bakı, Azərbaycan" !!}</p>
                                </div>
                                <div class="block">
                                    <h4>{!! $dictionary['business_hours'] or "İş saatları" !!}</h4>
                                    <p>{!! $dictionary['business_hours_week'] or "Monday – Saturday: 8am to 6pm" !!}</p>
                                    <p>{!! $dictionary['business_hours_non'] or "Sunday: Closed" !!}</p>
                                </div>
                                <div class="block">
                                    <h4>{!! $dictionary['social_networks'] or "Sosial şəbəkələr" !!}</h4>
                                    <nav class="social">

                                        @foreach($social as $link)
                                            <a href="{{ $link->forward_url }}" target="_blank" class="{{ $link->type }}"><i class="fa fa-{{ $link->type }}"></i></a>
                                        @endforeach

                                    </nav>
                                </div>
                            </div>
                        </div>
                        <div class="tab_block">
                            <!-- ContactOffice -->
                            <div class="contact_office">

                                @if($page->children->count())

                                    @foreach($page->children as $block)
                                        <div class="col-md-4">
                                            <h2>{{ $block->name }}</h2>
                                            <div>
                                                {!! $block->summary !!}
                                            </div>
                                        </div>
                                    @endforeach

                                @else
                                    <h4>Burada ofislər yerləşəcək. Kateqoriyalardan əlaqə səhifəsini seçərək müvafiq ofislər yarada bilərsiz. Yaradarkən modulu <b>blok</b> seçməyiniz tövsiyyə olunur.</h4>
                                @endif

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- ContactTab End -->

@endsection

@push('scripts')

    <script src="https://maps.googleapis.com/maps/api/js?key={{ $config->google_api_key }}&language={{ $lang }}"></script>

    @include('web.elements.map-script', ['coordinates' => $config->location] )

    @include('web.elements.form')

@endpush