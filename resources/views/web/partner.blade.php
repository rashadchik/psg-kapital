@extends ('layouts.web', ['page_heading' => $pageTitle, 'page_image' => $page->cover ? asset('files/cache/images/'.$page->relation_page.'/'.$page->cover->image) : ""])

@section ('content')

    @include('web.elements.breadcrumb')

    <!-- InsidePage Begin -->
    <section class="inside_page">
        <div class="container">
            <div class="row">
                <div class="col-md-3 col-sm-4 col-xs-12">
                    @include('web.elements.sideBar')
                </div>
                <div class="col-md-9 col-sm-8 col-xs-12">
                    @include('web.elements.page-cover2')
                    <!-- Partners Begin -->
                    <div class="partners">
                        <div class="row">
                            <div class="col-md-6 col-xs-12">
                                {!! $page->summary !!}
                            </div>

                            <div class="col-md-6 col-xs-12 pull-right">
                                <div class="partners_form" id="divForm">
                                    <h4>{{ $dictionary['partner'] or "BİZİM PARTNYORUMUZ OLUN" }}</h4>

                                    {!! Form::open(['url'=>route('contact'), 'method'=>'POST', 'id' => 'form']) !!}

                                        <div class="item span">
                                            <label for="full_name">{!! $dictionary['full_name'] or "Adınız və soyadınız" !!}</label>
                                            {!! Form::text('full_name', null, ["class" => "alphaonly", 'id' => 'full_name' ]) !!}
                                        </div>

                                        <div class="item span form__cell">
                                            <label for="phone">{!! $dictionary['phone'] or "Mobil nömrəniz" !!}</label>
                                            {!! Form::text('phone', null, ['id' => 'phone' ]) !!}
                                        </div>

                                        <div class="item span">
                                            <label for="email">{!! $dictionary['email'] or "E-poçt" !!}</label>
                                            {!! Form::text('email', null, ['id' => 'email' ]) !!}
                                        </div>

                                        @if(!is_null($page->content_extra))
                                        <div class="item">
                                            <label for="type">{!! $dictionary['partner_service'] or "Xidmətin növü" !!}</label>
                                            {!! Form::select('service', explode(',', $page->content_extra), null) !!}
                                        </div>
                                        @endif

                                        <div class="item">
                                            <p class="pull-left span">
                                                {!! Form::checkbox('terms', 1, false, ['id' => 'terms', 'class' => 'css-checkbox']) !!}
                                                <label for="terms" class="css-label label-item">
                                                    @if(isset($dictionary['partner_condition']))
                                                        {!! str_replace(':link', route('condition', $page->slug), $dictionary['partner_condition']) !!}
                                                    @else
                                                        Mən <a href="{{ route('condition', $page->slug) }}" target="_blank">şərtlərlə</a> razıyam
                                                    @endif
                                                </label>
                                            </p>

                                            {!! Form::button($dictionary['send'], ['id' => 'loadingButton', 'data-loading-text' => g_icon('refresh', null, 'spinning'), 'autocomplete' => 'off', 'type' => 'submit']) !!}
                                        </div>

                                        {!! Form::hidden('page', $page->id) !!}

                                    {!! Form::close() !!}

                                    <p id="msgBox" style="color:#c23934; display:none"></p>

                                </div>

                                <h4 id="successMessage" style="display: none"></h4>

                            </div>

                        </div>
                    </div>
                    <!-- Partners End -->
                </div>
            </div>
        </div>
    </section>
    <!-- InsidePage End -->

@endsection


@push('scripts')
    @include('web.elements.form', ['phone' => true, 'withoutMsg' => true, 'terms' => true])
@endpush