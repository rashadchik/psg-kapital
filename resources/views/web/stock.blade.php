@extends ('layouts.web', ['page_heading' => $page->name, 'page_image' => !is_null($page->image_original) ? asset("files/cache/images/$page->id/$page->image_original") : ""])

@section ('content')

    @include('web.elements.breadcrumb')

    @include('web.elements.content', ['class1' => 'col-md-9 col-xs-12', 'class2' => 'col-md-3 col-xs-12', 'banner' => true])

    <section class="bgwhite">
        <div class="container">
            <div class="row">


                <div class="pi_text_block">
                    <div class="col-md-8 col-sm-7 col-xs-12">
                        <div class="text text-justify">
                            {!! $page->content_extra !!}
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-5 col-xs-12">
                        <div class="partners_photo text-center">
                            <img src="img/delete/logos.jpg" alt="">
                        </div>
                    </div>
                </div>

                <div class="row padding-bottom-30">
                    <div class="col-md-8">

                        @if($page->children->count())

                            @foreach($page->children as $child)
                            <!-- PIDetail -->
                            <div class="pi_detail">
                                <div class="row">
                                    <div class="col-md-4 col-sm-5 col-xs-4 col-mob-12">
                                        <img src="{{ asset("files/cache/images/$child->id/$child->image_original") }}" alt="{{ $child->name }}">
                                    </div>
                                    <div class="col-md-8 col-sm-7 col-xs-8 col-mob-12">
                                        <h2 class="title">{{ $child->name }}</h2>
                                        <div class="text">
                                            {!! $child->summary !!}
                                        </div>
                                    </div>
                                </div>
                            </div>
                            @endforeach

                        @endif

                    </div>
                    <div class="col-md-4">
                        @include('web.elements.application_form', ['bg' => 'bggrey', 'class3' => 'margin-top-35'])
                    </div>
                </div>

                <div class="row padding-bottom-70">
                    <div class="col-md-10 col-md-offset-1">
                        <div class="row">

                            @if($stocks->count() > 0)
                                @foreach(config('config.stockType') as $key => $stock)

                                    <div class="col-md-6">
                                        <div class="data_table">
                                            <p>{{ $dictionary["stock_text_$key"] or "Aşağıda göstərilən cədvəldə dünyanın məşhur ETF (Birja Fondlarının)  qızıl, gümüş, SPX, Nasdaq və s. kimi birja fonlarının son bir ildəki gəlirliyi göstərilmişdir." }} </p>
                                            <table class="text-center">
                                                <thead>
                                                <tr>
                                                    <th>{{ $dictionary["stock_name_$key"] or $stock }} </th>
                                                    <th>{{ $dictionary["stock_indicator_1"] or "1-illik göstərici" }}</th>
                                                    <th>{{ $dictionary["stock_indicator_3"] or "3-illik göstərici" }}</th>
                                                    <th>{{ $dictionary["stock_divident"] or "Dividend" }}</th>
                                                </tr>
                                                </thead>
                                                <tbody>

                                                @foreach($stocks as $stock)

                                                @if($stock->type == $key)
                                                <tr>
                                                    <td data-title="{{ $dictionary["stock_name_$key"] or $stock }}">{{ $stock->name }}</td>
                                                    <td data-title="{{ $dictionary["stock_indicator_1"] or "1-illik göstərici" }}">{{ $stock->indicator1 }}%</td>
                                                    <td data-title="{{ $dictionary["stock_indicator_3"] or "3-illik göstərici" }}">{{ $stock->indicator3 }}%</td>
                                                    <td data-title="{{ $dictionary["stock_divident"] or "Dividend" }}">{{ $stock->dividend }}</td>
                                                </tr>

                                                @endif

                                                @endforeach

                                                </tbody>
                                            </table>
                                        </div>
                                    </div>

                                @endforeach
                            @endif

                        </div>
                        <p class="p_qeyd">* {{ $dictionary["loan_note"] or "Göstərilən cədvəllər ayda bir dəfə yenilənir" }}</p>

                        @include('web.elements.banner', ['type' => 2, 'class' => 'margin-top-30'])
                    </div>
                </div>

            </div>

        </div>
    </section>

@endsection
