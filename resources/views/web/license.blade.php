@extends ('layouts.web', ['page_heading' => $pageTitle, 'page_image' => $page->cover ? asset('files/cache/images/'.$page->relation_page.'/'.$page->cover->image) : ""])

@section ('content')

    @include('web.elements.breadcrumb')

    <!-- InsidePage Begin -->
    <section class="inside_page">
        <div class="container">
            <div class="row">
                <div class="col-md-3 col-sm-4 col-xs-12">
                    @include('web.elements.sideBar')
                </div>
                <div class="col-md-9 col-sm-8 col-xs-12">
                    @include('web.elements.page-cover2')
                    <!-- Licenses Begin -->
                    <div class="licenses">
                        <h1>{{ $page->name }}</h1>
                        <div class="row">

                            @foreach($page->galleries as $gallery)
                                <article class="col-sm-4 col-xs-6 col-mob-12">
                                    <figure>
                                        <a href="{{ asset("files/cache/images/$page->relation_page/$gallery->filename") }}" class="zoom">
                                            <img src="{{asset("files/cache/images/$page->relation_page/thumb/$gallery->filename")}}">
                                        </a>
                                    </figure>
                                </article>
                            @endforeach

                        </div>
                    </div>
                    <!-- Licenses End -->
                </div>
            </div>
        </div>
    </section>
    <!-- InsidePage End -->

@endsection