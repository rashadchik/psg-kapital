@extends ('layouts.web', ['page_heading' => $page->name])

@section ('content')

    @include('web.elements.breadcrumb')

    <section class="news">

        @if(!empty($years->count() > 0))

        <div class="container">
            <div class="row">
                <h1 class="col-xs-12">{{ $page->name }}</h1>
                <!-- DateFilter -->
                <div class="col-xs-12 date_filter">
                    <div class="year clearfix">
                        <span>{{ $dictionary['search_article'] or 'Xəbəri ilinə görə axtarın'}}</span>
                        {!! Form::select('year', $years, $thisYear, ['class' => 'select'] ) !!}
                    </div>

                    <div class="month clearfix">
                        @foreach(config('config.months.'.$lang.'_min') as $key => $month)
                            <a href="?month={{ $key }}" title="{{ $month }}" class="news_link">{{ $month }}</a>
                        @endforeach
                    </div>
                </div>
            </div>
            <div class="row">
                <!-- NewsList -->
                <div class="news_list" id="post-news">
                    @include('web.elements.ajax-news', ['news' => $news])
                </div>
            </div>

            <div class="row ajax-loading" style="display:none;z-index:100">
                <p><img src="{{ asset('images/ajax-loading.svg') }}" width="50px"></p>
            </div>
        </div>

        @endif
    </section>

@endsection


@push('scripts')

<script type="text/javascript">

    var page = 1;
    @if($news->count() == 6)
    $(window).scroll(function() {
        if($(window).scrollTop() + $(window).height() >= $(document).height() - 200) {
            if(requestSent) {return;}
            page++;
            loadMoreData(page);
        }
    });
    @endif

    function loadMoreData(page){

        requestSent = true;

        $.ajax(
            {
                url: '?year={{$thisYear}}&page=' + page,
                type: "get",
                beforeSend: function()
                {
                    $('.ajax-loading').show();
                }
            })
            .done(function(data)
            {
                $('.ajax-loading').hide();


                if(data.html == ""){
                    return;
                }
                else{
                    $("#post-news").append(data.html);
                }

                requestSent = false;

            })
            .fail(function(jqXHR, ajaxOptions, thrownError)
            {
                $('.ajax-loading').html("{{ trans('locale.whops') }}");
                requestSent = false;
            });
    }

    $('.select').on("change", function(e){
        $('#post-news div').remove();
        var select_url=$(this).find(":selected").val();
        e.preventDefault();
        $.ajax({
            type : "get",
            url:'?year=',
            data: {"year" : select_url},
            success: function(response){
                $('#post-news').html(response.html);
            },
            error: function() {
            }
        });
    });


    $('.news_link').on("click", function(e){
        this_url=$(this).attr('href');
        var select_year=$('.select').find(":selected").val();
        $('.ajax-loading').fadeIn();
        $('#post-news div').css('opacity','0.6');
        e.preventDefault();
        $.ajax({
            type : "get",
            url: this_url,
            data: {"year" : select_year},
            success: function(response){
                $('.ajax-loading').hide();
                $('#post-news').html(response.html);
            },
            error: function() {
                $("#post-news").html("<h3>{{ trans('locale.whops') }}</h3>");
            }
        });
    });

</script>

@endpush