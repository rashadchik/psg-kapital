@extends ('layouts.web', ['page_heading' => $page->name])

@section ('content')

    @include('web.elements.breadcrumb')

    <!-- PdfList Begin -->
    <section class="inside_page">
        <div class="container">
            <div class="row">
                <div class="col-md-3 col-sm-4 col-xs-12">
                    @include('web.elements.sideBar')
                </div>
                <div class="col-md-9 col-sm-8 col-xs-12">
                    @include('web.elements.page-cover2')

                    <div class="pdf_list">
                        <h1>{{ $page->name }}</h1>
                        <div class="row">

                            @foreach($page->documents as $doc)
                                <div class="col-xs-12">
                                    <article>
                                        <h2>{{ $doc->summary }}</h2>
                                        <span class="pdf">
                                            <a href="{{ asset($doc->document) }}" target="_blank">
                                                <i></i>PDF
                                            </a>
								        </span>
                                    </article>
                                </div>
                            @endforeach

                        </div>
                    </div>

                </div>
            </div>
        </div>
    </section>
    <!-- PdfList End -->
@endsection