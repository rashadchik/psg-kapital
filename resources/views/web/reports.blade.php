@extends ('layouts.web', ['page_heading' => $page->name, 'page_image' => $page->cover ? asset('files/cache/images/'.$page->relation_page.'/'.$page->cover->image) : ""])

@section ('content')

    @include('web.elements.breadcrumb')

    <!-- Reports Begin -->
    <section class="inside_page">
        <div class="container">
            <div class="row">
                <div class="col-md-3 col-sm-4 col-xs-12">
                    @include('web.elements.sideBar')
                </div>
                <div class="col-md-9 col-sm-8 col-xs-12">
                    @include('web.elements.page-cover2')
                    <!-- Reports Begin -->
                    <div class="reports">
                        <h1 class="">{{ $page->name }}</h1>
                        <div class="row">

                            @foreach($page->documents as $doc)
                                <div class="col-md-4 col-sm-6 col-xs-6 col-mob-12">
                                    <article>
                                        <span class="date"><span>{{ $doc->year }}</span></span>
                                        <div class="body">
                                            <p>{{ $doc->summary }}</p>
                                            <span class="pdf">
                                                <a href="{{ asset($doc->document) }}" target="_blank"><strong>PDF</strong> {{ $dictionary['download'] or 'yüklə' }}</a>
                                            </span>
                                        </div>
                                    </article>
                                </div>
                            @endforeach

                        </div>
                    </div>
                    <!-- Reports End -->
                </div>
            </div>
        </div>
    </section>
    <!-- Reports End -->
@endsection