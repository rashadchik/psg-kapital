@extends ('layouts.web', ['page_heading' => $page->name, 'page_image' => !is_null($page->image_original) ? asset("files/cache/images/$page->id/$page->image_original") : ""])

@section ('content')

    @include('web.elements.breadcrumb')

    @include('web.elements.content')

    <section class="bgwhite padding-40">
        <div class="container">
            <div class="row">
                <div class="col-md-7">
                    <div class="pi_text_block padding-0">
                        <h2 class="title">{{ $dictionary['analysis_service'] or "Xidmətin növləri" }}</h2>
                        <div class="text">
                            {!! $page->content_extra !!}
                        </div>
                    </div>
                </div>
                <div class="col-md-5">
                    @include('web.elements.banner', ['type' => 1])
                </div>
            </div>
            <div class="row">
                @include('web.elements.banner', ['type' => 2, 'class' => 'col-xs-12 margin-top-30'])
            </div>
        </div>
    </section>

@endsection
