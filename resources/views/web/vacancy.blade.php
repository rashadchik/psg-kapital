@extends ('layouts.web', ['page_heading' => $page->name])

@section ('content')

    @include('web.elements.breadcrumb')

    <section class="vacancies">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    @include('web.elements.page-cover2')
                </div>
            </div>
            <div class="row">
                <h1 class="col-xs-12">{{ $page->name }}</h1>
            </div>
            <div class="row">
                <div class="col-xs-12 vtable">
                    <table class="table table-bordered">
                        <thead>
                        <tr>
                            <th>{{ $dictionary["vacancy_name"] or "Vakansİyanın adı" }}</th>
                            <th>{{ $dictionary["vacancy_experience"] or "İş təcrübəsi" }}</th>
                            <th>{{ $dictionary["vacancy_salary"] or "Əmək haqqı" }}</th>
                            <th>{{ $dictionary["vacancy_end_date"] or "Bİtmə tarİxİ" }}</th>
                            <th style="width: 140px;">&nbsp;</th>
                        </tr>
                        </thead>
                        <tbody>

                        @forelse($vacancies as $vacancy)
                            <tr>
                                <td data-title="{{ $dictionary["vacancy_name"] or "Vakansİyanın adı" }}ı"><span>{{ $vacancy->name }}</span></td>
                                <td data-title="{{ $dictionary["vacancy_experience"] or "İş təcrübəsi" }}"><span>{{ $vacancy->description }}</span></td>
                                <td data-title="{{ $dictionary["vacancy_salary"] or "Əmək haqqı" }}"><span>@if($vacancy->salary == 0) {{ $dictionary['salary_agreement'] or 'Razılaşma yolu ilə' }} @endif</span></td>
                                <td data-title="{{ $dictionary["vacancy_end_date"] or "Bİtmə tarİxİ" }}"><span>{{ blogDate($vacancy->end_date) }}</span></td>
                                <td data-title="{{ $vacancy->name }}"><a href="{{ route('showPage', $vacancy->slug) }}" title="{{ $vacancy->name }}" class="more">{{ $dictionary['read_more'] }}</a></td>
                            </tr>
                            @empty
                            <tr>
                                <td colspan="100%">{{ $dictionary['nothing_found'] or 'Mövcud vakansiya tapılmadı' }}</td>
                            </tr>
                        @endforelse

                        </tbody>
                    </table>
                </div>
                <!-- Pagination Begin -->
                <nav class="col-xs-12 nav_pagination">

                    {!! $vacancies->links() !!}

                </nav>
                <!-- Pagination End -->
            </div>
        </div>
    </section>


@endsection
