@extends ('layouts.web', [ 'page_heading' => null ] )

@section ('content')

    @if($slider->count() > 0)
        @include('web.elements.slider')
    @endif

    @include('web.elements.currency')

    @include('web.elements.main-tab')

    @if(!is_null($servicePage))
        @include('web.elements.services')
    @endif

    @if(!is_null($newsPage))
        @include('web.elements.news')
    @endif

    @if($partners->count() > 0)
        @include('web.elements.partners')
    @endif

    <!-- IndexMap Begin -->
    <section class="index_map">
        <div id="map"></div>
    </section>
    <!-- IndexMap End -->

@endsection


@push('scripts')

    <script src="https://maps.googleapis.com/maps/api/js?key={{ $webConfig->google_api_key }}&language={{ $lang }}"></script>

    @include('web.elements.map-script', ['coordinates' => $webConfig->location])
@endpush
