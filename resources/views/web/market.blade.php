@extends ('layouts.web', ['page_heading' => $page->name, 'page_image' => !is_null($page->image_original) ? asset("files/cache/images/$page->id/$page->image_original") : ""])

@section ('content')

    @include('web.elements.breadcrumb')

    @include('web.elements.content')

    <section class="bgwhite padding-40">
        <div class="container">
            <div class="row">
                <div class="col-md-7">

                    @if($page->children->count())
                    <!-- FAQ -->
                    <div class="faq">

                        <h1>{{ $dictionary['faq'] or 'Sual Cavab' }}</h1>

                        @foreach($page->children as $block)
                            @if($block->template_id == 11)

                                <div class="item">
                                    <div class="head">
                                        <h2>{{ $block->name }}</h2>
                                    </div>
                                    <div class="body">
                                        <p>{!! $block->summary !!}</p>
                                    </div>
                                </div>

                            @endif
                        @endforeach

                    </div>

                    @else
                        <h4>Burada sual-cavab olacaq. Kateqoriyalardan {{ $page->name }} səhifəsini seçərək müvafiq sual-cavabları yarada bilərsiz. Yaradarkən modulu <b>blok</b> seçməyiniz mütləqdir.</h4>
                    @endif

                </div>
                <div class="col-md-5">

                    @include('web.elements.deal')

                    @include('web.elements.banner', ['type' => 1, 'class' => 'margin-bottom-30'])

                </div>
            </div>
        </div>
    </section>

@endsection
