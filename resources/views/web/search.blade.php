@extends ('layouts.web', ['page_heading' => $page->name])

@section ('content')

    @include('web.elements.breadcrumb')

    <section class="search_page">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    @include('web.elements.page-cover2')
                </div>
            </div>
            <div class="row">
                <h1 class="col-xs-12">{{ $page->name }}</h1>

                <div class="col-xs-12 result">
                    {!! str_replace( [ ':keyword', ':count' ], [ '<strong>'.$keyword.'</strong>', '<em>'.$results->count().'</em>' ], $dictionary['search_result']) !!}
                </div>
            </div>
            <div class="row">

                <!-- List -->
                <div class="list">

                    @if($results->count() > 0)

                        @foreach($results as $result)
                            <article class="clearfix">

                                @if(!is_null($result->image))
                                    <div class="col-md-3 col-sm-4 col-xs-12">
                                        <figure>

                                            @if($result->type == 1)
                                                <a href="{{ route('showPage', $result->slug) }}">
                                                    <img src="{{ asset("files/cache/images/$result->id/$result->image") }}" alt="{{ $result->name }}">
                                                </a>
                                            @else
                                                <a href="{{ route('showArticle', [$result->category, $result->slug]) }}">
                                                    <img src="{{ asset("files/cache/images/$result->relation_page/thumb/$result->image") }}" alt="{{ $result->name }}">
                                                </a>
                                            @endif

                                        </figure>
                                    </div>
                                @endif


                                <div class="@if(!is_null($result->image)) col-md-9 col-sm-8 @else col-md-12 @endif col-xs-12">

                                    <h2>
                                        <a href="@if($result->type == 1) {{route('showPage', $result->slug)}} @else {{route('showArticle', [$result->category, $result->slug])}} @endif">
                                            {{ $result->name }}
                                        </a>
                                    </h2>

                                    <span class="date">{!! blogDate($result->published_at) !!}</span>

                                    <p>
                                        @if(str_contains($result->summary, [$keyword, mb_strtolower($keyword), mb_strtoupper($keyword), ucfirst_utf8($keyword), title_case($keyword)]))
                                            {{ str_limit(strip_tags($result->summary), 300) }}
                                        @endif

                                        @if(str_contains($result->content, [$keyword, mb_strtolower($keyword), mb_strtoupper($keyword), ucfirst_utf8($keyword), title_case($keyword)]))
                                            {{ str_limit(strip_tags($result->content), 300) }}
                                        @endif
                                    </p>
                                </div>
                            </article>
                        @endforeach

                    @endif

                </div>
            </div>
        </div>
    </section>

@endsection
