
<h4>Aşağıdakı 4 blok DEMO olaraq yaradılmışdır. Əgər siz hazırki səhifənin ({{ $page->name }}) daxilində bu blokları ayrıca yaratsaz, DEMO ləğv olunacaq və sizin yaratdıqlarınız əks olunacaq. Blok yaradarkən modulu <b>blok</b> seçməyiniz tövsiyyə olunur.</h4>
<div class="col-md-6 col-sm-6 col-xs-12">
    <article>
        <i class="col-md-4 col-xs-4 col-mob-12 padding-0 icon1"></i>
        <div class="col-md-8 col-xs-8 col-mob-12 text">
            <h2>Baxışımız</h2>
            <p>Azərbaycanda seçilmiş investisiya şirkəti kimi sayılıb, müştərilərimizə dünya səviyyəli investisiya təcrübəsini mükəmməl müştəri yönümlü xidmətlərlə təklif etmək, peşekar, məsuliyyətli və şəffaf şəkildə fəaliyyət göstərməkdir.</p>
        </div>
    </article>
</div>
<div class="col-md-6 col-sm-6 col-xs-12">
    <article>
        <i class="col-md-4 col-xs-4 col-mob-12 padding-0 icon2"></i>
        <div class="col-md-8 col-xs-8 col-mob-12 text">
            <h2>Prİnsİplərİmİz</h2>
            <ul>
                <li><span>Əsas prinsipimiz risklərə xüsusi diqqət yetirmək şərti ilə tədqiqat və texnologiyalara yatırım etmək;</span></li>
                <li><span>Hər bir müştəri bizim üçün dəyərli olduğu üçün onların müvəffəqiyyəti – bizim müvəffəqiyyətimizdir.</span></li>
            </ul>
        </div>
    </article>
</div>
<div class="col-md-6 col-sm-6 col-xs-12">
    <article>
        <i class="col-md-4 col-xs-4 col-mob-12 padding-0 icon3"></i>
        <div class="col-md-8 col-xs-8 col-mob-12 text">
            <h2>Dəyərlərİmİz</h2>
            <ul>
                <li><span>Şirkətimizin müştəriləri arasında şəffaflığı təmin etməklə onların etibarını qazanmaq və şirkətə olan sadiqliklərini təmin etmək;</span></li>
                <li><span>Müştərilərlə müştərək iş birliyimizin uzunmüddətli olması üçün davamlı araşdırmalar edir, yeni texnologiyalara yatırım edirik.</span></li>
            </ul>
        </div>
    </article>
</div>
<div class="col-md-6 col-sm-6 col-xs-12">
    <article>
        <i class="col-md-4 col-xs-4 col-mob-12 padding-0 icon4"></i>
        <div class="col-md-8 col-xs-8 col-mob-12 text">
            <h2>Mİssİyamız</h2>
            <ul>
                <li><span>Müştəri məmnuniyyətinə nail olmaq üçün ən yüksək standartlara uyğun xidmət göstərmək;</span></li>
                <li><span>Müştərilər ilə uzunmüddətli əməkdaşlığı təmin etmək;</span></li>
                <li><span>Şirkətimizin dəyərlərini bizimlə bölüşən ən yaxşı mütəxəssisləri cəlb etmək;</span></li>
            </ul>
        </div>
    </article>
</div>
