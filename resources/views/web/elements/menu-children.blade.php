<li @if(@$parentPage->id == $parent->id) class="active" @endif>
        <a href="javascript:void(0)" title="{{ $child->name }}">{{ $child->name }} <i class="fa fa-angle-{{$arrow}}" aria-hidden="true"></i></a>

        <ul class="list-unstyled">

            @foreach($children as $child)

                @if($child->menuChildren->count() && $child->template_id == 0)
                    @include('web.elements.menu-children', ['children' => $child->menuChildren, 'parent' => $parent, 'arrow' => 'right'])
                @else
                    <li><a href="{{ route('showPage', $child->slug) }}" title="{{ $child->name }}" target="{{ target($child->target) }}">{{ $child->name }}</a></li>
                @endif

            @endforeach

        </ul>
</li>