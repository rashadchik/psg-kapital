<div id="project_slider" class="col-xs-12">
    <div class="item">
        <article>
            <img src="img/delete/slider-project.png" alt="">
            <div class="caption">
                <h1>“SUN” YAŞ DƏSMALLAR INDI DƏ KÖRPƏLƏR ÜÇÜN</h1>
                <span class="date">Keçirilmə tarixi: 01.12.2017 | BAKI</span>
                <p>Xüsusi mikroməsaməli və kremli liflər sayəsində “Sun” universal ətirli yaş dəsmal körpənizin dərisini nəmləndirərək canlandıracaq, istədiyiniz yumşaqlığı təmin edəcək.</p>
                <a href="#" title="" class="read">ƏTRAFLI</a>
            </div>
        </article>
    </div>
</div>