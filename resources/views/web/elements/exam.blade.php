<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta http-equiv="content-type" content="text-html; charset=utf-8">
    <title>Exam result</title>

    <style type="text/css">
        td {
            line-height: 1.8em;
            text-align: right;
        //font-weight: bold;
        }

        tr {
            margin-bottom: 2em;
        }

        td.input {
            border-bottom: 0.5px solid #000;
            text-align: center;
            font-weight: normal;
            font-style: italic;
        }

        .title{
            padding-top:60px;
            text-align:center;
            font-style: italic;

            font-size:24px;

        }
        .title2{
            padding-top:30px;
            font-style: italic;
            font-size:18px;
        }

        .font1{
            text-align: center;
            vertical-align: top !important
        }
        .font1 span{
            font-size: 12px;
            margin-top: -20px
        }
        img{
            width:200px;
            margin-bottom: 20px
        }
    </style>

</head>
<body style="font-family:'dejavu sans';">

    @foreach($exam as $question)

        <p>{{ $loop->iteration }}) {{ $question->title }}</p>

        @if(!is_null($question->image_original))
            <img src="{{ asset("files/cache/images/$question->id/$question->image_original") }}" alt="{{ $question->title }}" style="width:100%">
        @endif


        <ul>
            @foreach($question->answer as $answer)

            <li @if(in_array($answer->id, $content['answers'])) style="color:red" @endif>
                {{ config("config.test.$loop->iteration") }}) {{ $answer->title }}
            </li>

            @endforeach
        </ul>


    @endforeach

</body>
</html>
