<!-- InsideSidebar -->
<aside class="inside_sidebar">
    <h3>{{ $parentPage->name }}</h3>
    @if($parentPage->children->count())

        <nav class="side_menu">
            @foreach($parentPage->children as $children)
                <a href="{{ route('showPage', $children->slug) }}" title="{{ $children->name }}" @if($children->id == $page->id) class="active" @endif>{{ $children->name }}</a>
            @endforeach
        </nav>

    @endif
</aside>