@if($parentPage->children->count())
<!-- SubMenumobile -->
<nav class="hidden-xs hidden-sm submenu">
        <div class="container">
                <div class="row">
                        <div class="col-xs-12">
                                <ul class="list-unstyled">
                                    @foreach($parentPage->children as $subMenu)
                                        <li @if($subMenu->id == $page->id) class="active" @endif>
                                                <a href="{{ route('showPage', $subMenu->slug) }}" title="{{ $subMenu->name }}">{{ $subMenu->name }}</a>
                                        </li>
                                    @endforeach
                                </ul>
                        </div>
                </div>
        </div>
</nav>
@endif