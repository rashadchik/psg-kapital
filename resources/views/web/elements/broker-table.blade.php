@if($page->brokerTable->count())
    <table class="pi_table2">
        <thead>
        <tr>
            <th>&nbsp;</th>

            @foreach(config('config.broker-table') as $key => $th)
                <th>{{ $dictionary[$key] or $th }}</th>
            @endforeach

        </tr>
        </thead>
        <tbody>

        @foreach($page->brokerTable as $data)

            <tr>
                @if($loop->first)
                    <td rowspan="5" class="first_td">{{ $dictionary['action_volume'] or 'Əməliyyat Həcmi' }}</td>
                @endif

                @foreach(config('config.broker-table') as $key => $dt)
                    <td data-title="{{ $dictionary[$key] or $dt }}">{{ $data->$key }}</td>
                @endforeach

            </tr>

        @endforeach

    </table>
@endif