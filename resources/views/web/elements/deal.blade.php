@if($page->details && ($page->details->loans > 0 || $page->details->proportions > 0))

    <div class="pi_cedvel v2 clearfix">
        <h1>{{ $dictionary['transaction_table'] or 'Bağlanmış əqdlər üzrə cədvəl' }}</h1>
        <div class="col-md-2">
            <h2>{{ $dictionary['action_volume'] or 'Əməliyyat Həcmi'}}</h2>
        </div>
        <div class="col-md-5">
            <div class="row">
                <h4>{{ $dictionary['loan'] or 'İstiqraz' }}</h4>
                <p>{{ $page->details->loans or 0 }}</p>
            </div>
        </div>
        <div class="col-md-5">
            <div class="row">
                <h4>{{ $dictionary['stock'] or 'Səhm' }}</h4>
                <p>{{ $page->details->proportions or 0 }}</p>
            </div>
        </div>
    </div>

@endif