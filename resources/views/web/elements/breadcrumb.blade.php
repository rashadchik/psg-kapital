<!-- BreadCrumbs Begin -->
<section class="breadcrumbs">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">

                {!! Breadcrumbs::render('web', $parentPage, $page, @$dictionary['home_page'], $articleSingle = false) !!}

                @if(isset($category) && $parentPage->template_id == 16 && $category->children->count())
                    <div class="pull-right hidden-xs">
                        @foreach($category->children as $child)
                            @if($child->id != $page->id)
                                <a href="{{ route('showPage', $child->slug) }}" title="{{ $child->name }}">{{ $child->name }}</a>
                            @else
                                <span class="current">{{ $child->name }}</span>
                            @endif

                            @if(!$loop->last)
                                <span class="arrow">/</span>
                            @endif
                        @endforeach
                    </div>

                @endif
            </div>
        </div>
    </div>
</section>
<!-- BreadCrumbs End -->