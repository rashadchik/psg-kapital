<!-- MainTab Begin -->
<section class="main_tab">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 main_tab_head">
                <h1>{{ $dictionary['main_description'] or ''}}</h1>
                <p>{!! $dictionary['main_text'] or ''!!}</p>
            </div>

            <nav class="col-xs-12 main_tab_nav">
                <a href="#" title="">{!! $dictionary['loan1'] or "Daxili Bazar İstiqrazları" !!}</a>
                <a href="#" title="">{!! $dictionary['loan2'] or "Xarici Bazar" !!}</a>
                <a href="#" title="">{!! $dictionary['loan3'] or "Mərkəzi Bankın Xəzinə Notları" !!}</a>
                <a href="#" title="">{!! $dictionary['loan4'] or "Maliyyə Nazirliyinin İstiqrazları" !!}</a>
            </nav>
        </div>


        <div class="row">
            <div class="main_tab_content">
                <div class="main_tab_panel">
                    <div class="col-sm-6">
                        <table class="tab_table">
                            <thead>
                                <tr>
                                    <th>{!! $dictionary['measuring_papers'] or "Qiymətli kağız" !!}</th>
                                    <th>{!! $dictionary['value'] or "Qiymət" !!}</th>
                                    <th>{!! $dictionary['profitability'] or "Gəlirlilik" !!}</th>
                                    <th>{!! $dictionary['nominal'] or "Nominal" !!}</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($loans as $loan)

                                    @if($loan->type == 1)

                                    <tr>
                                        <td>{{ $loan->title }}</td>
                                        <td>{{ $loan->prise_difference1 }}</td>
                                        <td class="@if($loan->prise_difference2  > 0)up @else down @endif ">{{ $loan->prise_difference2}}</td>
                                        <td>{{ $loan->name }}</td>
                                    </tr>

                                    @endif

                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    <div class="col-sm-5 col-sm-offset-1">
                        <div class="tab_list tab_list_unstyled">

                            {!! $dictionary['loan1_text'] or '<h3>Daxili bazarda ticarətin üstünlükləri</h3>' !!}

                        </div>
                    </div>
                </div>
                <div class="main_tab_panel">
                    <div class="col-sm-6">
                        <table class="tab_table">
                            <thead>
                            <tr>
                                <th>{!! $dictionary['foreign_measuring_papers'] or "Qiymətli kağız" !!}</th>
                                <th>{!! $dictionary['closed_price'] or "Bağlanış qiyməti" !!}</th>
                                <th>{!! $dictionary['opened_price'] or "Açılış qiyməti" !!}</th>
                                <th>{!! $dictionary['features'] or "Features" !!}</th>
                            </tr>
                            </thead>
                            <tbody>

                                @foreach($loans as $loan)

                                    @if($loan->type == 2)

                                        <tr>
                                            <td>{{ $loan->title }}</td>
                                            <td>{{ $loan->prise_difference1 }}</td>
                                            <td class="@if($loan->prise_difference2  > 0)up @else down @endif ">{{ $loan->prise_difference2 + 0}}</td>
                                            <td>{{ $loan->name }}</td>
                                        </tr>

                                    @endif

                                @endforeach

                            </tbody>

                        </table>
                    </div>
                    <div class="col-sm-5 col-sm-offset-1">
                        <div class="tab_list tab_list_unstyled">
                            {!! $dictionary['loan2_text'] or '<h3>Xarici bazarda ticarətin üstünlükləri</h3>' !!}
                        </div>
                    </div>
                </div>
                <div class="main_tab_panel">
                    <div class="col-xs-12">
                        <div id="chartdiv"></div>
                    </div>
                </div>
                <div class="main_tab_panel">
                    <div class="col-xs-12">
                        <div id="chartdiv1"></div>
                    </div>
                </div>
            </div>
        </div>


    </div>
</section>
<!-- MainTab End -->

@push('scripts')

    @include('web.elements.stockjs', ['id' => 'chart1', 'div' => 'chartdiv', 'title' => @$dictionary['loan3_description'], 'axesTitle' => @$dictionary['profitability'], 'data' => $chart1 ]);

    @include('web.elements.stockjs', ['id' => 'chart2', 'div' => 'chartdiv1', 'title' => @$dictionary['loan4_description'], 'axesTitle' => @$dictionary['profitability'], 'data' => $chart2 ]);

@endpush