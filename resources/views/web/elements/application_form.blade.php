<!-- Application Begin -->
<div class="application_form {{ isset($bg) ? $bg : '' }} {{ isset($class3) ? $class3 : '' }}">


    <div id="divForm">
        <h1>{{ $dictionary['apply'] or 'BİZƏ MÜRACİƏT ET'}}</h1>
        <p>{{ $dictionary['apply_description'] or 'Aşağıdakı formanı doldurmaqla bizə müraciət edə bilərsiniz' }}</p>

        {!! Form::open(['url'=>route('contact'), 'method'=>'POST', 'id' => 'form']) !!}

            <span class="span">
                {!! Form::text('full_name', null, ["class" => "alphaonly", "placeholder" => $dictionary['full_name'] ]) !!}
            </span>

            <span class="span">
                {!! Form::text('email', null, ["placeholder" => $dictionary['email'] ]) !!}
            </span>

            <span class="form__cell span">
                {!! Form::text('phone', null, ["id" => "phone", "placeholder" => $dictionary['phone'] ]) !!}
            </span>

            {!! Form::button(@$dictionary['apply_button'], ['id' => 'loadingButton', 'data-loading-text' => g_icon('refresh', null, 'spinning'), 'autocomplete' => 'off', 'type' => 'submit']) !!}

            {!! Form::hidden('page', $page->id) !!}

        {!! Form::close() !!}

        <p id="msgBox" style="color:#c23934; display:none"></p>

    </div>

    <h4 id="successMessage" style="display: none"></h4>

</div>
<!-- Application End -->


@push('scripts')
    @include('web.elements.form', ['phone' => true, 'withoutMsg' => true])
@endpush