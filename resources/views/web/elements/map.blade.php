<!-- MainMap Begin -->
<section class="main_map">
    <div class="container">
        <div class="row">
            <div class="col-md-4 col-sm-6">
                <!-- Application Begin -->
                <div class="application_form">

                    <div id="successBox">
                        <h1 id="successMessage"></h1>
                    </div>

                    <div id="divForm">
                        <h1>{{ $dictionary['contact_us'] }}</h1>
                        <p>{!! $dictionary['contact_us_description'] !!}</p>

                        {!! Form::open(['url'=>route('contact'), 'method'=>'POST', 'id' => 'ajax-form']) !!}

                        {!! Form::text('full_name', null, ["class" => "alphaonly", "placeholder" => $dictionary['full_name'] ]) !!}
                        {!! Form::text('email', null, ["placeholder" => $dictionary['email'] ]) !!}
                        <div class="form__cell">
                            {!! Form::text('phone', null, ["id" => "phone", "placeholder" => $dictionary['phone'] ]) !!}
                        </div>
                        {!! Form::button($dictionary['contact_us_button'], ['id' => 'loadingButton', 'data-loading-text' => g_icon('refresh', null, 'spinning'), 'autocomplete' => 'off', 'type' => 'submit']) !!}

                        {!! Form::hidden('phone-full', null, ['id' => 'hidden']) !!}

                        {!! Form::hidden('type', 4) !!}

                        {!! Form::close() !!}

                        <p>{!! $dictionary['contact_us_summary'] !!}</p>

                        <p id="msgBox" style="color:red"></p>
                    </div>

                </div>
            </div>
        </div>
    </div>
    <div id="mainmap"></div>
</section>
<!-- MainMap End -->