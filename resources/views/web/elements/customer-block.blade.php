
@if($page->details && ($page->details->customers > 0 || $page->details->transactions > 0))

    <section class="count_block">
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2 col-xs-12">
                    <div class="block pull-left col-xs-6 col-mob-12">
                        <span><strong>{{ $page->details->customers or 0 }}+</strong>{{ $dictionary['customer_count'] or 'Müştəri sayı' }}</span>
                    </div>
                    <div class="block pull-right col-xs-6 col-mob-12">
                        <span><strong>{{ $page->details->transactions or 0 }}+</strong>{{ $dictionary['transaction_count'] or 'Əqd sayı' }}</span>
                    </div>
                </div>
            </div>
        </div>
    </section>

@endif