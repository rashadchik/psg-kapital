@if($page->legalPhysical->count())

    <div class="pi_tab">
        <h1>{{ $dictionary['loyal_physical'] or 'Hüquqi və Fiziki şəxslər hesab açmaq üçün aşağıdakı sənədlər tələb olunur.' }}</h1>
        <nav class="pi_tab_nav clearfix">
            <a href="#" title="">{{ $dictionary['loyal_people'] or 'Hüquqi Şəxslər' }}</a>
            <a href="#" title="">{{ $dictionary['physical_people'] or 'Fiziki Şəxslər' }}</a>
        </nav>

        <div class="pi_tab_content">

            @foreach(config('config.legal-physical') as $key => $person)

                <div class="pi_tab_panel">
                    <table>

                        @foreach($page->legalPhysical as $data)

                            @if($data->type == $key)
                                <tr><td>{{ $data->title }}</td></tr>
                            @endif

                        @endforeach

                    </table>
                </div>

            @endforeach

        </div>

        @if($page->template_id == 5)
            @include('web.elements.banner', ['type' => 2])
        @endif

    </div>

@endif