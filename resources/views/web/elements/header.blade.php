<header id="header">

    @include('web.elements.top-line')

    <div class="container">
        <div class="row">
            <!-- HeaderInner -->
            <div class="header_inner clearfix">
                <!-- Logo -->
                <div class="col-sm-5 col-xs-5">
                    <div class="logo">
                        <a href="{{route('home')}}" title="{{ $webConfig->company_name }}">
                            <img src="{{ asset('images/logotype.png') }}" alt="{{ $webConfig->company_name }}">
                        </a>
                    </div>
                </div>
                <div class="col-sm-7 col-xs-7">
                    <div class="pull-right">
                        <!-- Lang -->
                        <div class="lang hidden-xs">

                            @foreach(config("app.locales") as $key => $locale)
                                @if($key != $lang)
                                    <a rel="alternate" hreflang="{{ $key }}" title="{{ $locale }}" href="@if(isset($menuWithoutSlug)) {{ url($key) }} @else {{ LaravelLocalization::getLocalizedURL($key, Menu::getRelationSlug($page->id, $key), [], true) }} @endif">
                                        {{ $locale }}
                                    </a>
                                @endif
                            @endforeach

                        </div>
                        <!-- SearchBtn -->
                        <button type="button" class="search_m_btn visible-xs"><i class="fa fa-search"></i></button>
                        <!-- ChatBtn -->
                        <div class="chat_btn" style="display: none">
                            <a href="#" title=""><span><i class="fa fa-commenting"></i>{{ $dictionary['online_chat'] or 'Online Çat' }}</span></a>
                        </div>
                        <!-- MenuBtn -->
                        <button type="button" class="menu_btn visible-xs"><i class="fa fa-bars"></i></button>
                        <!-- 9119 -->
                        <span class="call9119 hidden-xs"></span>
                    </div>
                </div>
            </div>
        </div>
        <div class="row hidden-xs">
            <!-- Menu -->
            <nav class="menu">
                <ul class="list-unstyled">

                    @foreach( $menu as $child )
                        @if($child->visible != 3 && $child->visible != 4)

                            @if($child->menuChildren->count())
                                @include('web.elements.menu-children', ['children' => $child->menuChildren, 'parent' => $child, 'arrow' => 'down'])
                            @else
                                <li @if(@$parentPage->id == $child->id) class="active" @endif><a href="{{ route('showPage', $child->slug) }}" title="{{ $child->name }}" target="{{ target($child->target) }}">{{ $child->name }}</a></li>
                            @endif

                        @endif
                    @endforeach

                </ul>
            </nav>
        </div>
    </div>

</header>