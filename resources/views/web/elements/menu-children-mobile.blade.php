<li class="{{ $class }}">
    <a href="javascript:void(0)" title="{{ $child->name }}">{{ $child->name }}</a>

    <ul class="list-unstyled">

        @if($class == 'sub')
        <li class="back"><a href="javascript:void(0)">{{ $dictionary['go_back'] }}</a></li>
        @endif

        @foreach($children as $child)

            @if($child->menuChildren->count())
                @include('web.elements.menu-children-mobile', ['children' => $child->menuChildren, 'parent' => $child, 'class' => 'drop'])
            @else
                <li><a href="{{ route('showPage', $child->slug) }}" title="{{ $child->name }}">{{ $child->name }}</a></li>
            @endif

        @endforeach

    </ul>
</li>