@if($page->cover)
    <div class="row">
        <div class="col-xs-12">
            <!-- TextCover Begin -->
            <div class="text_cover" style="background-image: url({{ asset('files/cache/images/'.$page->relation_page.'/'.$page->cover->image) }});">
                {!! $page->summary !!}
            </div>
            <!-- TextCover End -->
        </div>
    </div>
@endif
