@if($page->services->count())
    <section class="bgwhite padding-bottom-50">
        <div class="container">
            <div class="row">
                <div class="col-md-10 col-md-offset-1">
                    <div class="row">
                        <!-- PIList Begin -->
                        <div class="pi_list">
                            <div class="col-xs-12">
                                <h1>{{ $text }}</h1>
                            </div>

                            @foreach($page->services as $services)
                                <div class="@if($loop->count % 2 == 1 && $loop->last) col-sm-12 @else col-sm-6 @endif col-xs-12">
                                    <article>
                                        <p>{{ $services->title }}</p>
                                    </article>
                                </div>
                            @endforeach
                        </div>
                        <!-- PIList End -->
                    </div>
                </div>
            </div>
        </div>
    </section>
@endif