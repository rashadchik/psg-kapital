<!-- PartnersCarousel Begin -->
<section class="partners_carousel">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div id="partners_carousel" class="owl-carousel">
                    @foreach($partners as $partner)
                        <div class="item">
                            <figure>
                                <img src="{{ asset("files/cache/images/$partner->id/$partner->image_original") }}" alt="{{ $partner->title }}">
                            </figure>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</section>
<!-- PartnersCarousel End -->