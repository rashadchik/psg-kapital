<!-- Search Begin -->
<section class="search_block">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">

                {!! Form::open(['url'=>route('showPage', 'search'), 'method'=>'GET']) !!}

                    {{ Form::text('keyword', null, ['placeholder' => $dictionary['search'] ]) }}

                {!! Form::close() !!}

            </div>
        </div>
    </div>
</section>
<!-- Search End -->