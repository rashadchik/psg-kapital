<script type="text/javascript">
    $(function(){

        $("#contact_form").validate({
            rules: {
                first_name: {
                    required: true,
                    lettersonly: true,
                    minlength: 3,
                    maxlength: 15
                },
                last_name: {
                    required: true,
                    lettersonly: true,
                    minlength: 3,
                    maxlength: 15
                },
                phone: {
                    required: true,
                    minlength:9,
                    maxlength:15
                },
                email: {
                    required: true,
                    email: true
                },

            },
            messages: {
                first_name: {
                    required: '{{ $dictionary['error_first_name_required'] or trans('validation.custom.first_name.required') }}',
                    lettersonly: '{{ $dictionary['error_first_name_length'] or trans('validation.custom.first_name.min') }}',
                    minlength: '{{ $dictionary['error_first_name_length'] or trans('validation.custom.first_name.min') }}',
                    maxlength: '{{ $dictionary['error_first_name_length'] or trans('validation.custom.first_name.min') }}',
                },
                last_name: {
                    required: '{{ $dictionary['error_last_name_required'] or trans('validation.custom.last_name.required') }}',
                    lettersonly: '{{ $dictionary['error_last_name_length'] or trans('validation.custom.last_name.min') }}',
                    minlength: '{{ $dictionary['error_last_name_length'] or trans('validation.custom.last_name.min') }}',
                    maxlength: '{{ $dictionary['error_last_name_length'] or trans('validation.custom.last_name.min') }}',
                },
                phone: {
                    required: '{{ $dictionary['error_phone_required'] or trans('validation.custom.phone.required') }}',
                    minlength: '{{ $dictionary['error_phone_validate'] or trans('validation.custom.minlength.digits_between') }}',
                },
                email: {
                    required: '{{ $dictionary['error_email_required'] or trans('validation.custom.email.required') }}',
                    email: '{{ $dictionary['error_email_validate'] or trans('validation.custom.email.email') }}',
                },

            },
            success: function(label) {
                label.html('').removeClass('err').addClass('ok');
            },
            highlight: function ( element, errorClass, validClass ) {
                $(element).addClass( "err" ).removeClass( "valid-item" );
            },
            unhighlight: function (element, errorClass, validClass) {
                $(element).addClass( "valid-item" ).removeClass( "err" );
            },
            submitHandler: function (form) {
                popupForm($(form));

                return false;
            }
        });

        jQuery.validator.addMethod("lettersonly", function(value, element) {
            return this.optional(element) || /^[a-z\s]+$/i.test(value);
        }, "Only alphabetical characters");

    });

</script>