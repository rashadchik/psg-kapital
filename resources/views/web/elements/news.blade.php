<!-- MainNews Begin -->

<section class="main_news">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 title_block">
                <h1>{{ $newsPage->description or $newsPage->name }}</h1>
                <p>{!! $newsPage->summary !!}</p>
            </div>
        </div>

        @if($newsPage->articles->count())
            <div class="row">
                <!-- NewsList -->
                <div class="news_list">

                    @foreach($newsPage->articles as $articles)
                        <div class="col-md-3 col-xs-6 col-mob-12">
                            <article>
                                <a href="{{ route('showArticle', [$newsPage->slug, $articles->slug]) }}" title="{{ $articles->title }}">
                                    <figure>
                                        <img src="{{ asset("files/cache/images/$articles->relation_page/thumb/$articles->image") }}" alt="{{ $articles->title }}">
                                    </figure>
                                    <div class="body">
                                        <h2>{{ $articles->title }}</h2>
                                        <span class="date">{{ blogDate($articles->published_at) }}</span>
                                    </div>
                                </a>
                            </article>
                        </div>
                    @endforeach

                </div>
            </div>
        @endif

    </div>
</section>

<!-- MainNews End -->`