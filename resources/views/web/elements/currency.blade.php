<!-- Valyuta Begin -->
<section class="valyuta">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div id="valyuta" class="owl-carousel">

                    @foreach($currencies as $key => $currency)
                    <div class="item">
                        <span>{{ $currency }}</span>
                        <span class="@if(isset($exchangeHistoricals[$key]) && $exchanges[$key] > $exchangeHistoricals[$key]) up @elseif(isset($exchangeHistoricals[$key]) && $exchanges[$key] < $exchangeHistoricals[$key]) down @endif">{{ $exchanges[$key] }}</span>
                    </div>
                    @endforeach

                </div>
            </div>
        </div>
    </div>
</section>
<!-- Valyuta End -->