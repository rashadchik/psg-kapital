<div id="testimonial" class="owl-carousel">

    @foreach($feedback as $feed)

        <article>
            <div class="col-md-4 col-xs-12">
                <figure>
                    <img src="{{ asset("files/cache/images/$feed->id/$feed->image_original") }}" alt="{{ $feed->full_name }}">
                </figure>
            </div>
            <div class="col-md-8 col-xs-12">
                <div class="text">
                    <p>{{ $feed->summary }}</p>
                </div>
                <span class="name">{{ $feed->full_name }}</span>
            </div>
        </article>

    @endforeach

</div>