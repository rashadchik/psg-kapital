@foreach($news as $article)

    <div class="col-md-3 col-xs-6 col-mob-12">
        <article>
            <a href="{{ route('showArticle', [$page->slug, $article->slug]) }}">
                <figure>
                    <img src="{!! asset("files/cache/images/$article->relation_page/thumb/$article->image") !!}" alt="{{ $article->title }}">
                </figure>
                <div class="body">
                    <h2>{{ $article->title }}</h2>
                    <span class="date">{{ filterWebDate($article->published_at) }}</span>
                </div>
            </a>
        </article>
    </div>

@endforeach