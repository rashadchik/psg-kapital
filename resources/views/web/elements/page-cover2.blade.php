<!-- Cover -->
@if($page->cover)
    <div class="cover" style="background-image: url({{ asset('files/cache/images/'.$page->relation_page.'/'.$page->cover->image) }})"></div>
@endif