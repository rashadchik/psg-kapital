<!-- Popup Begin -->
<section class="popup">
    <div class="popup_inner">
        <span class="closed"></span>
        <!-- Form -->

        <div class="form">
            <h2>{{ $dictionary['popup_title'] or 'SİZƏ ZƏNG EDƏK' }}</h2>
            <h3>{{ $dictionary['popup_description'] or 'Suallarınız cavabsız qalmasın' }}</h3>


            {!! Form::open(['url'=>route('contact'), 'method'=>'POST', 'id' => 'contact_form']) !!}

            {!! Form::text('first_name', null, ["class" => "alphaonly", "placeholder" => $dictionary['first_name'] ?: 'Adınız' ]) !!}

            {!! Form::text('last_name', null, ["class" => "alphaonly", "placeholder" => $dictionary['last_name'] ?: 'Soyadınız' ]) !!}

            {!! Form::text('email', null, ["placeholder" => $dictionary['email'] ]) !!}

            <span class="form__cell">
                {!! Form::text('phone', null, ["id" => "phone", "placeholder" => $dictionary['phone'] ]) !!}
            </span>

            {!! Form::button($dictionary['send'], ['id' => 'button', 'data-loading-text' => g_icon('refresh', null, 'spinning'), 'autocomplete' => 'off', 'type' => 'submit']) !!}

            {!! Form::hidden('popup', true) !!}

            {!! Form::close() !!}

        </div>
        <!-- FormSuccess -->
        <div class="form_success" style="display: none;" id="success_box">
            <i></i>
            <div id="success_msg"></div>
        </div>

        <p id="error_msg" style="color:#c23934; display:none"></p>

    </div>
</section>
<!-- Popup End -->

@push('scripts')
    @include('web.elements.form2')
@endpush