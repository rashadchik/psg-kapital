@if($page->banners->count())

    <!-- Banner Begin -->

    @foreach($page->banners as $banners)

        @if($banners->pos_type == $type)

            <div class="banner @if(isset($class)) {{ $class }} @endif">

                @if(is_null($banners->document))

                    @if($banners->popup == 1)
                        <a href="javascript:void(0)" class="open_popup">
                            <img src="{{ asset($banners->image_original) }}" alt="{{ $page->name }}">
                        </a>
                    @else
                        <img src="{{ asset($banners->image_original) }}" alt="{{ $page->name }}">
                    @endif
                @else
                    <a href="{{ asset($banners->document) }}" target="_blank">
                        <img src="{{ asset($banners->image_original) }}" alt="{{ $page->name }}">
                    </a>
                @endif

            </div>

        @endif

    @endforeach

    <!-- Banner End -->

@endif