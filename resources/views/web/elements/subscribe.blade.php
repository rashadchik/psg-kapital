<h2>
    {!! $dictionary['subscribe_text'] !!}
    <p>{{$dictionary['spam']}}</p>
</h2>
<div class="main-subscripe-form">
    <div class="row" style="margin-top: 42px;">

        {!! Form::open(['url'=>route('subscribe'), 'method'=>'POST', 'id' => 'subscribe-form']) !!}

        <div class="col-md-9">
            {!! Form::text('email', null, ["class" => "form-control sbsinp",  "placeholder" => $dictionary['your_email'] ]) !!}
        </div>

        <div class="col-md-3">
            {!! Form::button($dictionary['subscribe'], ['id' => 'loadingSubButton', 'data-loading-text' => g_icon('refresh', null, 'spinning'), 'autocomplete' => 'off','type' => 'submit']) !!}
        </div>

        {!! Form::close() !!}

    </div>
</div>
<div id="subscribeBox"></div>