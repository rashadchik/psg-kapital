<!-- TopBar Begin -->

<div class="topbar">
    <div class="container">
        <div class="row">
            <div class="col-sm-4 hidden-xs">
                <!-- Social -->
                <nav class="social">
                    @foreach($social as $link)
                        <a href="{{ $link->forward_url }}" target="_blank" class="{{ $link->type }}"><i class="fa fa-{{ $link->type }}"></i></a>
                    @endforeach
                </nav>
            </div>
            <div class="col-sm-8 col-xs-12">
                <div class="pull-right">
                    <!-- SearchBtn -->
                    <button type="button" class="search_btn hidden-xs"></button>
                    <span class="brd hidden-xs"></span>
                    <div class="header_valyuta">
                        <span class="txt">{{ $dictionary['psg_exchange'] or 'PSG Kapital məzənnələri' }}</span>

                        @if($currency->count() > 0)
                            <div class="cur_col c1 animate">

                                @foreach($currency as $value)
                                    <p @if($loop->first) class="active" @endif><strong>{{ $value->currency }}</strong></p>
                                @endforeach

                            </div>
                            <div class="cur_col c2">

                                @foreach($currency as $price)
                                    <p @if($loop->first) class="active" @endif><span>{{ $price->buying }} <em>•</em> {{ $price->sales }}</span></p>
                                @endforeach

                            </div>
                        @endif

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- TopBar End -->