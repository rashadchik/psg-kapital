<!-- Footer Begin -->

<footer id="footer">
    <div class="footer_inner">
        <div class="container">
            <div class="row">
                <div class="col-md-3 col-xs-12">
                    <!-- FooterLogo -->
                    <div class="footer_logo">
                        <a href="{{route('home')}}" title="{{ $webConfig->company_name }}">
                            <img src="{{ asset('images/logo_white.png') }}" alt="{{ $webConfig->company_name }}">
                        </a>
                    </div>
                    <!-- FooterAbout -->
                    <div class="footer_about">
                        <p>{{ $dictionary['about_us'] }}</p>
                    </div>
                    <!-- Social -->
                    <nav class="social clearfix">

                        @foreach($social as $link)
                            <a href="{{ $link->forward_url }}" target="_blank" class="{{ $link->type }}"><i class="fa fa-{{ $link->type }}"></i></a>
                        @endforeach

                    </nav>
                    <!-- FooterLink -->
                    <div class="footer_link">

                        @foreach( $menu as $catLeft )
                            @if($catLeft->visible == 4)
                                <a href="{{ route('showPage', $catLeft->slug) }}" title="{{ $catLeft->name }}">{{ $catLeft->name }}</a>
                            @endif
                        @endforeach

                    </div>
                </div>
                <div class="col-md-1 hidden-xs hidden-sm"></div>

                @foreach( $menu as $child )
                    @if($child->visible != 2 && $child->visible != 4)

                        <div class="col-md-2 col-sm-6 col-xs-6 hidden-mob">

                            <nav class="footer_menu">
                                <h3>{{ $child->name }}</h3>

                                @if($child->menuChildren->count())
                                    <ul class="list-unstyled">

                                        @foreach($child->menuChildren as $child)
                                            <li><a href="{{ route('showPage', $child->slug) }}" title="{{ $child->name }}">{{ $child->name }}</a></li>
                                        @endforeach

                                    </ul>
                                @endif

                            </nav>

                        </div>

                    @endif
                @endforeach

            </div>
        </div>
    </div>
    <div class="footer_bottom">
        <div class="container">
            <div class="row">
                <p class="pull-left">© {{ date('Y') }} {{ $webConfig->company_name }} | {{ $dictionary['copyright'] }}</p>

                <p class="pull-right">{!! trans('locale.site_by', ['site' => '<a href="https://marcom.az" target="_blank">Marcom</a>']) !!}</p>
            </div>
        </div>
    </div>


</footer>

<button type="button" class="btn_online_chat open_popup" style="display: none">Onlayn çat</button>
<!-- Footer End -->