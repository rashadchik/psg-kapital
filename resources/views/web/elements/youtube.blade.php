<div class="video-summary" style="background-image: url({!! asset("files/cache/images/$webConfig->main_video_cover") !!})">
    <div class="video-summary__wrapper container">
        <div class="video-summary__wrapper2">
            <a class="video-summary__link" href="https://www.youtube.com/watch?v={{$webConfig->main_video}}" rel="prettyPhoto" title="{{$webConfig->company_name}}">
                <span class="sprite sprite--video-play video-summary__sprite hidden--xs hidden--sm"></span>
                <span class="clearfix"></span>
                <div class="video-summary__heading">
                    {!! $dictionary['video_title'] !!}
                </div>
                <span class="clearfix"></span>
                <span class="video-summary__desc">{{ $dictionary['video_description'] }}</span>
                <div>
                    <span class="sprite sprite--video-play video-summary__sprite hidden--md hidden--lg"></span>
                </div>
            </a>
        </div>
    </div>
</div>