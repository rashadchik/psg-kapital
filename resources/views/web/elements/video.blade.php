<!-- MainVideo Begin -->
<section class="main_rolik">
    <div class="container">
        <div class="row">
            <div class="col-sm-10 col-sm-offset-1 col-xs-12 text-right">
                <a class="popup-youtube" href="https://www.youtube.com/watch?v={{ $webConfig->main_video }}" title=""><i class=""></i></a>
                <h2>{!! $dictionary['video_title'] !!}</h2>
                <p>{{ $dictionary['video_summary'] }}</p>
            </div>
        </div>
    </div>
</section>
<!-- MainVideo End -->