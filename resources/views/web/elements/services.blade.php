<!-- MainServices Begin -->
<section class="main_services">
    <div class="container">
        @if($servicePage->count() > 0)

            <div class="row">
                <div class="col-xs-12 title_block">
                    <h1>{{ $dictionary['our_service'] or 'XİDMƏTLƏRİMİZ' }}</h1>
                    <p>{!! $dictionary['our_service_text'] or ''!!}</p>
                </div>
            </div>

            <div class="row">
                <div id="services_carousel" class="owl-carousel">
                    @foreach($servicePage as $child)

                        <div class="item">
                            <article class="col-xs-12">
                                <a href="{{ route('showPage', $child->slug) }}" title="{{ $child->name }}">
                                    <figure style="background-image: url({{ asset('images/2d_icon/'.config('config.template-icon.'.$child->template_id).'_'.$lang.'.png?v=1') }});"></figure>
                                </a>
                            </article>
                        </div>

                    @endforeach
                </div>
            </div>

        @endif

    </div>
</section>
<!-- MainServices End -->