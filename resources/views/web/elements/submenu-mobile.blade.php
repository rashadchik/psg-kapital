@if($parentPage->children->count())
<!-- SubMenumobile -->
<nav class="col-xs-12 visible-xs visible-sm sub_menu_mobile">

    @foreach($parentPage->children as $subMenu)
        <a href="{{ route('showPage', $subMenu->slug) }}" title="{{ $subMenu->name }}" @if($subMenu->id == $page->id) class="active" @endif>{{ $subMenu->name }}</a>
    @endforeach

</nav>
@endif