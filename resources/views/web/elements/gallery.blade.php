@foreach($gallery->galleries as $image)

    <div class="col-md-3 col-sm-3 col-xs-4 col-mob-6">
        <figure>
            <a href="{{asset("files/cache/images/$gallery->relation_page/$image->filename")}}" title="{{ $gallery_title }}" class="zoom"><img src="{{asset("files/cache/images/$gallery->relation_page/thumb/$image->filename")}}" alt="{{ $gallery_title }}"></a>
        </figure>
    </div>


@endforeach
