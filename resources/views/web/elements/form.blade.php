<script type="text/javascript">
    $(function(){

        $("#form").validate({
            rules: {
                full_name: {
                    required: true,
                    lettersonly: true,
                    minlength: 3,
                    maxlength: 40
                },
                @if(isset($phone))
                phone: {
                    required: true,
                    minlength:9,
                    maxlength:15
                },
                @endif

                email: {
                    required: true,
                    email: true
                },
                subject: {
                    required: true,
                    minlength: 3,
                    maxlength: 60
                },
                text: {
                    required: true,
                    minlength: 30,
                    maxlength: 100
                },

                @if(isset($terms))
                terms: {
                    required: true,
                },
                @endif
            },
            messages: {
                full_name: {
                    required: '{{ $dictionary['error_full_name_required'] or trans('validation.custom.full_name.required') }}',
                    lettersonly: '{{ $dictionary['error_full_name_length'] or trans('validation.custom.full_name.min') }}',
                    minlength: '{{ $dictionary['error_full_name_length'] or trans('validation.custom.full_name.min') }}',
                    maxlength: '{{ $dictionary['error_full_name_length'] or trans('validation.custom.full_name.min') }}',
                },
                email: {
                    required: '{{ $dictionary['error_email_required'] or trans('validation.custom.email.required') }}',
                    email: '{{ $dictionary['error_email_validate'] or trans('validation.custom.email.email') }}',
                },
                subject: {
                    required: '{{ $dictionary['error_subject_required'] or trans('validation.custom.subject.required') }}',
                    minlength: '{{ trans('validation.custom.text.min', ['min' => 3]) }}',
                    maxlength: '{{ trans('validation.custom.text.max', ['max' => 60]) }}',
                },
                text: {
                    required: '{{ $dictionary['error_text_required'] or trans('validation.custom.text.required') }}',
                    minlength: '{{ trans('validation.custom.text.min', ['min' => 30]) }}',
                    maxlength: '{{ trans('validation.custom.text.max', ['max' => 1000]) }}',
                }
            },
            success: function(label) {
                label.html('').removeClass('error').addClass('ok');
            },
            errorPlacement: function(error, element) {
                @if(!isset($withoutMsg))
                    $(element).parents('.span').append(error);
                @endif
            },
            highlight: function ( element, errorClass, validClass ) {
                $(element).parents(".span").addClass( "error-item" ).removeClass( "valid-item" );
            },
            unhighlight: function (element, errorClass, validClass) {
                $(element).parents(".span").addClass( "valid-item" ).removeClass( "error-item" );
            },
            submitHandler: function (form) {
                ajaxForm($(form));

                return false;
            }
        });

        jQuery.validator.addMethod("lettersonly", function(value, element) {
            return this.optional(element) || /^[a-z\s]+$/i.test(value);
        }, "Only alphabetical characters");

    });

</script>