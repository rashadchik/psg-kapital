<!-- MobileMenu Begin -->

<nav class="mobile_menu">
    <ul class="list-unstyled">

        @foreach( $menu as $child )
            @if($child->visible != 3 && $child->visible != 4)

                @if($child->menuChildren->count())

                    @include('web.elements.menu-children-mobile', ['children' => $child->menuChildren, 'parent' => $child, 'class' => 'sub'])

                @else
                    <li><a href="{{ route('showPage', $child->slug) }}" title="{{ $child->name }}">{{ $child->name }}</a></li>
                @endif

            @endif
        @endforeach

    </ul>

    <div class="bottom">
        <div class="m_lang">

            @foreach(config("app.locales") as $key => $locale)
                @if($key != $lang)
                    <a rel="alternate" hreflang="{{ $key }}" title="{{ $locale }}" href="@if(isset($menuWithoutSlug)) {{ url($key) }} @else {{ LaravelLocalization::getLocalizedURL($key, Menu::getRelationSlug($page->id, $key), [], true) }} @endif">
                        {{ $locale }}
                    </a>
                @endif
            @endforeach

        </div>
    </div>
</nav>