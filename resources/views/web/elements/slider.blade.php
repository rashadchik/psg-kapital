<!-- Slider Begin -->
<section class="slider">
    <div id="slider" class="carousel slide" data-ride="carousel">
        <div class="carousel-inner" role="listbox">

            @foreach($slider as $slides)

            <div class="item @if($loop->first) active @endif">
                <img src="{{ asset("files/cache/images/$slides->id/$slides->image_original") }}" alt="{{ $slides->title }}">
                <div class="body">
                    <div class="container">
                        <div class="col-md-8">
                            <div class="caption">
                                <h4>{{ $slides->title }}</h4>
                                <h1>{{ str_limit($slides->summary, 100) }}</h1>

                                @if(!is_null($slides->link))
                                    <a href="{{ url($slides->link) }}" title="{{ $slides->title }}">{{ $dictionary['read_more'] }}</a>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            @endforeach

        </div>
        <div class="slider_nav">
            <div class="container">
                <ul class="list-unstyled">
                    @foreach($slider as $slides)
                        <li data-target="#slider" data-slide-to="{{ $loop->iteration-1 }}" @if($loop->first) class="active" @endif style="cursor: pointer">
                            <h2>{{ $slides->title }}</h2>
                            <p>{{ str_limit($slides->summary, 100) }}</p>
                        </li>
                    @endforeach
                </ul>
            </div>
        </div>
    </div>

    <div class="slider_application">
        @include('web.elements.application_form')
    </div>
</section>

<!-- Slider End -->