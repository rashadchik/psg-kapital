<section class="container">

    @include('web.elements.page-cover')

    <div class="row">
        <div class="@if(isset($class1)) {{ $class1 }} @else col-md-8 @endif">
            <!-- PIDetail -->
            <div class="pi_detail">
                <div class="row">

                    @if(!is_null($page->image_original))
                        <div class="@if(isset($class4)) {{ $class4 }} @else col-md-4 col-xs-3 @endif col-mob-12 text-center">
                            <img src="{{ asset("files/cache/images/$page->id/$page->image_original") }}" alt="{{ $page->name }}">
                        </div>
                    @else
                        <div class="col-md-12">
                            <div class="text">
                                {!! $page->content !!}
                            </div>
                        </div>
                    @endif

                    @if(!is_null($page->image_original))
                        <div class="col-xs-12 col-mob-12" style="float: none">
                            <div class="text">
                                {!! $page->content !!}
                            </div>
                        </div>
                    @endif

                </div>
            </div>
        </div>

        @if(!isset($withoutRightSide))

        <div class="@if(isset($class2)) {{ $class2 }} @else col-md-4 @endif">

            @if(isset($form))
                <!-- Application Begin -->
                @include('web.elements.application_form')
                <!-- Application End -->
            @else

                <aside class="inside_sidebar">
                    <h3>{{ $parentPage->name }}</h3>

                    @if($parentPage->children->count())
                        <nav class="side_menu">
                            @foreach($parentPage->children as $children)
                                <a href="{{ route('showPage', $children->slug) }}" title="{{ $children->name }}" @if($children->id == $page->id) class="active" @endif>{{ $children->name }}</a>
                            @endforeach
                        </nav>
                    @endif

                </aside>

                @if(isset($banner))
                    @include('web.elements.banner', ['type' => 1, 'class' => 'margin-top-30 text-center'])
                @endif

            @endif

        </div>

        @endif

    </div>

</section>
