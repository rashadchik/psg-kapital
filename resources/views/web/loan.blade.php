@extends ('layouts.web', ['page_heading' => $page->name, 'page_image' => !is_null($page->image_original) ? asset("files/cache/images/$page->id/$page->image_original") : ""])

@section ('content')

    @include('web.elements.breadcrumb')

    @include('web.elements.content', ['class1' => 'col-md-9 col-xs-12', 'class2' => 'col-md-3 col-xs-12', 'banner' => true])

    <section class="bgwhite">
        <div class="container">
            <div class="row padding-bottom-30">
                <div class="col-md-8 col-xs-12">

                    @if($page->children->count())

                    <div class="istiqraz_list">

                        @foreach($page->children as $child)

                            @if(!$loop->last)
                                <div class="row">
                                    <div class="col-md-4 col-sm-4 col-xs-4 col-mob-12">
                                        <img src="{{ asset("files/cache/images/$child->id/$child->image_original") }}" alt="{{ $child->name }}">
                                    </div>
                                    <div class="col-md-7 col-sm-8 col-xs-8 col-mob-12">
                                        <h2>{{ $child->name }}</h2>
                                        <p>{!! $child->summary !!}</p>
                                    </div>
                                </div>
                            @endif

                        @endforeach
                    </div>

                    @endif

                    @foreach($page->children as $child)

                        @if($loop->last)
                            <div class="pi_text_block">
                                <h2 class="title">{{ $child->name }} </h2>
                                <div class="@if(!is_null($child->image_original)) col-sm-9 col-xs-8 @else col-sm-12 @endif col-mob-12">
                                    <div class="row">
                                        <div class="text text-justify">
                                            <p>{!! $child->summary !!}</p>
                                        </div>
                                    </div>
                                </div>

                                @if(!is_null($child->image_original))

                                <div class="col-sm-3 col-xs-4 col-mob-12 text-center">
                                    <img src="{{ asset("files/cache/images/$child->id/$child->image_original") }}" alt="{{ $child->name }}">
                                </div>

                                @endif
                            </div>
                        @endif

                    @endforeach


                </div>

                <div class="col-md-4 col-xs-12">
                    @include('web.elements.application_form', ['bg' => 'bggrey'])


                    @if($rekvisit)
                        <!-- IstiqrazTable Begin -->
                            <table class="istiqraz_table">
                                <thead>
                                <tr>
                                    <th>{{ $dictionary['loan'] or "İstiqraz" }}</th>
                                    <th>{{ $dictionary['loan_bakcell'] or "Bakcell MMC" }}</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td>{{ $dictionary['loan_registration'] or "Qeydiyyat nömrəsi" }}</td>
                                    <td>{{ $rekvisit->registration_number }}</td>
                                </tr>
                                <tr>
                                    <td>{{ $dictionary['loan_nominal'] or "Nominal dəyər" }} </td>
                                    <td>{{ $rekvisit->nominal_value }}</td>
                                </tr>
                                <tr>
                                    <td>{{ $dictionary['loan_coupon'] or "Kupon" }} </td>
                                    <td>{{ $rekvisit->coupon }}%</td>
                                </tr>
                                <tr>
                                    <td>{{ $dictionary['loan_payment'] or "Coupon Ödənişləri" }}</td>
                                    <td>{{ $rekvisit->coupon_payment }} </td>
                                </tr>
                                <tr>
                                    <td>{{ $dictionary['loan_time'] or "Müddət" }}</td>
                                    <td>{{ $rekvisit->expiration_date }}</td>
                                </tr>
                                </tbody>
                            </table>
                            <!-- IstiqrazTable End -->
                    @endif

                </div>

            </div>

            <div class="row">
                <div class="col-xs-12">

                    @if($loans->count() > 0)
                        @foreach(config('config.loan-type') as $key => $loan)

                            <div class="data_table">
                                <h1>{{ $dictionary["loan_type_$key"] or $loan }}</h1>

                                <table>
                                    <thead>
                                    <tr>
                                        <th>{{ $dictionary['loan_name'] or "İSTİQRAZIN ADI" }}</th>
                                        <th>{{ $dictionary['loan_type'] or "NÖVÜ" }}</th>
                                        <th>{{ $dictionary['loan_price'] or "QİYMƏTİ" }}</th>
                                        <th>{{ $dictionary['loan_income'] or "İLLİK GƏLİR" }}</th>
                                        <th>{{ $dictionary['loan_coupon'] or "KUPON" }}</th>
                                        <th>{{ $dictionary['loan_exchange'] or "SATILDIĞI BİRJA" }}</th>
                                        <th>{{ $dictionary['loan_country'] or "ÖLKƏ" }}</th>
                                    </tr>
                                    </thead>
                                    <tbody>

                                    @foreach($loans as $loan)

                                    @if($loan->loans == $key)
                                    <tr>
                                        <td data-title="{{ $dictionary['loan_name'] or "İSTİQRAZIN ADI" }}">{{ $loan->name }}</td>
                                        <td data-title="{{ $dictionary['loan_type'] or "NÖVÜ" }}">{{ $loan->type }}</td>
                                        <td data-title="{{ $dictionary['loan_price'] or "QİYMƏTİ" }}">{{ $loan->price }} USD</td>
                                        <td data-title="{{ $dictionary['loan_income'] or "İLLİK GƏLİR" }}">{{ $loan->introduction1 }}%</td>
                                        <td data-title="{{ $dictionary['loan_coupon'] or "KUPON" }}">{{ $loan->coupon }}%</td>
                                        <td data-title="{{ $dictionary['loan_exchange'] or "SATILDIĞI BİRJA" }}">{{ $loan->birja }}</td>
                                        <td data-title="{{ $dictionary['loan_country'] or "ÖLKƏ" }}">{{ $loan->title }}</td>
                                    </tr>
                                    @endif
                                    @endforeach

                                    </tbody>
                                </table>

                            </div>

                        @endforeach
                    @endif

                    <p class="p_qeyd">* {{ $dictionary['loan_note'] or "Göstərilən cədvəllər ayda bir dəfə yenilənir" }}</p>
                </div>
            </div>

        </div>
    </section>

@endsection
