@extends ('layouts.web', ['page_heading' => $page->name, 'page_image' => !is_null($page->image_original) ? asset("files/cache/images/$page->id/$page->image_original") : ""])

@section ('content')

    @include('web.elements.breadcrumb')

    @include('web.elements.content', ['form' => true])

    @include('web.elements.page-service', ['text' => isset($dictionary['portfolio_service']) ? $dictionary['portfolio_service'] : 'Xidmət növü'])

    <section class="bgwhite padding-bottom-50">
        <div class="container">
            <div class="row">
                <div class="col-md-10 col-md-offset-1">
                    @include('web.elements.broker-table')

                    <!-- Banner Begin -->
                    <div class="col-xs-12 call_banner">
                        @include('web.elements.banner', ['type' => 2])
                    </div>
                    <!-- Banner End -->
                </div>

            </div>
        </div>
    </section>

@endsection
