@extends ('layouts.web', ['page_heading' => $page->name, 'page_image' => !is_null($page->image_original) ? asset("files/cache/images/$page->id/$page->image_original") : ""])

@section ('content')

    @include('web.elements.breadcrumb')

    @include('web.elements.content', ['form' => true])

    @include('web.elements.page-service', ['text' => isset($dictionary['broker_service']) ? $dictionary['broker_service'] : 'PSG Kapital Broker xidməti üzrə aşağıdakıları təklif edir'])

    @include('web.elements.customer-block')

    <section class="bgwhite padding-40">
        <div class="container">
            <div class="row">
                <div class="col-md-8">
                    @include('web.elements.broker-table')

                    @include('web.elements.legal-physical')
                </div>

                <div class="col-md-4">
                    @include('web.elements.banner', ['type' => 1, 'class' => 'margin-bottom-30'])
                </div>

            </div>
        </div>
    </section>

@endsection
