@extends ('layouts.web', ['page_heading' => $page->name, 'page_image' => !is_null($page->image_original) ? asset("files/cache/images/$page->id/$page->image_original") : ""])

@section ('content')

    @include('web.elements.breadcrumb')

    @include('web.elements.content', ['class1' => 'col-md-9', 'class2' => 'col-md-3'])

    @if($page->children->count())

    <section class="container">
        <div class="row">
            <div class="col-xs-12">
                <table class="pi_table">
                    <thead>
                    <tr>
                        <td>{{ $dictionary['risk_profile'] or "Indicative / Investor Profile" }}</td>
                        <td>{{ $dictionary['risk_description'] or "Təsvir" }}</td>
                    </tr>
                    </thead>
                    <tbody>

                    @foreach($page->children as $child)
                        <tr>
                            <td data-title="{{ $dictionary['risk_profile'] or "Indicative / Investor Profile" }}"><strong>{{ $child->name }}</strong></td>
                            <td data-title="{{ $dictionary['risk_description'] or "Təsvir" }}">{!! $child->summary !!}</td>
                        </tr>
                    @endforeach

                    </tbody>
                </table>
            </div>
        </div>
    </section>
    @else
        <h3 class="text-center">İnvestor profili cədvəlini yaratmaq üçün {{ $page->name }} səhifəsi daxilində bloklar yaradın.</h3>
    @endif


    @if($exam->count() > 0)

    <!-- Exam Begin -->
    <section class="exam">

        {!! Form::open(['url'=>route('exam'), 'method'=>'POST', 'id' => 'exam_form']) !!}
        <div class="container">
            <div class="row">
                <div class="col-xs-12">

                    @foreach($exam as $question)
                        <div class="block">
                            <h4>{{ $loop->iteration }}) {{ $question->title }}</h4>

                            @if(!is_null($question->image_original))
                                <div class="photo">
                                    <img src="{{ asset("files/cache/images/$question->id/$question->image_original") }}" alt="{{ $question->title }}">
                                </div>
                            @endif

                            <div class="options">

                                @foreach($question->answer as $answer)

                                <span>
                                    {!! Form::radio("answer[$question->id]", $answer->id, false, ['id' => 'answer'.$answer->id, 'class' => 'css-checkbox']) !!}
                                    <label for="answer{{ $answer->id }}" class="css-label">{{ config("config.test.$loop->iteration") }}) {{ $answer->title }}</label>
                                </span>

                                @endforeach

                            </div>
                        </div>
                    @endforeach

                </div>
            </div>
        </div>

        <!-- ExamResult Begin -->
        <section class="exam_result">
            <div class="container">
                <div class="row">
                    <div class="col-md-6 col-md-offset-3" id="buttonBox">
                        {!! Form::button(@$dictionary['complete_test'] ?: 'Tamamla', ['id' => 'result', 'data-loading-text' => g_icon('refresh', null, 'spinning'), 'autocomplete' => 'off', 'type' => 'submit']) !!}
                    </div>

                    <div id="successBox" style="display:none">

                        <div class="col-md-8 col-xs-12">
                            {!! $dictionary['success_test_msg'] or '<h1><strong>Təbrik edirik.</strong> Siz testi uğurla keçdiniz.</h1>'!!}
                        </div>

                        <div class="col-md-4 col-xs-12">
                            <div class="point">
                                <h3>{{ $dictionary['test_point'] or 'Toplam xal' }} <strong id="successMsg"></strong></h3>
                            </div>
                        </div>

                    </div>

                </div>

                <p id="msgBox"></p>

            </div>
        </section>
        <!-- ExamResult End -->
        {!! Form::close() !!}

    {!! Form::open(['url'=>route('test'), 'method'=>'POST', 'id' => 'test_form']) !!}

    <!-- Answer Begin -->
        <div class="container" id="answerBox" style="display: none">
            <div class="answer">
                <div class="row">
                    <h2 class="col-xs-12">{{ $dictionary['customer_risk_profile'] or 'Müştəri Risk Profili Sorğu Vərəqəsi' }} </h2>
                </div>
                <div class="row">
                    <div class="col-md-3 col-xs-4 col-mob-12">
                        <label>{{ $dictionary['first_name'] }}</label>
                        {!! Form::text('first_name', null, ["class" => "alphaonly user", "placeholder" => $dictionary['first_name'] ?: 'Adınız' ]) !!}
                    </div>
                    <div class="col-md-3 col-xs-4 col-mob-12">
                        <label>{{ $dictionary['last_name'] }}</label>
                        {!! Form::text('last_name', null, ["class" => "alphaonly user", "placeholder" => $dictionary['first_name'] ?: 'Adınız' ]) !!}
                    </div>
                    <div class="col-md-3 col-xs-4 col-mob-12 form__cell">
                        <label>{{ $dictionary['phone'] }}</label>
                        {!! Form::text('phone', null, ["id" => "phone", "placeholder" => $dictionary['phone'] ]) !!}
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-3 col-sm-5 col-xs-7 col-mob-12">
                        <label>{{ $dictionary['birthday'] or 'Təvəllüd' }}</label>

                        {!! Form::select('day', array_combine(range(1,31), range(1,31)), 19, ['class' => 'day']) !!}

                        {!! Form::select('month', config("config.months.$lang"), 5, ['class' => 'month']) !!}

                        {!! Form::select('year', array_combine(range(date('Y') - 7, date('Y') - 100), range(date('Y') - 7, date('Y') - 100)), 1990, ['class' => 'year']) !!}

                    </div>
                    <div class="col-md-3 col-sm-7 col-xs-5 col-mob-12">
                        <label>{{ $dictionary['email'] }}</label>
                        {!! Form::text('email', null, ["placeholder" => $dictionary['email']]) !!}
                    </div>
                    <div class="col-md-3">
                        <label>&nbsp;</label>
                        {!! Form::button(@$dictionary['test_result_button'] ?: 'NƏTİCƏNİ ÖYRƏN', ['id' => 'button_test', 'data-loading-text' => g_icon('refresh', null, 'spinning'), 'autocomplete' => 'off', 'type' => 'submit']) !!}
                    </div>
                </div>

                <div class="row">
                    <h4 id="div_msg"></h4>
                </div>
            </div>
        </div>
        <!-- Answer End -->

        {!! Form::close() !!}

    </section>
    <!-- Exam End -->
    @endif

@endsection


@push('scripts')
    <script>

        $(function(){

            $("#test_form").validate({
                rules: {
                    first_name: {
                        required: true,
                        lettersonly: true,
                        minlength: 3,
                        maxlength: 15
                    },
                    last_name: {
                        required: true,
                        lettersonly: true,
                        minlength: 3,
                        maxlength: 15
                    },
                    phone: {
                        required: true,
                        minlength:9,
                        maxlength:15
                    },
                    email: {
                        required: true,
                        email: true
                    },

                },
                errorPlacement: function(error, element) {

                },
                success: function(label) {
                    label.html('').removeClass('err').addClass('ok');
                },
                highlight: function ( element, errorClass, validClass ) {
                    $(element).addClass( "err" ).removeClass( "valid-item" );
                },
                unhighlight: function (element, errorClass, validClass) {
                    $(element).addClass( "valid-item" ).removeClass( "err" );
                },
                submitHandler: function (form) {
                    testForm($(form));

                    return false;
                }
            });

            jQuery.validator.addMethod("lettersonly", function(value, element) {
                return this.optional(element) || /^[a-z\s]+$/i.test(value);
            }, "Only alphabetical characters");

        });




    </script>
@endpush
