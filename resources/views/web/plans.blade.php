@extends ('layouts.web', ['page_heading' => $page->name, 'page_image' => $page->cover ? asset('files/cache/images/'.$page->relation_page.'/'.$page->cover->image) : ""])

@section ('content')

    @include('web.elements.breadcrumb')

    <!-- InsidePage Begin -->
    <section class="inside_page">
        <div class="container">
            <div class="row">
                <div class="col-md-3 col-sm-4 col-xs-12">
                    @include('web.elements.sideBar')
                </div>
                <div class="col-md-9 col-sm-8 col-xs-12">

                    @include('web.elements.page-cover2')

                    <!-- XUTList Begin -->
                    <div class="xut_list">
                        <h1 class="">{{ $page->name}}</h1>
                        <div class="row">

                            @foreach($page->documents as $doc)

                                <div class="col-md-4 col-sm-6 col-xs-6 col-mob-12">
                                    <article>
                                        <div class="body">
                                            <h2 class="text-uppercase">{!! $doc->summary !!}</h2>
                                            <span><a href="{{ asset($doc->document) }}" title="{{ $doc->summary }}" target="_blank">PDF {{ $dictionary["download"] or "YÜKLƏ" }}</a></span>
                                        </div>
                                    </article>
                                </div>

                            @endforeach

                        </div>
                    </div>
                    <!-- XUTList End -->
                </div>
            </div>
        </div>
    </section>

@endsection