@extends ('layouts.web', ['page_heading' => $page->name, 'page_image' => !is_null($page->image_original) ? asset("files/cache/images/$page->id/$page->image_original") : ""])

@section ('content')

    @include('web.elements.breadcrumb')

    @include('web.elements.content')

    <section class="bgwhite">
        <div class="container">
            <div class="row">
                <div class="col-md-7">
                    <div class="pi_text_block">
                        <h2 class="title">{{ $dictionary['analysis_service'] or "İPO ( İlkin Kütləvi Təklif) prosesi aşağıdakı kimi baş tutur" }}</h2>
                        <div class="text">
                            {!! $page->content_extra !!}
                        </div>
                    </div>
                </div>
                <div class="col-md-5">
                    @include('web.elements.deal')
                </div>
            </div>
        </div>
    </section>

    @include('web.elements.customer-block')

    <section class="bgwhite padding-40">
        <div class="container">
            <div class="row">
                <div class="col-md-8">
                    @include('web.elements.legal-physical', ['banner' => true])
                </div>

                <div class="col-md-4">
                    @include('web.elements.banner', ['type' => 1, 'class' => 'm-margin-top-30 margin-bottom-30'])

                    @include('web.elements.application_form', ['bg' => 'bggrey'])
                </div>

            </div>
        </div>
    </section>

@endsection
