@extends ('layouts.web', ['page_heading' => $pageTitle, 'page_image' => $page->cover ? asset('files/cache/images/'.$page->relation_page.'/'.$page->cover->image) : ""])

@section ('content')

    @include('web.elements.breadcrumb')

    <!-- InsidePage Begin -->
    <section class="inside_page">
        <div class="container">
            <div class="row">
                <div class="col-md-3 col-sm-4 col-xs-12">
                    @include('web.elements.sideBar')
                </div>
                <div class="col-md-9 col-sm-8 col-xs-12">
                    @include('web.elements.page-cover2')
                    <!-- About Begin -->
                    <div class="about">
                        <h1 class="title">{{ $page->name }}</h1>
                        <div class="text">
                            {!! $page->content !!}
                        </div>
                    </div>
                    <!-- About End -->
                </div>
            </div>
        </div>
    </section>
    <!-- InsidePage End -->

    @if($page->template_id == 1)

        <!-- AboutBlocks Begin -->

        <section class="about_blocks">
            <div class="container">
                <div class="row">
                    @if($page->children->count())

                        @foreach($page->children as $block)
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <article>
                                    <i class="col-md-4 col-xs-4 col-mob-12 padding-0 icon{{ $loop->iteration }}"></i>
                                    <div class="col-md-8 col-xs-8 col-mob-12 text">
                                        <h2>{{ $block->name }}</h2>
                                        <p>{!! $block->summary !!}</p>
                                    </div>
                                </article>
                            </div>
                        @endforeach

                    @else
                        @include('web.elements.about-block')
                    @endif
                </div>
            </div>
        </section>

        <!-- AboutBlocks End -->

    @endif

@endsection