@extends ('layouts.web', ['page_heading' => $pageTitle])

@section ('content')

    @include('web.elements.breadcrumb')

    <!-- InsidePage Begin -->
    <section class="inside_page">
        <div class="container">
            <div class="row">
                <div class="col-md-3 col-sm-4 col-xs-12">
                    @include('web.elements.sideBar')
                </div>
                <div class="col-md-9 col-sm-8 col-xs-12">
                    <!-- About Begin -->
                    <div class="leader_appeal">
                        <div class="row">
                            <div class="col-md-12">
                                <h1>{{ $page->name }}</h1>
                                <div class="text">
                                    {!! $page->content !!}
                                </div>
                                <span class="imza"></span>
                            </div>
                        </div>
                    </div>
                    <!-- About End -->
                </div>
            </div>
        </div>
    </section>
    <!-- InsidePage End -->

@endsection