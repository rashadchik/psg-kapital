@component('mail::message')

    #Toplam bal: {{ $content['point'] }}

    - Ad, soyad: {{ $content['first_name'].' '.$content['last_name'] }}

    - Email: {{ $content['email'] }}

    - Telefon: {{ $content['phone'] }}

    - Təvəllüd: {{ $content['day'].'/'.$content['month'].'/'.$content['year'] }}

@endcomponent