@component('mail::message')

    #Əməkdaşlıq üçün müraciət olunan səhifə: {{$content['page']}}

    İstifadəçinin məlumatları:

    - Ad, soyad: {{$content['full_name']}}

    - Email: {{$content['email']}}

    - Telefon: {{$content['phone']}}

@endcomponent