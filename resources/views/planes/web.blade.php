<!DOCTYPE html>
<html lang="{{$lang}}">
<head>

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-110972741-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-110972741-1');
    </script>


    <meta charset="utf-8">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
    <meta name="theme-color" content="#ffffff">
    <meta property="author" content="{{ $webConfig->company_name }}">
    <meta property="og:title" content="{{ $page_heading }}">
    <meta property="og:description" content="{{ isset($page_description) ? strip_tags($page_description) : @$page->meta_description }}">
    <meta property="og:url" content="{{request()->url()}}">
    <meta property="og:image" content="{{isset($page_image) ? asset($page_image):  asset('images/psg-default.jpg')}}">
    @if(isset($page))
        <meta name="keywords" content="{{ $page->meta_keywords }}">
        <meta name="description" content="{{ $page->meta_description }}">
    @endif
    <title>@if(!is_null($page_heading)){{ $page_heading }} | @endif{{ $webConfig->company_name }}</title>

    <link rel="shortcut icon" href="{{ asset('images/favicon/favicon.ico') }}">
    <link rel="apple-touch-icon" sizes="180x180" href="{{asset('images/favicon/apple-touch-icon.png')}}">
    <link rel="icon" type="image/png" sizes="32x32" href="{{ asset('images/favicon/favicon-32x32.png') }}">
    <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('images/favicon/favicon-16x16.png') }}">
    <link rel="manifest" href="{{ asset('images/favicon/manifest.json') }}">
    <link rel="mask-icon" href="{{ asset('images/favicon/safari-pinned-tab.svg') }}" color="#5bbad5">

    <link href="{{ asset(mix('css/style.css')) }}" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('assets/amcharts/style.css?v=1') }}" type="text/css">

    <!-- Bootstrap core CSS -->
    <!-- Custom styles for this template -->
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>

    <!--Start of Tawk.to Script-->
    <script type="text/javascript">
        var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
        (function(){
            var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
            s1.async=true;
            s1.src='https://embed.tawk.to/5a313b76f4461b0b4ef88721/default';
            s1.charset='UTF-8';
            s1.setAttribute('crossorigin','*');
            s0.parentNode.insertBefore(s1,s0);
        })();
    </script>
    <!--End of Tawk.to Script-->

    @yield('layout')

    <script src="{{asset(mix('js/script.js'))}}"></script>

    <!-- AmCharts -->
    <script type="text/javascript" src="{{ asset('assets/amcharts/amcharts.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/amcharts/serial.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/amcharts/amstock.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/amcharts/themes/light.js') }}"></script>

    @if($lang != 'en')
    <script type="text/javascript" src="{{ asset('assets/amcharts/lang/'.$lang.'.js') }}"></script>
    @endif

    @stack('scripts')
</body>
</html>
